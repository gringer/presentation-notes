---
title: "Diceomancer - Relics"
author: "David Eccles"
date: 2024-11-28
---

# Diceomancer Pages

* [cards](../cards)
* [classes](../classes)
* [relics](../relics)
* [strategy](../strategy)

{{< toc >}}

# Relics (Sorted alphabetically)

Please let me know if any relics are missing, or there are any other issues; I have only included relics that have I have seen on streams that I have watched.

 * **4D Pocket** - You will not discard your hand at the end of the turn. Instead, discard 3 random cards at the end of the action.
 * **Adaptive Capacitor** - When mana overflows: Increase mana pool capacity by 1.
 * **Ankle Weights** - You cannot have Slow applied to you.
 * **Anvil** - You can upgrade cards 1 more time at the upgrade spot. (Each additional upgrade costs more money.)
 * **Astrolabe** - [Requires 2+ Charges] Activation Effects: During this turn, cards can be played using mana of any colour. Charge Conditions: When the Time Passes, charge.
 * **Bamboo** - When you cast a monocoloured card: Add (+). Can only be triggered 6 times each turn.
 * **Battle Pass** - When picked up: Lose 5(*). After combat ends, the loot will include an additional common card.
 * **Belt** - [Requires 3+ Charges] Activation Effects: Gain D8 Temporary Power. Charge Conditions: When the Time Passes, charge.
 * **Bent Spoon** - Your Blind and Evasion Check results gain a +7 bonus.
 * **Big glove** - When the combat starts: Your hand limit is 99.
 * **Black chip** - add 8 to all checks
 * **Blade armour** - [Requires 1+ Charge] Activation Effects: Gain 8 Block. Gain 8 Thorn. Charge Condition: After a short rest, charge.
 * **Bloodthirster** - When an enemy's health bar is depleted: Gain 3 Critical Hit. When you lose HP, Gain 1 Temporary Power.
 * **Blocker fluid** - [Distorted] effect from cards not cast by you will not resolve.
 * **Boxing Gloves** - When you lost HP: gain 5 Evasion.
 * **Broken Heart** - At the start of my action: Trigger all effects that activate upon depleting a health bar for a random enemy. It can only be triggered 2 times per combat.
 * **Broken Pencil** - When the combat starts: Gain 3 Hoard.
 * **Broken Sword** - [Requires 3+ charges] Activation Effects: Gain D20 Tough. Gain D6 Haste. Charge Conditions: When you lose HP, charge.
 * **Bucket Meal** - [Requires 4+ charges] Activation Effects: Gain 16 Tough. Gain 8 Block. Gain 4 Temporary Power. Charge Conditions: Enter an unvisited spot, charge.
 * **Cake** - [Requires 7+ Charges] Activation Effects: All numbers on cards in your hand are increased by 3 for this combat. Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Can** - At the start of the 4th turn, gain 3 Power.
 * **Card Dealer** - After using The One Die: Draw D4 cards. It can only be triggered 2 times per combat.
 * **Card Pack** - [Requires 1+ Charge] Activation Effects: Draw D4 cards. Charge Conditions: After a short rest, charge.
 * **Cataphract Armour** - When an enemy's health bar is depleted: Gain D4 block. Gain D4 Temporary Power.
 * **Chaos Pendant** - When generating a card, it will be chosen from a random card pool.
 * **Chained Dice** - After using The One Die: Discover 1 non-purple card. Can only be triggered 1 time each turn.
 * **Chicken Wing** - When the combat starts: Heal 2 HP.
 * **Chip** - When resolving a card's [Check] keyword or card effects related to checks, add 8 to the result.
 * **Clock** - At the start of the 12th turn, gain an extra turn.
 * **Cockroach** - [Requires 3+ Charges]** - Activation Effects: Apply 2 Panic to yourself. Apply 18 Vulnerable to all enemies. Charge Conditions: When the Time Passes, charge.
 * **Colour Stone** - When an enemy's health bar is depleted: Gain a random mana.
 * **Creepy Dice** - After using The One Die: Discover 1 black card. Can only be triggered 1 time each turn.
 * **Crown** - When you cast a rare card: Gain 3 Temporary Power, Gain 3 Block.
 * **Dancing Dice** - When you lose HP, Charge The One Die by D4. It can only be triggered 2 times per combat.
 * **Detergent** - [Requires 4+ Charges] Activation Effects: Turn all mana to (+). Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Dice Generator** - After using The One Die: Add (P).
 * **Dice Upgrade Kit** - Upon acquisition, upgrade The One Die to the next level (no effect if it is already at the highest level).
 * **Dinosaur Sculpture** - Cards you cast can't be countered.
 * **Disco Ball** - [Requires 1+ Charge] Activation Effects: Add (R)(G)(B)(P). Charge Conditions: After a short rest, charge.
 * **Disconnected Hand** - At the start of my action: Discover 1 black card. Add (L).
 * **Double Edged Blade** - When the combat starts: Apply 3 Power to everyone.
 * **Donut** - At the start of my action: Add (+). At the end of my action, Add (+)(+).
 * **Double Pendulum** - At the end of my turn: Draw 1 purple card from the draw pile. It can only be triggered 2 times per combat.
 * **DouLi** - When the combat starts: gain 3 D6 Critical Hit.
 * **Drumstick** - When you lose HP: Heal 2 HP. It can only be triggered 3 times per combat.
 * **Dumbbell** - When the combat starts: Gain D6 Temporary Power.
 * **Empty Battery** - When the combat starts: Increase mana pool capacity by 1.
 * **Expansion Pack** - When picked up: From 10 cards, choose up to 2 to permanently add to your deck.
 * **Eraser** - When picked up: Select a card and remove its [Distorted] keyword.
 * **Ether** - Before the [Distorted] takes effect, Check 12. If successful, it will not take effect.
 * **Fire Belt** - When you lose HP: Apply D8 Burn to a random enemy.
 * **First Aid Kit** - [Requires 5+ Charges] Activation Effects: Exert all [Malady] cards in your hand and draw and equal number of cards. Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Fish Tank** - When the combat starts: Summon a water element.
 * **Flash Light** - When an enemy's health bar is depleted: Gain 2 Haste. It can only be triggered 2 times per combat.
 * **Florida Water** - When picked up: Delay the spread of the Purple Mist D6 times.
 * **Fluorescent Stone** - After paying (+): Apply D6 Vulnerable to a random enemy.
 * **Fly** - When an enemy's health bar is depleted: Add (P). Can only be triggered 1 time each turn.
 * **Full Piggy Bank** - When picked up: Get 99 (*).
 * **Foam Spray** - [Requires 4+ Charges] Activation Effects: Remove all buffs from all enemies. Charge Conditions: Enter an unvisited spot, charge.
 * **Folding Fan** - For each 3 enemy health bar broken, gain D20 Evasion.
 * **Forget-me-not** - [Requires 1+ Charge] Permanently destroy all cards in the discard pile, then destroy this relic. Charge Conditions: None.
 * **Four-leaf Clover** - In events, your Check results gain a +7 bonus.
 * **Future Projector** - When the combat starts: Add copies of all [Construct] cards' corresponding [Constructed] cards to your discard pile.
 * **Fusion Dice** - When entering an unvisited spot, Check 10. If it succeeds, charge The One Die.
 * **Gashapon** - When the combat starts: Add 2 "Mystery Box" cards to your hand.
 * **Gemstone** - When picked up: Get 10 (*).
 * **Giant Dice** - When picked up: Transform your The One Die Relic into a D20 Die.
 * **Gift Box** - When picked up: add D4 random relics.
 * **Glasses** - When you Discover Cards, add 2 additional options.
 * **Golden Potion** - When picked up: Heal 50 HP. Destroy a target Malady card.
 * **Halo** - When picked up: Increase max HP by 30. Heal 50 HP. All loot cards are upgraded.
 * **Hammer** - When an enemy's health bar is depleted: Apply D4 Paper Skin to a random enemy.
 * **Hand Crank Generator** - [Consume 3 charges] Activation Effects: Increase mana pool capacity by D4. Charge Conditions: When the Time Passes, charge. Charge count can exceed the requirement and accumulate.
 * **Hazy Dice** - After using The One Die: Gain D4 block.
 * **Hitchhiker's Guide** - You will not gain panic.
 * **Incinerator** - After paying (R): apply 3 Burn to a random enemy. When Burn is resolved for anyone, add (R).
 * **Insurance For Engagement** - When the charge reaches 2: Get D4 (*). Charge Conditions: When the combat stats, charge.
 * **Insurance For Danger** - When you lose HP, Get D4 (*). It can only be triggered 2 times per combat.
 * **Iron Helmet** - [Requires 4+ Charges] Activation Effects: Gain 16 Tough. Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Iron Stomach** - When you pick up corrupted relics, you won't get curse cards.
 * **Jacket** - When you lose HP: Gain 6 Critical Hit. When the combat starts, gain 6 Critical Hit.
 * **Jade Ring** - When the combat starts: Gain 8 Dice Cheat: MAX.
 * **Jigsaw** - At the end of your turn, if you have (R), (G), and (B), draw 3 cards.
 * **Juice** - At the start of your first 4 actions, gain (+).
 * **Knee Pads** - At the start of my action: Gain 4 Block. It can only be triggered 1 time per combat.
 * **Knuckle** - When an enemy's health bar is depleted: Gain D6 Critical Hit. Gain D6 Evasion.
 * **Lightning Band** - When mana overflows: Draw D4 cards. Can only be triggered 1 time each turn.
 * **Lightning Dagger** - When mana overflows: Gain D4 Temporary Power. Can only be triggered 1 time each turn.
 * **Lightning Shield** - When mana overflows: Gain D4 Fortified. Can only be triggered 1 time each turn.
 * **Little Fury** - When the combat starts: Gain 2 Fury.
 * **Little Pony** - [Requires 2+ Charges] Activation Effects: Apply 6 Block to all characters. Apply 6 Luck to everyone. Charge Conditions: Enter an unvisited spot, charge.
 * **Leech** - When an enemy's health bar is depleted: Add (L). Can only be triggered 1 time each turn.
 * **Lollipop** - At the end of my action: Add 2 random mana (from (R)(G)(B)). It can only be triggered 3 times per combat.
 * **Loudspeaker** - [Requires 1+ Charges] Activation Effects: Transform up to 1 card with [Construct] in your hand. Charge Conditions: after a short rest, charge.
 * **Magic Glove** - [Requires 1+ Charges] Activation Effects: Gain D12 Luck. Apply D12 Bad Luck to all enemies. Charge conditions: after a short rest, charge.
 * **Magic Hat** - When the combat starts: Add 2 "Sleight of Hand" cards to your hand.
 * **Magnetic Dice** - When you roll the dice: roll an additional die and take the higher result.
 * **Mechanical Claw** - When you Discover cards, you may pick 1 additional card.
 * **Microbot** - Double the construction speed for the cards with [Construct].
 * **Midas Touch** - [Consumes 3 charges] Activation Effects: (*) becomes 15. Charge Conditions: When the Time Passes, charge. Charge count can exceed the requirement and accumulate.
 * **Mine** - [Requires 1+ Charge] Activation Effects: Gain D20 Thorn. Charge Conditions: After a short rest, charge.
 * **Mini Dice** - When picked up: Transform your The One Die Relic into a D4 Die. Charge The One Die by 4D4.
 * **Mini Motor** - [Requires 3+ Charges] Activation Effects: Add (+)(+). Charge Conditions: When the Time Passes, charge.
 * **Mini Trebuchet** - At the start of the 3rd turn, deal 2D12 damage to all the enemies.
 * **Mint Candy** - [Requires 1+ Charge] Activation Effects: Prevent the next [Distorted] effect from resolving. Charge Conditions: After a short rest, charge.
 * **Multiverse Portal** - Whenever you cast an [Exert] card, check 10. If successful, resolve the card an additional time. This effect can trigger 5 times per combat.
 * **Neural Overclocker** - [Requires 3+ Charges] Activation Effects: Add (+)(+). Gain D20 Haste. Charge Conditions: When you lose HP, charge.
 * **Only Fools Block** - When you gain Block, instead gain an equal amount of Evasion. Additionally, your Evasion Check results gain an additional +3 bonus.
 * **Old Coin** - After passing any Check, gain D4 Critical Hit.
 * **Ooze** - When resolving the [Modify] effect, you may choose one additional card.
 * **Ouroboros** - At the end of your action, if you have no cards in hand and no mana, gain an extra turn.
 * **Paint Bucket** - At the start of your action, if all the cards in your hand include a certain colour, gain 2 power.
 * **Pale Moon** - All mana you gain will be (+) (This does not affect the process of converting cards into mana).
 * **Piggy Toy** - When the combat starts: Gain D6 Thick Skin.
 * **Pirate Cannon** - When mana overflows: Deal 7 damage to a random enemy.
 * **Platinum Chip** - When the combat starts: Upgrade a random card.
 * **Poop** - [Requires 3+ Charges] Activation Effects: Apply 3 Dice Cheat: MIN to everyone. Charge Conditions: When the Time Passes, charge.
 * **Pot** - When an enemy's health bar is depleted: Gain D6 block. Can only be triggered 1 time each turn.
 * **Power Ring** - When the charge reaches 3: Gain 3 Power. Charge Conditions: When mana overflows, charge.
 * **Power Bank** - When mana overflows: Gain 2 Energetic. It can only be triggered 2 times per combat.
 * **Psychic Shock Lance** - [Consume 2 Charges] Activation Effects: Deal damage to all enemies equal to the number of cards in your hand. Charge Conditions: Enter an unvisited spot, charge. Charge count can exceed the requirement and accumulate.
 * **Radiance** - At the start of everyone's action: deal D4 damage to all enemies. At the end of everyone's action: deal D4 damage to all enemies.
 * **Rainbow Candy** - After converting a multicolour card, you gain mana of the colours that were not converted.
 * **Refresher** - [Requires 3+ Charges] Activation Effects: Increase the charge of all activated relics by 3, except for Refresher. Refresher's charge requirement increases by 2 in this combat. Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Reverse Card** - [Consume 2 Charges] Activation Effects: Reverse everyone's Luck / Bad Luck and Dice Cheat: MAX / Dice Cheat: MIN. Charge Conditions: When the Time Passes, charge. Charge count can exceed the requirement and accumulate.
 * **Road Sign** - When resolving the [Choose] effect, you may choose 1 additional option.
 * **Roller Painter** - After casting 3 monocolour cards in a single turn, gain 3 Temporary Power.
 * **Roulette Dice** - When the combat starts: Charge The One Die by D4.
 * **Rubber Dice** - After using The One Die: Apply D6 Powerless to all enemies.
 * **Scalpel** - Cards with [Malady] can be converted into mana.
 * **Scissor** - You can delete cards 1 more time at the remove spot. (Each additional removal costs more)
 * **Sharpening Stone** - When an enemy's health bar is depleted: Gain D4 Temporary Power. Can only be triggered 1 time each turn.
 * **Shattered Photo** - When reshuffling cards from the discard pile into the draw pile: Increase mana pool capacity by D4. Add (P)(+).
 * **Shredder** - [Requires 3+ Charges] Activation Effects: Exert the target card in hand and gain (+) equal to its cost. Charge Conditions: When you cast a card, charge.
 * **Shrink Ray** - [Requires 1+ Charges] Activation Effects: Set all numbers in enemies' intentions to 1. Charge Conditions: After a short rest, charge.
 * **Small Bag** - At the start of my action: Look at the top 3 cards of the draw pile, draw 1 of them, and discard the rest. It can only be triggered X times per combat.
 * **Small Bonfire** - After the short rest, increase your max HP by 5.
 * **Small Mirror** - Copy and resolve the 5th card you play each turn.
 * **Smoke Belt** - When you lose HP: Gain D6 block. Can only be triggered 2 times each turn.
 * **Soap** - [Requires 1+ Charge] Activation Effects: Remove all debuffs from yourself. Charge Conditions: After a short rest, charge.
 * **Soda** - When the combat starts: Add (+)(+).
 * **Soft Armor** - When you lose HP: Add (+).
 * **Space Battleships** - When the combat starts: Deal D12 damage to a random enemy. Repeat D8 times.
 * **Stir-fried Pork with Green Peppers** - When picked up: Increase max HP by 10. Heal 8 HP.
 * **Strange Fragment** - At the start of my action: select up to 1 card. It permanently transforms into another random card. It can only be triggered 1 time per combat.
 * **Super Battery** - When the charge reaches 3: Increase mana pool capacity by 2. Charge Conditions: When a whole new turn starts, charge.
 * **The One Die** - [Consume 2 charges] Activation Effects: Permanently change the target number to D6 Charge Conditions: Enter an unvisited spot, charge. Charge count can exceed the requirement and accumulate.
 * **Thornmail** - At the start of my action: gain 3 Thorn. It can only be triggered 1 time per combat.
 * **Trash Can** - When the charge reaches 3: Add D4 junk cards to your hand. Charge Conditions: When a whole new turn starts, charge.
 * **Trumpet** - When an enemy's health bar is depleted, Gain 4 Luck. Can only be triggered 1 time each turn.
 * **Turtle** - at the start of the 2nd turn, gain D8 fortified.
 * **Ultimate VIP Card** - When you buy something in the shop: Get D6 (*).
 * **Vest** - Your Tough and Critical Hit Check results gain a +5 bonus.
 * **Walking Stick** - When you lose HP: Draw 2 cards. It can only be triggered 3 times per combat.
 * **Warehouse Model** - You can refresh the cards 1 additional time in the shop.
 * **Water Bottle** - When the combat starts: Gain 2 Haste.
 * **Wheelchair** - [Requires 1+ Charge] Activation Effects: Apply D20 Vulnerable to all enemies. Apply D4 Paper Skin to all enemies. Charge Conditions: After a short rest, charge.
 * **White Paper** - When the combat starts: Discover 1 shop card.
 * **Wooden Dice** - When the combat starts: Gain D6 Luck.
 * **Wooden Drone** - When you cast a card: Deal 3 damage to a random enemy. It can only be triggered 4 times per combat.
 * **Wooden Shield** - [Requires 3+ Charges] Activation Effects: Gain 2D8 Block. Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Wooden Sword** - [Requires 3+ Charges] Activation effects: Deal 2D12 damage to all enemies. Charge Conditions: When the Time Passes, charge.
 * **Wrench** - At the start of the 3rd turn, Discover 1 card with [Modify].
 * **Writ of Mandate** - [Requires 0 Charges] Activation effects: Lose 3 (*), gain an equal amount of Power.
 * **Vambrace** - [Requires 4+ Charges] Activation effects: Gain 10 block. Convert your Block into Evasion. Charge Conditions: When an enemy's health bar is depleted, charge.
 * **Vine Armour** - At the end of my action: Gain 2 block. It can only be triggered 3 times per combat.
 * **Voodoo** - [Requires 3+ Charges] Activation effects: Apply 8 Bad Luck to all enemies. Charge Conditions: When the Time Passes, charge.
 * **Yoga Mat** - Your Weak and Vulnerable Check results gain a +6 bonus.

# Relics (Sorted by Triggered Activation Condition)

Sorted alphabetically within each activation category

## Manually Activated (click to activate; right click to discard)

* **Astrolabe** - [Requires 2+ Charges] Activation Effects: During this turn, cards can be played using mana of any colour. Charge Conditions: When the Time Passes, charge.
* **Belt** - [Requires 3+ Charges] Activation Effects: Gain D8 Temporary Power. Charge Conditions: When the Time Passes, charge.
* **Blade armour** - [Requires 1+ Charge] Activation Effects: Gain 8 Block. Gain 8 Thorn. Charge Condition: After a short rest, charge.
* **Cake** - [Requires 7+ Charges] Activation Effects: All numbers on cards in your hand are increased by 3 for this combat. Charge Conditions: When an enemy's health bar is depleted, charge.
* **Broken Sword** - [Requires 3+ charges] Activation Effects: Gain D20 Tough. Gain D6 Haste. Charge Conditions: When you lose HP, charge.
* **Bucket Meal** - [Requires 4+ charges] Activation Effects: Gain 16 Tough. Gain 8 Block. Gain 4 Temporary Power. Charge Conditions: Enter an unvisited spot, charge.
* **Card Pack** - [Requires 1+ Charge] Activation Effects: Draw D4 cards. Charge Conditions: After a short rest, charge.
* **Cockroach** - [Requires 3+ Charges]** - Activation Effects: Apply 2 Panic to yourself. Apply 18 Vulnerable to all enemies. Charge Conditions: When the Time Passes, charge.
* **Detergent** - [Requires 4+ Charges] Activation Effects: Turn all mana to (+). Charge Conditions: When an enemy's health bar is depleted, charge.
* **Disco Ball** - [Requires 1+ Charge] Activation Effects: Add (R)(G)(B)(P). Charge Conditions: After a short rest, charge.
* **First Aid Kit** - [Requires 5+ Charges] Activation Effects: Exert all [Malady] cards in your hand and draw and equal number of cards. Charge Conditions: When an enemy's health bar is depleted, charge.
* **Foam Spray** - [Requires 4+ Charges] Activation Effects: Remove all buffs from all enemies. Charge Conditions: Enter an unvisited spot, charge.
* **Forget-me-not** - [Requires 1+ Charge] Permanently destroy all cards in the discard pile, then destroy this relic. Charge Conditions: None.
* **Hand Crank Generator** - [Consume 3 charges] Activation Effects: Increase mana pool capacity by D4. Charge Conditions: When the Time Passes, charge. Charge count can exceed the requirement and accumulate.
 * **Iron Helmet** - [Requires 4+ Charges] Activation Effects: Gain 16 Tough. Charge Conditions: When an enemy's health bar is depleted, charge.
* **Little Pony** - [Requires 2+ Charges] Activation Effects: Apply 6 Block to all characters. Apply 6 Luck to everyone. Charge Conditions: Enter an unvisited spot, charge.
* **Loudspeaker** - [Requires 1+ Charges] Activation Effects: Transform up to 1 card with [Construct] in your hand. Charge Conditions: after a short rest, charge.
* **Magic Glove** - [Requires 1+ Charges] Activation Effects: Gain D12 Luck. Apply D12 Bad Luck to all enemies. Charge conditions: after a short rest, charge.
* **Midas Touch** - [Consumes 3 charges] Activation Effects: (*) becomes 15. Charge Conditions: When the Time Passes, charge. Charge count can exceed the requirement and accumulate.
* **Mine** - [Requires 1+ Charge] Activation Effects: Gain D20 Thorn. Charge Conditions: After a short rest, charge.
* **Mini Motor** - [Requires 3+ Charges] Activation Effects: Add (+)(+). Charge Conditions: When the Time Passes, charge.
* **Mint Candy** - [Requires 1+ Charge] Activation Effects: Prevent the next [Distorted] effect from resolving. Charge Conditions: After a short rest, charge.
* **Neural Overclocker** - [Requires 3+ Charges] Activation Effects: Add (+)(+). Gain D20 Haste. Charge Conditions: When you lose HP, charge.
* **Poop** - [Requires 3+ Charges] Activation Effects: Apply 3 Dice Cheat: MIN to everyone. Charge Conditions: When the Time Passes, charge.
* **Psychic Shock Lance** - [Consume 2 Charges] Activation Effects: Deal damage to all enemies equal to the number of cards in your hand. Charge Conditions: Enter an unvisited spot, charge. Charge count can exceed the requirement and accumulate.
* **Refresher** - [Requires 3+ Charges] Activation Effects: Increase the charge of all activated relics by 3, except for Refresher. Refresher's charge requirement increases by 2 in this combat. Charge Conditions: When an enemy's health bar is depleted, charge.
* **Reverse Card** - [Consume 2 Charges] Activation Effects: Reverse everyone's Luck / Bad Luck and Dice Cheat: MAX / Dice Cheat: MIN. Charge Conditions: When the Time Passes, charge. Charge count can exceed the requirement and accumulate.
* **Shredder** - [Requires 3+ Charges] Activation Effects: Exert the target card in hand and gain (+) equal to its cost. Charge Conditions: When you cast a card, charge.
* **Shrink Ray** - [Requires 1+ Charges] Activation Effects: Set all numbers in enemies' intentions to 1. Charge Conditions: After a short rest, charge.
* **Soap** - [Requires 1+ Charge] Activation Effects: Remove all debuffs from yourself. Charge Conditions: After a short rest, charge.
* **The One Die** - [Consume 2 charges] Activation Effects: Permanently change the target number to D6 Charge Conditions: Enter an unvisited spot, charge. Charge count can exceed the requirement and accumulate.
* **Wheelchair** - [Requires 1+ Charge] Activation Effects: Apply D20 Vulnerable to all enemies. Apply D4 Paper Skin to all enemies. Charge Conditions: After a short rest, charge.
* **Wooden Shield** - [Requires 3+ Charges] Activation Effects: Gain 2D8 Block. Charge Conditions: When an enemy's health bar is depleted, charge.
* **Wooden Sword** - [Requires 3+ Charges] Activation effects: Deal 2D12 damage to all enemies. Charge Conditions: When the Time Passes, charge.
* **Writ of Mandate** - [Requires 0 Charges] Activation effects: Lose 3 (*), gain an equal amount of Power.
* **Vambrace** - [Requires 4+ Charges] Activation effects: Gain 10 block. Convert your Block into Evasion. Charge Conditions: When an enemy's health bar is depleted, charge.
* **Voodoo** - [Requires 3+ Charges] Activation effects: Apply 8 Bad Luck to all enemies. Charge Conditions: When the Time Passes, charge.

## Single Use

* **Dice Upgrade Kit** - Upon acquisition, upgrade The One Die to the next level (no effect if it is already at the highest level).
* **Expansion Pack** - When picked up: From 10 cards, choose up to 2 to permanently add to your deck.
* **Eraser** - When picked up: Select a card and remove its [Distorted] keyword.
* **Golden Potion** - When picked up: Heal 50 HP. Destroy a target Malady card.
* **Florida Water** - When picked up: Delay the spread of the Purple Mist D6 times.
* **Full Piggy Bank** - When picked up: Get 99 (*).
* **Gemstone** - When picked up: Get 10 (*).
* **Giant Dice** - When picked up: Transform your The One Die Relic into a D20 Die.
* **Gift Box** - When picked up: add D4 random relics.
* **Halo** - When picked up: Increase max HP by 30. Heal 50 HP. All loot cards are upgraded.
* **Mini Dice** - When picked up: Transform your The One Die Relic into a D4 Die. Charge The One Die by 4D4.
* **Stir-fried Pork with Green Peppers** - When picked up: Increase max HP by 10. Heal 8 HP.

## Start of Turn / Start of Action

* **Broken Heart** - At the start of my action: Trigger all effects that activate upon depleting a health bar for a random enemy. It can only be triggered 2 times per combat.
* **Can** - At the start of the 4th turn, gain 3 Power.
* **Clock** - At the start of the 12th turn, gain an extra turn.
* **Disconnected Hand** - At the start of my action: Discover 1 black card. Add (L).
* **Donut** - At the start of my action: Add (+). At the end of my action, Add (+)(+).
* **Juice** - At the start of your first 4 actions, gain (+).
* **Knee Pads** - At the start of my action: Gain 4 Block. It can only be triggered 1 time per combat.
* **Mini Trebuchet** - At the start of the 3rd turn, deal 2D12 damage to all the enemies.
* **Paint Bucket** - At the start of your action, if all the cards in your hand include a certain colour, gain 2 power.
* **Radiance** - At the start of everyone's action: deal D4 damage to all enemies. At the end of everyone's action: deal D4 damage to all enemies.
* **Small Bag** - At the start of my action: Look at the top 3 cards of the draw pile, draw 1 of them, and discard the rest. It can only be triggered X times per combat.
* **Strange Fragment** - At the start of my action: select up to 1 card. It permanently transforms into another random card. It can only be triggered 1 time per combat.
* **Super Battery** - When the charge reaches 3: Increase mana pool capacity by 2. Charge Conditions: When a whole new turn starts, charge.
* **Thornmail** - At the start of my action: gain 3 Thorn. It can only be triggered 1 time per combat.
* **Trash Can** - When the charge reaches 3: Add D4 junk cards to your hand. Charge Conditions: When a whole new turn starts, charge.
* **Turtle** - at the start of the 2nd turn, gain D8 fortified.
* **Wrench** - At the start of the 3rd turn, Discover 1 card with [Modify].

## End of Turn / End of Action

* **4D Pocket** - You will not discard your hand at the end of the turn. Instead, discard 3 random cards at the end of the action.
* **Double Pendulum** - At the end of my turn: Draw 1 purple card from the draw pile. It can only be triggered 2 times per combat.
* **Jigsaw** - At the end of your turn, if you have (R), (G), and (B), draw 3 cards.
* **Lollipop** - At the end of my action: Add 2 random mana (from (R)(G)(B)). It can only be triggered 3 times per combat.
* **Ouroboros** - At the end of your action, if you have no cards in hand and no mana, gain an extra turn.
* **Vine Armour** - At the end of my action: Gain 2 block. It can only be triggered 3 times per combat.

## Start of Combat

* **Broken Pencil** - When the combat starts: Gain 3 Hoard.
* **Big glove** - When the combat starts: Your hand limit is 99.
* **Chicken Wing** - When the combat starts: Heal 2 HP.
* **Double Edged Blade** - When the combat starts: Apply 3 Power to everyone.
* **DouLi** - When the combat starts: gain 3 D6 Critical Hit.
* **Dumbbell** - When the combat starts: Gain D6 Temporary Power.
* **Empty Battery** - When the combat starts: Increase mana pool capacity by 1.
* **Fish Tank** - When the combat starts: Summon a water element.
* **Future Projector** - When the combat starts: Add copies of all [Construct] cards' corresponding [Constructed] cards to your discard pile.
* **Gashapon** - When the combat starts: Add 2 "Mystery Box" cards to your hand.
* **Insurance For Engagement** - When the charge reaches 2: Get D4 (*). Charge Conditions: When the combat stats, charge.
* **Jade Ring** - When the combat starts: Gain 8 Dice Cheat: MAX.
* **Little Fury** - When the combat starts: Gain 2 Fury.
* **Magic Hat** - When the combat starts: Add 2 "Sleight of Hand" cards to your hand.
* **Piggy Toy** - When the combat starts: Gain D6 Thick Skin.
* **Platinum Chip** - When the combat starts: Upgrade a random card.
* **Roulette Dice** - When the combat starts: Charge The One Die by D4.
* **Soda** - When the combat starts: Add (+)(+).
* **Space Battleships** - When the combat starts: Deal D12 damage to a random enemy. Repeat D8 times.
* **Water Bottle** - When the combat starts: Gain 2 Haste.
* **White Paper** - When the combat starts: Discover 1 shop card.
* **Wooden Dice** - When the combat starts: Gain D6 Luck.

## End of Combat

* **Battle Pass** - When picked up: Lose 5(*). After combat ends, the loot will include an additional common card.

## Events / Meta

* **Anvil** - You can upgrade cards 1 more time at the upgrade spot. (Each additional upgrade costs more money.)
* **Four-leaf Clover** - In events, your Check results gain a +7 bonus.
* **Fusion Dice** - When entering an unvisited spot, Check 10. If it succeeds, charge The One Die.
* **Iron Stomach** - When you pick up corrupted relics, you won't get curse cards.
* **Scissor** - You can delete cards 1 more time at the remove spot. (Each additional removal costs more)
* **Small Bonfire** - After the short rest, increase your max HP by 5.
* **Ultimate VIP Card** - When you buy something in the shop: Get D6 (*).
* **Warehouse Model** - You can refresh the cards 1 additional time in the shop.

## Dice / Ability Checks

* **Bent Spoon** - Your Blind and Evasion Check results gain a +7 bonus.
* **Black chip** - add 8 to all checks.
* **Chip** - When resolving a card's [Check] keyword or card effects related to checks, add 8 to the result.
* **Ankle Weights** - You cannot have Slow applied to you.
* **Hitchhiker's Guide** - You will not gain panic.
* **Magnetic Dice** - When you roll the dice: roll an additional die and take the higher result.
* **Old Coin** - After passing any Check, gain D4 Critical Hit.
* **Only Fools Block** - When you gain Block, instead gain an equal amount of Evasion. Additionally, your Evasion Check results gain an additional +3 bonus.
* **Vest** - Your Tough and Critical Hit Check results gain a +5 bonus.
* **Yoga Mat** - Your Weak and Vulnerable Check results gain a +6 bonus.

## Card Casting

* **Bamboo** - When you cast a monocoloured card: Add (+). Can only be triggered 6 times each turn.
* **Blocker fluid** - [Distorted] effect from cards not cast by you will not resolve.
* **Crown** - When you cast a rare card: Gain 3 Temporary Power, Gain 3 Block.
* **Dinosaur Sculpture** - Cards you cast can't be countered.
* **Ether** - Before the [Distorted] takes effect, Check 12. If successful, it will not take effect.
* **Multiverse Portal** - Whenever you cast an [Exert] card, check 10. If successful, resolve the card an additional time. This effect can trigger 5 times per combat.
* **Rainbow Candy** - After converting a multicolour card, you gain mana of the colours that were not converted.
* **Roller Painter** - After casting 3 monocolour cards in a single turn, gain 3 Temporary Power.
* **Scalpel** - Cards with [Malady] can be converted into mana.
* **Shattered Photo** - When reshuffling cards from the discard pile into the draw pile: Increase mana pool capacity by D4. Add (P)(+).
* **Wooden Drone** - When you cast a card: Deal 3 damage to a random enemy. It can only be triggered 4 times per combat.
* **Small Mirror** - Copy and resolve the 5th card you play each turn.

## Card Generation / Discovery

* **Chaos Pendant** - When generating a card, it will be chosen from a random card pool.
* **Glasses** - When you Discover Cards, add 2 additional options.
* **Mechanical Claw** - When you Discover cards, you may pick 1 additional card.
* **Microbot** - Double the construction speed for the cards with [Construct].
* **Ooze** - When resolving the [Modify] effect, you may choose one additional card.
* **Road Sign** - When resolving the [Choose] effect, you may choose 1 additional option.

## Health Bar Depleted

* **Bloodthirster** - When an enemy's health bar is depleted: Gain 3 Critical Hit. When you lose HP, Gain 1 Temporary Power.
* **Cataphract Armour** - When an enemy's health bar is depleted: Gain D4 block. Gain D4 Temporary Power.
* **Colour Stone** - When an enemy's health bar is depleted: Gain a random mana.
* **Flash Light** - When an enemy's health bar is depleted: Gain 2 Haste. It can only be triggered 2 times per combat.
* **Fly** - When an enemy's health bar is depleted: Add (P). Can only be triggered 1 time each turn.
* **Folding Fan** - For each 3 enemy health bar broken, gain D20 Evasion.
* **Hammer** - When an enemy's health bar is depleted: Apply D4 Paper Skin to a random enemy.
* **Knuckle** - When an enemy's health bar is depleted: Gain D6 Critical Hit. Gain D6 Evasion.
* **Leech** - When an enemy's health bar is depleted: Add (L). Can only be triggered 1 time each turn.
* **Pot** - When an enemy's health bar is depleted: Gain D6 block. Can only be triggered 1 time each turn.
* **Sharpening Stone** - When an enemy's health bar is depleted: Gain D4 Temporary Power. Can only be triggered 1 time each turn.
* **Trumpet** - When an enemy's health bar is depleted, Gain 4 Luck. Can only be triggered 1 time each turn.

## Lose HP

* **Boxing Gloves** - When you lost HP: gain 5 Evasion.
* **Dancing Dice** - When you lose HP, Charge The One Die by D4. It can only be triggered 2 times per combat.
* **Drumstick** - When you lose HP: Heal 2 HP. It can only be triggered 3 times per combat.
* **Fire Belt** - When you lose HP: Apply D8 Burn to a random enemy.
* **Insurance For Danger** - When you lose HP, Get D4 (*). It can only be triggered 2 times per combat.
* **Jacket** - When you lose HP: Gain 6 Critical Hit. When the combat starts, gain 6 Critical Hit.
* **Smoke Belt** - When you lose HP: Gain D6 block. Can only be triggered 2 times each turn.
* **Soft Armor** - When you lose HP: Add (+).
* **Walking Stick** - When you lose HP: Draw 2 cards. It can only be triggered 3 times per combat.

## The One Die

* **Card Dealer** - After using The One Die: Draw D4 cards. It can only be triggered 2 times per combat.
* **Chained Dice** - After using The One Die: Discover 1 non-purple card. Can only be triggered 1 time each turn.
* **Creepy Dice** - After using The One Die: Discover 1 black card. Can only be triggered 1 time each turn.
* **Dice Generator** - After using The One Die: Add (P).
* **Hazy Dice** - After using The One Die: Gain D4 block.
* **Rubber Dice** - After using The One Die: Apply D6 Powerless to all enemies.

## Paying / Generating Mana

* **Fluorescent Stone** - After paying (+): Apply D6 Vulnerable to a random enemy.
* **Incinerator** - After paying (R): Apply 3 Burn to a random enemy. When Burn is resolved for anyone, add (R).
* **Pale Moon** - All mana you gain will be (+) (This does not affect the process of converting cards into mana).

## Mana Overflow

* **Adaptive Capacitor** - When mana overflows: Increase mana pool capacity by 1.
* **Lightning Band** - When mana overflows: Draw D4 cards. Can only be triggered 1 time each turn.
* **Lightning Dagger** - When mana overflows: Gain D4 Temporary Power. Can only be triggered 1 time each turn.
* **Lightning Shield** - When mana overflows: Gain D4 Fortified. Can only be triggered 1 time each turn.
* **Pirate Cannon** - When mana overflows: Deal 7 damage to a random enemy.
* **Power Bank** - When mana overflows: Gain 2 Energetic. It can only be triggered 2 times per combat.
* **Power Ring** - When the charge reaches 3: Gain 3 Power. Charge Conditions: When mana overflows, charge.

