---
title: "Diceomancer - Cards"
author: "David Eccles"
date: 2024-11-28
---

# Diceomancer Pages

* [cards](../cards)
* [classes](../classes)
* [relics](../relics)
* [strategy](../strategy)

# Card notes

The Star - It changes numbers to a *resolved* D20; modified cards will get [Distorted]
