---
title: "Diceomancer - Classes"
author: "David Eccles"
date: 2024-11-28
---

# Diceomancer Pages

* [cards](../cards)
* [classes](../classes)
* [relics](../relics)
* [strategy](../strategy)

# Sage (B)

Starting deck:
* {B} (B/+) Block - Gain 3 block
 -> {B} (B/+) Block+ - Gain 4 block. [Miracle]: Gain 2 block.
* {B} (B/+) Block - Gain 3 block
 -> {B} (B/+) Block+ - Gain 4 block. [Miracle]: Gain 2 block.
* {B} (B/+) Block - Gain 3 block
 -> {B} (B/+) Block+ - Gain 4 block. [Miracle]: Gain 2 block.
* {B} (+) Strike - Deal 4 damage
 -> {B} (B/+) Strike - Deal 5 damage. [Miracle]: Deal 4 damage.
* {B} (+) Strike - Deal 4 damage
 -> {B} (B/+) Strike - Deal 5 damage. [Miracle]: Deal 4 damage.
* {B} (+) Strike - Deal 4 damage
 -> {B} (B/+) Strike - Deal 5 damage. [Miracle]: Deal 4 damage.
* {B/+} (+) Crossbow - Deal 12 damage [Loaded ammo 1] / Crossbow-Reload - [Reload 1]; [Keep] [B]
  -> {B/+} (-) Crossbow+ - Deal 12 damage [Loaded ammo 1]; [Rebound] / {B/+} Crossbow-Reload+ - [Reload 1], [Keep], [Bonus]
* {P} (P/+) Block - Gain D6 block
 ->
* {P} (+) Strike - Deal D8 damage.
 -> Apply 2 Bad Luck. Deal D8 damage.
* {P} (-) The One Die - Permanently change the target number to D6 [Exert]

Relic ability:

After an enemy's health bar is depleted: Draw 1 card. Gain 2 block.

Class keywords:

* [Reload]
* [Miracle]

# Berserker (R)

Starting deck:
* {R} (+) Plaster - Heal 3 Injury [Exert].
* {R} (+) Plaster - Heal 3 Injury [Exert].
* {R} (+) Plaster - Heal 3 Injury [Exert].
* {R} (+) Plaster - Heal 3 Injury [Exert].
* {R} (R/+) Strike - Deal 4 damage.
 -> {R} (R/+) Strike - Deal 6 damage. [Assault]: Add (R).
* {R} (R/+) Strike - Deal 4 damage.
 -> {R} (R/+) Strike - Deal 6 damage. [Assault]: Add (R).
* {R} (R/+) Strike - Deal 4 damage.
 -> {R} (R/+) Strike - Deal 6 damage. [Assault]: Add (R).
* {R} (R/+) Strike - Deal 4 damage.
 -> {R} (R/+) Strike - Deal 6 damage. [Assault]: Add (R).
* {R} (R)(R) Heavy Strike - Deal 12 damage. Gain 2 Fury.
 -> {R} (R)(R) Heavy Strike - Deal 16 damage. Gain 3 Fury.
* {P/R} (P/R) Plaster - Heal D6 Injury [Exert].
 -> {P/R} (P/R) Plaster - Heal D6 Injury. [Assault]: Add (P). [Exert]
* {P} (+) Strike - Deal D8 damage.
 -> Apply 2 Bad Luck. Deal D8 damage.
* {P} (-) The One Die - Permanently change the target number to D6 [Exert]


Relic ability:

After an enemy's health bar is depleted: Add (R). Increase mana pool capacity by 1. It can only be triggered 3 times per combat.

Class keywords:

* [Burn]
* [Storm]
* [Assault]

# Summoner (G)

Starting deck:
* {G} (G/+) Block - Gain 3 block
 -> {G} (G/+) Block+ - Gain 4 block.
* {G} (G/+) Block - Gain 3 block
 -> {G} (G/+) Block+ - Gain 4 block.
* {G} (G/+) Block - Gain 3 block
 -> {G} (G/+) Block+ - Gain 4 block.
* {G} (+) Strike - Deal 4 damage
 -> {G} (G/+) Strike - Deal 5 damage. [Coordinated 2].
* {G} (+) Strike - Deal 4 damage
 -> {G} (G/+) Strike - Deal 5 damage. [Coordinated 2].
* {G} (+) Strike - Deal 4 damage
 -> {G} (G/+) Strike - Deal 5 damage. [Coordinated 2].
* {G/R} (+) Fire Elemental - [Summon 4]: Deal 4 damage to a random enemy.
  -> {G/R} (-) Fire Elemental - [Summon 4]: Deal 4 damage to a random enemy.
* {P} (P/+) Block - Gain D6 block
 ->
* {P} (+) Strike - Deal D8 damage.
 -> Apply 2 Bad Luck. Deal D8 damage.
* {P} (-) The One Die - Permanently change the target number to D6 [Exert]


# Builder (G + B)

Starting deck:
* {B/G} (B/+) Block [B/G] -> (-) Block [B/G]
* {B/G} (B/+) Block [B/G] -> (-) Block [B/G]
* {B/G} (B/+) Block [B/G] -> (-) Block [B/G]
* {B/G} (B/+) Block [B/G] -> (-) Block [B/G]
* {B/G} (G/+) Strike [B/G] -> (-) Strike [B/G]
* {B/G} (G/+) Strike [B/G] -> (-) Strike [B/G]
* {B/G} (G/+) Strike [B/G] -> (-) Strike [B/G]
* {B/G} (G/+) Strike [B/G] -> (-) Strike [B/G]
* {B} (B/+) Pipe
* {P} (-) The One Die [No upgrade]

Relic ability:

After an enemy's health bar is depleted: Draw 1 card. Gain (+).
