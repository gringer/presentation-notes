---
title: "Diceomancer - Strategy"
author: "David Eccles"
date: 2024-11-28
---

# Diceomancer Pages

# Diceomancer Pages

* [cards](../cards)
* [classes](../classes)
* [relics](../relics)
* [strategy](../strategy)

# Dice Use

Due to the class activation effects from depleting health bars, mixed
with Focus effects, in most cases it is better to roll down the
right-most health bar instead of the highest health bar.

# Relics

## Re-rolling Relic Loot

There is an early-stage upgrade "You can use dice during the
loot". This means that the number of relics to choose from can be
changed with The One Die, and any change in number will re-roll the
relic selection.

## Corrupted Relics

Changing numbers on relics with The One Die creates a corrupted
relic. When such a relic is picked up, you also gain a curse card.

## Relic Frequency

Think about how often a relic might be activated when choosing them.

Here is an approximate ordering based on activation frequency, bearing
in mind that activation frequency changes depending on what other
relics and cards are active:

1. At the start of my action + At the end of my action
1. When an enemy's health bar is depleted
1. When mana overflows
1. At the start of my action
1. At the end of my action
1. When you lose HP
1. When the charge reaches
1. When the combat starts
1. When picked up
