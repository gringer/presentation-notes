---
title: "Nanopore Flow Cell Loading"
author: "David Eccles"
---

# Method - Flow Cell Loading

Once a sequencing library has been prepared, it needs to be loaded
into a flow cell for sequencing. Proceed with the 12μl of library as
input for ONT R10.4.1 flow cell loading, as per the RBK114.24 kit:

* [Flongle Flow Cell](https://community.nanoporetech.com/docs/prepare/library_prep_protocols/rapid-sequencing-gdna-barcoding-sqk-rbk114/v/rbk_9176_v114_revj_27nov2022/loading-the-flongle-flow-cell?devices=flongle)
* [MinION Flow Cell](https://community.nanoporetech.com/docs/prepare/library_prep_protocols/rapid-sequencing-gdna-barcoding-sqk-rbk114/v/rbk_9176_v114_revj_27nov2022/priming-and-loading-the-spoton-flow-cell?devices=minion)
* [PromethION Flow Cell](https://community.nanoporetech.com/docs/prepare/library_prep_protocols/rapid-sequencing-gdna-barcoding-sqk-rbk114/v/rbk_9176_v114_revj_27nov2022/priming-and-loading-the-promethion-flow-cell?devices=promethion)

[these loading instructions are identical regardless of the sequencing
kit used]

A summary of loading for each type of flow cell follows; please
consult the protocols above for more information.

## Loading - Flongle Flow Cells

This uses the gentle negative-pressure loading method from the
community, with 5µl of library loaded in 30µl mix. Note that this
protocol is *different* from ONT’s official Flongle loading protocol:

https://community.nanoporetech.com/posts/a-very-gentle-relatively

1. Prepare Flow Cell Flush solution:
   * 117 µl Flow Cell Flush (FCF)
   * 3 µl Flow Cell Tether (FCT)
   * mix ***by pipettting***
1. Unseal the flow cell
1. Add tape (see **Seal Tape** below) to the rectangular waste port (see
   the sticker, or **Waste Port** below)
1. Drop 30 μl of flush solution onto the loading port to form a dome
1. Place pipette into loading port and dial up about 5 μl to check for
   bubbles (dial up to remove bubbles if they exist)
1. If liquid is not dropping from the loading port (see **Loading Port**
   below), place pipette into the exposed waste port and dial up 30 μl
   or until liquid starts dropping from the loading port
1. Set pipette to 30 μl, press down while in mid air to expel air,
   then place into the exposed waste port and slowly release the
   plunger. If liquid starts dropping from the loading port, stop
   releasing and lift up the pipette
1. Repeat the last step with a faster release speed until liquid
   starts dropping from the loading port
1. Wait 5 minutes (while preparing library for loading)
   * 15 µl Sequencing Buffer (SB)
   * 10 µl Library Beads, mixed ***by pipetting*** immediately before use
   * 5 µl DNA library
   * mix ***by pipetting***
1. Remove the tape from the waste port
1. Drop another 30 μl of flush solution onto the loading port, wait
   for it to drain through
1. Drop 30 μl of sequencing library onto the loading port, wait for it to drain    through
1. Re-seal the flow cell by rolling a finger across the plastic
   adhesive cover, trying to avoid putting pressure on the flow cell
   matrix

## Flongle Glossary

![Flongle flow cell image taken by Sventja von Daake; annotations by
 David Eccles](pics/Flongle_flow_cell_annotated.jpeg)

* **Seal Tape** - Pretty much any tape will do. We use whatever lab
  tape we have lying around. All it needs to do is reduce the air flow
  to the rectangular waste port enough that pulling air through the
  circular waste port will suck inwards from the loading port, instead
  of inwards from the other waste port.

* **Loading Port** - The Flongle loading port is a slightly spherical
  indentation in the Flongle flow cell with a hole at the middle
  bottom of it. When liquid is dropped onto it and the flow cell has
  good plumbing, the liquid level should drop (similar to how the
  SpotON port works with the MinION flow cells).

* **Waste Port** - The waste ports are on the outer edge of the
  Flongle flow cell. On the new R10.4.1 Flongle flow cells, one is a
  circular waste port, and one is rectangular. The rectangular port
  has a wiggly channel leading to it. The sticker images above them
  have different shapes.

## Loading - MinION Flow Cells

This method uses BSA (can probably be omitted), with a library load
amount of 12 µl in 75 µl:

1. Prepare Flow Cell Flush solution:
   * 1170 µl Flow Cell Flush (FCF), i.e. an entire tube
   * 12.5 µl Bovine Serum Albumin (BSA) at 20 mg/µl [recombinant BSA
     seems to work as well]
   * 30 µl Flow Cell Tether (FCT)
   * mix ***by pipetting***
1. Load 800µl flush solution into ***Priming port***
1. Wait 5 minutes (while preparing library for loading)
   * 37.5 µl Sequencing Buffer (SB)
   * 25.5 µl Library Beads (LIB), mixed ***by pipetting*** immediately
     before use
   * 12 µl DNA library
   * mix ***by pipetting***
1. Lift up SpotON port cover
1. Load 200µl flush solution into ***Priming port***
1. Add 75µl library dropwise into ***SpotON port***

Loading - PromethION Flow Cells

BSA is not used for PromethION flow cell loading, and the library load
amount is 12 µl in 200µl:

1. Prepare Flow Cell Flush solution:
   * 1170 µl Flow Cell Flush (FCF), i.e. the entire tube
   * 30 µl Flow Cell Tether (FCT)
   * mix ***by pipetting***
1. Load 500µl flush solution into inlet port
1. Wait 5 minutes (while preparing library for loading)
   * 100 µl Sequencing Buffer (SB)
   * 68 µl Library Beads (LIB), mixed ***by pipetting*** immediately before use
   * 12 µl DNA library
   * 20 µl Elution Buffer (EB)
   * mix ***by pipetting***
1. Lift up SpotON port cover
1. Load 500µl flush solution into inlet port
1. Load 200µl library into inlet port by slow pipetting
