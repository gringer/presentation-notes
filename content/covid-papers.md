---
title: "COVID Papers"
author: "David Eccles"
---

<img src="/pics/COVID_brain.jpeg" alt="top: your brain blood vessels without COVID-19; bottom: your brain blood vessels with COVID-19 [destroyed / perforated]" width="50%">

# COVID Papers

A collection of research papers on COVID, demonstrating its severe
impact on our immune system, our body, and our society... and some
ways in which it can be controlled and eliminated.

Yes, *eliminated*. The COVID virus, SARS-CoV-2, is fairly vulnerable
when it's outside people. Aotearoa demonstrated that graduated
protective measures work to reduce and eventually eliminate
circulating COVID virus. Infections only started rising again once the
original guidelines were relaxed by "loosening the pressure in the
tires" for Auckland businesses; see my minutes summary at the end of
this collection.

If you find this collection of papers useful, please consider buying
me a [ko-fi](https://ko-fi.com/gringer). Or better yet, think of me
when you have a little bioinformatics project that I could help out
with. More details about what I do can be found here:

https://www.gringene.org/services.html

## Resources

* [Reducing COVID-19 Transmission](http://bit.ly/lesscovid) printable sheet

## Long COVID

* [Long COVID syndrome in children: neutrophilic granulocyte dysfunction and its correlation with disease severity](https://doi.org/10.1038/s41390-024-03731-1)

* [Evaluating Long-Term Autonomic Dysfunction and Functional Impacts of Long COVID:
A Follow-Up Study](https://doi.org/10.1101/2024.10.11.24315277)

* [Mechanisms of long COVID and the path toward therapeutics](https://doi.org/10.1016/j.cell.2024.07.054)

* [Long COVID and associated outcomes following COVID-19 reinfections: Insights from an International Patient-Led Survey](https://doi.org/10.21203/rs.3.rs-4909082/v1)

* [Long COVID science, research and policy](https://doi.org/10.1038/s41591-024-03173-6)

* [Long COVID: major findings, mechanisms and recommendations](https://doi.org/10.1038/s41579-022-00846-2)

* [Postacute Sequelae of SARS-CoV-2 in Children](https://doi.org/10.1542/peds.2023-062570)

* [Natural course of post-COVID symptoms in adults and children](https://doi.org/10.1038/s41598-024-54397-y)

## Viral Life Cycle

* [A novel SARS-CoV-2 recombinant transmitted from a patient with an acute co-infection](https://doi.org/10.1016/j.lanmic.2024.100998)

## Immune System Damage

* [Lethal COVID-19 associates with RAAS-induced inflammation for multiple organ damage including mediastinal lymph nodes](https://doi.org/10.1073/pnas.2401968121)

* [Bystander monocytic cells drive infection-independent NLRP3 inflammasome response to SARS-CoV-2](https://doi.org/10.1128/mbio.00810-24)

* [Rapid progression of CD8 and CD4 T cells to cellular exhaustion and senescence during SARS-CoV2 infection](https://doi.org/10.1093/jleuko/qiae180)

* [Differential decline of SARS-CoV-2-specific antibody levels, innate and adaptive immune cells, and shift of Th1 / inflammatory to Th2 serum cytokine levels long after first COVID-19](https://doi.org/10.1111/all.16210)

* [Tissue-based T cell activation and viral RNA persist for up to 2 years after SARS-CoV-2 infection](https://doi.org/10.1126/scitranslmed.adk3295)

* [MDA5-autoimmunity and interstitial pneumonitis contemporaneous with the COVID-19 pandemic (MIP-C)](https://doi.org/10.1016/j.ebiom.2024.105136)

* [Viral afterlife: SARS-CoV-2 as a reservoir of immunomimetic peptides that reassemble into proinflammatory supramolecular complexes](https://doi.org/10.1073/pnas.2300644120)

* [Megakaryocyte infection by SARS-CoV-2 drives the formation of pathogenic afucosylated IgG antibodies in mice](https://doi.org/10.1101/2023.07.14.549113)

* [Loss of Bcl-6-Expressing T Follicular Helper Cells and Germinal Centers in COVID-19](https://doi.org/10.1016/j.cell.2020.08.025)

* [Interstitial macrophages are a focus of viral takeover and inflammation in COVID-19 initiation in human lung](https://doi.org/10.1084/jem.20232192)

## Systemic Bodily Damage

### Multiple Organs / Effects

* [Blood–brain barrier disruption and sustained systemic inflammation in individuals with long COVID-associated cognitive impairment](https://doi.org/10.1038/s41593-024-01576-9)

* [Infectivity of deceased COVID-19 patients](https://doi.org/10.1007/s00414-021-02546-7)

### Skin

* [COVID-19 infection is associated with an elevated risk for autoimmune blistering diseases while COVID-19 vaccination decreases the risk: A large-scale population-based cohort study of 112 million individuals](https://doi.org/10.1016/j.jaad.2024.10.063)

### Heart / Blood

* [COVID-19 Is a Coronary Artery Disease Risk Equivalent and Exhibits a Genetic Interaction With ABO Blood Type](https://doi.org/10.1161/ATVBAHA.124.321001)

* [RNA-Seq analysis of human heart tissue reveals SARS-CoV-2 infection and inappropriate activation of the TNF-NF-κB pathway in cardiomyocytes](https://doi.org/10.1038/s41598-024-69635-6)

* [Fibrin drives thromboinflammation and neuropathology in COVID-19](https://doi.org/10.1038/s41586-024-07873-4)

* [One-fourth of COVID-19 patients have an impaired pulmonary function after 12 months of disease onset](https://doi.org/10.1371/journal.pone.0290893)

* [Cardiovascular effects of the post-COVID-19 condition](https://doi.org/10.1038/s44161-023-00414-8)

* [Pulmonary Dysfunction after Pediatric COVID-19](https://doi.org/10.1148/radiol.221250)

### Muscles

* [Infection and chronic disease activate a systemic brain-muscle signaling axis](https://doi.org/10.1126/sciimmunol.adm7908)

### Mitochondria

* [Targeted Down Regulation Of Core Mitochondrial Genes During SARS-CoV-2 Infection](https://doi.org/10.1101/2022.02.19.481089)

* [Altered mitochondrial respiration in peripheral blood mononuclear cells of post-acute sequelae of SARS-CoV-2 infection](https://doi.org/10.1016/j.mito.2024.101849)

### Brain / Nervous System

* [The risk of cognitive decline and dementia in older adults diagnosed with COVID-19: A systematic review and meta-analysis](https://doi.org/10.1016/j.arr.2024.102448)

* [Cognitive and psychiatric symptom trajectories 2–3 years after hospital admission for COVID-19: a longitudinal, prospective cohort study in the UK](https://doi.org/10.1016/S2215-0366(24)00214-1)

* [Post-hospitalisation COVID-19 cognitive deficits at one year are global and associated with elevated brain injury markers and grey matter volume reduction](https://doi.org/10.1038/s41591-024-03309-8)

* [SARS-CoV-2 Rapidly Infects Peripheral Sensory and Autonomic Neurons, Contributing to Central Nervous System Neuroinvasion before Viremia](https://doi.org/10.3390/ijms25158245)

* [Increased frequency and mortality in persons with neurological disorders during COVID-19](https://doi.org/10.1093/brain/awae117)

* [Long COVID and its association with neurodegenerative diseases: pathogenesis, neuroimaging, and treatment](https://doi.org/10.3389/fneur.2024.1367974)

* [SARS-CoV-2 infection causes dopaminergic neuron senescence](https://doi.org/10.1016/j.stem.2023.12.012)

* [Long COVID is associated with severe cognitive slowing: a multicentre cross-sectional study](https://doi.org/10.1016/j.eclinm.2024.102434)

* [Cognition and Memory after Covid-19 in a Large Community Sample](https://doi.org/10.1056/NEJMoa2311330)

### Intestines

* [Association between gut microbiota development and allergy in infants born during pandemic-related social distancing restrictions](https://doi.org/10.1111/all.16069)

* [Incidence of persistent SARS-CoV-2 gut infection in patients with a history of COVID-19: Insights from endoscopic examination](https://doi.org/10.1055/a-2180-9872)

## Control & Elimination

### Vaccination / Prevention

* [Cohort study of cardiovascular safety of different COVID-19vaccination doses among 46 million adults in England](https://doi.org/10.1038/s41467-024-49634-x)

* [A dual inhibitor of PIP5K1C and PIKfyve prevents SARS-CoV-2 entry into cells](https://doi.org/10.1038/s12276-024-01283-2)

* [Hybrid immunity to SARS-CoV-2 arises from serological recall of IgG antibodies distinctly imprinted by infection or vaccination](https://doi.org/10.1016/j.xcrm.2024.101668)

* [Toward a Radically Simple Multi-Modal Nasal Spray for Preventing Respiratory Infections](https://doi.org/10.1002/adma.202406348)

### Treatment

* [Favorable Antiviral Effect of Metformin on SARS-CoV-2 Viral Load in a Randomized, Placebo-Controlled Clinical Trial of COVID-19](https://doi.org/10.1093/cid/ciae159)

### Dosage / Infectivity

* [Quantity of SARS-CoV-2 RNA copies exhaled per minute during natural breathing over the course of COVID-19 infection](https://doi.org/10.1101/2023.09.06.23295138)

* [Two doses of the SARS-CoV-2 BNT162b2 vaccine enhance antibody responses to variants in individuals with prior SARS-CoV-2 infection](https://doi.org/10.1126/scitranslmed.abj0847)

### Airflow

* [Transient transmission of droplets and aerosols in a ventilation system with ceiling fans](https://doi.org/10.1016/j.buildenv.2023.109988)

* [Ambient carbon dioxide concentration correlates with SARS-CoV-2 aerostability and infection risk](https://doi.org/10.1038/s41467-024-47777-5)

### Masks / Respirators

* [Relative efficacy of masks and respirators as source control for viral aerosol shedding from people infected with SARS-CoV-2: a controlled human exhaled breath aerosol experimental study](https://doi.org/10.1016/j.ebiom.2024.105157)

### Air Cleaning / Sterilisation

* [222 nm far-UVC light markedly reduces the level of infectious airborne virus in an occupied room](https://doi.org/10.1038/s41598-024-57441-z)

* [A CFD study on the effect of portable air cleaner placement on airborne infection control in a classroom](https://doi.org/10.1039/D4EM00114A)

### Contact Tracing

* [Tracing household transmission of SARS-CoV-2 in New Zealand using genomics](https://doi.org/10.1038/s44298-024-00032-6)

* [Substantial transmission of SARS-CoV-2 through casual contact in retail stores: Evidence from matched administrative microdata on card payments and testing](https://doi.org/10.1073/pnas.2317589121)

----

# Other Associated papers

* [Associations of Cognitive Function Scores with Carbon Dioxide, Ventilation, and Volatile Organic Compound Exposures in Office Workers: A Controlled Exposure Study of Green and Conventional Office Environments](https://dx.doi.org/10.1289/ehp.1510037)

* [The anatomy of brainwashing](https://doi.org/10.1126/science.adp1705)

----

# Aotearoa's Descent into COVID Madness

A summary of [COVID meeting minutes](https://fyi.org.nz/request/17765-minutes-for-meetings-covid-19-national-response-groups-and-covid-19-chief-executives-board#incoming-75491).

## Quick Summary

* 28th Sept - introduced concept of "loosening the pressure in the tires"
* 30th Sept - planned to transition to a new framework, despite good support for AL system
* 3rd Oct - planning continued, despite case spread

## More Verbose Summary

19 September 2021:

2.1. We can expect the tail end of this virus to be different. The clusters are made up of multiple families, all interlinked, across big households. Due to this, it is expected that case numbers will remain at current levels for some time as we reach the edges of these clusters and the remaining household contacts become positive cases.

28th September 2021:

5. Loosening of Alert level settings in Auckland 'loosening the pressure in the tires' over the next few weeks will be discussed with Ministers at the 1130hrs tomorrow 29th September.

30th September 2021:

1.1 Concern over the Auckland situation, noting rising case numbers and hospitalisation numbers increasing. Evidence emerging that a second (but linked) outbreak is occurring with challenging characteristics.

1.2 Confidence the outbreak is still contained to Auckland and the strategy is working.

1.3 Social licence remains despite high alert levels for extended period of time.

2.4 Additional papers being lodged next Monday, and Tuesday include:

* Vaccination of non-New Zealand citizens
* traffic light frameworks

5. Comms Update:

5.1 Noted declining levels in social licence; citing abusive behaviour towards border workers and an attack in a supermarket over mask usage.

5.2 Work underway on establishing what has been successful this outbreak. Noted the effectiveness on pre-existing frameworks and how that assisted public transparency and the timeliness of comms distribution.

5.3 Noted that overall New Zealanders are comfortable with the Governments response and Alert Level Framework.

***And here's the kicker. Despite overall support and comfort with the Alert Level Framework...***

5.4 Comms are looking at how to transition to a new framework through an evolution approach rather than a revolution.

3rd October 2021:

1.1 The Prime Minister announced today 3 October 1300hrs that certain areas around Raglan and Hamilton will have a bespoke boundary and Alert Level 3 settings will be applied due to positive cases reported in the areas.

3rd October 2021:

5.2.4. Policy issue on implications of today’s situation on tomorrow’s Cabinet decision which should be worked through with the NRLT this afternoon.
