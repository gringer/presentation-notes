---
title: "Basecalling 2018 - Richard Ronan"
author: "David Eccles"
date: 2018-12-14
---

# Richard Ronan

These are presentation notes created by David Eccles from Richard
Ronan's presentation on 13th December 2018

## Novel statements selected by David Eccles

## Overview

Go over changes while making them

Currently:
 MinION Albacore (CPU), available as standalone download
 GridION / PromethION use Guppy, available as standalone to developers

All systems moving to Guppy, standalone Guppy will be available to
everyone. MinKNOW will feed directly into basecaller.

Guppy was designed to harness computing power of GPUs. Massive speed
improvements to basecalling (35k/s with Intel Core i7, 1500k bases/s
with NVIDIA GTX 1080Ti).

 - Will be CPU-enabled

Guppy will do 1D^2 with GPU

Sequencing summary file

Removal of Dogfish, substantially simplifies the software stack

## Folder structure overhaul

Output dir / experiment id / sample id / {start_time}

Multi-read fast5 files:
* File names no longer have read / channel number

## Flip-flop basecaller

 - not going into detail
 - now available via github via flappie

## Demultiplexing
 - done during run, rather than separate analysis

Guppy allows multiple flow cell types and 