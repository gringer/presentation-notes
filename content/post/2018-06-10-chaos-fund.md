---
title: "Chaos Fund"
author: "David Eccles"
date: 2018-06-10
---

# General Concept

*Note: this is a concept for an as-yet unimplemented funding model. Anyone is free to implement, adapt or share this concept under the terms of the [Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/4.0/).*

The Chaos Fund (henceforth referred to as "the fund") is a variable-award fund that is set up to provide individual people with an equal opportunity to get the award.

The fund is distributed to all people with a New Zealand bank account who can write an abstract stating how they intend to use their award. Abstracts should be at least 100 words and no more than 500 words, can be written (or expressed) in any language (including as a video), and can be created by a representative of the award recipient. The fund is only for individual applicants and cannot be distributed to companies or groups of people (although award recipients are subsequently free to distribute their award as they choose).

# Distribution period

The fund is distributed when there is at least one accepted application, and the amount per award recipient exceeds $100, but no more frequently than once per month, and no less frequently than once per year. The distribution should start on the nearest weekday to the 12th of the month following when the distribution criteria are met.

# Applicant exclusion

Any abstract that indicates that the fund will be used for criminal activity will be rejected. After consideration, the following are also grounds for exclusion:

* No description of how the money will be spent (explicit or implied)
* Any indication that the applicant is a company or group of people
* Abusive, or otherwise derogatory statements
* Meaningless text (e.g. "Akelf self mcsig. Sidafark lawtey goeng, godgas!")
* Highly-repetitive text (e.g. "Give me money. Give me money. Give me money.")
* Abstracts that are too long (longer than 500 words)
* Abstracts that are too short (shorter than 100 words)

# Distribution amount

The total amount to be distributed by the fund to award recipients is calculated based on 90% of the net income (interest plus donations minus administrative costs) that the fund has received since the last award distribution. The distribution amount is divided equally among applicants, rounded down to the nearest dollar. If at least three months has passed since the last award distribution, and the calculated distribution amount is less than $1, then all award applications are rejected; this rejection does not alter the date of the last award distribution.

To be explicit, donations are considered as part of net income for the first round of funding. Any money left over in each round (i.e. 10% of net income, or 100% if all applications are rejected) will be absorbed into the fund and contribute to interest in the next round of the fund.

# Information collection

The only information required and collected from each recipient is a New Zealand bank account number and an abstract. A notification address (email, postal, or phone) can also be provided that will be used for notification of distribution or rejection. Recipients are free to disclose additional information in their abstract if desired. When an application is rejected, a brief summary of the reasons for rejection will be provided to the notification address (if provided).

This information will be stored only until the recipient's award is distributed, or their application is rejected, whichever comes first (unless a longer storage duration is required by law).

Applicants will also be provided with an application ID at the time of their application (if applied using a method that is able to produce instantaneous feedback), with the application ID also sent to the applicant's notification address (if supplied). This ID will also be retained only until award or rejection, unless it is linked (prior to the next fund distribution date) to a donation, in which case the ID and notification address will be retained for at least one year, but no shorter than what is required by law.

# Notification

Abstract submitters will be notified via the notification address (if provided) about award distribution or abstract rejection as soon as possible after distribution or rejection has occurred. If the notification method is by phone, three attempts (on three separate working days) will be made to contact the recipient in person to make sure that the recipient is aware of the distribution or rejection. If the notification method is by postal address or email, only one attempt for contact will be made. Notification costs are considered to be administrative costs, and will not be subtracted from the amount distributed to the recipient.

When a donation is made to the account, with a reference including an active application ID that also includes a notification address, a donation receipt and report on the status of the Chaos fund will be sent to the notification address. The donation can be linked to a rejected application during the current funding round. Application IDs will not be considered active if the application is not part of the current funding round.

# Report format

In the event that a donation has been received with a linked notification address, a report will be generated containing the following information:

* Amount awarded per recipient
* Date of last fund distribution
* Date of current fund distribution
* Proportion (not number) of rejected applications
* Proportion (not number) of accepted applications

The report format is deliberately vague to exclude personally-identifiable information, and also to obscure the total size of the fund.

# Fund termination

The fund will terminate when *all* applicants submit an application that requests for the fund to be terminated (i.e. a unanimous request for termination) and is less than 100 words long (i.e. would otherwise be rejected). When this happens the remainder of the fund (after administrative costs are taken into account) will be distributed equally among all applicants who provided account details.
