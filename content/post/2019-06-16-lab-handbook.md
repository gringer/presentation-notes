---
title: "Lab Handbook"
author: "David Eccles"
date: 2019-06-16
---

# History

This is a skeleton of a lab handbook, for my future reference. It is based on a Twitter thread, found [here](https://twitter.com/samuelmehr/status/1139733291899080705).

Any additions to that are my own aspirations about my ideal lab.

# Mission Statement

[Research goals; philosophy of how the lab works; chronological and academic history of the lab; ideas about future projects; why we do what we do]

* Diversity of thought is important
  * all tolerant cultures are welcome
  * when no one is speaking, we strive to listen to the silence

* Equal pay is important - everyone working over 10h/week is on the same salary

* Open by design - the lab is built on a foundation of openness, kindness, humility, and privacy
  * people are encouraged to share their thoughts, but should be given the freedom to choose the way in which their thoughts are shared.

* Every project is paired - we want to encourage collaboration, so expect that each project will be tackled by at least two people.

# Ethics & Safety

[irb procedures; safety procedures; what to do when something goes wrong; what to do/who to call in an emergency]

# Behaviour

[official code of conduct; policies on scientific integrity, sexual harassment, discrimination; lab culture stuff "the kind of lab we are trying to be" and "things about our lab students like/dislike"; what to do if you have a problem; university-level information]

# Roles & Expectations

[who's who and what they do all day, including the PI; expectations of everybody from everyone else's perspective]

# Open Science

[Why and how the lab deals with sharing data, code, materials; how the lab does version control; how to do this stuff with lab collaborators]

* Where possible, any electronic material should be revision-controlled

* Where there are no ethical reasons against it, data will be made public as soon as possible after it is created

* Protocols and methods will be made publicly available

# Communication

[how people in the lab talk to each other; things like "always use Discourse", "phone calls are only for emergencies"; how long to wait before nudging the PI on something you need; how meetings with PI work and what to prepare for them; check-ins/stand-ups/huddles]

# Logistics

[when and for how long people work; policy on remote work; vacation; where the lab is located and how to get there (especially how to tell *others* how to get there); how to book a conference room; what to do if you get locked out]

# Internal Resources

[these are usually a long list of things people in the lab need access to: servers, software packages, commonly used web tools, shared credentials, room keys and other physical resources, etc]

*Note: Every internal resource that is not publicly available should have a justification statement to explain why it is not publicly available*

# External Resources

[how to get a library card; which building has the best photocopier; useful websites and tutorials, etc]

# Onboarding

[master list of everything that new lab members need to deal with when starting out, including credentials, software, hardware, keys, university ID, etc]

# Recurring Events

[logistics and structure of lab meetings, departmental seminars, etc; daily and weekly task lists (differing by lab roles)]

# Day-to-day

[dress code & hygiene; can you bring your dog to the lab; person X has a peanut allergy so please pack your lunch carefully; health stuff; work-life balance]

# Engagement

[how lab website and social media work; expectations surrounding public outreach, including from official lab accounts; social media policy for personal accounts; protocol for recruiting participants in the community; being a good departmental citizen]

# How to run experiments

[detailed instructions for doing research; some labs do this for all experiments separately while others have a general set of instructions like "how you should interact with participants"; how to compensate participants]

# Data analysis

[how the lab does analyses; expectations concerning how code is written, version controlled, and archived; how to handle data protection and security. this one overlaps with OPEN SCIENCE quite a bit]

# Publications

[how the lab deals with authorship, including the differences between listing in acknowledgments & co-authors; checklist of everything to do before a paper is published; policies on preprints, postprints, and open access; preferences about journals]

* Every publication should be placed on a preprint server (or similar publicly-available permanent record) prior to submission for peer review

# Conferences

[how to give a talk; which conferences do lab members usually attend and why; technology considerations surrounding visualizations; data considerations for work-in-progress; discussion of #betterposter or #worseposter, etc]

# Money

[what grants pay for the lab and why; what the lab will and won't pay for, usually (differs by roles); what grants people could/should apply for; what to expect for conference expenses, computers, etc]

# Offboarding

[what to do when you leave the lab; making sure all data & materials are archived properly; transferring credentials; how to stay in touch (what happens to your Discourse account)]

# Mentorship & Development

[how to choose a project; how to get feedback from others on new ideas; how to initiate collaborations inside/outside the lab; doing a thesis/dissertation; professional development at university level; rec letters]

# What to do if you need help

[how to get support from inside or outside the lab; schedule of routine training for new members; things that bear repeating like "always ask questions!"; how to make mistakes productively]

# Reading list

[a list of papers, blog posts, and news articles that all students and lab members should read]

# Example lab manuals

* https://github.com/alylab/labmanual @mariam_s_aly
* https://osf.io/fztua/ @PaulMinda1
* https://github.com/WhitakerLab/Onboarding @thewhitakerlab
* http://www.sexchrlab.org/lab#/expectations @sexchrlab
* https://faculty.newpaltz.edu/glenngeher/new-paltz-evolutionary-psychology-lab-member-orientation-page/ @glenngeher
* https://tyelab.org/ @kaymtye
* https://osf.io/kgd9b/wiki/home/ @lingtax
* http://jonathanpeelle.net/blog/2016/01/07/maintaining-a-lab-manual @jpeele
* https://ccmorey.github.io/labHandbook/ @candicemorey
* https://docs.google.com/document/d/1phmA17c_hkAfLZN1yMXtFlCYlEqXEc8izEQsfoqgdTY/edit @talialer