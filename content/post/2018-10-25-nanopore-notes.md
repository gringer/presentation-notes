---
title: "Nanopore Notes"
author: "David Eccles"
date: 2018-10-25
---

People ask me a few things from time to time about nanopore
sequencing. I've started collating my responses and presentations,
which you can find here. Consider this an "AQ", rather than a "FAQ".

If you find these notes useful, please consider buying me a
[ko-fi](https://ko-fi.com/gringer). Or better yet, think of me when
you have a little bioinformatics project that I could help out
with. More details about what I do can be found here:

https://www.gringene.org/services.html

<img src="/pics/unscienced_nanopore_pointilised.png" alt="An
'unscienced' image of nanopore sequencing, showing the helicase
[unzipper thingy] feeding DNA [stringy stuff with candy on it] into a
nanopore [really, really, really tiny string muncher], which ends up
in a string toilet. The DNA is zapped with an electric current, and a
gauge measures how much the zap hurt." width="80%">

# Walkthroughs / Tutorials / Protocols

* GPU Calling in MinKNOW [(walkthrough)](/2021/10/08/gpu-calling-in-minknow/)
* Demultiplexing with LAST [(protocol)](https://dx.doi.org/10.17504/protocols.io.7vmhn46)
* Preparing Reads for Stranded Mapping [(protocol)](https://dx.doi.org/10.17504/protocols.io.74vhqw6)
* Stranded Mapping [(protocol)](https://dx.doi.org/10.17504/protocols.io.bnk6mcze)
* Stranded Count Table [(protocol)](https://dx.doi.org/10.17504/protocols.io.bndema3e)
* Differential Expression with DESeq [(protocol; not very polished)](https://dx.doi.org/10.17504/protocols.io.799hr96)
* Plasmid Sequence Analysis [(protocol)](https://dx.doi.org/10.17504/protocols.io.by7bpzin)
* Genome Firework Plot [(tutorial)](/2017/09/08/firework-plot/)
* Repeat Spiral Plot [(tutorial)](/2018/01/10/repeat-spiral-plot/)

# Nanopore Papers (peer reviewed, with me as a co-author)

* Energy requirements for cancer metastasis [paper](https://doi.org/10.3389/fonc.2024.1362786)
* Real-time genomics for One Health [paper](https://doi.org/10.15252/msb.202311686)
* Mitochondrial DNA removal changes cancer [paper](https://doi.org/10.3389/fphys.2020.543962)
* River water sequencing [paper](https://doi.org/10.1093/gigascience/giaa053)
* Tree Lab in Sub-Saharan Africa [paper](https://doi.org/10.3390/genes10090632)
* Learning how to get good yield [paper](https://doi.org/10.1111/1755-0998.12938)
* Humboldt colloquium [eBook, chapter 5](https://www.humboldtaustralia.org.au/wp-content/uploads/2018/09/Our-Changing-World-in-the-South-Pacific.pdf)
* *Nippo* whole genome [paper](https://doi.org/10.1186/s12915-017-0473-4)
* Chimeric reads from amplicons [paper](https://doi.org/10.12688/f1000research.11547.2)
* R9 *E. coli* sequencing [paper](https://doi.org/10.12688/f1000research.11354.1)
* *Nippo* mitochondrial genome [paper](https://doi.org/10.12688/f1000research.10545.1)
* Multi-lab *E. coli* sequencing [paper](https://doi.org/10.12688/f1000research.7201.1)
* Influenza [paper](https://doi.org/10.3389/fmicb.2015.00766)
* Mitochondrial transfer [paper](https://doi.org/10.1016/j.cmet.2014.12.003)

# Nanopore Posters (not peer reviewed)

* Transcript expression changes (QRW) [poster](http://dx.doi.org/10.7490/f1000research.1117503.1)
* Cells with/without mtDNA (LC) [poster](http://dx.doi.org/10.7490/f1000research.1115509.1)
* *Nippo* whole genome (QRW) [poster](http://dx.doi.org/10.7490/f1000research.1114888.1)
* *Nippo* whole genome (LC) [poster](http://dx.doi.org/10.7490/f1000research.1114015.1)
* *Nippo* mitochondria (AGTA) [poster](http://dx.doi.org/10.7490/f1000research.1113382.1)
* Music from DNA (LC) [poster](http://dx.doi.org/10.7490/f1000research.1112810.1)
* MinION sequencing (WHBRS) [poster](http://dx.doi.org/10.7490/f1000research.1110953.1)
* Rhapsody of errors (LC) [poster](http://dx.doi.org/10.7490/f1000research.1110614.1)

# Nanopore Presentations (not peer reviewed)

* Chimeric Reads and Where to Find Them [slides](https://doi.org/10.7490/f1000research.1117667.1)
* Reflections on repetitive repeats with REPAVER [slides](https://doi.org/10.7490/f1000research.1117636.1)
* A pore update (MIMR) [slides](https://doi.org/10.7490/f1000research.1116830.1)
* Women In Nanopore Sequencing (Otago) [slides](https://f1000research.com/slides/8-343)
* Sequencing from food to figures (linux.conf.au) [video](https://www.youtube.com/watch?v=CHCAb-PAqUI);
  [data](https://zenodo.org/record/2548026)
* Repetitive sequences (QRW) [slides](http://dx.doi.org/10.7490/f1000research.1116032.1)
* Update on nanopore sequencing (MIMR) [slides](http://dx.doi.org/10.7490/f1000research.1115840.1)
* Repetitive sequences (MIMR) [slides](http://dx.doi.org/10.7490/f1000research.1115325.1)
* Cells with/without mtDNA (MIMR) [slides](http://dx.doi.org/10.7490/f1000research.1115283.1)
* Private sequencing in a public world (Humboldt) [slides](http://dx.doi.org/10.7490/f1000research.1115127.1)
* *Nippo* whole genome (LC Lightning talk) [slides](http://dx.doi.org/10.7490/f1000research.1114016.1)
* The jungle of MinION data (PoreCamp AU) [slides](http://dx.doi.org/10.7490/f1000research.1113659.1)
* Bioinformatics Institute seminar (AUT) [video](https://www.youtube.com/watch?v=8K_O1aPM-WI)
* Sensor-stimulating Sequencing (TEDxWellington) [video](https://www.youtube.com/watch?v=yTgZVVHJMlw)
* Rhapsody of errors (NZ NGS) [slides](http://dx.doi.org/10.7490/f1000research.1110951.1)

# Clive Brown's Presentations

* Sub-$1000 human genomes on Nanopore [video](https://nanoporetech.com/about-us/news/tech-update-sub-1000-genomes-flongle-and-other-goodies) [notes](https://docs.google.com/document/d/1BiU-82vrOyqGwyvjEdSS07jwK_dUBTOvJ7MgSlMmYIE)
* GridION X5 - The Sequel [video](https://nanoporetech.com/videos/clive-brown-gridion-x5-sequel) [notes](https://gringer.gitlab.io/presentation-notes/2017/03/15/gridion-x5-the-sequel/)
* No Thanks, I've Already Got One [video](https://plus.google.com/events/cuk2eofrekvogeam12klluivqts)
* A Wafer-thin Update [video](https://community.nanoporetech.com/posts/technology-update-from-cli)
* Owl Stretching with Examples [video](https://londoncallingconf.co.uk/lc/2015-plenary#128281064)
* Inside The SkunkWorx [video](https://nanoporetech.com/resource-centre/talk/inside-skunkworx)
* Some mundane and incremental updates [video](https://nanoporetech.com/videos/clive-brown-cto-nanopore-technology-update-london-calling-conference-2017) [notes](https://gringer.gitlab.io/presentation-notes/2017/05/04/london-calling-2017/#clive-brown)
* Plenary from London Calling 2018 [video](https://londoncallingconf.co.uk/lc18/whats-on?field_talk_value=plenary#modal=CliveBrown:PlenaryfromLondonCalling2018) [notes](https://gringer.gitlab.io/presentation-notes/2018/05/24/london-calling-2018/#clive-brown)
* Plenary from Nanopore Community Meeting 2018 [video](https://vimeo.com/303444091) [notes](https://gringer.gitlab.io/presentation-notes/2018/12/06/nanopore-community-meeting-2018/#clive-brown)
* Plenary from London Calling 2019 [video](https://vimeo.com/338137948) [notes](https://gringer.gitlab.io/presentation-notes/2019/05/23/london-calling-2019/#clive-brown)
* Plenary from Nanopore Community Meeting 2019 [video](https://vimeo.com/377694045) [notes](https://gringer.gitlab.io/presentation-notes/2019/12/06/nanopore-community-meeting-2019/#clive-brown)
* [London Calling 2021](/2021/05/20/london-calling-2021#clive-brown)

# Nanopore Community Meeting / London Calling Presentation notes
* [Nanopore Community Meeting 2024](/2024/09/17/nanopore-community-meeting-2024/)
* [London Calling 2024](/2024/05/22/london-calling-2024/)
* [London Calling 2023](/2023/05/18/london-calling-2023/)
* [Nanopore Community Meeting 2022](/2022/12/06/nanopore-community-meeting-2022/)
* [London Calling 2022](/2022/05/18/london-calling-2022/)
* [Nanopore Community Meeting 2021](/2021/12/01/nanopore-community-meeting-2021/)
* [London Calling 2021](/2021/05/20/london-calling-2021/)
* [London Calling 2020](/2020/04/18/london-calling-2020/)
* [Nanopore Community Meeting 2019](/2019/12/06/nanopore-community-meeting-2019/)
* [London Calling 2019](/2019/05/23/london-calling-2019/)
* [Nanopore Community Meeting 2018](/2018/12/06/nanopore-community-meeting-2018/)
* [London Calling 2018](/2018/05/24/london-calling-2018/)
* [London Calling 2017](/2017/05/04/london-calling-2017/)

# Email / message discussions

The following bits and pieces are collected from piecemeal discussions
that I've had with various people. In order to reduce issues about
authorship, the comments represent only my side of the discussion,
with some additional changes where clarification is necessary.

## 2025-Mar-04

### Rapid PCR Barcoding

The rapid PCR barcoding kit can be used with shorter reads, and higher
concentrations can be used with a drop in PCR cycles, but some
experimentation will be needed to make sure that the loading
concentration is reasonable (I prefer about 20 ng/μl).

The rapid PCR barcoding kit works best on untargeted sequences
(i.e. shotgun sequencing). If you're already creating amplicons
(i.e. you already have primers) and don't care too much about the cut
point (as is implied by the use of the PCR barcoding kit), it may be
easier to use the rapid barcoding kit (i.e. the non-PCR version) on
your amplified products.

### Read Fragmentation

The rapid fragmentation buffer fragments randomly, so there will be a
wide range of different fragments that have a mean length of half the
mean length of the original library.

Here's a demonstration of what random fragmentation can do to a
library that has an initial L50 of about 9.5 kb:

<img src="/pics/read_fragmentation.png" alt="Histograms showing [top]
an unfragmented library with L50 of about 9.5 kb, and [bottom] the
same library randomly fragmented with L50 of about 6.5 kb."
title="Read fragmentation histograms" width="512"/>

This assumes no sequencing bias, and the removal of reads shorter than
50bp. I've noticed that the L50 of the sequenced library doesn't seem
to change much based on the spread of the original library.

Code for completeness:

```
#!/bin/Rscript

meanLibraryLength <- 9100;
sdLibraryLength <- 2000;

sdIsLogScale <- TRUE;
readCount <- 10000;

if(sdIsLogScale){
    rnorm(mean=log(meanLibraryLength),
          sd=log(meanLibraryLength+sdLibraryLength) -
              log(meanLibraryLength), n=readCount) |>
        exp() |>
        round() |>
        sort(decreasing=TRUE) -> library.lengths;
} else {
    rnorm(mean=meanLibraryLength, sd=sdLibraryLength, n=readCount) |>
        round() |>
        sort(decreasing=TRUE) -> library.lengths;
}

N50 <- sum(cumsum(library.lengths) / sum(library.lengths) < 0.5);
L50 <- library.lengths[N50];

frag.length <-
    runif(n=readCount);

c((library.lengths * frag.length) |> ceiling(),
  (library.lengths * (1-frag.length)) |> ceiling()) |>
    sort(decreasing=TRUE) -> frag.lengths;

seq.lengths <- frag.lengths[frag.lengths >= 50];

N50.seq <- sum(cumsum(seq.lengths) / sum(seq.lengths) < 0.5);
L50.seq <- seq.lengths[N50.seq];

layout(mat=matrix(c(1,2),nrow=2));
oRange <- hist(library.lengths / 1000, plot=FALSE);
sRange <- hist(seq.lengths / 1000, plot=FALSE);
(library.lengths / 1000) |>
    hist(main=sprintf("Original Library; mean = %0.3f kb; L50 = %0.3f kb",
                      round(mean(library.lengths/1000)), L50/1000),
         xlab = "Sequence Length (kb)", breaks=50,
         xlim=range(c(oRange$breaks, sRange$breaks)));
(seq.lengths / 1000) |>
    hist(main=sprintf("Sequenced Library; mean = %0.3f kb; L50 = %0.3f kb",
                      round(mean(seq.lengths/1000), 3), L50.seq/1000),
         xlab = "Sequence Length (kb)", breaks=50,
         xlim=range(c(oRange$breaks, sRange$breaks)));
```

## 2025-Feb-26

### Amplicon Analysis

If you're pooling amplicons of multiple lengths, they need to be at
least pooled in equimolar proportions, and probably skewing a bit
further towards the longer reads if there's a substantial difference.

Creating a pooling calibration curve for determining the pooling
fraction should be easy: sequence a DNA ladder with known band
molarities and sequences. Unfortunately it can be difficult to know
the sequences, ladders often duplicate sequences, and the band
molarities are often determined based on eyeballing a gel, rather than
any other quantitative measure. Using [pPSU
ladders](https://doi.org/10.1038/s41598-017-02693-1) might help with
that.

## 2025-Feb-25

### Roche SBX

I've now read through [Roche's SBX
paper](https://doi.org/10.1101/2025.02.19.639056), and have a few
thoughts. I'm not sure where to put them, but this will do for
now. I'll explain a bit further on why I don't think it's *true*
nanopore sequencing. Because the paper doesn't have any stated
license, all of my words are paraphrased. I've tried to avoid
mentioning any numbers (with a couple of exceptions), and am trying to
be conscious of representing my own thoughts, rather than a direct
comment on the text in the paper.

My first impression: "But it's going through the pore in reverse!"

My second big thought: "If you're going to all that effort in making
big stuff to attach to small stuff, why not skip the nanopore
entirely, and do direct imaging of the expanded polymer? Surely
there's room for a fluorophore, or a metal ion in there."

Oh, they're using lipid bilayers? That means they either give the
customers the means to load the flow cells with pores, or they have
substantial shipping issues.

Pulsed negative voltage drives the things through the pores, with a
frequency of 1 kHz, interspersed with a 6μs "more power" pulse to
force each base unit through.

I find this intriguing, given that the images suggest the push is from
the smaller side; seems like that would pop pores out as well.

Oh, and it's measuring *current*, which means that (as with ONT) it's
likely measuring carrier ion transition speed rather than the
transition speed of the actual structure (which would presumably need
to be kept fairly constant due to the pulsing).

Hmm phasing issues due to pores getting blocked, or being too open;
it's a very delicate balance there. I could imagine if a pore's
getting forced open repeatedly over the course of a run, phasing
issues would increase as the run progresses.

Less of an issue with single molecules, though.

Ah, enhanced by a good old "we don't know how this works, but it stops
working when we remove it."

Oh good, they did actually test the orientation and found lower noise
when the expandomer was fed into the narrow end first. Makes sense;
it's not an obvious first choice, but I can see how it might help deal
with the signal-to-noise ratio.

I don't understand the distinction between samples per second
(i.e. [Sps](https://e2e.ti.com/support/microcontrollers/msp-low-power-microcontrollers-group/msp430/f/msp-low-power-microcontroller-forum/511190/relation-between-the-khz-vs-ksps))
and cycles per second (i.e. Hz). But even if that's only the
low[er]-frequency filtered data going into the raw data file, it's
going to be a *big* file.

Early in the introduction they rubbished existing direct nanopore
sequencing measurement due to homopolymer issues (among other things),
and now they're showing a plot with a 5-mer that has a somewhat wide
error range. I would have liked to see what happens with a 10-mer, but
those are quite rare.

I can see that the pulsing is likely to substantially reduce
homopolymer length issues (beyond general phasing INDEL issues that
will affect the sequences mostly uniformly), but I can see a chance
for alternative base inclusions that would break up homopolymer
sequences.

Current read mappers deal with homopolymer contraction and expansion
by using hompolymer-compressed indexes. I expect that will be less
needed here, and the usual SNP-tolerant mapping approaches will be
sufficient.

Less pain here for bioinformaticians, in other words.

The error distribution histograms support this
hypothesis. Substitutions are the most common errors (but obviously
not easily predictable from the base type due to the chemistry shift),
with some systematic substitution errors; about twice as common as
deletions, which are about twice insertions.

Translocating / sequencing about a quarter of the available
time. Which... doesn't seem all that different from ONT's sequencing
speed on a per-pore level.

Ah, yes. That's a niggling thought I have about all translational
sequencing approaches: there is error in the translation *as well as*
error in the sequencing. So it's important to consider both when
evaluating the overall error in the technology.

In other words, the accuracy of a translational sequencing device is
limited to the accuracy of the translational chemistry.

I remember having *lots* of thoughts about this regarding the SOLiD
technology, which had an *additional* error mode due to an additional
relative translation chemistry.

It does indeed look like their plan is to pre-process the raw signal
data on the device, like what Illumina does. So that's how they're
getting around the information bottleneck.

Even with that... 500 million bases per second is *a lot* of data that
needs to be stored somewhere.

"In a perfect world, where all our pores work on an 8-million array
device, and we can solve this little issue of the chemistry being
limited to a few hundred bases" - Roche, probably.

Surely they wouldn't be making those claims if they hadn't made an
actual functioning device, right?

My final big thought on this is that it really doesn't seem all that
different from what Illumina and PacBio already do.

Sure, Roche is using fancy nanopores to read the translated DNA, but
that's just a thingy whatsit stamp collector.

At its core, it's still a model-dependant sequencing device.

They claim SBX should be compared to ONT, but it'd be more
appropriately compared with PacBio (which is also a translational
single-molecule sequencer). PacBio suffers because highly-accurate
measurement of polymerisation is achingly slow, making up for that
with massively-parallel sequencing.

In Roche's case, they bypass sequencing-by-polymerisation by doing a
faster polymerisation during sample prep (on an even more massive
scale). I agree with their claim that separating the sample prep and
sequencing processes makes it much easier to optimise the different
stages.

What ONT is doing - observational sequencing of native DNA - is
*hard*.

ONT's approach is not completely model-free because it's looking at
carrier ion current, but it's the closest we have so far.

The next level down would be a direct observation of DNA; a
microscope-like DNA sequencer. ONT have themselves floated this idea
as a voltage-sensing nanopore sequencing device.

## 2025-Feb-21

### 16S Taxonomic Profiling

I've always hated the "How long is a piece of string?" answer, so I
try to avoid giving answers of that nature when answering
bioinformatics questions.

One recent Nanopore Community question asked, "How many reads per
sample would be enough for taxonomic profiling?"

Here's my response....

"What do you want to do with the taxonomic profiling once the
sequencing is done? That will inform how many reads you should be
aiming for."

But this "answer" isn't particularly useful, because the *question*
was not particularly useful. I can do better, because I have
experience working with data.

My personal preference is to multiply the lowest amount I strictly
need by 10. So for taxonomic profiling where I want to quantify down
to 1% abundance for each sample (i.e. one in a hundred reads), I would
try to get (at a minimum) 1000 reads per sample.

For consensus amplicon assembly or plasmid, I could probably manage
with as few as 10 full-length reads (or 20 reads with a mean length
equal to half the amplicon length), but I usually splash out with 100
reads because they are so quick and cheap for that purpose.

For haploid genome assembly from a single sample, I aim for 10X
coverage; 20X for diploid (e.g. human genomes). If I'm doing shotgun
sequencing and looking for genetic variants in a human sample down to
10% frequency, then 200X coverage (100X for each chromosome).

But there are other things that can make the piece of string longer or
shorter. Depending on how many samples I am running at the same time,
I might increase that amount by a factor of 2-10 to account for
barcode imbalance, with further adjustments for unclassified reads
(for difficult or unusual samples), low-quality read rate (if I cared
about that... which I don't), or host contamination (if present).

My "ten times the minimum" rule of thumb works in many situations I
encounter and allows me to estimate yield and feasibility, but it
doesn't work everywhere, and doesn't make sense in some situations.

As one example, for single-cell sequencing, I've settled on "one read
per transcript per cell" [based on a couple of papers I've read].

There's no one single best answer for "how many reads per sample?"
that will work in all situations, which is why providing plenty of
context around your question is *really* important.

### Flow cell storage

Overnight, it's probably best to store the flow cells outside a fridge
in a cool, dark place, unless you're *really* sure you have a
non-cycling fridge that won't go below 4°C. Low temperatures (below
4°C), as present in many "frost-free" domestic fridges, can cause a
lot of damage to flow cells.

### Current-free pores

Pores that have no connected voltage gradient won't move polymers
through the pores. Inactive pores can die during the run, but that's
not due to them being burnt out by excess current.

You might have noticed that there's an option at the start of the run
to reserve pores. This setting intentionally shuts down the sequencing
of "good" pores that require too much current for sequencing (relative
to the rest of the flow cell), and waits for the rest of the flow cell
to catch up before resuming sequencing of that pore. If the flow cell
is functioning normally and pores are not being damaged by other means
(e.g. chemical destruction), then this pore "shepherding" action will
increase the overall yield of the sequencing run.

## 2025-Feb-19

### Pore counts

The MinION and PromethION flow cells are designed with multiplexed
sequencing wells to account for expected biological pore loss during
manufacture, and for dispersion effects when loading the pores. On
MinION flow cells, each of the 512 sequencing channels [is connected
up to four different sequencing
wells](https://www.youtube.com/watch?v=kQK3C2TCpPc&t=363s).

An initial pore count of over 1400 should have most of the 512
channels available for sequencing. With a maximally-unlucky
distribution of pores (each working channel gets all four working
pores), there would be 350 channels available, but it's likely to be
substantially higher than that (based on my experience with MinION
flow cells, I'd guess >470).

Bear in mind that there will be additional flow cell degredation
beyond that, which is only noticeable after starting sequencing.

### Mk1C replacement

People interested in a DIY Mk1C-like experience might be interested in
this writeup by Miles Benton, where he goes through the design
oddities of the MinIT and Mk1C, and proposes a cheaper, better
alternative:

https://hackmd.io/@Miles/B1U-cOMyu

There's a Wiki and parts list here (a bit outdated, because technology
has improved substantially in the few years since this was written):

https://github.com/sirselim/jetson_nanopore_sequencing/wiki/Parts-list

[although, given that ONT are canning the Mk1C, they may not keep up
their development of Arm software versions of MinKNOW and dorado]

> **Main components**

> The components listed below act as a replacement for a desktop
  computer or laptop to run MinKnow and interface with the ONT
  MinION. The benefit of the Nvidia Jetson family of 'single board
  computers' is in their price and performance. The key feature being
  the onboard GPU, which, on the Xavier models at least, is more than
  able to keep up with live base calling a MinION flow cell. They also
  act as nice little headless base call servers.

> * Nvidia Jetson Xavier NX / Xavier AGX(confirmed working by external
    collaborators on Jetson TX2 as well, but this board is starting to
    show it's age, Xavier NX isn't much more expensive for a large
    overall upgrade)
       *     Jetson Xavier NX [16 GB] (link) [confirmed]
       *     Jetson Xavier AGX [64 GB] (link) [confirmed]

> * NVMe solid state hard drive (you can go cheaper here, but a high
    speed drive does provide a little performance boost)
       *     Samsung 970 EVO Plus 1TB M.2 (2280), NVMe SSD [confirmed]
  * micro SD card(needed for the Xavier NX, OS drive)
       *     SanDisk 64GB Mobile Extreme Pro microSDXC  [confirmed] 

> **Portability components**

> Below is a list of what we are currently using to have a fully
  portable sequencing unit. This is ideally what you're wanting to add
  if you plan to take a MinION out into the field (from a compute
  perspective, wet-lab reagents and equipment are also
  required). There is obviously a wide range of components that can be
  mixed and matched here, but the below are confirmed compatible and
  working in our hands - see the above picture gallery for our set up.

> * touch screen
       *     generic (no name?) 7 inch LCD 1024x600 HDMI touchscreen [confirmed]
  * power pack / battery
       *     RavPower 27000mAh 85W Power House Model: RP-PB055 [confirmed]
  * solar panel
       *     Choetech 80W Foldable & Portable Solar Panel Charger with DC and USB Type Ports [confirmed]

Note that Miles suggests at least a Xavier NX, and 1TB hard drive. I
expect that the Jetson Xavier AGX could probably be replaced by a
Jetson AGX Orin 64 GB
([link](https://developer.nvidia.com/buy-jetson?product=all)).

## 2025-Feb-17

### PromethION Flow Cells / Genome Coverage

the output of a PromethION flow cell is typically 50-150 Gb. Higher
yields are obtained by having more consistent read lengths, which
means being careful not to prematurely fragment DNA prior to sample
prep, syringe shearing (or similar) to break up really long DNA, then
further small fragment exclusion (e.g. bead purification). DNA repair
helps with improving base call quality, and also helps a little with
reducing pore blockages.

I don't think I ever got enough good PromethION flow cells to be able
to establish what pore count constituted "good". For MinION a pore
count of 1400 was good, and a pore count of 1200 was adequate (both
substantially higher than the ONT warranty level), so that would
transfer to a PromethION pore count of 8400 for good (likely to reach
the highest yields, assuming good sample prep), and 7200 for adequate.

If you're concerned about whole-genome assembly, you only need about
15X for nanopore sequencing to have a good likelihood of covering the
entire genome (it's 30X for Illumina because of the short reads),
which gives about 1-3 genomes from a single PromethION flow cell
depending on how good the flow cell and sample prep is.

That 15X value is based on simulations I did of read length. Here's some R code to demonstrate this:

```
library(tidyverse);

getActualCoverage <- function(genomeSize, readLength, coverage){
  # sample randomly from the genome
  startPoss <- sample(genomeSize, coverage * genomeSize / readLength, 
                      replace = TRUE);
  endPoss <- pmin(genomeSize, startPoss + (readLength - 1));

  tibble(start=startPoss, end=endPoss) %>%
    arrange(startPoss) %>%
    mutate(gap = lead(start) - end) %>%
    filter(gap > 0) -> gapData
  if(nrow(gapData) > 0){
    gapData %>%
      summarise(readCoverage=coverage, gapCount = n(), 
                medianGapLength = median(gap), meanGapLength=mean(gap),
                mappedCoverage = (genomeSize - meanGapLength * gapCount) 
                / genomeSize)
  } else {
    tibble(readCoverage=coverage, gapCount=0, medianGapLength=0,
           meanGapLength=0, mappedCoverage=1);
  }
}
```

With a 10Mb genome and 650bp effective read length, sampled randomly
across the entire genome, there's a good chance of covering every base
at least once with about 15X coverage from the input sequences - FWIW,
genome size doesn't influence this all that much. With 15kb reads, the
required depth is a little bit less (about 10X), and about 8X for
100kb reads.

## 2025-Feb-12

### Read splitting / filtering

For a sequencing run that sequences both ITS and 16S, they should be
easily separable by mapping. This means that you can map to either the
primers (with Bowtie2 or LAST) or a representative sequence for
filtering out reads:

https://bioinformatics.stackexchange.com/a/23190

For primer filtering, I have my own demultiplexing scripts that take
as input a FASTA file containing barcode and/or adapter sequences, and
separate reads into multiple output files based on the barcodes
found. In this case you could declare the "barcodes" to be the primer
sequences:

https://gitlab.com/gringer/bioinfscripts/-/blob/master/fastq-dental.pl

## 2025-Feb-05

### Sequencing Costs

P2 Solo is the clear winner for medium-scale sequencing (i.e. 1-5
human genomes). Because of a very low service / warranty cost, the
cost recovery time (in terms of number of sequencing runs) is very
low. See here for my writeup on that:

https://te-ara-paerangi.community/page/view-post?id=734

ONT previously split their products between CapEx (you own the device)
and OpEx (you rent the device) such that the CapEx options were not
very attractive in comparison, but I think their proposed updated
scheme (which was due to kick in at the end of last month) changes
that.

If I've got the numbers right, the old CapEx option was $23k USD for
the P2 Solo sequencer only, whereas their new proposed CapEx option is
$28k USD for the sequencer, 16 flow cells, and sufficient sequencing
kits to work with those kits (probably 4 sequencing kits, but maybe
more).

If you get in before they change over the pricing, then you can get a
non-owner device for $10k USD with 8 flow cells and 2 sequencing kits
included.

Regardless, the ONT service / support charge for the P2 Solo is $1k
USD per year, and the consumable cost for sequencing is about $1.2k
per run. I don't get the impression from their price change
announcement that the service costs will change, so would expect this
cost to stay at $1k per year.

For direct-to-consumer sequencing, I'd recommend you also offer a
*really cheap* option, which would mean getting a Mk1D device ($3k
USD), a Flongle adapter ($1.5k USD, including 12 Flongle flow cells),
and a rapid barcoding kit (SQK-RBK114.24; $700 USD), which would allow
people to get a taste of sequencing for a marginal consumable cost of
as low as $6 USD per sample (12 Flongle flow cells: $810 USD; Rapid
barcoding kit for 12 runs, each with 24 samples). Obviously you would
add labour and profit overheads to this as a service provider.

Flongle output is only about 200 Mb, so that's only about 8 Mb per
sample (if you're lucky with barcode balancing), but it might be
enough to get people interested. Most people I've talked to have said
they are interested in ancestry testing (for which 8 Mb per sample is
plenty). On the clinical / medical route there's also a quick "what
virus / bacteria is causing my current cough / sore throat?", which
can probably be answered by amplicon sequencing (for which 8 Mb is
also enough).

But there's also a completely untapped artistic / creative market for
sequencing. With sequencing costs low enough, some people might get
sequencing done purely for the novelty of it, and then do completely
unexpected things with the data. If sequencing costs are low enough
that someone can waltz in from the street, give you $20 and a cheek
swab, and you can email them their DNA in a day or so, they're
probably not going to care too much about how much data there is.

Also, FWIW, you only need about 15X for nanopore sequencing to have a
good likelihood of covering the entire genome - it's 30X for Illumina
because of the short reads, which gives about 1-3 genomes from a
single PromethION flow cell depending on how good the flow cell and
sample prep is.

### Rhapsody Workflow

I have experience with data analysis of a few Illumina BD Rhapsody
runs, but not Nanopore reads [despite my many requests to have that
done]. I ended up writing [my own cell barcode QC / processing
script](https://gitlab.com/gringer/bioinfscripts/-/blob/master/synthSquish.pl)
so that I could make it easier for other single cell software to
process the reads.

As an initial pilot study, I proposed taking the Illumina Rhapsody
library, splitting it in half prior to size selection, then processing
that library through the LSK114 kit. Unfortunately, there were
complications due to Sample Tags and AbSeq that made the simple act of
"splitting in half" a difficult operation.

If that were successful, then the next step would have been to process
the library up until the Illumina adapter addition stage, then slot in
the remainder of the LSK114 protocol (i.e. add nanopore adapters
rather than Illumina adapters)... assuming that made sense.

If that were successful, then the plan was to experiment with using
reverse-complement-modified ONT primers for creating nanopore reads
directly off the beads. It would have been technically challenging,
but with a huge payoff in terms of cost and sample preparation time if
it worked.

... but we never got around to doing that.

As an initial plan, I proposed just take a fully-prepared Illumina
library (i.e. a library ready to be sent away for sequencing) and
treating it as double-stranded DNA input for the LSK114 kit, e.g. this
protocol:

https://nanoporetech.com/document/ligation-sequencing-amplicons-sqk-lsk114?device=PromethION

While I'm not familiar with the sample prep side of the Rhapsody
protocol, there are a couple of easy modifications to the standard
approach which should substantially improve cost-effectiveness:

* If possible, sequence things that get amplified separately as
  separate sequencing runs. We found with the Illumina runs that it
  was too easy for Sample Tags to be over-represented or
  under-represented, causing downstream issues with data analysis. One
  of the advantages of Nanopore sequencing is that you can sequence
  until you have enough data, and stop sequencing at that point.

* As with Illumina sequencing, it'll work best if the library length
  distribution is as tight as possible. While Nanopore can sequence
  long reads and reads of vastly different lengths, there is PCR
  amplification bias for vastly different lengths, and some
  competition effects on the flow cells when sequencing.

## 2025-Feb-04

### 16s Amplicon Sequencing

The advantages of 16s for sample preparation become disadvantages for
phylogenetics. Due to 16s being a highly conserved gene there is not
much difference between species, and sequencing error can play a role
in how kraken2 classifies reads. 16s studies are further complicated
by horizontal gene transfer and gene duplication, meaning that even
when a database has been carefully curated, the samples being studied
may have a different 16s profile from that recorded in those curated
databases.

### Metagenomic Analysis / Kraken2 / Bracken

Kraken2 produces results that have inappropriately high precision at
the read level; that's why Bracken is needed.

Bracken can classify at multiple phylogenetic levels, but only does
one level at a time in its output. By default it classifies at the
species level:

> Braken uses the taxonomy labels assigned by Kraken, a highly
  accurate metagenomics classification algorithm, to estimate the
  number of reads originating from each species present in a sample.

[from [here](https://ccb.jhu.edu/software/bracken/)]

> While Kraken classifies reads to multiple levels in the taxonomic
  tree, Bracken allows estimation of abundance at a single level using
  those classifications (e.g. Bracken can estimate abundance of
  species within a sample).

[from [here](https://github.com/jenniferlu717/Bracken?tab=readme-ov-file#installation)]

According to [the
documentation](https://ccb.jhu.edu/software/bracken/index.shtml?t=manual#step3),
Bracken can distribute read counts downwards (e.g. from genus to
species) if a lower classification reaches a particular threshold:

> Additional Options:  
>  
> * CLASSIFICATION_LEVEL [Default = 'S', Options =
    'D','P','C','O','F','G','S']:: specifies the taxonomic rank to
    analyze. Each classification at this specified rank will receive
    an estimated number of reads belonging to that rank after
    abundance estimation.
> * THRESHOLD [Default = 10]:: specifies the minimum number of reads
    required for a classification at the specified rank. ***Any
    classifications with less than the specified threshold will not
    receive additional reads from higher taxonomy levels when
    distributing reads for abundance estimation.***

Bracken is a program that carries out **B**ayesian **R**eestimation of
**A**bundan**c**e with **K**rak**en**. It can be confident about
species-level reassignment because of Bayes' theorem.

In other words, because Bracken is considering all the reads as
evidence, combined with its knowledge about the kmer composition of
all the species.

Bracken has this idea of a read threshold for declaring a species X as
present in the sample. If there are not enough reads that Kraken2
reports as being from another species Y from the same genus, then it
is more likely that those reads are from X than they are from Y, even
if Kraken2 cannot confidently assign all the reads to the species
level.

Also, even if there are more than the threshold level of reads that
Kraken2 has assigned to species Y, Bracken may decide (with an
understanding of the kmer composition of both species) to reassign
those reads to species X instead.

Unlike Kraken2, Bracken *can* see the wood for the trees; Kraken2
doesn't use priors, Bracken does.

Kraken2's difficulties around read assignment are due to how the
Kraken2 databases are set up. If Kraken2 knows that species X and
species Y exist somewhere in the entire world, and it sees a kmer that
is present in both of them (but not in any other species), then it
will assign that kmer to the common ancestor of X and Y.

Bracken mostly only cares about the world that is your sample (or
samples); that's why it can be more confident about its
assignments. If that world sample is not representative of the
population it is sampled from, then there can be specificity issues
(false positive assignments), but that's a general problem with lots
of scientific research.

Kraken2 could be "fixed" by taking out all references to sequence Y
(i.e. regenerating the database without sequence Y), but then Kraken2
would increase both false positive and false negative results. It's
quite common, for example, for Kraken2 to "misclassify" any eukaryotes
as human, because often human is the only eukaryote that is added to
the Kraken2 database [increasing false positives, or reducing
specificity]. It's also common for Kraken2 to have trouble identifying
novel bacteria - less of an issue now than it used to be because the
databases are getting more comprehensive - in which case it does
random stuff; if you're lucky, those novel reads will be unclassified
[increasing false negatives, or reducing sensitivity].

## 2025-Jan-25

### Pore Shepherding

Reserved pores are where the ideal pore voltage of the pore is
substantially more negative than the rest of the pores, such that
sequences aren't getting through the pores at the used voltage. By
delaying activity on these pores until the other pores catch up,
sequencing yield is improved (the alternative being to make the
voltage more negative on all pores, which leads to pores getting
damaged faster). This issue was first noticed and fixed by John Tyson,
who came up with a stop-gap solution that was called "shepherding":

https://pmc.ncbi.nlm.nih.gov/articles/PMC5793790/#s3d

It's possible to disable the pore reserve function and "front-load"
the sequencing, meaning higher yields at the start of the
run. Sometimes this leads to higher yields full-stop, when the pores
degrade fast enough to offset the benefit of pore reservation.

## 2025-Jan-24

### Summary

Oxford nanopore sequencing is an observational sequencing method,
using an electrically-charged nanopore to detect - via changes in the
carrier ion current - the structure of polymers as they move through
it.

## 2025-Jan-23

### Chimeric Reads

My research team wrote a paper on the problem of chimeric reads using
ligation kits, at a time when nanopore sequencing was *only* ligation
based:

https://doi.org/10.12688/f1000research.11547.2

More recent versions of dorado are able to split reads up if multiple
adapter sequences are found. I prefer just discarding chimeric reads;
they appear to be formed randomly, and are enough of a pain to split
up reliably that it's not worth the minimal improvement in read counts
that splitting would achieve. In fact, the splitting act itself could
introduce statistical bias, especially when chimeric reads have lower
quality for particular sequences.

### Flongle Sequencing

I recommend you plan your Flongle experiments assuming 200 Mb output
per flow cell (despite what ONT claims is the expected yield). Given
this, a 96-sample run on a Flongle flow cell is probably not going to
produce useful results, but your own evaluation may differ depending
on how many samples you're trying to run at once, or how low amplicon
coverage you can handle.

Sample concentrations, etc. should be fine for Flongle, assuming
you're following the Flongle protocol. If only a MinION protocol is
available for a particular sample type, then follow the MinION
protocol up until just before the last bead purification step, then
follow a Flongle protocol (from a different sample type that includes
Flongle,
e.g. [here](https://nanoporetech.com/document/ligation-sequencing-gdna-native-barcoding-v14-sqk-nbd114-24?device=Flongle))
from that purification step onwards.

The general rule of thumb for Flongle runs is that half the amount of
a MinION library is loaded with a total sequencing library volume of
30μl.

## 2025-Jan-17

### BSA

Based on the observation that ONT *still* hasn't reported on testing
of recombinant vs non-recombinant BSA, I'd assume that ONT hasn't
tested different BSA treatments either.

Just try it and see.

The yield difference with vs without BSA is not much (if any), so I
doubt there'll be a significant difference with different types of
BSA.

There are other changeable things that contribute more to flow cell
yield, and these would need to be near-perfect (or at least identical
across runs) to be able to reliably test different BSA concoctions. I
have doubts as to whether these were properly controlled in ONT's BSA
tests:

* Read length (ideal is something like 5-20 kb, which gives a good
  balance of keeping the pores busy vs not jamming them up)
* Flow cell quality (field of green, rather than a field of
  wildflowers)
* Flow cell loading fraction (as little dark green as possible)
* Sample purity (or how much the DNA looks like unmodified,
  PCR-amplified DNA)
* Light

[I'm ignoring the things that can't be changed or observed, like the
sequencing kit chemistry, and the pore chemistry]

## 2025-Jan-10

### P2 Solo Data Transfer

If you're proposing feeding data from MinKNOW across the network to be
stored on a network drive... don't do that. Especially with the
PromethION flow cells, the data output is high enough that data
transfer rates can become limiting, and any interruption to the
network will cause problems with sequencing. Data from the run should
be stored locally (i.e. directly onto the computer attached to the P2
Solo), and can be transferred across the network once it is written
out to a complete pod5 file.

If you want to control and monitor MinKNOW from the remote PC, then
the MinKNOW settings need to be tweaked to allow remote connections. I
have some instructions on how to do that here:

https://gringer.gitlab.io/presentation-notes/2021/10/08/gpu-calling-in-minknow/#enabling-remote-access-optional

In any case, you need to get MinKNOW running properly locally before
it will work from a remote computer. A USB-C hub may not work well
(and won't work at all if the hub is not plugged into a high-speed USB
socket); the P2 has pretty high data transfer requirements, so needs a
good cable that can handle lots of data being pushed through it.

## 2025-Jan-08

### Skipped reads and CPU Calling

Skipped reads are almost always due to the computational demand of
base calling being too high. It's a big problem for sequencing if
*all* reads are skipped, because it needs some amount of feedback for
sequencing to work properly (e.g. for temperature / voltage
adjustments during sequencing).

There are configuration settings that can be changed to force it to do
CPU-only calling, but it's hard to know what the best approach is
without specific information about the computer installation and setup.

In any case, some of the troubleshooting commands here should help
work out what the specific problem is:

​https://gringer.gitlab.io/presentation-notes/2021/10/08/gpu-calling-in-minknow/#troubleshooting​​​

Skipped reads can be re-called after a sequencing run is finished. If
there are problems doing that within MinKNOW, it might be easier to
download dorado separately and run it outside of MinKNOW on the reads
from the sequencing runs.

## 2024-Dec-20

### Rapid Barcoding Protocol

[This rapid sequencing DNA -
barcoding](https://nanoporetech.com/document/rapid-sequencing-gdna-barcoding-sqk-rbk114)
protocol suggests 200ng for each barcode, but there is a subsequent
bead cleanup step, and... oh, wow... *all* of that is adapted then
loaded onto the flow cell!?

That's wild.

In any case, I've noticed across many years that ONT doesn't believe
that molarity or input overloading has an impact on the rapid kit,
which is why they don't recommend a molar amount to load on the flow
cell. As far as I can tell, their theory is something like this:

* DNA of any size is always fragmented down to the same size range by
  the fragmentation buffer

* Because the added adapter amount is the limiting factor of the kit,
  you can add as much input DNA as you want without reducing
  sequencing yield

My own experience suggests otherwise.

When I've run plasmid and ladder DNA on the flow cell, it tends to be
fragmented at a position that is fairly uniform across the length of
the sequence (excluding possibly the first and last 5-10 bases), and
often only cuts once. Most circular plasmid and mitochondrial
sequences are represented by a single read covering [almost] the
entire sequence that differs randomly in its cut site (which makes
consensus assembly wonderfully simple). When I do barcoded sequencing
of plasmids of different sizes (typically around 4-12 kb), I need to
adjust the load amount based on their size, otherwise the barcode
fraction in the sequenced reads is wonky (roughly in proportion to the
template DNA length).

I've also noticed that overloading DNA, especially if it's from a
high-molecular-weight source, tends to jam up the sequencer with the
rapid kit, requiring flow cell flushing and reloading to recover
pores.

My preference when doing multiple samples is to fragment them all at
the recommended concentration (i.e. 200 ng per sample, or 8.5 μl of 25
ng/μl), and then pool together based on the expected read length of
each sample. I add 1 μl of the sample that I need to add the least of,
then add other samples in proportion to their length relative to that
sample, then take 11μl of that pool for the next [adapter addition]
step. If there would be less than 11μl in the pool, then I multiply
the added amounts by a suitable factor.

Similar to ONT, I don't care too much about the actual amount loaded;
it's the proportions between samples that matter the most. Unlike ONT,
I try to avoid putting too much DNA into the flow cell. I also don't
do bead cleanup for amplicon or plasmid samples, because they're
usually already pretty clean when they're given to me.

If you're interested in that protocol in a more prescriptive and
verbose fashion, see here:

https://www.gringene.org/nanopore/protocols/Sample_Preparation_Rapid_Barcoding_current.pdf

## 2024-Dec-12

### Estimated Read Lengths

The estimated read length is based on the number of electrical samples
taken while the software thinks that an adapter molecule is engaged
with the pore. This can differ considerably from the basecalled
sequence depending on the local electrochemical changes in the
sequence, which slow down or speed up the transition of molecules
through the pore.

Because that estimated length is based on the software's guess at
adapter engagement / disengagement, it can occasionally get that
wrong, and declare a "read" to include many minutes of sensor
noise. I've noticed this to be a particular issue when the flow cells
are underloaded, with only a few actively-sequencing pores. In this
case estimated lengths can differ wildly from the actual sequence
length, and sometimes the sensor noise will get "basecalled" as well.

Also, I think the basecalled histograms by default show only passed
reads. There are options within the graph settings to change
this. Also, the called length plot obviously only shows called reads,
so won't include reads that are currently going through the pores (or
skipped reads due to adaptive sampling settings, or poor CPU/GPU
performance).

## 2024-Dec-09

### Washing Flow Cells

The problem with re-using flow cells is that many people think a flow
cell can be fully-recovered by washing, and try to use it again for
high-throughput sequencing. Flow cells function a bit like a
battery. They have a certain amount of sequencing capacity in them
(about 1-3 days, depending on the quality of the sample prep and flow
cell), and that capacity can't be recharged by washing.

The purpose of the wash kit is to clear DNA, RNA and other debris from
pores so that they can be rerun on other samples. The washing [only]
removes most of the old bound sequence, so that it interferes less
with additional library; washing can't recover dead pores.

To get the maximum yield from multiple samples, it's best to multiplex
and run them all on a fresh flow cell. Washed flow cells are best used
for lower-yield applications, e.g. plasmid sequencing, or length QC
prior to a full sequencing run.

Another common use for flow cell washing is for ultra-long DNA
sequencing: to clear pores that have been blocked by long DNA, after
which more of the same library is loaded. In that case carryover from
previous washes doesn't matter.

More generally, if you care about sample carryover / contamination
from a wash kit, there are probably cheaper ways to get better
results.

### RNA / cDNA Read Lengths

We don't do any length cutoffs for our cDNA sequencing beyond what
happens with an 0.8X Ampure XP bead concentration after PCR. I'm aware
that the bead cleanup will thin out short sequences, but won't get rid
of them entirely. [The image in this
post](http://enseqlopedia.com/2012/04/how-do-spri-beads-work/)
suggests that an 0.8X cleanup should still retain a reasonable
fraction of 100bp sequences (corresponding to fairly short sequences
once the polyA and adapter length is considered). With direct RNA
sequencing, I've found polyadenylated RNA sequences in the reads that
are too short to map (i.e. less than 15bp between polyA and the
sequence end).

For individual sequences (i.e. one cDNA template per read), I suspect
that the PacBio protocols will necessarily put a lower bound on the
length of template, because there's a minimum length for proper
circularisation of DNA ([about
150-200bp](https://biology.stackexchange.com/a/15675/70838)), and
ligation efficiency is erratic for non-integral helix repeat lengths
(so practically, the lower limit is closer to 500bp). Excluding short
reads would artificially increase the mean read lengths from a
sequencing run.

There is another
[MAS-Iso-Seq](https://doi.org/10.1038/s41587-023-01815-7)
concatenation protocol for PacBio (which I believe is similar to the
rolling circle to concatemeric consensus (R2C2) nanopore sequencing
method proposed by [Christopher Vollmers
group](https://doi.org/10.1016/j.jbc.2021.100784)), and that protocol
shouldn't have the same lower limit on cDNA length. As long as the
concatenated product can be circularised, the individual components
can be short.

On the other end of the scale, I'm aware that the amplification stage
of PCR-cDNA barcoding can exclude longer reads - they take longer to
replicate, so there's a higher chance that reads won't be
fully-extended by the time the extension cycle finishes. People have
reported on ONT forums that RNA and direct cDNA sequencing typically
produces longer maximum read lengths.

## 2024-Dec-07

### RNA barcoding

ONT doesn't have any barcoding options (or kits) for direct/native RNA
demultiplexing. The biggest issue is that the barcodes would typically
be encoded via DNA primers, and the basecalling is carried out using
an TheseRNA sequencing model; this leads to substantial calling
inaccuracy within the primer / adapter / barcode region.

I am aware of two research papers on demultiplexing options that have
been tested with the RNA004 kit:

1. SeqTagger (Eva Maria Novoa's group) - https://www.biorxiv.org/content/10.1101/2024.10.29.620808v1
2. WarpDemuX (Max von Kleist's group) - https://www.biorxiv.org/content/10.1101/2024.07.22.604276v2

Their approach to demultiplexing is slightly different. SeqTagger
segments the signal and runs a DNA basecaller. Notably, it claims to
work with non-polyA primers that are used in targeted sequencing
applications. Because SeqTagger uses dorado (with a custom-trained
model), it can be carried out on a GPU, allowing for fast calling and
demultiplexing.

WarpDemuX uses signal pre-processing and signal-level dynamic time
warping, and can be inserted into the sequencing process very early on
(with modified MinKNOW, while the barcode is being sequenced). This
allows barcode classification (and balancing / rejection) usually
before the sequencing has gone past the polyA region of the RNA. They
provide a suggested barcode set to improve their algorithm. Despite
not using a GPU, the authors claim that the code is fast enough for
real-time classification when using Cython.

Inventors of both methods have filed patents.

There is also an older method, DeePlexiCon, which (as far as I know)
has only been tested on the earlier RNA kits.

## 2024-Dec-04

### Channel Pore Counts

Multiple pore checks affect the flow cell slightly, but that's usually
only a problem for Flongle flow cells (which have smaller reservoirs
and more sensitive electronics). Pore scans shouldn't be a problem for
PromethION flow cells.

The "total number of pores" actually represents the number of physical
sequencing wells that are embedded with a single good working
pore. The "pores available for immediate sequencing" actually
represents the number of electrical channels that can be linked to at
least one (of four possible) good sequencing wells through the
multiplexing switch (MUX). ONT calls them both "pores" as a simplified
language to reduce the number of "what does this mean" questions.

I consider a read length mean of 10-50kb to be the approximate "safe"
range that gives the best performance for sequencing, in terms of
maximizing pore usage and minimizing pore blocking. With a 50kb mean,
the normal upper range of read lengths tends to be around 150-300kb.

Longer reads can be used together with a nuclease wash kit, which is
best used when the number of teal "unavailable" channels in the MUX
scans increases to about half of the useful (non-zero) channel
count. This happens a few hours before the sequencing yield curve
starts to obviously lose linearity.

## 2024-Dec-03

### Q Scores From The Basecaller

I only ever use the modeled Q score for relative filtering of nanopore
reads (i.e. fetching the highest-quality reads from a sample), or
looking at sequencing / basecalling trends over time. As long as it
has a good correlation with mapping rates (which it does), the
absolute value doesn't really matter.

I recommend that, where possible, you trust your own mapping results
in preference to the modeled Q scores. The Q scores are there as a
guide, and change around a bit depending on the basecalling
algorithm. I've found them best used for relative read classification
within a sequencing run, rather than for comparisons across runs
and/or basecalling methods.

Using mapping results against a known good reference should especially
be the approach used when comparing different technologies, where the
basecalling models are going to be wildly different.

### Mitochondrial DNA and Adaptive Sampling

Adaptive sampling can help a little bit for mitochondrial DNA
sequencing - it's in the right target range of effectiveness - but for
mitochondrial DNA multiplexing you'd be better off doing mitochondrial
/ plasmid isolation first, or long-range PCR. The cost equations don't
work out well for adaptive sampling on a MinION flow cell, whereas
adaptive sampling on a PromethION flow cell is computationally
intensive due to the sheer amount of data produced.

Mitochondrial DNA is normally around 100 times the coverage of genomic
DNA, so a 20X human gDNA PromethION run could, without specific
selection (or adaptive sampling), be used for up to about 50-100
samples (depending on how good the barcode balancing is). With
adaptive sampling this could probably be brought up to 100 (or 96)
samples on one flow cell even with fairly poor barcode
balancing. Divide those numbers by 10 for a MinION flow cell.

With mitochondrial / plasmid isolation or long-range PCR, the majority
of reads would be mitochondrial, meaning that the same 20X-ish
coverage per sample for 100 samples could be achieved comfortably on a
Flongle flow cell... about six times over.

Mitochondrial / plasmid isolation or long-range PCR cost would need to
be greater than about $8 USD per sample in order for it to be cheaper
to use a PromethION flow cell with adaptive sampling. That same
threshold is about $60 USD per sample on a MinION flow cell. And
that's ignoring the additional cost of a PromethION flow cell in terms
of run time, basecalling time, read filtering, data storage, and
unintended / unwanted clinical discoveries.

## 2024-Dec-01

### Trying It Out Yourself

There are frequent questions on the nanopore community forums about
people wanting to do very specific things with the latest unpublished
technology, and asking for advice prior to trying anything out.

When I'm helping people, I like to know that they've put some prior effort into finding the answer out themselves before asking. Here are some questions around that:

* What is the context around the problem that you have?
* What have you already tried?
* Why didn't it work / how do you know it didn't work?
* Do you have any results to demonstrate the issues?

## 2024-Nov-27

### Kit Adapters

The adapter reagent in the sequencing kits is the limiting reagent,
and should provide sufficient adapter (with waste) for six runs;
you're paying for six runs worth of sequencing from each kit.

For the Native Barcoding kit with 40μl NA and 5μl needed per run, it
seems like it should be enough for at least six runs from each kit.

If you want more runs from a kit (but lower yield per run), you could
try switching to the rapid barcoding kit, and saving diluted adapter
for future use. ONT's official protocol wastes 80% of the adapter, but
the kit still has enough for six runs when this adapter is wasted.

I suggest saving diluted rapid adapter in [my own protocol](https://www.gringene.org/nanopore/protocols/Sample_Preparation_Rapid_Barcoding_current.pdf).

### Sequencing Run QC

Check the pore occupancy of your sequencing run about 30 minutes into
the run, by looking at (light green + adapter) / (light green +
adapter + dark green). It should be above 85% for a well-prepared
ligation prep, and above 65% for a well-prepared rapid prep. If pore
occupancy is below this (e.g. 50-60%), then doubling the loaded sample
concentration should increase the flow cell yield substantially (my
guess would be >50% more reads) because flow cells sequence best and
longest when they're occupied as much as possible.

Unavailable pores taking up more than 50% of the useful pores
(available + reserved + unavailable) after 24 hours of sequencing
suggests that reversibly-blocked pores are affecting sequencing
yield. Assuming there is enough library left over, I'd recommend a
flow cell wash and reload (using the wash kit) after 24 hours (or
whenever blocked pore fractions rise above 50%), which should recover
a substantial portion of these pores. If there wasn't enough library,
increasing the number of PCR amplification cycles for the next library
preps (e.g. 2 additional cycles) would be a good idea.

Also, consider the pore scans at the start of the run - not just the
raw QC count statistic, but also the statistics from each
category. Large numbers of saturated pores (e.g. 10% of pores) can
suggest flow cell issues that lead to poor flow cell performance
throughout the sequencing run. It would be a good idea to discuss this
with ONT (e.g. via <support@nanoporetech.com>). However, if the QC
pore counts are above the level that ONT considers acceptable (5,000
for PromethION, 800 for MinION, 50 for Flongle), I wouldn't expect too
much interest from them.

## 2024-Nov-25

### Barcode pool normalisation

It's difficult to optimise sequencing with gDNA from different plant
species; they will have different unexpected crud hanging around with
the DNA, leading to different effects on DNA sequencing depending on
the species. In this context, it's not surprising to see a large
dynamic range in yield.

It's important to keep molecule counts for the different barcodes
similar, and that's difficult to do with multiple species on the same
sequencing run - relative molarity is more important than absolute
molarity / concentration.

To deal with this en-mass, I think the best approach would be to get
concentrations as close as possible, do a pilot sequencing run with
lots of samples to establish relative yields, then do subsequent
subset sequencing runs that sequence groups of samples with similar
yields.

Another alternative approach is to carry out serial sequencing runs as
normal, and resequence low-yield samples on the next sequencing
runs. This approach will lead to over-representation of high-yield
species, which would be substantially less cost-effective for
sequencing many different samples.

Fluorimeter-based assessment of sample concentration is helpful for
dealing with large-scale concentration differences, but pilot
sequencing runs (following an attempt at molarity normalisation) are
the best way to get an even barcode spread. Despite their cost and
additional complexity, pilot sequencing runs usually end up saving
money because there's less wasted sequencing on high-molarity samples.

## 2024-Nov-19

### RNA yield

RNA basecalling is slower than DNA basecalling (130 bases per second
instead of 400 bases per second). For that reason, and also because of
the quality issues when basecalling RNA, and also because of the high
input requirements, and also because of the lack of multiplexing
options, and also because of the different flow cell required, you
should only use RNA sequencing when you care about sequence
modification. In other cases, it's better to use a cDNA method - I
prefer the barcoded PCR-cDNA kits.

### Plasmid sequencing

Adding ligation adapters to transposase-fragmented sequences is
unlikely to work properly, and would be more expensive than other
alternatives. The rapid barcoding kit incorporates a rapid adapter
bonding point that I expect would interfere with the ligation
sequencing sample preparation.

There is already a 96-barcode option for the rapid barcoding kit:

https://store.nanoporetech.com/productDetail/?id=rapid-barcoding-sequencing-kit-96-v14

Rapid barcoding produces a lot of single-cut fragments, and the cut
point is random, which helps a lot for assembly and variant detection.

I don't personally see much benefit in scaling plasmid sequencing to
hundreds of samples per run. From plasmids on Flongle flow cells, the
largest number of samples I'd be confident in sequencing would be 24,
and even then I'd expect a few dropouts unless I had good prior
knowledge about sample performance.

12 Flongle flow cells * 24 samples per run with RBK works out to be
$810 + $700 USD + shipping + consumables, or about $6 per sample, and
that's before saving rapid adapter (which could reduce that cost to
~$3.50.

At that price I don't think the extra hassle of the PCR workflow is
worth it.

For completeness... people who talk about the cheapest Nanopore
sequencing (less than a dollar per sample; down to a few cents per
sample at massive scale with pipetting robots) use custom barcoded
amplicons together with the ligation sequencing kit. Here's an
example:

https://doi.org/10.1111/cobi.14162

ONT has support for non-rapid PCR barcoding coupled with native
barcoding (i.e. combinatorial barcoding up to 96*96 samples), but it
seems to be a more fiddly process than ordering modified primers on
PCR plates:

https://nanoporetech.com/document/ligation-sequencing-dual-barcoding-v14

## 2024-Nov-15

### Plasmid sequencing

Rapid barcoding has worked fine for me for plasmid lengths down to 2kb
(I haven't worked with anything shorter than that), and DNA ladder
lengths down to 100bp. Where the official ONT protocol mentions "high
molecular weight plasmid DNA", I suspect they mean "unfragmented or
low-fragmented DNA".

When I sequence from plasmid DNA, I prefer it prepared at 25 ng/μl;
that gives consistently good results when run through the rapid
barcoding workflow, even for bad Flongle runs. It also means that no
further concentration is needed after barcode fragmentation.

See the protocol I use here:

https://www.gringene.org/nanopore/protocols/Sample_Preparation_Rapid_Barcoding_current.pdf

## 2024-Nov-13

### Taxonomic classification

As a first recommendation, I suggest using the rapid PCR barcoding kit
($700 USD) with Flongle flow cells ($810 for 12), because it will give
you better taxonomic resolution (higher specificity / lower rate of
misclassification) compared to a 16S kit (or custom amplicons):

https://store.nanoporetech.com/productDetail/?id=rapid-pcr-barcoding-kit-24-v14

Even if you're going with ONT 16S kits (which are only available in a
24-sample barcoding format for $900 USD), you should still get
sufficient data out of 24 samples run on Flongle flow cells. It will
also have a substantially lower consumable cost compared to MinION
flow cells (consider that Flongle is $810 USD for 12 flow cells,
whereas MinION is $700 USD for one flow cell).

Assuming 200 Mb of sequencing yield and 1.5kb amplicon length, that
will give you about 5000 reads per sample, which my ten times rule of
thumb would suggest is enough for taxonomic classification down to
about 0.5% abundance. For additional coverage, you could run that same
24-sample library on up to 3 Flongle flow cells and still end up
cheaper than running 96 samples on a MinION flow cell.

Comparing the 24/96-native barcode ligation sequencing kit and custom
(user-created) 16S amplicons... The 96-barcode kit is $100 more
expensive ($800 USD vs $700 USD), and claims to have sufficient
reagents for 3 runs of 96 barcodes, or 12 runs of 24 barcodes (versus
the 24-barcode kit's 6 runs of 24 barcodes). Since you've got 200
samples to sequence, which is greater than 24 samples * 6 runs, it
would be cheaper for you to use the 96-barcode kit even if you're
running 24 samples per run.

The native-barcoding multiplexing kits are otherwise essentially
identical; they have the same reagents, both use plates for the
barcode adapters, the barcode sequences are identical, and the
protocols are basically the same - with smaller volumes used for the
96-barcoding kit.

For completeness, for MinION flow cells I assume 5 Gb of sequencing
yield for experimental design purposes. With 1.5kb amplicon length,
that will give about 30,000 reads per sample, which my ten times rule
of thumb would suggest is enough for taxonomic classification down to
about 0.03% abundance. That's about double the yield of what you'd get
on Flongle flow cells for a similar cost.

Whether or not that difference in yield would be a useful difference
is going to depend on your specific application - my own opinion is
that proportional classification lower than 1% (beyond
presence/absence) is not particularly useful for longitudinal studies,
so I expect I would get equal value out of a single Flongle flow cell
running 24 samples versus a MinION flow cell running 96 samples.

## 2024-Oct-30

### Hydroelectric model of nanopore sequencing

Variation in the relationship between signal length and read length of
nanopore reads is expected. The nanopores measure electrical current
(i.e. the movement of carrier ions), which is not a direct
representation of DNA itself, so there will be bursts and stutters
that are both sequence dependent and not entirely predictable.

My favourite model to demonstrate current flow in a nanopore
sequencing device is a hydroelectric dam that is designed to deliver a
fairly regular mean outflow current.

<img src="/pics/Outer_Wilds_DLC_Dam_Symbalily_Nanopore_small.png"
alt="Image of a hydroelectric dam from Outer Wilds, as experienced by
Symbalily [from YouTube]. The image has been annotated to represent
the nanopore sequencing model, with input side [reservoir], output
side [downstream], and nanopore [outflow pipe]." title="Image of a
hydroelectric dam from Outer Wilds" width="512"/>

The current of the water has some local changes around a fairly
regular mean outflow current which are dependent on the large things
that are passing through together with the water (e.g. weed,
branches), but also chaotic / unpredictable changes that dependent on
the internal eddy currents that happen with all fluid-like things.

## 2024-Oct-24

### Fragment QC from TapeStation

You can't work out N50 from TapeStation results, because it's not good
at quantifying short reads.

Nanopore sequencing depends on molecular concentration. The ideal
statistic for nanopore sequencing would be mean fragment length,
because that is the closest to what is useful for calculating molarity
(in combination with a DNA concentration measure) - this is different
from N50.

I'm not familiar with TapeStation statistics beyond the visual plots,
but if you can get a mean fragment length (rather than peak fragment
length), that would be better - bearing in mind the issues with short
read quantification.

If the mean fragment length is not available, but you can get at the
raw data output by the device, it would be possible to calculate
this. Assuming data is available as <length>,<intensity> pairs, and
that intensity is linearly correlated with the total number of bases,
then the non-normalised proportion of sequences at each point is
<intensity> / <length>. So the mean fragment length would be
calculated as:

sum(<intensity>) / sum(<intensity> / <length>) [excluding the high and low control peaks]

i.e.

<total number of bases> / <total number of reads>

## 2024-Oct-23

### Error Profiles

Illumina and Nanopore reads have very different error profiles.

Illumina trimming / error filtering is mostly about getting rid of
low-quality bases near the end of the read, whereas Nanopore reads
tend to be bad or good across the full length of the read.

It's unlikely that end-trimming based on quality scores will make any
difference to mapping or assembly. At best you might improve things by
getting rid of adapters because, confusingly, adapter sequences have
low-quality signal (despite being the most observed sequences in
nanopore sequencing). A more reliable way to get rid of adapter
sequences is to trim 65 base pairs from both ends of a read.

Also, Nanopore reads errors can be affected by non-standard base
modifications. These modifications are not detectable at all in
Illumina reads, assuming the clonal amplification is possible.

There is some evidence that some secondary structures formed from DNA
sequences can reduce the efficiency of amplification
(e.g. G-quadruplexes), which could lead to unexpected biases in
Illumina reads (including not being able to sequence through the
structures at all). Secondary structure formation can also cause
issues in nanopore reads, but it should be a detectable issue
(e.g. sequencing up until the point of disruption is carred out, but
is halted by a blocked pore, or leads to a substantial shift in the
electrical signal).

## 2024-Oct-18

### Sequencing Well Size

The sequencing wells on a MinION flow cell are physically 100μm in
width, which is about 300kb of a linear DNA molecule. If I assume that
the pore is in the centre of a well, then longer than about 150kb
means the DNA has to knot up within the well before it can go through
the pore (increasing the chance of blockage). That might explain why
N50s of >100kb are so difficult to get.

I think PromethION wells are smaller - they're at least more densely
packed - so the longest practical sequence length might be shorter
than that.

Maximum mapped / split-sequenced read length from previous runs is
about 4 Mb, but they're vanishingly rare.

https://twitter.com/DanTurnez/status/1301448798158364672


### Flow Cell Washing

It's not strictly necessary to wash flow cells when reloading
libraries, but it's a good idea. In most cases where reloading is
carried out, the wash kit is likely to also be useful (either to
unblock pores, or to get rid of DNA from old runs).

## 2024-Oct-15

### Rapid vs ligation kits

I've noticed that the rapid kits are more prone to clogging than the
ligation kits, presumably because there's less cleanup.

There may be some issues with reads <10kb if you've got
highly-repetitive genomes or you're sequencing heterozygous
populations rather than individual organisms (e.g. a tube full of
worms), but it should otherwise be okay.  In highly-repetitive genomes
long tandem repeat arrays won't be properly covered, which means that
assembled contigs won't represent complete chromosomes.

Bear in mind that molarity matters for short reads; at a guess I'd say
load at 50-100 fmol... however, using the recommended 200ng per sample
should get to about the right loading amount with reads with an
average length around 5-10kb. Loading too much (or more correctly,
adding too much DNA into the adapter binding reaction) will mean that
there's a lot of non-adapted DNA floating around to compete with the
other adapted DNA, which could lead to reduced yield.

FWIW, adding both too much and too little DNA will deplete the flow
cell quicker than expected; the main difference from QC plots is that
an overloaded flow cell will have more turquoise in the MUX scan
plots, and can be mostly recovered with a flow cell wash.

### Rapid Barcoding vs Ligation Sequencing

There's no substantial difference between rapid and ligation kits in
regards to basecalling quality. Once the helicase gets through the
adapter region of the sequence, the DNA looks identical in both cases.

I've only done a few ligation runs on PromethION, and those were
experimental / test runs, so it's not a great representation of actual
yield. Regardless, here are stats from our first P2 Solo run after a
software update that dealt with our geometric flow cell issues (using
LSK114). This was for the ONT Lambda DNA sample:

* Run time: 73 hours (as two runs of 21 hours + 52 hours)
* Yield (bases): 120 Gb
* Yield (reads): 69 M
* Duplex rate (pairs): 7.27%
* Duplex rate (split reads): 1.72%
* File storage needed: about 1.8 TB

There was one ligation run from a population sample involving
highly-repetitive DNA (genome size ~600Mb) which reached 50 Gb with no
reloading / washing. I wasn't expecting too much from that sample, as
it was a pain for us to sequence in the past.

When I've asked around in the past, people have been getting 50-150 Gb
yield from PromethION runs with the ligation kit, and our experience
has been similar when we've been running the flow cells in the
expected way (and not, for example, with Cas9-cut DNA).

With the rapid adapters, I mostly sequenced cDNA, and we were getting
about 60-90 Gb. Unfortunately I can't be more specific about that
because I no longer have any data beyond what was kept in my public
Mastodon posts (which get auto-deleted after a couple of months if
they haven't been boosted by anyone else).

In general, I'd expect rapid run yield to be about 80% of that of a
ligation sequencing run. I suspect yield is lower because non-adapted
sequence goes through the pores and uselessly uses up sequencing
capacity.

## 2024-Oct-09

### Plasmid sequencing

Because plasmid sequencing has a low yield requirement, it's quite
rare that I'll have a Flongle plasmid run which doesn't work at all
for plasmid assembly, even with fairly large differences between
samples. As mentioned in [my
protocol](https://www.protocols.io/view/plasmid-sequence-analysis-from-long-%20%3E%20reads-36wgq4n5yvk5),
"I don't do any additional cleanup [prior to adapter addition] for
purified plasmid DNA; they tend to sequence very well on flow cells
without that cleanup."

For normalisation, start with samples that are either equal
concentration or equal length, and then adjust volumes accordingly to
make the molarities equal.

The transposase doesn't care much about sample concentration. It's
added in extreme excess in order to get fragment lengths from long DNA
into the right range; the adapter is the limiting factor for rapid
barcoding. For plasmids under 20kb, most plasmids will be fragmented
only once by the transposase, if at all.

In terms of practicality, it's easier to normalise molarity prior to
starting with the nanopore sample prep. Then the post-fragmentation
pooling step is simply to add all the samples into a single tube, and
take 11μl from that tube into the adapter reaction.

However, normalising molarity needs to be done per-run. What seems to
be easiest for the people I do plasmid sequencing for is for them to
give me samples that are diluted down to 20 ng/μl [that has been my
rapid barcoding sweet spot], and for them to also tell me the plasmid
lengths. I fragment at that concentration, then use the lengths to
work out the amounts to add into the pool: add 1μl of the shortest
plasmid, and up to 10μl of the other plasmids.

If I calculate that I'll need to add more than 10μl of a sample to
properly normalise (e.g. mixing a 2kb plasmid with a 30kb plasmid),
then I'll instead split into multiple runs to make sure that all
plasmid lengths within a single run are within that single log10
dynamic range.

For sequencing yield, it's the relative lengths within a run that
matter. Sequencing runs best when all the sequenced DNA is
approximately the same length. Because the transposase is added in
extreme excess, there's a lot of forgiveness around sample
concentration before it starts affecting the overall yield of a
sequencing run.

### cDNA sequencing

The 10 minute extension time for cDNA PCR amplification was tested a
few times with no obvious downsides. I'm not fully convinced that it
needs to be that long for cDNA (especially for quantification), but it
doesn't seem to be hurting things. For what it's worth, the MARC river
water project team settled on a 6 minute extension time for 1-3kb
fragments from the rapid PCR barcoding kit (based on a bit of
testing), so that might be a good starting point.

The newer SQK-PCB114.24 kit has essentially an identical formulation
to the SQK-PCB111.24 kit when using V14 rapid adapters, so I wouldn't
expect any differences. We didn't test that kit because we still had
reagents from the old kit to use up.

I don't pay much attention to RIN, preferring to look at the band
plot. If there's a reasonable spread of lengths with some RNA longer
than 1kb then it should do well.

I have noticed an effect with fresh vs frozen extractions. Fresh is
best: try to minimise the time between RNA extraction and conversion
to cDNA, and take care with pipetting when working with the RNA. If I
were extracting RNA from samples myself (which I never do...), I would
go straight from RNA to reverse transcription and strand switching,
then hold the resultant cDNA at 4°C until it's ready for sequencing.

## 2024-Sep-26

### 16S / barcoding issues

The 16S gene is a poor fit for taxonomic classification; you'll get
better results (i.e. more specific results) by using a shotgun
metagenomic approach (e.g. rapid PCR barcoding):

https://store.nanoporetech.com/productDetail/?id=rapid-pcr-barcoding-kit-24-v14

16S often only resolves down to the family level (or worse), and
people often want species or strain-level resolution.

Given that there now exist comprehensive whole-genome metagenomic
databases, and the sample prep of the Nanopore rapid PCR barcoding kit
is nearly identical to that of the 16S kit, there's not much reason
for doing 16S beyond small samples. In other words, 16S may have some
use where high sensitivity is needed (e.g. low-quality DNA or small
samples).

In addition, the rapid PCR barcoding kit can be used to assemble full
chromosomes for abundant samples, which is helpful for identifying
novel species / variants, and resistance gene plasmids.

The 16S barcoding kit offered by ONT (SQK-16S114.24) is a full-length
16S amplicon kit that uses PCR followed by rapid adapter attachment,
but other PCR expansion kits are not compatible with rapid attachment
chemistry. If you're using PCR followed by rapid barcoding, then using
the PCR settings in MinKNOW, the software is going to get *really*
confused.

### DNA Quantification

It has been my experience that DNA-bound fluorescence-based methods
like qubit or quantus give the best representation of DNA
concentration for what matters for nanopore sequencing. But if a
nanodrop is all you've got, then use that. Bear in mind that because
sequencing costs are so high, it may be a false economy to use a
nanodrop because its cheaper.

In other words:

pilot sequencing run on a nanopore device

is better than [has a closer quantitative representation than]

quantus / qubit

is better than

nanodrop

Somewhere in the middle there is an electrophoresis gel, which gives a
good representation of concentration for long reads, but doesn't work
so well for short contaminating reads.

## 2024-Sep-25

### Rubbish reads

Sensor noise and other rubbish moving through the pores occasionally
gets "detected" as a read and sequenced. This most often happens with
low-yield runs with fewer than 100k reads.

When I'm a bit curious about these reads, I will plot the signal trace
of a few of them, and it's almost always a noisy flat line. More
often, I'll run a few of the non-repetitive-looking fragments through
NCBI BLAST, find out that they don't map to anything, and leave it at
that.

The signal traces are stored in the POD5 files, indexed by the unique
read ID that is output in the FASTQ files. There's a bit of
information in how to write python code to display the signals in the
pod5-file-format documentation:

https://github.com/nanoporetech/pod5-file-format/blob/master/python/pod5/README.md#plotting-signal-data-example

Here's some code I wrote a few months ago for displaying the signal
from a pod5 file containing a single read:

```
#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pod5 as p5

with p5.Reader("1ab27819.pod5") as reader:
    for read_record in reader.reads():
        # Get the signal data and sample rate
        sample_rate = read_record.run_info.sample_rate
        signal = read_record.signal

        print("ID: %s (%d samples)" % (read_record.read_id, len(signal)))

        # Compute the time steps over the sampling period
        time = np.arange(len(signal)) / sample_rate

       # Split signal and time into multiple parts
        split_count = 5
        split_idx = len(signal) // split_count
        signal_parts = [signal[i * split_idx : (i + 1) * split_idx] for i in range(split_count)]
        time_parts = [time[i * split_idx : (i + 1) * split_idx] for i in range(split_count)]

        # Adjust the last segment to include any remaining data
        if len(signal) % split_count != 0:
            signal_parts[-1] = signal[(split_count - 1) * split_idx :]
            time_parts[-1] = time[(split_count - 1) * split_idx :]

        # Create subplots
        fig, axs = plt.subplots(split_count, 1, figsize=(20, 15))

        # Plot each part
        for i in range(split_count):
            axs[i].plot(time_parts[i], signal_parts[i], linewidth=0.25)
            axs[i].set_xlabel('Time (s)')
            axs[i].set_ylabel('Signal')
            axs[i].set_ylim(0, 1000)

        # Adjust layout
        plt.tight_layout()
        # Plot using matplotlib
        #plt.plot(time, signal)
        figure = plt.gcf() # get current figure
        figure.set_size_inches(20, 15)
        # when saving, specify the DPI
        plt.savefig('signal_1ab27819.png', dpi=200)
```

If there is a small sample issue from a sample that is showing lots of
really long reads in the MinKNOW results, you might find benefit from
syringe shearing reads prior to sequencing. See the shearing portion
of John Tyson's protocol for more information:

https://www.protocols.io/view/modified-lsk109-ligation-prep-with-needle-shear-an-ewov18j9ogr2/v1

### Fastq-dental operation

For getting adapter sequences for fastq-dental, it's a little easier
to clone the entire repository, but downloading those files is fine.

The NBD barcodes have now been added from the ONT chemistry
document. It wasn't in there previously because I've never done native
barcoding, so didn't have any way to test it. For anyone having a go
with them, be warned that there may be some teething problems
(e.g. all reads failing because the orientation is wrong).

A fallback barcode file, `barcode_base.fa` is provided; it may work
when other full barcode sequences including adapters aren't
available. However, because it doesn't include adapters, there's a
greater chance that sample DNA sequences will be misidentified as
barcodes.

As with any result production, especially when testing things out for
the first time, it's a good idea to inspect the results to make sure
they make sense.

I usually don't do any trimming at all. Most of my work is based
around genome assembly and cDNA reads. For genome assembly, the
assemblers include adapter trimming steps for excluding common
sequences; for cDNA reads, the presence of barcode sequences doesn't
substantially impact the mappability of reads.

When I need to do trimming (e.g. when I'm trying to do a semi-manual
kmer assembly of DNA ladder sequences), I choose a trim length that
guarantees that the barcode and adapter will be clipped, and use my
other fastx tools for that (e.g. `fastx-fetch.pl -triml 65
reads.fastq` to trim 65bp off the start of a read).

When I designed my demultiplexing script, demultiplexing wasn't
available in guppy or dorado.

It has been designed with an emphasis on excluding chimeric reads that
contain multiple barcode adapters, weighing specificity (making sure
barcode sequences match the expected sequence, and are not chimeric)
over sensitivity (making sure all possible barcode sequences are
detected).

Chimeric read detection is something that ONT's demultiplexers still
have trouble with, because there are so many weird things that happen
with trying to join bits of DNA together.

More information about fastq-dental is included in the program
documentation, and in the readme file in the gitlab repository. For a
sane perl installation, you can get the program documentation with the
following command:

    perldoc fastq-dental.pl

The program was created to separate out reads into different bins
based on detected barcode sequences, and can handle forward and
reverse-oriented adapter sequences. However, by default it won't
exclude reads that don't have barcodes on both ends. When I did
experiments with this myself, it didn't substantially impact the
specificity of barcoding beyond what was already done from chimeric
read detection.

However, adapter/barcode location metadata is included in the
sequences after binning, and this can be used for subsequent filtering
if desired. There's also a way to specify adapter sequences as
unique/essential in the adapter file, which can be used to filter out
the reads that have barcodes on both ends [sort-of documented, but it
hasn't had much testing].

## 2024-Sep-11

### Rapid fragmentation molarity

16s and 18s full length amplicon sequences can be combined in a single
rapid barcoding run; make sure that the samples have the same
molarity. For example, assuming 16s length of 1.5kb, and 18s length of
2kb, you should have 2/1.5 = 1.33 times the concentration of 18s
amount per sample for a given 16s sample.

Amplicon and whole-genome samples can be combined in a single rapid
barcoding run; make sure that the samples have the same molarity, or
as close as you can estimate it. For example, assuming 15kb average
read length and 1.5kb amplicon length, you should have ten times the
concentration of extracted bacterial DNA as the concentration of
amplicon DNA.

As a specific example, say you have 16s samples prepared at 20 ng/μl,
and want to add in 18s and bacterial DNA samples (using the above
length estimates):

* 16s samples - prepare 9 μl at 20 ng/μl, add 1 μl barcoded fragmentation buffer
* 18s samples - prepare 9 μl at 27 ng/μl, add 1 μl barcoded fragmentation buffer
* bacterial DNA samples - prepare 9 μl at 200 ng/μl, add 1 μl barcoded fragmentation buffer

## 2024-Sep-04

### Rapid Fragmentation of short reads

I've done one experiment sequencing a DNA ladder with the rapid
barcoding kit, and I found the reads to span almost the full length of
the template sequence with a fairly uniform distribution, with minimal
bias.

https://genomic.social/@gringene/109809902539853553

You might be able to see that there's a gap of about 10-20 bases at
the start and end of the sequence where there's no fragmentation. I'm
not sure if this is truly unfragmented sequence, or it's just too
short to map. In any case, such short sequences aren't all that useful
to me.

Bear in mind that the average length will be half the target region
*plus* the adapter sequences, which are quite long for the barcoded
rapid adapters (90bp + trade secret sequences). So an average length
of 250-300bp starting from a 300bp amplicon wouldn't surprise me too
much, especially if it were an estimate.

## 2024-Aug-24

### cDNA Sequencing

I have been getting good results with >1M reads per sample on *MinION*
flow cells for differential expression analysis, which usually meant
running about 6-12 samples per flow cell.

For PromethION flow cells, this means that a cDNA barcoding kit
running 24 samples will also work well.

In the last cDNA run I worked on, we were running 20 samples on a
PromethION flow cells, and getting over 10M reads for most samples. I
was comparing this to an Illumina run we did with 30 samples, and
although the number of bases sequenced in each run was similar, we
actually got more detected genes on the PromethION run than the
Illumina run, despite having about 1/10th the number of reads.

With regards to alternative splicing, while I haven't done any
substantial amounts of quantitative analysis, I typically map to
transcripts rather than genes, and was able to do a targeted analysis
of a few genes (and their transcript isoforms) using the above scale
of data (i.e. over 1M reads per sample). We were also able to clearly
see alternative splicing in the mapped reads when looking at the
mapping with a genome browser.

## 2024-Aug-15

### Bulk File Output

At the bottom of the "Output Settings" dialog, there's an expandable
arrow for "Show advanced options", which will allow you to create a
bulk FAST5 file (note: not a POD5 file) that includes all the raw
signal from a selection of the channels on the flow cell, or the flow
cell.

However, bear in mind that you will not have enough disk space to
store all that data for the full flow cell for a full sequencing run,
and probably not enough disk throughput for storing that data for any
length of time.

Assuming 2,675 sequencing channels, 5kHz sampling speed, and 4 bytes
per sample...

* 51 megabytes per second
* 3 gigabytes per minute
* 180 gigabytes per hour
* 12.6 terabytes per 3-day run

For MinION, the numbers are a bit more reasonable, assuming 512
sequencing channels:

* 9 megabytes per second
* 586 megabytes per minute
* 34.3 gigabytes per hour
* 1.61 terabytes per 2-day run

For Flongle, also reasonable with 126 sequencing channels:

* 2.4 megabytes per second
* 144 megabytes per minute
* 8.45 gigabytes per hour
* 202 gigabytes per 1-day run

It's possible that the actual number of bytes per sample is higher,
maybe as high as 20 bytes per sample; it's been a while since I last
made a bulk file.

## 2024-Aug-13

### Flow cell loading amounts

Here are the ONT loading recommendations from the basic LSK114 protocol:

> [MinION]((https://community.nanoporetech.com/docs/prepare/library_prep_protocols/genomic-dna-by-ligation-sqk-lsk114/v/gde_9161_v114_revv_30jul2024/adapter-ligation-and-clean-up?devices=minion)) - Depending on your required data output, prepare your final library to 35-50 fmol for high output of simplex data, or 10-20 fmol for duplex data, in 12 µl of Elution Buffer (EB).
>
> [PromethION](https://community.nanoporetech.com/docs/prepare/library_prep_protocols/genomic-dna-by-ligation-sqk-lsk114/v/gde_9161_v114_revv_30jul2024/adapter-ligation-and-clean-up?devices=promethion) - Depending on your required data output, prepare your final library to 35-50 fmol for high output of simplex data, or 10-20 fmol for duplex data, in 32 µl of Elution Buffer (EB).
>  
> [Flongle](https://community.nanoporetech.com/docs/prepare/library_prep_protocols/genomic-dna-by-ligation-sqk-lsk114/v/gde_9161_v114_revv_30jul2024/adapter-ligation-and-clean-up?devices=flongle)- Prepare your final library to 5-10 fmol in 5 µl of Elution Buffer (EB).

In other words:

* MinION - 35-50 fmol in 12 μl EB
* PromethION - 35-50 fmol in 32 μl EB
* Flongle - 5-10 fmol in 5 μl EB

Although as
[/u/ButtlessBadger](https://www.reddit.com/r/nanopore/comments/1eq9hg6/amount_of_dna_that_need_to_be_inputted_for/lhqlo0b/)
states on reddit, loading 80-120 fmol may work better. The R10.4.1
flow cells are more difficult to overload, and have more problems when
they're being underloaded.

MinION and PromethION flow cells should be loaded with the same number
of molecules, but the amount is distributed through a larger volume on
PromethION flow cells.

This load amount should be identical for all ligation protocols. The
load amount is different for rapid-adapter protocols, but ONT don't
provide any specific numbers, and the protocols are inconsistent. I
expect it should be about double the load amount (i.e. 150-200 fmol
for MinION/PromethION, 20-40 fmol for Flongle), to account for free
sequences that don't have any bound adapter.

## 2024-Aug-07

### Rapid primer extension

*[See also [here](#2024-may-08)]*

There is a way to design custom primers involving (as a pre-PCR step)
attaching the rapid primers from the the "Rapid PCR Barcoding Kit /
cDNA-PCR Barcoding Kit" via primer extension.

There's no way to do it without using ONT kit components because the
rapid adapter bonding point is required, and that structure alone is
not disclosed by ONT.

The ordered primers need to bind directly to the ONT primers (either
from the PCR-cDNA barcoding kit, or the rapid PCR barcoding kit), and
extend those primers to generate the desired primer sequence. That
means they need to have (in order) the reverse-complement of the
target sequence, followed by the reverse-complement of the primer
anchor sequence:

    Ordered Forward primer:     5’ – <reverse complement of forward primer sequence> – GCAATATCAGCACCAACAGAAA – 3’
    Ordered Reverse primer:     5’ – <reverse complement of reverse primer sequence> – GAAGATAGAGCGACAGGCAAGT – 3’

[Adapter sequences from [here](https://community.nanoporetech.com/technical_documents/chemistry-technical-document/v/chtd_500_v1_revaq_07jul2016/adapter-sequences)]

After this extension step:

    Forward primer: <rapid bonding point> –  TTTCTGTTGGTGCTGATATTGC – forward primer sequence
    Reverse primer: <rapid bonding point> –  ACTTGCCTGTCGCTCTATCTTC – reverse primer sequence

... which is then fed into a standard PCR reaction.

We've tried this experimentally a couple of times, and it seemed to
work as intended in terms of generating the required extended primer
products.

## 2024-Jul-13

### Mitochondrial sequencing

Our best results for mitochondrial sequencing were from a long-range
PCR, but the rapid barcoding protocol would be a good thing to try
first because it's cheaper and quicker. Mitochondrial genomes are more
abundant than nuclear genomes, so will fall out in the results even
when sequencing at low coverage.

There's no need for adaptive sampling because the mitochondrial genome
is already present at increased proportions.

If you are going to use ligation sequencing, you'll need to linearise
the mitochondrial genome first - this is not necessary if using the
rapid barcoding kit, because the fragmentation also linearises the
DNA.

### cDNA Sequencing

Most optimisation around PCR sequencing is around optimising the PCR
itself.

If your yield is about 1/10th of what you want, then as a simple
initial fix try running the PCR for four more cycles.

A broad size distribution is typical for cDNA sequencing; we usually
get a similar range (i.e. 0.2kb - 10kb), averaging 0.6 - 1.5 kb per
read depending on how good the input RNA is. Fresh is best - better,
less degraded RNA will produce longer reads, more reads, and a more
productive sequencing run overall.

### Minimum computer requirements (MinION)

ONT supports newer macbooks with M1/M2 chips. In fact if you're
interested in doing real-time basecalling without an NVIDIA video
card, those chips are pretty much the only way to do it.

The typical yield of a MinION sequencing run is about 5-15 gigabases,
which in produced gzipped FASTQ files will take up to about 30 GB. For
a P2 Solo sequencing run, PromethION flow cells produce about 50-150
gigabases (i.e. gzipped FASTQ files up to about 300 GB).

However, if you are only able to do fast basecalling during
sequencing, or no basecalling at all (likely if you're talking about
minimum specifications), then you'll need to do the basecalling
offline, and the raw POD5 data format used by Nanopore sequencers
takes up about ten times the FASTQ storage space (i.e. 100-300 GB per
MinION run, or 1-3 TB per P2 Solo run).

It is possible to make do without this, and shunt data files off the
laptop elsewhere as needed, but the simplest, easiest, cheapest
approach is to get an SSD that will fit an entire run.

## 2024-Jul-12

### Optimising Yield

For well-prepared samples and moderate-length DNA (up to about 50kb),
the yield is fixed and dependent mostly on the flow cell. Adding less
sample than the ideal amount won't decrease that yield substantially,
and adding more sample than the ideal amount will probably decrease
yield.

In other words, add less of the samples you don't want to sequence as
much, and the sequences for the other samples will increase.

There is a caveat to this relating to molarity, long DNA, and occupied
pores. If the pores are mostly unoccupied, the yield of other samples
can be increased by spiking in a small amount of clean, long DNA
(i.e. the ONT lambda control sample), which will keep the pores
occupied while other long DNA is migrating towards the pores. In that
situation, the amount of lambda reads will increase as well as the
number of reads from other samples.

## 2024-Jun-17

### ONT Wariness

There are a few reasons I can point to for why people are wary of
Oxford Nanopore. Here are some well-known public ones:

* Early research papers reported poor accuracy, and people treat that as the current state of the technology
* They have a weird marketing model: they have their own conference, rely primarily on customer word-of-mouth, and prefer to sponsor individual researchers rather than conferences
* Sequencers are too cheap; cheap is treated as low-quality
* They're a moving target; continually innovating in substantial ways, which makes it difficult to pin down one consistent thing that they do well
* They're not Illumina
    - If a process has always been done with Illumina, why change?
    - If a software application has been designed for Illumina sequencing, it'll probably not work for ONT
    - Illumina has been around for longer, so they're more likely to be around in the future
* They have non-compete clauses for product use
* They release early; products begin their commercial life in [open beta](https://en.wikipedia.org/wiki/Software_release_life_cycle#Open_and_closed_beta)
* The CTO has strong opinions on how things should be done, and makes lots of bold claims

Within the ONT community, there are other issues:

* They have a not-invented-here attitude. Many times when an externally-developed program seems promising, the developers get hired in to ONT
* They tend to ignore community feedback until it becomes a big, obvious issue (and sometimes even after that)
* Their discussion forums are not public
* Their software licenses are not [OSF-approved](https://help.osf.io/article/148-licensing)
* Software is frequently broken in weird ways, and those breaks are hard to fix / workaround
* Software updates occasionally include substantial changes with little or no warning, and with little or no community testing
* Explicit archives of past software versions are not available; it's mostly a game of guess-the-URL
* Protocols are confusing: inconsistently verbose, with occasional obvious mistakes and copy/paste errors
* Protocols are not properly version-controlled; protocol updates are often released silently with no changelog
* They're trying to develop full workflows, mostly in-house (from sample prep through to analysis / reports)
* It's difficult to get help as a new user of the technology, which is a problem because many new users are stepping into self-managed sequencing for the first time

Despite all that, among high-throughput sequencers it's the cheapest
consumable cost per-run by a considerable margin (especially on the
Flongle platform). If ONT just concentrated on the sequencing part
(i.e. making good DNA/RNA/protein sequencers, with associated sample
preparation kits), I'd be a much happier user. I believe that the
other stuff they're trying to bolt on hurts their image; they could
move faster by embracing community support.

## 2024-Jun-11

### Experimental planning

My usual estimates for experimental planning and funding are the rough
minimum normal range of yield for nanopore flow cells:

* Flongle - 200 Mb / flow cell
* MinION - 5 Gb / flow cell
* PromethION - 50 Gb / flow cell

When I'm doing sequencing and/or variant analysis, I prefer to have
about ten times the minimum coverage needed to determine an
outcome. For just determining sequence or SNPs, the minimum is 1X, so
I prefer to plan for 10X coverage. For genotypes (in diploid
organisms), the minimum is 2X, so I prefer to plan for 20X coverage.

When working with multiple samples, there is a high dynamic range in
coverage that increases as the number of samples increases. I prefer
to make an allowance of 2-4 times extra to account for that dynamic
range, and re-sequence anything that falls under what I need.

So, as an example when looking for genotypes, I'd be planning for an
average coverage of 80X for the minimum normal range.

With 5kb amplicons and 96 samples, with the expectation that some
samples will probably need to be resequenced, that comes out as 40 Mb
per amplicon (calculated 38,400 kb per amplicon). So for the different
flow cell types:

* Flongle - 5 amplicons / flow cell
* MinION - 125 amplicons / flow cell
* PromethION - 1250 amplicons / flow cell

For additional cost savings, check on gels, TapeStation, or similar
before sequencing, and don't sequence anything that doesn't have a
detectable product.

## 2024-Jun-07

### Barcode mixing

If reads are seen that are longer than expected in multi-barcode runs
(or in single-sample runs, for that matter), have a look at the actual
read sequences to see if they include internal read adapters. I would
expect that the longer-than-expected reads are in-silico chimeric
reads, which typically happen in around 0.1% of the reads.

There are now options in the basecaller to split reads in most cases
when this happens, but they may not be set by default because it
requires a bit more processing effort.

Native barcodes and PCR barcodes are meant to be added at both ends
during sample prep, so '--barcode-both-ends' can be helpful to make
sure that only a barcode at both ends is considered valid.

There is a lot of weird stuff that happens during sample prep when you
dig deep into the reeds; a barcode failing to ligate on one of the two
ends is a relatively tame example.

### Shorter rapid fragmentation lengths

The transposomes with loaded adapter bonding sequences are single-use,
so incubation time changes won't change fragmentation lengths all that
much.

However, decreasing the relative DNA concentration should reduce
fragment sizes. As the transposase is usually added in neat
(i.e. undiluted), the only approach available to decrease relative DNA
concentration is to reduce the amount of DNA in solution. On the
opposite side of things, ONT provides an FRA dilution buffer in its
[ultra-long DNA Sequencing
Kit](https://store.nanoporetech.com/productDetail/?id=ultra-long-dna-sequencing-kit-v14),
which *increases* the relative DNA concentration by reducing the
transposase concentration.

Another option would be to use the [Rapid PCR Barcoding
Kit](https://store.nanoporetech.com/productDetail/?id=rapid-pcr-barcoding-kit-24-v14),
which has transposable concentrations that are already optimised for
shorter PCR-able fragments (typically under 5kb, but expected to
average around 2kb). You would need to follow it up with at least a
couple of rounds of PCR with the ONT primers, but the resultant
products should then be pretty close to your target range without too
much fuss.

### Flow cell loading volume

The flow cell matrix stores about 50μl (I'm not completely sure on
that; there's an ONT diagram floating around somewhere with details),
so there's already a bit of extra slop for the input channel of the
flow cell when loading with 75μl.

More than that can be loaded (especially if excess is removed from the
waste channel), but it's not usually a good idea to do that because
library will be lost. If you want to add more library, the best thing
to do is to increase the loaded DNA concentration and still add 75μl.

## 2024-May-25

### Storage of flow cells

After washing and using potassium ferrocyanide storage buffer (labelled 'S'), flow
cells should be stored at 8-16ºC for as short as possible, but as much as a few years.

There's no right answer for how long flow cells should be stored for, because it
depends on what the state of the flow cell is prior to storage, and the application
that the flow cell will be used for after storage.

Storage of flow cells at 4ºC for extended periods of time is a bad idea because
there's a high risk that the temperature will drop below 4ºC and damage the flow
cell. But assuming a non-cycling fridge and storage at 8ºC (or the mid upper
range of whatever it says on the package), they can be stored and kept for a
long time without substantially degrading the flow cell. It's not nothing, but
the degradation is low enough that other variation effects (e.g. sample
preparation, contamination) will play a bigger role in flow cell performance.

When sequencing a single amplicon, or [as filler for breaks in a presentation
during a talk](https://twitter.com/gringene_bio/status/1110315998270283776), all
of those things are unlikely to matter because the output of only a couple of
good working pores would be enough.

However, for a second run sequencing human gDNA for assembly, the flow cell was
probably useless for that application even before storing.

## 2024-May-08

### Reverse-complement PCR

Reverse-complement PCR is not the same as ONT's four-primer PCR protocol, but it's a similar process that is more efficient and less likely to generate useless PCR byproducts.

The ordered primers need to bind directly to the ONT primers (either from the PCR-cDNA barcoding kit, or the rapid PCR barcoding kit), and extend those primers to generate the desired primer sequence. That means they need to have (in order) the reverse-complement of the target sequence, followed by the reverse-complement of the primer anchor sequence:

    Ordered Forward primer:     5’ – <reverse complement of forward primer sequence> – GCAATATCAGCACCAACAGAAA – 3’
    Ordered Reverse primer:     5’ – <reverse complement of reverse primer sequence> – GAAGATAGAGCGACAGGCAAGT – 3’

[Adapter sequences from [here](https://community.nanoporetech.com/technical_documents/chemistry-technical-document/v/chtd_500_v1_revaq_07jul2016/adapter-sequences)]

After a first cycle extension:

    Forward primer: <rapid bonding point> –  TTTCTGTTGGTGCTGATATTGC – forward primer sequence
    Reverse primer: <rapid bonding point> –  ACTTGCCTGTCGCTCTATCTTC – reverse primer sequence

... which then feed into a standard PCR reaction. If desired, that first extension step can be done without any sample (i.e. as a two-step PCR), which allows for additional PCR optimisation.

We've tried this experimentally a couple of times, and it seemed to work as intended in terms of generating the extended primer products.

FWIW, our entire experiment was unsuccessful because were were attempting a linear amplification with these primers (and therefore had only a small number of our desired amplified products), but the reverse-complement PCR primer generation part worked, with *lots* of validation of that working in the sequenced reads.

## 2024-Apr-29

### Capital Costs

Title ownership options for Nanopore Sequencing are available. It approximately
doubles the initial purchase, and doesn't include any flow cells (which more
than offset the initial purchase). Title ownership is basically offered as a
purchase for people who care more about show than money.

If you want to compare like-for-like in terms of bases output for PacBio Revio,
then the P2 Solo is the one (i.e. entry-level PromethION sequencer). The CapEx
cost for that is $23k USD; less than 1/20th the cost of a Revio.

### Compute Costs

P2i, P24, and P48 have included compute; it makes up the majority of the cost.

P2 Solo doesn't include compute, but the compute demands are fairly low. A
high-end NVIDIA video card is sufficient to cover the sequencer for 1-4 runs per
month.

Storage is indeed expensive, but again if you want to compare like-for-like,
then Nanopore's excess needs are a 4-8 TB SSD for temporary storage until
basecalling is done and the raw signal data is discarded (depending on whether
you want one or two flow cells). Beyond that, the storage costs of the two
platforms will be similar.

### Base Calling

A PCR-amplified (or cDNA-converted) product sequenced on Nanopore will work just
as well from a known species as an unknown species.

Calling models still work well on DNA from unknown species as well, because DNA
is DNA. Due to unexpected sequence modifications, native DNA from "unknown"
species can sometimes call more poorly than well-known species... but that's
because it has unexpected sequence modifications, which PacBio can't detect at
all. If that's a concern, then don't feed native DNA to an ONT sequencer.

No other platform can observe methylation directly. Nanopore is unique among the
established sequencing platforms because it's a model-free chemistry;
discovering new chemical modifications is a software problem.

For all other platforms, the DNA/RNA needs to be modified and converted into
standard A/C/G/T DNA in order to be sequenced, which means that all the
non-standard modifications (e.g. methylation, abasic sites) are lost before the
sequences even get to the sequencer.

PacBio can recover a small amount of information about modifications from
looking at polymerase dwell time, but I can't really see how that could be
generalisable to detecting multiple modifications at the same time.

### Democratizing Sequencing

Of the available high-throughput sequencers, ONT has the lowest minimum run
cost, from a rapid sample run on a Flongle flow cell. The cost is low enough
that it ends up being cheaper than Sanger sequencing when using rapid barcoding
to run more than 4 amplicons in both forward and reverse orientation. Bearing
that in mind, the "democratizing sequencing" potential at the low end is quite
substantial.

At the slightly higher-end range of sequencing, the aforementioned P2 Solo uses
exactly the same flow cells as ONT's highest-end sequencers, with the same
output per flow cell. It's probably not going to be a useful solution for
farmers in Africa, but it's cheap enough to allow moderate-sized labs with 1-4
sequencing runs per month to get into multi-sample cDNA and single cell
sequencing.

### Cost Changes in the Future

ONT's system, kits, and flow cell prices have generally either stayed the same
or dropped, despite inflation and an increase in market share (with the
exception being kits which had more included barcodes and/or reagents than the
previous versions). Looking at the claimed value on commercial invoices for flow
cell returns, I'd say that there's still a fair amount of room for cost
increases before ONT needs to look at price increases. The situation's probably
similar with other companies as well; I'd say the high-throughput sequencing
prices are already artificially high due to Illumina's effective monopoly, and
competition from the pesky long-read upstarts is more likely to drive prices
down than up.

## 2024-Apr-21

### Pore clogging with R10.4.1

Pore clogging issues are not unexpected.

As Warren Bach explained to me a few weeks ago (and as various other ONT staff
have explained in their presentations), the double reader head in the R10 pore
means that there are double the number of pinch points in the pore, each which
has a chance to clog.

Bearing this in mind, it makes sense that R10 performs best if the sequencing
flow rate is as constant as possible, because any stops and starts are going to
increase the likelihood of blockages - consider how dense traffic will often get
jammed at the places where there are changes in speed.

As with all nanopore sequencing, you should get the best performance out of an
R10 flow cell if it's loaded to saturation with medium-length sequences (e.g.
1-20kb), but not too overloaded that the sequences are going to compete for pore
access. I guess R10 is more sensitive (i.e. has a narrower band of optimal
molarity) because of the blockage issues.

I could imagine in the future ONT adding a nanoscopic overlay just above the
pores which encourages laminar flow of DNA sequences through to the pores, and
is designed in such away that the clogging risk is minimal (e.g. holes with
chamfered edges).

## 2024-Apr-10

### Flow Cell Loading Protocols

Flow cell loading protocols should really be separated from library preparation
protocols. The flow cell loading protocols are identical for all types of DNA
libraries (and identical for all types of RNA libraries).

If a Flongle or PromethION protocol isn't available for a specific library
preparation protocol, you can take the 12 µl of prepared library and treat it as
prepared library for the basic LSK114 protocol. This is the protocol that ONT
puts the most care and attention into, so check here first if something in the
other protocols doesn't make sense:

https://community.nanoporetech.com/docs/prepare/library_prep_protocols/genomic-dna-by-ligation-sqk-lsk114/v/gde_9161_v114_revu_29jun2022

If that link doesn't work (e.g. ONT begins using proper versioning, and stops
using URLs with embedded dates).... Flongle libraries use a 5µl library added to
25µl of sequencing solution (30µl total). PromethION libraries use an
equal-sized library diluted to 32 µl in elution buffer (EB) and added to 168 µl
sequencing solution (200 µl total).

There's one caveat: the software does perform differently for ligation adapters
and rapid adapters, so you need to make sure you at least choose a ligation or
rapid kit respectively when sequencing in order for sequencing and read
segmentation to be performed properly. Most other kit-related things (e.g.
getting the barcode kit wrong) can be fixed up by re-calling reads after the run
is finished.

## 2024-Apr-08

### Experimental design

It has been my consistent position that if you design Flongle
experiments to produce decent results with a yield of 200 Mb per run
(or write funding applications expecting that yield), you'll almost
always be happy with the results regardless of how bad the flow cells
perform. I'm not saying they'll always produce 200 Mb; that is
generally the *minimum* output of our Flongle runs (with the one shown
here being a low-yield exception).

More generally, if you design experiments for the minimum normal
range, then most of the issues about yield disappear: it doesn't
matter if you're using rapid adapters or ligation adapters; it doesn't
matter if you're using BSA; it doesn't matter if you're loading 5
fmol, or 50 fmol, or 500 fmol; it doesn't matter if your sample has
been left in the fridge for the past couple of months; it doesn't
[usually] matter if the flow cell is a wildflower flow cell that
crashes in its first 10 hours of running; it doesn't [usually] matter
if you use only passed reads from fast base calling; it doesn't
[usually] matter if you get relative concentrations wrong for samples
when calculating molarity; it doesn't [usually] matter if the sample
is contaminated.

### Loading amount

With the rapid barcoding kit, the actual amount doesn't matter as much
as the relative amount for different barcodes, because the limiting
factor in the rapid kits is the adapter itself, rather than the amount
of template DNA added. So especially for short reads (under 1kb),
overloading is unlikely to be a big issue.

The closer you can get it to ideal the better your yield will be, but
for most runs using RBK (especially on Flongle flow cells) the small
differences in yield from that are eclipsed by the large differences
in flow cell quality. On the other side of molarity, the Flongle flow
cells have a smaller liquid reservoir than MinION (and PromethION)
flow cells, which seems to work favourably for sensitivity. In other
words, Flongle flow cells will usually sequence fine at lower load
amounts of, say, 1-5 fmol.

I'm not completely sure about the reservoir size, but it's probably
something like this (based on the library load amount for the
different loading protocols):

* **Flongle** - 25 μl (30 μl loaded)
* **MinION** - 50 μl (75 μl loaded)
* **PromethION** - 150μl (200 μl loaded)

The flow cells work a bit like a battery in that even if the pores
stay healthy, the sequencing capacity will still deplete over
time. This is due to carrier ions (i.e. the electrical current that is
detected by the sequencer) shifting from the input channel to the
output channel; ONT's name for this is "mediator", as explained from
about 11 minutes into [this
video](https://player.vimeo.com/video/651473380). A larger reservoir
will take more time to deplete the carrier ions that are essential for
sequencing; this theoretical model explains why the MinION and
PromethION flow cells can have longer run times than Flongle flow
cells.

<img src="/presentation-notes/pics/Nanopore_electrode.svg"
alt="symbolic representation of ONT flow cells" width="100%"/>

## 2024-Mar-29

### Flow cell waste

If you have green liquid in your waste channel, you should probably
double-check your sample preparation processes. Potassium ferrocyanide
(the main component of the storage buffer) has a yellow colour.

Potassium ferrocyanide crystals can form and block liquid flow if the
flow cell is left to dry out for a long time. Luckily, it's readily
soluble in water and flush buffer, so dropping and sucking up flush
buffer on the crystals a few times is often enough to unblock the flow
cell.

However, it's better if the crystals don't form in the first
place. This can be mostly avoided by taping up the flow cell waste
channels when the flow cell is being stored. Just make sure that the
tape is removed before opening valves (e.g. the priming port cover) so
that there are no air pressure surprises.

## 2024-Mar-25

### Short Read Sequencing / Flow Cell Yield

Nanopore sequencers are high-throughput sequencing devices that can handle a
extremely wide range of read lengths, from a few tens of bases in "short read
mode" (ONT's software claims 20bp) up to a few megabases.
The R10.4.1 flow cells have a channel sequencing rate that is fixed (through
chemistry and temperature settings) at about 400 bases per second, so the
theoretical maximum sequencing yield is about 35 Gb over the course of a 2-day
run, but a more typical yield in well-prepared flow cells (from experienced
users) is 5-15 Gb per run, with the lower yields typically happening for both
short and really long reads for different reasons.
For short reads, switching time (between one sequence finishing and the next
starting) has a big impact on sequencing yield. Beyond just the time it takes to
do that switching (when the pores are not sequencing anything), the open pores
during the switching period will draw lots more carrier ions through, which
quickly depletes the flow cell.
Assuming a yield of 1 Gb for a first run, and every read 250bp (i.e. 200bp +
50bp for adapters), I would expect around 4 million reads to come off the
sequencer, with a mean of about 20,000 reads per amplicon if there are 200
amplicons that are loaded at identical concentrations. 20,000 times average
coverage gives a lot of room for sample preparation errors before it begins to
impact the outcome of most downstream analyses.
Processing reads with different composition is something that downstream
analysis tools do really well. A typical cDNA sequencing run will have millions
of reads from tens of thousands of different transcripts that are binned and
separately processed. A 200-amplicon run with easily-distinguishable
compositions is a simpler form of that.
I'm leaning on the "easily distinguishable composition" quite heavily here. If
there's a chance that amplicons could be confused, then they need to have some
other distinguishing factor added (e.g. native barcode adapters, or additional
primer tails).

## 2024-Mar-24

### cDNA read demultiplexing

My demultiplexing scripts have been detecting SSP-based UMIs and adding them to
the demultiplexed fastq metadata for a while now, but I've finally gone the rest
of the way and produced a UMI lookup table as one of the default outputs. That
table can be used to inject UMIs into the 'RX' field of SAM alignments using
another accessory script that I wrote:

```
# Set up shell variable to script location
$ scripts="~/bioinfscripts"

# Demultiplex reads
$ ${scripts}/fastq-dental.pl called_${x}.fastq.gz -barcode ${scripts}/dental_db/barcode_full_PBK114.fa -adapter ${scripts}/dental_db/adapter_seqs.fa -mat PCB114.mat

# Filter out unnecesary barcodes manually using nano
$ cp demultiplexed/counts.orig.txt demultiplexed/barcode_counts.txt
$ nano demultiplexed/barcode_counts.txt

# Orient reads and generate per-barcode UMI tables
$ for bc in $(awk '{print $2}' demultiplexed/barcode_counts.txt);
     do echo ${bc};
     ${scripts}/fastq-dental.pl -orient demultiplexed/reads_${bc}.fq.gz \
          -muxdir demultiplexed/oriented_${bc};
   done

# Map reads to the transcriptome, and inject UMIs after mapping
mkdir -p mapped
for bc in $(awk '{print $2}' demultiplexed/barcode_counts.txt);
   do echo ${bc};
   minimap2 -t 10 -a -x map-ont reference/gencode.vM34.transcripts.fa.idx \
     demultiplexed/oriented_${bc}/reads_BCoriented.fq.gz | \
     ${scripts}/samAddUMIs.pl -umi demultiplexed/oriented_${bc}/UMIs_all.txt.gz | \
     samtools sort > mapped/reads_${bc}_oriented_vs_M34-t.bam;
done
```

I presume somewhere out there are downstream tools that will process the RX tags
to do per-transcript molecule filtering, but it's unlikely that's done for any
nanopore-specific mappers (e.g. bambu, oarfish) because UMIs have only been a
recent addition to the nanopore sequences.

Aside: putting a bit of work into training the read *aligner* (rather than the basecaller) can help substantially in improving the apparent accuracy of reads. My demultiplexing scripts use last-train to work out appropriate parameters for read alignment, and are here shown to increase the substitution identity for barcode/adapter detection from 99.8797% (q29) up to 99.9445% (q33):

```
Parsing barcodes and adapters... done; successfully generated LAST database.
.......... Training LAST using first read batch (10000 reads):
  # substitution percent identity: 99.8797
  # substitution percent identity: 99.9309
  # substitution percent identity: 99.9349
  # substitution percent identity: 99.9356
  # substitution percent identity: 99.9357
  # substitution percent identity: 99.9358
  # substitution percent identity: 99.9358
  # substitution percent identity: 99.9445
Done - trained first read batch in 1.1 seconds.
```

## 2024-Mar-23

### Native barcoding / First Steps

As a general rule for science experiments, ask first *before* buying reagents,
and especially before using (or buying) expensive machines.
Do a lambda run first. That will help you understand the timings, and what a
good sequencing run looks like. It will work out cheaper in both time and cost,
because you won't be fussing around trying to correct things that are part of
normal sequencing, or not correcting things that are unexpected.
I don't really understand why so many people underestimate the complexity of
sample prep and data analysis to such a degree. Lambda sequencing is simple;
it's a single sample, and a single moderately long sequence of known length. 200
amplicons is complex; 200 *different* amplicons is (a bit) easier than a single
amplicon in 200 samples. Flow cell washes are complex. Barcoding is complex. For
nanopore, a short read sequencing run (< 1kb) is complex.
If at all possible, get someone else who is familiar with nanopore sequencing to
do the sample prep the first time, and watch them. This will help you understand
the many protocol issues there are which are unconsciously glossed over by
experienced users.
Failing that, make your sequencing needs simpler, especially for a first
sequencing run. I understand that most of these will involve a lot of waiting,
or might not be possible (e.g. if samples have already been prepared); that's
why it's important to ask the questions about the experiment *before* paying
money for reagents and sequencing:

*    Flow cell washes (especially for amplicon or plasmid runs) can be avoided by buying Flongle flow cells (adapter is $1460 USD and comes with 12 flow cells; after that it's $810 per 12 flow cells).
*    If amplicons can be redesigned to be longer, then do that. If you can get them to 400bp or longer, then you'll have fewer issues with short reads rapidly depleting the flow cell sequencing capacity. Depending on applications, you may also be able to use the rapid barcoding [fragmentation] kit instead of the native barcoding kit, which is a simpler, quicker sample preparation process.
*    When sequencing lots of samples, consider redesigning primers to incorporate barcode sequences. For 200 samples, you could do it all in one sequencing run using the 24-barcode NBD kit if you had 12 separate primers with unique barcodes on them. Downstream analysis will be more complex, but the sequencing will be easier.

But... assuming none of that can be done, and you want to leap head-first into
this, here are some tips for first runs from short reads:
*    Short reads (especially < 200bp) will use the flow cell up faster. As a consequence, sequencing yield will be reduced, and this cannot be recovered from flow cell washes. Expect sequencing output to drop substantially across multiple washes.
*    One way to alleviate the depletion issue is to spike in a longer sequence, e.g. add 10% lambda DNA to your sample. This will keep pores occupied when they're not being used for sequencing the target DNA, which will increase the life of the flow cell and the yield of the samples.
*    Your first library preparation will take a lot longer than what is mentioned in the protocol. There are "one-pot" approaches (where sample is kept on beads rather than being transferred into new tubes) which will slightly reduce time, but a first run with 24 samples will take *ages*. Block out at least half a day for sample preparation; all the reagent hunting, tube switching, checking protocols, and pipetting will take a long time.
*    As far as I'm aware, NEB reagents have produced the best results according to ONT's internal research. Going off-protocol for a naive first attempt at nanopore sequencing is a bad idea. There's a huge amount of detail in the on-line methods, and it's quite easy to accidentally miss the most important things (e.g. keeping ethanol away from the adapters) because the protocols make it seem like everything is important.

## 2024-Mar-16

### RBK Multiplexing / Serial Sequencing

Long DNA molecules (>100 kb) won't kill a pore, but they will stop it from
sequencing temporarily. A long sample prep combined with regular use of the
nuclease wash kit will allow sequencing to continue for a long time.

Pore life is degraded faster by the pores staying unoccupied during a sequencing
run, which happens more often with short molecules (<1 kb).

However, if you have different-sized RBK libraries, you'll get higher yield by
running them in equimolar concentrations at the same time. One way to achieve
this is to make a rough guess, sequence for half an hour, then wash the flow
cell and re-adjust the pool (i.e. re-pool, adapt, mix with sequencing buffer)
based on those results.

## 2024-Mar-13

### cDNA Sequencing

The cDNA synthesis steps add ONT anchor sequences onto the template sequences.
These allow the ONT universal rapid barcode primers to bind and extend. The
strand-switch primer includes an additional unique molecular identifier (UMI;
represented by 'V' in the SSP sequence below) to help with read deduplication
during post-sequencing QC, and also includes 3 2' O-methyl RNA Gs to anchor on
to the end of sequences that have been created using a reverse polymerase:

    SSP 5' - TT-TCTGTTGGTGCTGATATTGC-TTTVVVVTTVVVVTTVVVVTTVVVVTTT-mGmGmG - 3'
    VNP 5' - AC-TTGCCTGTCGCTCTATCTTC-TTTTTTTTTTTTTTTTTTTTVN - 3'

[note: the initial TT/AC were present in earlier barcode primers, but not the
primers in the newest V14 kits]

The `TTT...TTTVN` region of the VNP can be replaced by gene-specific sequences
if targeted amplification is desired, bearing in mind that the
reverse-transcription reaction happens at 42ºC.

If making these primers yourself (which I *would not* advise for SSP, due to its
more complicated structure), bear in mind that they should be HPLC purified.
Based on various protocols, I believe the primers should be diluted /
reconstituted to 2µM. Adding an additional 5' phosphate to the primers may also
be a good idea, as this will allow them to then be directly ligated to ligation
adapters using the ligation sequencing kit.

## 2024-Mar-07

Flongles should be loaded with 5-10 fmol, according to the [LSK amplicon protocol](https://community.nanoporetech.com/docs/prepare/library_prep_protocols/ligation-sequencing-amplicons-sqk-lsk114/v/acde_9163_v114_revr_29jun2022/adapter-ligation-and-clean-up?devices=flongle):

> Prepare your final library to 5-10 fmol in 5 µl of Elution Buffer (EB).
> Important: We recommend loading 5-10 fmol of this final prepared library onto the R10.4.1 flow cell.  

Flow cells can be overloaded, which is a particular problem when the input DNA
is long and unfragmented. Long DNA can knot up and block pores, requiring wash
kits to digest the DNA to clear the pores (assuming the enzymes can get access
to the DNA). Too much input DNA also means that not enough will be adapted,
meaning that the unadapted DNA competes for pores, and the effective loading
fraction will be reduced - this is more of a problem with the rapid adapter
kits, where the adapter is a limiting factor in the chemistry.

ONT doesn't believe molarity is important for transposase kits. I do, especially
when reads are short enough (which is certainly the case for the 1200bp
amplicons in the midnight kit).

Regardless, depth is a matter of sequencing time, not loading concentration. The
flow cell should be loaded with DNA that has a high enough molarity (or
concentration) that it can sequence at its maximum speed, regardless of how many
amplicons are being sequenced.

For planning and grant purposes, assume that the Flongle flow cell will produce
200 Mb of output, and work off that. For example with 50 amplicons, that means
that the budget per amplicon is 4 Mb of sequence on average, which gives 3000X
worth of coverage using 1200bp amplicons (e.g. 15 samples at 200X average
coverage).

## 2023-Nov-13

### Advice for first-time users

* Sequence lambda first, ideally with the ligation kit if you can stomach the cost
of ligation reagents. It will likely be a good run, and will help show you what
a good run looks like. A 6-hour run with LSK114 settings is fine; the flow cell
can be washed and re-used (using the flow cell wash kit) after the lambda run is
over.

* Flow cell degradation over the course of a run is normal. The degradation
can be more or less extreme depending on the length of sequences going through
the pore (longer is usually better, up to about 50kb), the number of pores that
are occupied and sequencing (more is better), sample contamination, flow cell
quality, running temperature, applied voltage, and probably a few other things.

* Because flow cell and pore degradation is normal, do not expect a flow cell wash
to recover all the pores on the flow cell. The wash kit doesn't replenish flow
cells back to full sequencing capacity; it just removes the sequencing library
and a bit of debris (including DNA) that might be blocking pores. The flow cells
contain biological proteins that can be destroyed, and destroyed pores will not
come back to life after a flow cell wash.

* The time you run a flow cell for is experiment dependent. 10-20kb sequences
will take less than a minute to get through the sequencing pores (at 400 bases
per second), so unless you're sequencing megabase-length DNA, you probably don't
have to worry about run time interfering with the ability to sequence DNA. A
flow cell can be run for anything from about 5 minutes (to give sequences a
chance to get to the pores) up to 72 hours (or longer, in the rare circumstances
that the flow cell survives for longer). I usually stop MinION runs once they go
below about 10 pores actively sequencing, as there's not much additional benefit
gained from sequencing beyond that point.

## 2023-Nov-01

### Bacterial genome assembly

Running multiple samples on the same flow cell is possible using barcoding. The
most commonly used options are [rapid
barcoding](https://store.nanoporetech.com/rapid-barcoding-sequencing-kit-24-v14.html)
(cheaper per run/sample), and [native ligation
barcoding](https://store.nanoporetech.com/native-barcoding-kit-24-v14.html)
(more yield per run/sample, potential for higher accuracy). If you don't have
much DNA for each sample (unlikely for bacterial isolates), then [rapid PCR
barcoding](https://store.nanoporetech.com/rapid-pcr-barcoding-kit-24-v14.html)
can be used instead (shorter reads, but should be plenty long enough for most
stuff that gets done with bacteria).

Ryan Wick (from Kathryn Holt's lab) has been doing a lot of research with
different base calling models and assemblers for bacterial assembly. Their lab
has produced a tutorial on how to assemble bacterial genomes:

https://github.com/rrwick/Perfect-bacterial-genome-tutorial/wiki

Looking at his most recent accuracy post might be helpful as well:

https://rrwick.github.io/2023/10/24/ont-only-accuracy-update.html

## 2023-May-27

### Plasmid sequencing tips

When preparing plasmid DNA for sequencing, I follow one piece of advice that Dr.
Divya Mirrington (ONT) gave me about pooling: create a pooled sample with
volumes that you're confident about, then remove an aliquot from that for adding
the remaining reagents [paraphrased]. I don't do any additional cleanup for
purified plasmid DNA; they tend to sequence very well on flow cells without that
cleanup.

My key requirement for plasmid  sequencing is a concentration of >20 ng/μl
(ideally by qubit or  quantus). Concentrations over 100 ng/μl should be diluted
down. If the plasmids can be diluted down to all exactly the same concentration
(but at least 20 ng/μl), or they're all similar lengths, then that makes
creating an equimolar pool much easier.

For more details, see [my Plasmid Sequence Analysis
protocol](https://www.protocols.io/view/plasmid-sequence-analysis-from-long-reads-36wgq4n5yvk5/v8).

According to ONT's RBK114 gDNA protocol...

> We recommend loading a maximum of 200 ng of the DNA library for Flongle flow
> cells. If necessary, take forward only the necessary volume for 200 ng of DNA
> library and make up the rest of the volume to 5.5 µl using Elution Buffer (EB).

It does surprise me a little that this amount doesn't seem to have changed for
RBK114 vs RBK004. The newer kits are meant to be more sensitive, and may perform
better (with less blocking) if less DNA is loaded onto the flow cell.

## 2023-Apr-03

### Library concentration with rapid adapters

The thing that matters most is the number of adapted molecules going onto the
flow cell. This molecule count can be reached any number of ways, so for more
samples the amount added per sample can be  reduced.

With the rapid kits, the amount of adapter added is the limiting factor, and
there should always be an excess of molecules with adapter bonding sites in the
prepared library. For this reason, it's more important to make sure that samples
are loaded in equimolar quantities than any particular concentration (although
I have found that 20-100ng/µl seems to work well for most situations with the 
rapid kits).

## 2023-Apr-01

Almost any nanopore kit can be used for metagenomics if you're brave enough.

It used to be the case that non-barcoded kits were \$600, and barcoded kits were
\$650. The standard ligation kit (SQK-LSK114) is still \$600, but the variation
in barcode count and volume now means that other kit costs are less predictable.
Prices are always available on the Nanopore store (no login needed):

https://store.nanoporetech.com/productDetail/?id=ligation-sequencing-kit-v14

The ONT kits and flow cells are only part of the cost for metagenomics, and for
the ligation and native barcoding kits you'll need to purchase additional
reagents for sequencing, in particular ligation enzyme, and end repair / tailing
enzyme.

For our river water studies, we used a custom enzyme cocktail to break open the
cells, followed by a Qiagen Power water kit to extract DNA, followed by the
rapid PCR barcoding kit for sequencing:

https://doi.org/10.1093/gigascience/giaa053

Nowadays Sigma sells Metapolyzyme ($491 NZD), which is a pre-prepared enzyme mix
that does a better job than the cocktail we used:

https://www.sigmaaldrich.com/NZ/en/product/sigma/mac4l

The Power water kit for DNA extraction is here ($671). Depending on your
application, there may be a better kit (e.g. PowerSoil), or you might be able to
get away with a simpler DNA extraction protocol:

https://www.qiagen.com/us/products/discovery-and-translational-research/dna-rna-purification/dna-purification/microbial-dna/dneasy-powerwater-kit?catno=14900-50-NF

The rapid PCR barcoding kit is here (\$650). Note that this uses Kit9 adapters,
so will only work with R9.4.1 flow cells, either MinION flow cells (\$900 each),
or Flongle flow cells (\$810 for 12, or \$1500 for the Flongle adapter (a
one-time purchase) plus 12 flow cells), depending on the required yield. The
rapid PCR barcoding kit doesn't include DNA polymerase, so that will need to be
purchased as well if you don't have it:

https://store.nanoporetech.com/productDetail/?id=rapid-pcr-barcoding-kit

https://store.nanoporetech.com/productDetail/?id=flongle-flow-cell-pack

As ONT is in the process of shifting over to kit14 and R10.4.1, I expect that
the rapid PCR barcoding kit will eventually be replaced with a kit14 equivalent.
The cost of the kit should be the same, though.

In addition to that, you'll need to purchase other general lab consumables and
have access to the required general lab equipment (and staff labour) for doing
all these things. Beyond that, there file storage costs and computation costs
(both for sequencing and data analysis) that may need to be considered,
especially if you've never done any sequencing before.

## 2023-Mar-31

### Rapid barcoding

When pooling multiple samples together on the same run, one easy approach is to
dilute all samples to 20ng/μl, create a pool with equal amounts for all samples,
then take 10μl from the pool.

If one of the samples has a lower concentration than 20 ng/µl then more maths
and more pipetting of small[er] volumes is needed.

What I find the easiest to think about is working out how much of the others I
would need to add to equalise concentration to 1 µl of the lowest-concentration
sample.

As one example, let's say there are 12 samples, and one of them is at a
concentration of 12 ng/µl, and the others are all at 20 ng/µl. So the amount in
1µl of the 6 ng/µl sample is the same as 0.6µl in the other samples. If there is
10 µl of everything, then I would add 10µl of the 12 ng/µl sample to 6µl of all
the other samples*, and then take 10µl from the resultant pool.

The most important thing to get right is equimolar concentration for all
samples. For targeted sequencing (like 16s), this is easier because the same
length can be assumed for all samples, so equimolar = equal concentration =
equal ng quantity in an arbitrary volume.

\* or proportionally less for all samples, depending on how much excess liquid I want and how confident I am about small-volume pipetting

## 2023-Mar-26

### Amplicon / plasmid sequencing on Flongle flow cells

Even when we have wildly different read counts for 12 samples, we'll usually get
100X coverage from the lowest count sample after an overnight Flongle run. 100X
coverage for the highest count sample is often reached in about half an hour. I
think 24 samples per run is feasible on a Flongle, especially if low-coverage
samples can be re-run separately on another flow cell. More than that is getting
in the realm of what might be better using a 96-barcode kit and a MinION flow
cell (but it still might be worth making the attempt on Flongle first).

My Flongle yield estimate for the purpose of funding and experiment planning is
200 Mb. If an experiment can work with 200 Mb yield, it's probably going to be
okay on a Flongle, and if 200 Mb is plenty, it makes a lot more sense to use a
Flongle flow cell rather than a MinION flow cell. Actual yields are often higher
than that, but I find it better to prepare for the [almost] worst case.

## 2023-Mar-16

### Chimeric reads

Chimeric reads can happen during sample preparation (where sequences are
enzymatically ligated together prior to sequencing), and also in-silico as a
result of the pores getting reloaded too fast and the software not detecting the
switch.

Most of the time this can be picked up by mapping reads to the ONT adapter
sequences, and discarding any reads with internal adapter sequences (rather than
the expected end-joined adapter sequences).

Our chimeric reads paper demonstrates this phenomenon on the early ONT ligation
kits. The kits are different now, but the ligation part of sample prep is still
essentially the same:

https://doi.org/10.12688%2Ff1000research.11547.2

The occurrence of sample prep chimeric reads can be substantially reduced (or
possibly eliminated) by using rapid adapters, which use a chemical reaction to
join adapters onto DNA that has added adapter bonding sites. Those sites can be
added via PCR, fragmentation, ... or ligation [probably not the best idea due to the increased risk of chimeric reads].

### Sample QC

If you want to do QC on nanopore reads (especially ones that don't have constant
lengths), please use something that is specifically designed for nanopore reads.
Nanoplot, for example:

https://github.com/wdecoster/NanoPlot

https://doi.org/10.1093/bioinformatics/bty149

FastQC is designed for short reads. There is an expectation with FastQC that all
reads will have identical length, which makes it easier to determine from
looking at a position / quality plot where issues are in reads. For long-read
sequencing (in this case nanopore), this does not apply: reads are variable
length with no length-based quality degredation (i.e. similar base calling
qualities across the full length of the read). This means that the *average*
base quality from a read is a good enough measure for QC, which is why it is
used for nanopore-specific QC programs. Position-based QC is *not* appropriate,
because the different read lengths lead to shot noise effects.

[While it might be useful to look at position-specific read qualities near the
start and end of a read (where pore loading and ejecting may contribute to lower
qualities), I'm not aware of any programs that do this.]

## 2023-Mar-12

### Flow cell storage

I only remove waste liquid before returning flow cells, or if there's a chance
it could overflow if I put more liquid into the sample loading port. Leaving the
liquid in there acts provides a bit of protection against the flow cell drying
out, although it's nowhere near as good as a piece of good sealing lab tape over
the waste ports.

If you want to store exactly at 4ºC, you should make sure that your fridge
doesn't dip below that temperature (i.e. use a temperature logger), because flow
cells can be destroyed by getting too cold - I had this problem with a bar
fridge that I used for demonstrating live sequencing during a conference. If you
can, store the flow cells in the middle of the temperature range indicated on
the flow cell packet.

## 2023-Mar-09

### Wildflower flow cell patterns

As I've reported briefly in the past, we've been seeing a lot of R9.4.1 flow
cells with what I like to call a "wildflower" pattern, because the detailed
channel plot looks a bit like a field of wildflowers, with a large mix of
different types of channel classification and no obvious pattern.

What happens with these flow cells is that they tend to crash quickly. We see
steep decreases in sequencing capacity, in this most recent case losing about
50% of sequencing capacity overnight, despite having a well-loaded flow cell
(this was with a rapid adapter cDNA kit; it's quite rare for us to see loading
over about 75%). I've noticed that this sharp decrease is commonly linked to the
"active feedback" and "channel disabled" pore states.

This looks quite different from the more homogeneous channel state that we
expect to see on flow cells (here shown is a reasonably good channel state
image; I think this was prior to loading anything on the flow cell, which is why
the total read count is so low).

Unfortunately, the wildflower pattern isn't recognised as bad in the initial
flow cell QC check (the QC message for the first flow cell was "Flow Cell
FAT13376 has 1059 pores available for sequencing"), and it's only once we start
sequencing that we notice this bad patterning.

I haven't yet worked out why this is happening, but it's not to do with sample
prep. In the past, we have seen the same effect from flow cells when there's
nothing loaded on them, and also prior to loading any priming mix (i.e. when
doing a 5 minute sequencing run before doing any priming). This does mean that
we can do an additional channel state QC prior to priming to identify these
cells... but we need to remember to do that.

It also doesn't seem to be related to storage time (we've had this happen on
flow cells that were checked the day after they arrived) or shipping conditions
(about a year ago we had two flow cells that had a long stopover in Australia,
one of which exhibited this problem, one which didn't).

It has been challenging discussing this issue over email with ONT in the past,
because every support email seems to end up with a different person. They aren't
generally aware of this problem, so almost always fall back on it being a sample
loading issue (which is certainly not the case in situations where we haven't
done priming or loading). This means it can be a challenge getting replacements
for these flow cells: they're showing fine on the initial count-based QC, so
don't count as poor quality as part of the standard "fewer questions asked"
warranty replacement.

This does seem to be more of an issue currently with R9.4.1 flow cells, but I
thought I'd do a more full writeup just in case it ends up as an issue with
R10.4.1 flow cells later on.

### Flow cell operation

Flow cells function a bit like a battery. They have a certain amount of
sequencing capacity in them (about 1-3 days, depending on the quality of the
sample prep and flow cell), and that capacity can't be recharged by washing. The
purpose of the wash kit is to clear DNA, RNA and other debris from pores so that
they can be rerun on other samples.

Unlike batteries, flow cells actually perform better if they are working under
high load as much as possible (i.e. the pores are fully occupied and actively
sequencing). For the best sequencing yield, it is better to multiplex samples
and run them at the same time on a fresh flow cell.

## 2022-Mar-07

### Flow cell storage

5 months should be fine, assuming the fridge is a commercial one which doesn't drop below 4C. It's less likely that ONT will give you a refund for a bad flow cell, but we've found that as long as flow cells don't dry out or freeze, they still seem to be usable after a year or so.

I don't recommend ordering more than is needed in the next 1-2 months (bearing in mind that an order shipment can be spread over a year, with payment on delivery), but don't chuck away flow cells that are past their expiry date without running a QC check on them first.

## 2022-Mar-03

### ONT Customer support

Unfortunately, this is par for the course for ONT: gaslighting users that complain until there's a sufficient amount of supporting posts on the community, and then suddenly "We have seen that a small number of users are experiencing issues. Here's a software / hardware / protocol change that *definitely* fixes the problem. No, you can't see our data that demonstrates this; just trust us."

Based on how they have dealt with issues in the past, I don't think much will change with regards to support.

It's a shame, because it ends up being a substantial time and money-saving approach on all sides to pay attention to assumed-rare concerns early on in the process of development. It'd be a much better process to work with the community to understand the problems, rather than ONT doing their own internal research and coming back with an imperfect fix that they believe is perfect, but fails to account for something obvious.

## 2022-Feb-25

### Flow cell degredation

Flow cells get worse over time, and washing out flow cells doesn't improve the
time-related degradation. To get the maximum yield from multiple samples, it's
best to multiplex and run them all on a fresh flow cell.

There are multiple reasons for this. On MinION and PromethION flow cells, there
are four sequencing wells electrically linked up to each signal sensing circuit.
When a run starts, it chooses a working well from any available working wells.
Over the course of the run, pores stop working for various reasons (they're
biological systems), so the channel will switch over to another working pore.
Eventually all four pores will stop working. If a run has shifted from 1500
available pores down to 500 available pores, there's a good chance that most
channels will only have one or two linked working sequencing wells, increasing
the chance the entire channel will stop working in the subsequent runs.

Another reason is carrier ion depletion. Flow cells start with a certain amount
of carrier ions above the pores, and the electrical current shifts these ions
through the pores together with the DNA / RNA sequences. Over time, the ratio of
carrier ions on each side changes, requiring more energy to push the carrier
ions and polymers through at the same rate. The flow cells compensate for this
by making the voltage more negative, but this change in voltage is also more
damaging for the pores. Eventually the carrier ions will be so depleted on the
input side such that *no* voltage will be able to shift more carrier ions across
without destroying all the pores, and further sequencing then becomes
impossible.

## 2022-Dec-22

### Metagenomic sequencing

The MinION flow cells sequence DNA at about 400 bases per second across up to
512 different sequencing channels, so at 75% efficiency, it will sequence about
150,000 bases per second, equivalent to a 16S amplicon being covered about 100
times per second (or about 350,000 times per hour).

Decisions on how long the sequencing should be run for are dependent on the
specifics of the project. My rule of thumb is to require a factor of 10 higher
coverage than the minimum needed for a target threshold, i.e. if I want to check
for presence/absence of something that's in 1% of the sample, then I would
prefer about 100 x 10 = 1,000 times coverage (which would be easily reached
within the first minute or so of 16S sequencing, assuming everything's going
okay).

However, bear in mind that 16S is targeted sequencing, not metagenomic
sequencing, and can only hint at what's in a sample. 16S is also a quite
conserved gene, with a fair amount of horizontal transfer happening between
bacteria, so even a perfect match does not necessarily indicate confirmation of
population composition.

For species or strain-level identification, it would be better to use a whole
genome shotgun sequencing approach, such as the rapid PCR barcoding kit (which
has almost the same sample preparation protocol as the 16S kit, but with an
additional fragmentation step at the start).

## 2022-Dec-15

### Sequencing yield

Nanopore sequencers are a continuous throughput device, unlike pretty
much every other sequencer on the market. It's better to think of them
in terms of "bases per second", rather than total yield, although
sequencing rate does decrease over time as the flow cells get worn
out.

MinION flow cells can have up to 512 channels running at 400 bases per
second, so the maximum theoretical yield is 204,800 bases per second,
or about 700 Mb per hour, or about 17 Gb per day. Due to the decrease
in sequencing rate over time, and variation in sample type that
influences flow cell performance, a yield of 5-20 Gb seems to be
typical (based on a poll I did on Twitter in June 2022).

Flongle flow cells (need a Flongle adapter) can have up to 126
channels running (i.e. theoretical maximum sequencing rate of 50,400
bases per second), but due to the design of the flow cell (no "backup"
sequencing wells, vs the MinION's 3 backup wells), yield is less than
what would be expected based on the performance of a MinION flow
cell. I've found that most of my runs are 0.1 - 1 Gb for a 1-day
Flongle run, although I don't put much effort into getting the best
out of Flongle flow cells because even 0.1 Gb is plenty for our most
common use of plasmid sequencing (which also applies to amplicon
sequencing).

PromethION flow cells (need a PromethION sequencing device) can have
up to 2675 channels running (i.e. theoretical maximum sequencing rate
of 1,070,000 bases per second), with similar performance per well to
MinION flow cells. They're more commonly used for cleaner samples by
established sequencing centres, so yields tend to be higher than
MinION flow cells (even after adjusting for pore count), about 50-150
Gb per run seems to be typical.

These sequencing rates and yields are likely increase in the future,
as sequencing reagents, pore chemistry, and software improve. In
particular, the yields I'm familiar with are based on R9.4.1 flow cell
chemistry, and ONT is now shifting to R10.4.1 chemistry (and
associated sequencing kits), which improve yield by making the pores
better at capturing sequences, and increasing their endurance over
long sequencing runs.

Because nanopore is a single-molecule sequencer, error rates are
consistent across the entire length of the read (as is also the case
with PacBio), and sequenced read lengths tend to map quite well to
input DNA length. Due to the way the sequencer works, nanopore
sequencing actually gets better yield with longer reads (up to about
50kb), because there's a small delay between one read leaving the pore
and another one loading on.

## 2022-Nov-29

### Flow cell depletion

Run time definitely affects flow cell use. There are carrier ions in the flow
cell that travel from the input to the output channel over the course of a
sequencing run (or multiple sequencing runs). As the relative carrier ion
concentration changes, the voltage needs to be made more negative to maintain a
proper current flow for sequencing. Eventually, the carrier ions are depleted so
much that this current can't be maintained, and the flow cell stops sequencing.

This depletion cannot be recovered from (or reset) by end-users, so a flow cell
will eventually become useless.

FWIW, read length also influences the number of times a flow cell can be
re-used. There's a small delay between when one DNA strand finishes sequencing
and the next one loads into the pore, during which time the pore is unblocked
and letting carrier ions rush through. Longer sequences (as long as they don't
knot up and block the pores) mean that fewer carrier ions will get through the
pores in the same amount of time, meaning the flow cell will last longer.

Because the pores are proteins, they can also be destroyed irreversibly by other
means, which is another reason why flow cells will eventually degrade over time.

## 2022-Nov-11

### cDNA Assembly from nanopore reads

This is one of those rare circumstances where sequencing accuracy is going to be a problem. Here are some of my thoughts on the matter:

* According to Callum Parr, "The direct cDNA kit is trash" [[ref](https://community.nanoporetech.com/posts/direct-rna-or-ligation-kit#comment_44449]). This might be related to accuracy issues [[ref](https://community.nanoporetech.com/posts/too-many-fail-reads-from-d).
* By design, direct cDNA has low yield; it's explicitly not an amplification protocol. The idea is that the lack of amplification means that reads will be longer, and read distributions will be more representative of the actual composition. For de-novo assembly it's better to have sufficient coverage than a perfect compositional match, but PCR amplification could lead to truncated sequences if the polymerase falls off before the reverse transcription is complete. Some people have found direct RNA sequencing produces longer reads than direct cDNA, but the base calling algorithms are worse; nanopore hasn't updated their RNA callers in tandem with DNA calling.
* As far as I'm aware, there is no established protocol/workflow for transcriptome assembly from nanopore reads alone (except possibly [RATTLE](https://doi.org/10.1101/2020.02.08.939942)). Because there is a big dynamic range in transcript abundance, one suggestion I've seen is to use Flye in metagenomic assembly mode.
* If you have Illumina cDNA reads available, then you can use [Trinity](https://github.com/trinityrnaseq/trinityrnaseq/wiki/Running-Trinity) in long-read mode. This requires error-corrected long reads (e.g. processed through the read correction step of Canu). The long reads are used to cluster short reads for subsequent assembly.
* The most common nanopore base calling error is short deletions (e.g. 1-2bp), likely due to variability in the translocation speed leading to dropped signal. For reference-based mapping this is not usually a problem, but it *is* a problem for de-novo assembly, especially when base-accurate protein translation is needed.

## 2022-Nov-03

### small-scale reference-based analysis

For amplicon mapping, where mapping at the single base level matters, and there are only a few samples, my recommendation would be to use a trained [LAST](https://gitlab.com/mcfrith/last#install) model, create a BAM file using `maf-convert` and [samtools](https://www.htslib.org/download/), then eyeball the results in a graphical BAM viewer like [Tablet](https://ics.hutton.ac.uk/tablet/download-tablet/), [IGV](https://software.broadinstitute.org/software/igv/download) or [JBrowse2](https://jbrowse.org/jb2/download/):

```
# create index
lastdb -uRY4 -R01 reference.fa reference.fa
# train mapping model
last-train -Q 1 -P 10 reference.fa reads.fq.gz > trained.mat
# do the mapping
lastal -P 10 -p trained.mat reference.fa reads.fq.gz > mapped.paf
# convert output to BAM format [i.e. draw the rest of the owl]
maf-convert sam mapped.paf | \
  samtools view -b --reference reference.fa - | \
  samtools sort > reads_vs_mapped.bam
# index created BAM file
samtools index reads_vs_mapped.bam
# view the mapped reads in your favourite BAM browser
Tablet reads_vs_mapped.bam reference.fa
```

This approach won't give any quantifiable numbers (although there are ways to extract that from the BAM files), but should get quickly to answering specific questions about disease-causing mutations.

## 2022-Nov-01

### Metagenomic Assembly

Kat Holt has a lot of useful information about microbial assembly and nanopore
sequencing; I recommend having a look:

https://holtlab.net/category/software/

30-50X is a good target for genome assembly. Really high coverage can cause rare
artefacts to pop out in the final assembly, and lead to assembly taking *heaps*
longer than it does at lower coverage. One thing to bear in mind is that
nanopore reads have a long tail of low-quality reads, so it can help to do some
light filtering based on average base quality prior to assembly. I've found that
aligned accuracy is well correlated with predicted accuracy by the basecaller,
and have a plasmid assembly protocol here which discusses how to do quality
filtering:

https://dx.doi.org/10.17504/protocols.io.by7bpzin

From what I've heard from others, it seems that flye is pretty good for
metagenomic assembly, especially for mixed samples where there will be multiple
genomes with different average coverage levels. Racon probably isn't needed with
recent nanopore basecalling [i.e. sup model], and if any correction *is* used,
it's best to concentrate on INDELs and stay away from SNPs. Nanopore SNP
consensus accuracy, especially when it's not mixed with a homopolymer sequence,
is *really* good, and there are a lot of programs that will over-correct
nanopore assemblies at SNPs.

If possible, I'd recommend separating out forward-mapped and reverse-mapped
reads, carrying out separate assemblies, and concentrate on fixing any places
where the assemblies don't agree. The different electronic profiles for the
different strands should increase the likelihood of finding a correct consensus
assembly.

Another thing to try is Trycycler (Holt lab again...), which uses the results
from multiple assemblies to create a more complete/correct assembly: "if all
goes well when running Trycycler, small-scale errors will be the only type of
error in its consensus long-read assembly."

https://github.com/rrwick/Trycycler/wiki#an-important-caveat

For final QC and annotation, it seems like Bakta is the recommended approach,
according to the creator of Prokka (which was the previous gold-standard
annotation software):

https://twitter.com/torstenseemann/status/1565471892840259585

https://github.com/oschwengers/bakta/releases

### cDNA Data Analysis

I do things quite a lot differently with ONT vs Illumina data.

For Illumina data, I need to apply transcript length normalisation (I call it
VSTPk) when comparing different genes within the same dataset (some details
[here](https://rupress.org/jem/article/214/1/125/42206/Th2-responses-are-primed-by-skin-dendritic-cells#5741950)).
Illumina can't really properly do isoform expression, so I collapse the reads
down to gene level prior to doing a differential expression analysis.

For ONT data the transcript sampling carried out by the sequencer is at the
transcript level, so I don't apply any transcript length normalisation: at
similar expression levels, a 300-base transcript will appear in the reads at
around the same proportion as a 2000-base transcript. Reads produced by the
sequencer are full-length transcript (with some filtering to make sure the only
analysed reads are full-length), which means there's no guessing about what
the full transcript would look like (as is needed with Illumina reads).
Expression (in terms of reads mapped to each base position) is also very flat
across the entire length of the transcript. This means that I *can* be
confident about doing isoform-level differential expression analysis (down to
very low counts), doing the collapsing to gene level *after* results have been
produced. Closest/best thing I have to a reference for that is
[here](https://dx.doi.org/10.17504/protocols.io.5qpvonn2bl4o/v17), but it's
still a work in progress. Our first nanopore sequencing cDNA studies led to
[this paper](https://doi.org/10.3389/fphys.2020.543962).

The sample prep for Illumina vs ONT is different as well. The most common
stranded Illumina prep involves fragmented cDNA amplified from random
hexamers, which means that you get lots of different types and stages of RNA
(including ribosomal RNA, microRNA, and unprocessed intronic "straight off the
genome" RNA). Some people want that (e.g. looking at transcript velocity by
comparing pre-processed vs mRNA), but most don't, and it's a bit of work at
the sample prep level and in analysis to clean the data to get rid of the
unwanted RNA.

All ONT RNA and cDNA sequencing kits use polyA anchors (somewhat similar to how
most single cell sequencing sample prep is done), so non-polyA RNA needs to be
polyadenylated prior to sequencing. There's a lot less noise, but the
information produced by default is less complete. If you have a particular gene
target that you want to sequence, the polyA anchor can be replaced with a
gene-specific target and amplified by PCR, meaning that only that target will
get seen / sequenced by the sequencer. This target-specific cDNA sequencing is
the method that would be best used to look at isoform expression of a specific
gene.

Reads can be mapped to the target using minimap2 or LAST, and the results of
that mapping used to filter the reads specifically for that target. For a handful
of targets, I have previously used CD-HIT-EST to isolate different isoforms,
but expect there are more recent & better tools available. I'm not really sure
what the next step after that is; I usually just hand the transcript isoforms
over to the biologists and let them peer at the sequences to make their own 
discoveries.

## 2022-Aug-05

### Adaptive Sampling

For adaptive sampling, scale matters: the length of non-target regions, the
length of the Region Of Interests (ROIs), the N10/L10 of the reads, and the
separation distance of the ROIs. Using the ONT recommendations, if your regions
of interest are less than twice the N10/L10 away from each other, then they
should be combined into a single region. That will give the best chance of
capturing target reads.

Scale will influence the frequency of the following situations:

1. Outside ROI, outside buffer [non-target; discarded]
2. starting bases within ROI [target; retained]
3. partially within ROI, starting bases in buffer region [target; retained]
4. outside ROI, starting bases in buffer region [non-target; retained]

The ratio of Situation #1 to Situation #2 can help to work out if adaptive
sampling is useful at all.

Situation #4 is the only situation that is a potential problem (and would be
influenced by changes in the buffer region size). A smaller buffer region will
reduce the occurrence of non-target retained reads, but it will also reduce the
occurrence of target retained reads (i.e. situation #3).

It's a false positive / false negative tradeoff. If you want to guarantee that
the only retained reads are those that include the ROI, then set the BED region
to be exactly the same as the region of interest; this will drop some reads with
partial hits. If you want to keep as many reads including the ROI as possible,
then set the BED region to include a buffer on both ends that is the maximum
expected read length; this will include some reads with no hits.

## 2022-Jul-30

### Reduced representation sequencing (ddRAD-Seq)

From what I can see about the method on the Illumina website,
ddRAD-Seq looks similar enough to [rapid PCR
barcoding](https://store.nanoporetech.com/rapid-pcr-barcoding-kit.html),
which is an established MinION protocol that works well for
metagenomic purposes. Unlike ddRADSeq, the transposase fragmentation
is essentially unbiased (from what I can tell in the data I've looked
at), so it should work well for low coverage sequencing.

For sequencing particular genomic regions, it may be better to design
PCR primers that span regions of at least 600bp (probably what would
be used for Sanger sequencing - although the longer the better), then
use a [Rapid Barcoding
kit](https://store.nanoporetech.com/rapid-barcoding-kit.html) to
fragment the amplicons. This will allow for a lot more samples from
the same total yield, potentially opening up the possibility of using
a Flongle flow cell ($67.50 per run, rather than $900 per run) to
still get enough data.

It would be technically possible to take the product of a restriction
enzyme double digest, ligate nanopore sequencing adapters, then
sequence on a MinION, but there's not really much point in doing that
with other options available. There are a few other targeted
sequencing approaches (including a CRISPR/Cas9 kit, and an adaptive
sequencing approach that does target selection *during* sequencing);
I've just discussed the cheapest / easiest ones to get started with.

Aside: ONT is in the process of improving their kit chemistry over the
next few months; there should be better fragmentation kits available
by the end of the year.

## 2022-Jul-01

### Plasmid sequencing on Flongle

The standard ONT Flongle sequencing protocol is to do half-reactions and load
half libraries, so I keep with ONT’s suggestions in that regard (including using
0.5µl adapter).

I increase the incubation time versus the ONT protocol because I don’t heat in a
PCR machine – I use my hands and a heating block. The increase in time was a
recommendation from Dr. Divya Mirrington during a sequencing workshop she led at
our institute.
 
Another tip from Dr. Mirrington was to pool samples together and take the
required amount from that pool. As long as the input DNA is sufficiently
concentrated (which is usually the case), then this should work fine. It is
possible to eke out a little bit more sequencing from small samples by
multiplexing them together, in which case concentrating with XP beads is needed,
but I’ve found that I don’t usually need to do this for plasmid sequencing
because the yield requirements are so low (i.e. only 100 reads from a 10kb
sequence, which is easily within the Flongle output, even when multiplexing 12
samples for a single run).
 
ONT recommends a few things that I haven’t found to substantially change our
Flongle output. This includes *always* using XP beads after pooling (even when
sequencing a single sample), and using the Flongle reagents in glass vials. For
us, the most significant change that gave us consistently good results was to
load Flongle flow cells using the negative pressure “gentle loading” technique.
 
## 2022-Jun-02

### metagenomic sequencing

RBK004 is a multiplexing kit, but it's less so than RBK110.96.

The statistic that matters is the number of adapted molecules that are loaded
onto the flow cell. Given that, RBK004 is designed for up to 12 samples and has
input requirements of 400 ng, the input requirement for the [up to] 96-sample
RBK110.96 would be expected to be (12 / 96) * 400 = 50 ng... which is precisely
what you state.

These barcoding kits are typically used to sequence multiple samples at the same
time; sequencing fewer samples will require more input DNA to compensate. The
flowcell-loaded input requirements for the kits are identical: a single sample
on either of the RBK kits will have the same input requirement.

As an alternative, consider the rapid PCR barcoding kit (SQK-RPB004), which
fragments DNA and simultaneously adds ligation sites for rapid PCR primers. The
primers are barcoded, the fragmentation is not, which means that separate PCR
reactions need to be carried out for each sample. After amplification, the PCR
products are quantified, pooled and then rapid adapters are added. It's a quick
and fairly cheap protocol with minimal external costs (the most significant
being polymerase), and will give a higher yield than the unamplified barcoding
kits. However, because it's a PCR amplification kit, reads will be shorter (most
relevant for large genomes; less relevant for bacterial genome assembly), and it
will not be possible to do any methylation calling on assembled genomes.

## 2022-May-20

### Accuracy

Given that ONT are claiming Kit14/R10 with 520 bases per second gives
99.0% modal accuracy, which is already better than any R9.4.1 kits,
It's not so clear to me why 260 bases per second at 99.6% accuracy is
needed at all. Not for genome assembly, at least, or anything else
that uses consensus results. I expect a doubling of coverage should be
able to bump up the consensus accuracy a bit higher than a halving of
sequencing speed.

Regarding amplicon sequencing and consensus variation, it depends on
the variants. I like to dig deep into read mappings and look at the
sequence context. I also don't trust minimap2 to give reliable local
alignments.

It has been my experience that ONT reads are very reliable for SNPs,
especially when ensuring strand consistency. For single-base INDELs,
especially deletions, especially homopolymers, I have less confidence
about treating nanopore reads as correct. The sequencing and
basecalling model is prone to skipped signal.

If I'm concerned about an INDEL, then I'll use another technology to
confirm.

Once I get beyond single base changes (excluding homopolymers) in a
consensus sequence, it's more difficult to justify a sequencing error,
given how sequencing and basecalling is done. It's more likely that
there's an unmodeled base modification present, or a PCR artefact.

The concerns I have relate to systematic error, which can't be brushed
away with more reads.

It's a software problem: a basecalling problem, to be
specific. There's a lot of sequence-dependent variation in the
electrical signal that leads to unmodeled variation being interpreted
as something else. I still believe that this systematic error can be
dealt with by more careful observation of the sequence.

For example, Maybe there's a particular sequence 25bp upstream that
leads to the helicase skipping a bit, causing a rush of nucleotides
and/or misclassified bases.

## 2022-May-05

### Ligation kits

Ligation is a slower protocol with more hands-on time, increases the chance of
library prep read chimerism, and is more expensive.

The primary advantage of the ligation kit is that it produces a higher pore
occupancy under typical sample prep conditions, leading to a better sequencing
yield per flow cell (i.e. usually cheaper per base, more expensive per run). A
secondary advantage is that reads are full-length.

From a downstream analysis perspective, full-length reads only matter when
single molecules matter. If consensus is good enough (which is very common),
then fragmented reads are fine as well.

## 2022-May-02

### Mitochondrial Genome Assembly

I've found that for mitochondrial assembly, mapping is better with uncorrected
reads, at least for high-accuracy called reads. I've yet to check for
super-accuracy called reads, but I expect it to be similar:

https://github.com/marbl/canu/issues/1715

Curiously, I've subsequently found that while *mapping* is better with
uncorrected reads (suggesting they are more accurate), canu performs slightly
better with its "corrected" reads. In hindsight, this should have been obvious,
as the people developing their assembler will be familiar with the error modes
of their corrected reads, and therefore understand how to fix them.

After revisiting a bunch of approaches with my nippo reads, my current best
results have been achieved by filtering out the most accurate sequences (using
mean estimated accuracy reported by guppy, e.g. as found in the sequence summary
file) to a depth of 40-100X, then assembling using canu with default parameters.

Assembled sequences can be corrected using medaka:

https://github.com/nanoporetech/medaka

While Medaka has been trained to correct draft sequences output from the Flye
assembler (mapped with minimap2), it seemed to work okay for me for our
mitochondrial genome when using LAST for mapping, and canu as output. It still
didn't produce a 100% accurate genome; it had 10 errors from ~13k bases.

Regardless of assembly method, you should be manually curating assemblies prior
to submission. The mitochondrial genome is small enough that it can be checked
for obvious gene sequence issues, which is probably why NCBI have their
automated submission checks.

If there is an internal stop codon issue with mitochondrial genomes, check
to make sure that the nucleotide translation code is correct. While the
standard code works for most nuclear DNA, the mitochondrial translation
is different:

https://www.insdc.org/genetic-code-tables

What we also noticed with our worm mitochondrial genome (as has been
observed in other organisms) was that there are some genes that end within
the mitochondrial sequence before the end of the stop codon, and the last
codon is completed by the addition of polyA sequence. There's a worm stop
codon 'TAA', so if the gene ends on 'T' or 'TA', then translation will be
stopped in processed mRNA. More details about this can be found in our
mitochondrial assembly paper:

https://doi.org/10.12688/f1000research.10545.1

## 2022-Apr-21

### Bacterial Sequencing

In my opinion, two important questions for bacterial genome assembly are:

* How precious is my sample?
* How much am I willing to pay to get sequencing done?

If samples are difficult to obtain, or money is tight, then it's
important to get the best performance out of a single flow cell. That
means taking more care with sample prep, quantifying input DNA
amounts, running a TapeStation/Bioanalyzer to work out size
distributions, excluding any potentially contaminating samples,
carefully balancing barcodes based on average fragment size, and
underestimating the total yield (i.e. the 24-genome scenario I've
described above).

If, on the other hand, samples are not precious and it's okay to run
multiple flow cells, a more haphazard approach can be carried out. Run
96 samples on the first flow cell, and set aside any samples that
reach the genome coverage threshold (e.g. 20X from >10kb reads). Run
the remainder of the samples on subsequent flow cells, continuing to
exclude any samples that exceed the coverage threshold from the total
reads from all sequenced runs, or any samples that are clearly not
going to hit their target after any amount of sequencing. This
approach will probably end up being more successful and cheaper per
sample, but requires more time, and more thinking / analysis between
runs.

### cDNA Sequencing

For differential expression from mouse cDNA with the PCR-cDNA
barcoding kit, I've been happy with samples that have >1M reads. We
can usually get this from a MinION flow cell when multiplexing six
samples together - our usual flow cell yield is 8-12M reads. I expect
results would be similar for human cDNA.

The most important thing for good run yield is to make sure RNA
extraction is carried out from fresh sample, and converted to cDNA as
quickly as possible. We have needed to play around with the number of
PCR cycles, so be prepared to experiment with a non-precious sample
first. I've found that sequencing works best when the library
quantification for each sample just prior to pooling is at least 20ng
/ μl.

## 2022-Apr-13

### cDNA kit ordering

For some odd reason, it seems that ONT only provides their RNA control
(RCS) in the PCR-cDNA barcoding kit, SQK-PCB109. I recommend people
purchase this kit instead of the PCR-cDNA sequencing kit for a
different reason - it has an identical sample preparation as
SQK-PCS109, but allows for samples to be multiplexed on the same run:

https://store.nanoporetech.com/productDetail/?id=pcr-cdna-barcoding-kit

## 2022-Apr-12

### cDNA Sequencing

The ONT per-base cost for PromethION flow cells is similar to a
NovaSeq S2 chip. ONT reads for cDNA average around 600bp for
well-prepared fresh samples, so assuming this is being compared to 50M
x 75bp single-end reads per sample, you would expect about 5M reads
per sample from a PromethION with a similar cost.

Something else that is worth bearing in mind is that the ONT kits
don't produce any intronic reads. These can occupy a substantial
proportion of Illumina sequencing runs, and have an impact on
transcript count estimation. From a base-level transcript coverage
perspective, ONT reads have a much flatter profile, which also helps
for quantification and isoform discovery.

When I do sequencing on MinION flow cells ($1000 USD / run), I aim for
at least 1M reads per sample. This is usually achievable without much
effort when multiplexing up to 6 samples per run with the PCR-cDNA
barcoding kit. I wouldn't recommend single-sample kits (e.g. PCS111),
because I haven't found much benefit for downstream analysis for over
1M reads per sample. ONT claims that their newer Kit14 kits should
have even better reliability (thanks to an increased sensitivity
adapter and the use of UMIs), so it might be possible to do 12 samples
from a single MinION flow cell with >1M reads per sample.

### cDNA Pore Blocking

We have found that freshly-extracted RNA converted as soon as possible
into cDNA produces the best cDNA runs (in terms of read length
distribution). Unfortunately this cDNA also blocks pores, which can be
demonstrated by an increase in the teal 'Unavailable' statistic in the
extended MUX scan plot. Doing a nuclease flush (i.e. using the ONT
wash kit) then library reload will usually fix this, but that requires
additional library kept for loading, and monitoring the flow cell to
see when the sequencing rate starts to drop off.

I find it a little strange that our amplified cDNA seems to block at
shorter lengths than the gDNA blocking reported by the community
(~10kb cDNA vs ~100kb gDNA).

It might have something to do with sequence similarity. If there's a
high-abundance transcript, it's more likely to pair up with other
copies of itself (we noticed this when doing our chimeric read
investigation, and in-silico chimerism seems to happen more often with
amplicon sequencing), which may lead to pore blocking.

## 2022-Mar-13

### Canu Amplicon Assemblies

Assembly failures with Canu are likely to happen if amplicon reads are
generated using the ligation kit. Canu expects that reads are
fragmented representations of the reference sequence, and has a
threshold beyond which overlaps will not be considered. I can't recall
what this is, my memory is that it's at least 50%, and could be as
high as 80%.

In any case, if all the sequenced reads are full-length, Canu will not
attempt an assembly using the default options. This will also happen
if most reads are full-length, there is a very high coverage of the
target sequence, and Canu is selecting the longest available reads.

The Canu documentation suggests some tweaks to help improve assembly
of amplicons, which tell Canu to stop assuming so much about the input
data:

https://canu.readthedocs.io/en/latest/faq.html?highlight=amplicon#can-i-assemble-amplicon-sequence-data

## 2022-Mar-03

### Gentle Loading on Flongle

It does not surprise me that ONT staff have a bias towards preferring
their own in-house developed techniques and protocols, which are
working great for experienced staff. I wouldn't mind the resistance to
community-driven change so much if the protocols had proper version
control, or if ONT explicitly allowed other people to create their own
version-controlled protocols.

In lieu of that, ONT should show the whole community the data: data
relating to gentle loading, and also for the glass vial testing that
has been done. I want to see multiple tests and error bars. If I can
be convinced that the pipette loading method is better, then I would
be interested in revisiting it.

I'm not interested in wasting time & money on method development
research for ONT, especially for something as fundamental as how to
load a flow cell. That's something ONT should have nailed.

The way I see it, given that complaints keep coming through on the
community about poor pore performance between initial QC and loading,
it suggests a problem with the *protocols*, not the users. There needs
to be some point (the earlier the better) where ONT steps outside
their bias bubble and thinks, "Hmm... maybe this protocol is not as
good, easy or clear as we think it is."

Within my own reality bubble I observed a difference when I was first
starting out using the Flongle flow cells, and have my own explanation
for why it improves flow cell loading. In addition, I have found the
SpotON-like gentle loading technique to be a simpler and more
failure-resistant process - it reduces forces on the flow cell matrix,
and also reduces problems with bubbles being introduced into the flow
cell. If your goal is to enable analysis by anyone, shouldn't a more
fail-safe technique be used as the standard approach?

As long as other community members keep saying that they're using the
dropwise gentle-loading method, and it works for them, I'll keep
suggesting other people try it.

## 2022-Feb-22

### A Mental Model for Nanopore Sequencing

The mental model that I have in my head of a flow cell is a dam with a lake /
reservoir, and holes in the dam that slowly drain the lake, spinning turbines
that produce electricity (i.e. current). The interesting bits in the lake are
the things going through the holes that *aren't* water, but the water flow is
the only thing that can be measured: there are no cameras attached to the holes,
only current meters attached to the turbines.

It just so happens that those non-water things have a strange enough shape that
they interrupt the water flow in large and predictable ways, which means that
observing the water flow can be used as a proxy for observing the things
themselves.

If the holes get blocked, nothing can move through the holes, so there is no
current flow [sequencing cannot happen]. In that case, cleaning the holes allows
the dam to work better, because water can once again flow through the holes and
generate electricity.

However, eventually that water reservoir will get depleted. If the reservoir is
fully drained [ionic balance of the carrier ions in the flow cell is equalised],
then it doesn't matter how clean the holes are; the dam will produce no more
electricity [sequencing cannot happen].

As an aside, this model helps me to understand why keeping the pores occupied
with long DNA prolongs the sequencing life of the flow cell. The flow of carrier
ions is reduced when a pore is actively sequencing, so the reservoir drains
slower, and can keep sequencing for longer.

## 2022-Feb-13

### Ampure XP Purification

If bead purification is done after adding the rapid adapter (as per
the Flongle protocol), that's a problem. It also could be a problem if
done before and there's still some ethanol hanging around in the
eluate. The sequencing adapters get destroyed by ethanol; that's why
different buffers are used after adapters are ligated or bound to the
prepared template sequences.

When using a rapid kit with a single sample of clean DNA (as is the
lambda DNA provided by ONT), sample preparation can be done without
doing any bead purification at all. I find with the rapid kit that
it's best to have all the purification and concentration done before
starting the ONT protocol.

## 2022-Feb-11

### Chimeric Reads

Every platform that uses ligation in sample prep has issues with
chimeric reads to varying degrees. The difference with nanopore
sequencing is that they can [usually] be easily detected and filtered
out.

As one example of what can happen, we were recently doing single cell
sequencing of ~2.7k cells, sequenced on a NovaSeq, but when I looked
at cell barcodes, I got over 100,000 unique cell barcode combinations
that matched an expected 27bp pattern (most with only one read). What
I think was happening is that a small proportion of reads from the
other run at the same time as our run were bleeding through into our
sequencing run. There has been a recent discovery of SARS-CoV-2
sequences in metagenomic samples from Antarctica in 2018 which is more
likely explained by barcode spillover, as the samples were sequenced
in China near the start of the pandemic.

We did an experiment a few years back that tested a few different
approaches, and couldn't find any way to get rid of the chimeric reads
prior to sequencing:

https://doi.org/10.12688/f1000research.11547.2

The best way to get rid of chemically ligated reads completely for
nanopore sequencing is to not use ligation. In other words, use kits
that use the rapid adapter (e.g. rapid PCR barcoding kit, rapid
barcoding kit).

I've found that chimeric reads are really difficult to split properly
every time. For many instances, the splitting can be done by finding
adapters and splitting near them, but that doesn't always work. Some
sequences might be joined half-way through the sequence, others might
have no adapters at the join points.

For me, I try to detect them as best as possible when demultiplexing
reads, and exclude any reads that get mapped to multiple barcodes. As
there doesn't seem to be any systematic reason for chimeric reads,
excluding them only impacts total yield (and it's only a few percent
yield loss).

I use LAST for chimeric read detection. I don't have a fully-automated
workflow, but it's quick enough for me to copy-paste and change a few
bits as necessary. The protocol I've written up to demultiplex and
exclude chimeric reads is here:

https://dx.doi.org/10.17504/protocols.io.b28fqhtn

## 2022-Feb-10

### DNA concentration for Rapid Sequencing

Starting DNA concentration for the rapid kit depends on how many
adapted molecules are going through the pores; the longer the
sequences are, the more mass is required to make up the numbers.

It's frustrating to me that ONT don't mention ideal molarity for their
rapid sequencing kits (at all), but there is information in the LSK
protocols [i.e. <1 µg (or <100–200 fmol)], which should work well as a
reasonable first guess. There are tables comparing different
length/mass combinations with different fmol amounts on the QC pages:

https://community.nanoporetech.com/protocols/input-dna-rna-qc/v/idi_s1006_v1_revb_18apr2016/assessing-input-dna

Many people have recommended the Promega calculator if you want to
look at a specific length or mass:

https://worldwide.promega.com/resources/tools/biomath/

My rule of thumb is that at 1.5kb, ng is approximately the same as
fmol. I do rough estimates from there (e.g. at 15kb, fmol is about
1/10 of ng amount).

If there's no way molarity can be estimated, even approximately, then
my recommendation is to keep concentrations around 20-100 ng/μl; that
seems to work well for most applications.

## 2022-Feb-05

### Rapid Barcoding Kit

The RBK110.96 is a very new kit (I think it's only been available for
a month or so on the store), and we haven't been able to test that out
yet.

With the RBK004 kit, we typically get an assembleable amount of
plasmid sequences for up to 6 samples (loaded at approx 20 ng/μl)
coming out of the sequencer off a Flongle flow cell after about 2
hours of run time. We've done one run with 12 samples; it took about
double that length of time (i.e. about 4 hours). I guess that'd be
10-20 mins for a MinION flow cell. Apparently the RBK111 kits coming
out in the next month or so will be better.

Median size / N50 / etc. all depends on the input DNA. Most plasmid
reads that come off the sequencer are single cuts (i.e. full-length
plasmid, with a bit of the ends trimmed off).

### Live Sequencing Demonstrations

A prepared library is a great way to do live sequencing
demonstrations. Purified / concentrated DNA works very well with the
rapid barcoding kit.

My first public demonstration (in 2016) was with frozen tomato that I
extracted at home, then purified and prepared in the lab using a Zymo
concentration kit and a ligation sequencing kit (which was the only
kit available at the time). Sequencing and basecalling [was a bit
slower](https://www.youtube.com/watch?v=yTgZVVHJMlw&t=606s) back then;
I don't recommend using a slow wireless connection for remote
basecalling and competing with a hundred other people for Internet
access.

In 2019 I attempted a live extraction and sample prep from a salad
(with local basecalling), but mixed it in with a sample I'd prepared
earlier (I think it might have been tomato and yoghurt), and the
prepared sample is [what ended up coming out first from the
sequencer](https://www.youtube.com/watch?v=CHCAb-PAqUI&t=2605s).

Another option is having a prepared flow cell, fully loaded. This is
the best option if you want to guarantee (as much as possible) that
the demonstration will work. Which I did when I was carrying out the
first public sequencing run [on the Linux
version](https://www.youtube.com/watch?v=8K_O1aPM-WI&t=2490s) of the
basecalling software, because there's plenty of other things that can
go wrong.

... I really need to do clipped cuts of those videos; there's a lot of
waiting involved in live sequencing.

Regardless of what you decide to do, I recommend having lots of
backups. For my 2019 LinuxConf talk I had a prepared sample, but I
also brought along a loaded flow cell as a backup, and I also had a
full flowcell snapshot of a yoghurt run I did about a week earlier (so
I could simulate a sequencing run), and also had the sequencing
results stored from when I did that yoghurt run (yoghurt + Zymo clean
& concentrate + rapid barcoding kit).

## 2022-Feb-04

### Flow Cell Yield

I've realised that part of the misunderstanding about flow cell yield
may come from experience with Illumina sequencing from large service
centres, where sample preparation is carefully managed to produce a
high cluster density, meaning that the output from different Illumina
sequencing runs ends up being very similar (and close to Illumina's
reported maximum yield).

As a real-time "sequence anything" device that is targeted more
towards individual labs than experienced service centres, it's not
unexpected to see large variation in the performance of MinION across
different labs depending on sample preparation experience, sample
type, sample quality, run time, and flow cell quality.

For the runs I've done, I've found that I get the best estimate of
sequencing yield by looking at how a flow cell is performing in its
first hour, and multiplying by 24 to approximately work out yield over
a two-day run. That's because I've noticed (at least with 109 kits
with R9.4.1 flow cells) that I get about 50% of the ideal performance
over the course of a two-day run.

But there's a lot of variation in that number. The numbers I'm most
familiar with are our cDNA runs, and we typically get around 4-12
million reads with average read length of about 500-1000 bases
(i.e. 2-12 Gb per run). I think our highest yield for cDNA has been
something like 15 Gb and 18M reads.

Other people have found that HMW DNA will very quickly block
sequencing pores, reducing the yield. There are a lot of tricks to
work around this (e.g. nuclease digest, needle shearing, buffer
tweaking), and it can be a challenge to find information on what can
go wrong and how to fix it.

I recommend that new users to nanopore sequencing follow ONT's
recommendations and start off with a lambda run, so they can at least
know what a good sequencing run looks like. The lambda DNA provided in
ONT's control kit is clean and has a reasonably long length (but not
long enough to block pores), and works well with most of the sample
prep kits that ONT offers.

Knowing what a good run looks like can help users to notice a bad run
in the first few minutes of sequencing, stop the run, re-prepare
library, and rescue a flow cell, saving thousands of dollars. It's
definitely worth it to put in that little bit of extra effort before
jumping into the deep end of the flow cell.

## 2022-Jan-28

### cDNA Sequencing

Downstream bioinformatics will be easier and faster when using one of
the ONT cDNA kits. I recommend the PCR-cDNA barcoding kit (and
associated external reagents), because it allows strand-specific
analysis of multiple samples on one MinION flow cell. With the
existing kits, I prefer people to do no more than six samples per flow
cell, aiming for >1M reads per sample. It may help to do a Flongle run
on the same library first to get barcode balancing correct for the
MinION run.

Despite having done this for years, we're still struggling with
optimising our cDNA sample prep. I'm hoping that the new cDNA kits
[SQK-PCB111.12] will fix a lot of the issues we've been having.

I don't recommend using LSK/NBD on cDNA because it loses the advantage
of strand-specific sequencing, which makes it more difficult to
distinguish between sense and anti-sense RNA.

## 2022-Jan-27

### Base Calling on PromethION P2

I think it's probably worth noting that the effect of using a
less-capable GPU with a P2 Solo (without adaptive sampling) is that
the basecalling component of the sequencing will take longer. The
sequencing should still proceed and finish, and because calling during
the run uses a sample of the most recent reads, the calling in MinKNOW
should still be a real-time representation of the current state of the
flow cell (useful for monitoring Q scores, barcode distribution,
translocation speed, etc.).

What this will mean in a practical sense is that there may be a
basecalling delay after sequencing is finished (depending on how
productive the flow cell is through the entire course of its
run). From what I've seen on Twitter, non-ONT PromethION users are
currently getting about 200Gb of reads from good runs, which is about
ten times the output of good MinION runs. So if a GPU is only just
able to keep up with a 2-day MinION run, then the basecalling delay
will be a bit over two weeks for a 2-day PromethION run (from a single
flow cell).

Miles Benton has indicated that an RTX3090 should at least be able to
keep up with 5 MinIONs at once, in which case the basecalling delay
from a single PromethION run should be *at most* about 2 days (but it
will likely be much less than that).

Adaptive sampling is another matter, though, because it requires that
basecalling can keep up with sequencing in order to work properly. It
wouldn't surprise me if adaptive sampling on a P2 won't be practical
until there's another order-of-magnitude improvement in calling or
sampling (e.g. an efficient squiggle-space adaptive sampling that
doesn't require basecalling).

### Adaptive sampling and barcode proportions

MinKNOW will be able to show you a very good approximation of barcode
distribution even if it can't sequence everything in real time. I've
found that barcode distributions after 20 mins of cDNA sequencing are
a very close match to the distribution from a two day run (I remember
something like about 2% difference last time I checked). It will also
tell you how many reads are being [temporarily] skipped, so it's
possible to do a bit of calculations to get an estimate of the
sequenced counts per barcode based on the basecalled counts.

The benefit that adaptive sampling gives is that the remainder of
rejected reads are not sequenced at all by the sequencer
(i.e. discarding all but the first ~500bp), which can greatly increase
the quantitative throughput of the sequencer (with more benefit the
longer the average read length is), and increase the sequencing time
spent on reads that are not rejected. These advantages will not be
gained for post-hoc analysis after the run has finished, because the
accept/reject decisions are made at the time of sequencing.

## 2022-Jan-07

### Flongle as a Sanger competitor

For amplicon sequencing, the cheapest option is to process amplified
PCR products (>500bp) with the rapid barcoding kit. Flongle works well
for both long amplicons and plasmid DNA, and can be cheaper per sample
than Sanger sequencing, especially when sequencing different amplicons
in the same run with the same barcode:

* Flongle flow cells $810 / 12 flow cells
* Rapid barcoding kit $650 / 12 Flongle runs, 12 samples per run

So that works out to ~$12 per sample (consumable cost only, excluding
shipping). As long as amplicons or plasmids attached to the same
barcode have distinct sequences (e.g. different genes), the reads can
be split out in software after sequencing. I reckon 4 amplicons from
12 samples would still work well on a badly-performing Flongle, which
would work out to be $3 per amplicon. This seems to me to be
competitive with Sanger, especially if both forward and reverse
sequences are needed.

## 2022-Jan-06

### Adapters and molarity

There are molarity effects that come into play when attaching
adapters. Smaller molecules have less mass on a gel for an equivalent
total molecule count, so will appear fainter. This makes it difficult
to work out precisely how many small molecules there are, and counts
will usually be underestimated for small molecules. This matters when
adding in adapters, because the smaller molecules, due to their sheer
numbers, are going to take up the majority of adapters. This is why
it's important to use bead cleanup (or similar) to filter out small
template sequences prior to attaching adapter sequences (assuming the
resulting bias is acceptable).

## 2021-Dec-23

### Metagenomic sequencing

Have a look at the methods for the MinION Analysis and Reference
Consortium's metagenomic sequencing paper:

https://academic.oup.com/gigascience/article/9/6/giaa053/5855463#204848914

We used a 1.2 μm filter to remove suspended solids, then a 0.22 μm
filter for capturing microorganisms. The paper describes a bespoke
enzyme cocktail used with the DNeasy PowerWater DNA Isolation Kit, but
some of the consortium found that Sigma MetaPolyzyme works well as
well (with the same kit):

https://www.sigmaaldrich.com/US/en/product/sigma/mac4l

I used a 3D-printed filter with an attached 10mm vacuum tube for my
filtering. It has a base that screws onto a GL45-threaded bottle
(e.g. the blue-lidded bottles that are commonly used in the lab), a
hexagonal grid that sits on top of the base (to support the filter),
and a funnel that screws onto the top. It also needs a 10mm push-fit
tube connector (or similar) for connecting up the vacuum:

https://www.prusaprinters.org/prints/79389-vacuum-water-filter

Because we were doing rapid PCR barcoding for sequencing, we weren't
too concerned with breaking up the DNA with the PowerWater kit. It may
be necessary to modify the preparation process if reads longer than
1-2 kb are needed.

## 2021-Dec-17

### Sequencing Duration

Nanopore sequencers will keep going for as long as you let it run; the
user is in control of deciding when to end sequencing. It'll even keep
trying after actively sequencing pores drop to zero.

The nanopore sequencers work a little bit like a battery (or maybe a
capacitor): they have more capacity for sequencing at the start of the
run, and sequencing gradually slows down over time.

If you want to get the best sequencing performance
[i.e. fastest time to results], then you can run the flow cells for a
short period of time (e.g. 6 hours), and switch over to a new flow
cell at the end of that. If you want to squeeze out every last drop of
sequencing capacity from the flow cells, you could run the flow cells
until the number of available pores drops to zero (possibly with some
flow cell washes in the middle to clear away some gunk that clogs the
pores during the sequencing run).

Run time depends on application: a confirmatory sequencing of a PCR
product could be done in less than half an hour, whereas a human whole
genome sequencing run could take a few flow cells and a week's worth
of sequencing (depending on sample preparation ability).

### Small samples

It's very unlikely that libraries loaded into the flow cell will
deplete completely before the flow cell stops being able to sequence
anything more. The flow cell just doesn't have the sensitivity to pick
up and sequence everything that it's given.

In addition, running a flow cell on empty (i.e. with nothing
sequencing) is a quick way to run it down, because the sequencing
capacity depletes quicker when the pores are unoccupied. It's almost
never a good idea to load low inputs onto a flow cell. If there are
fewer than 100 reads after 10 minutes of sequencing, it's much more
cost-effective to stop the flow cell, work out what went wrong, and
load a better library next time.

If it's not possible to get away from a small sample, a better
approach is to load the sample together with a long, high quality DNA
library (e.g. ONT's lambda control sample). This will keep the pores
occupied, allowing more sequencing time for picking up the rare
sample. After sequencing has finished, the sample reads can be fished
out in software by discarding any reads that match the lambda genome.

## 2021-Dec-16

### Sample preparation

Performing an additional QC (tape station or gel) is a good idea,
especially when it's the first attempt on a new sample
type. Understanding the relationship between the library length and
the sequenced read length is useful for improving yield.

I've found that 20-100 ng / μl starting concentration seems to work
well for sequencing. Most often we're sequencing DNA from plasmids
5-12 kb, which has a very tight length distribution from a single cut
from the transposase enzyme.

If you've got plenty of library, only add in what you need for good
sequencing. It is possible to give the adapter binding process too
much template, and to overload flow cells, so pay attention to the
fmol recommendations in the ONT sample preparation guides.

If you do have enough library for reloading, consider doing a quick
initial run (e.g. 10-30 minutes) to check barcode proportions. After
looking at the results from that quick run, you can rebalance the
barcodes via a second adapter preparation and library load (i.e. add
more of the samples that are under-performing).

## 2021-Nov-18

### Loading Flongle flow cells

Here's my current approach for loading Flongle flow cells using a P200. I try to avoid pipetting directly into the loading port:

1. Unseal the flow cell
1. Add tape to waste port B
1. Drop 25 μl of flush solution onto the loading port to form a dome
1. Place pipette into loading port and dial up about 5 μl to check for bubbles (dial up to remove bubbles if they exist)
1. If liquid is not dropping from the loading port, place pipette into waste port A and dial up 20 μl or until liquid starts dropping from the loading port
1. Set pipette to 20 μl, press down while in mid air to expel air, then place into waste port A and slowly release the plunger. If liquid starts dropping from the loading port, stop releasing and lift up the pipette.
1. Repeat step 5 with a faster release speed until liquid starts dropping from the loading port

Occasionally I notice bubbles at the loading port while I'm in the middle of doing this, in which case I return to step 3.

Just before I'm ready to load library, I do the following:

1. Remove the tape from waste port B
1. Drop another 30 μl of flush solution onto the loading port, wait for it to drain through
1. Drop 30 μl of library onto the loading port, wait for it to drain through
1. Re-seal the flow cell by rolling my finger across the plastic adhesive cover, trying to avoid putting pressure on the flow cell matrix

## 2021-Sep-18

### Sequencing with low read count / yield

For low *concentration* sequencing, the best thing to do is spike the
sample into a lambda DNA run (e.g. 10% sample, 90% lambda). The lambda
will keep the pores occupied, increasing sequencing yield for the
remaining samples.

The MinION is a high-throughput sequencing device. Flongle flow cells
have an output of 200~2,000 megabases; MinION flow cells have an
output of 2,000~20,000 megabases (mostly dependent on sample prep
experience). If you only want a few reads, then use Sanger sequencing.

### Sequencing with low cost (per sample)

For a single sample, the cheapest possible sequencing cost is $125
(rapid sequencing kit + Flongle flow cell).

But when running multiple samples on the same flow cell, getting below
$20 USD / sample is easily possible. As one example, the standard
[rapid barcoding
kit](https://store.nanoporetech.com/rapid-barcoding-kit.html) allows
multiplexing for up to 12 samples on a single run, so that'd be $10.80
per sample on a Flongle flow cell. We use this kit frequently for
plasmid sequencing and assembly, getting >100X coverage from a Flongle
run after a few hours of sequencing. It also works well for PCR
amplicons that are longer than ~300bp. Flongle flow cells are trickier
to get working, but there's a huge cost saving once that process is
worked out.

If the sequences are easily distinguishable (e.g. different gene
targets), then inputs can be combined without multiplexing, because
the different bits can be fished out in software after sequencing. The
only limit to that is the output of the sequencer. You could sequence
amplicons from a hundred different genes in 12 samples for the same
cost as a single gene in 12 samples (with a hundredth of the yield per
gene). Twelve targets from 12 samples on a Flongle flow cell would
work out to a little under a dollar per target.

There is also now a [96-barcode rapid
kit](https://store.nanoporetech.com/rapid-barcoding-kit-1.html), which
can do 6 runs of 96 barcodes on a MinION flow cell (or 12 runs of 96
barcodes on a Flongle flow cell) for $990, so using a bulk-purchased
$500 MinION flow cell that works out to be $7 USD per sample, or $11
per sample on a single-purchased $900 MinION flow cell.

This is all out-of-the-box sequencing; no scaling needed.

Getting a bit more technically challenging, it's possible to also do
combinatorial multiplexing, using up to 96 PCR barcodes [kit cost $170
per run] in tandem with up to 96 ligation barcodes [kit cost $100 per
run], i.e. 9216 samples in one run. Calculating costs is trickier with
that, because there are per-sample costs associated with external
reagents required for the ligation kits.

## 2021-Sep-04

### Plasmid Sequencing

I follow one piece of advice that @Divya Mirrington gave me about
pooling: create a pooled sample with volumes that you're confident
about, then remove an aliquot from that for adding the remaining
reagents [paraphrased]. I don't do any additional cleanup for purified
plasmid DNA; they tend to sequence very well on flow cells without
that cleanup.

My key requirement for plasmid sequencing is a concentration of >20
ng/μl (ideally by qubit or quantus). Concentrations over 100 ng/μl
should be diluted down. If the plasmids can be diluted down to all
exactly the same concentration (but at least 20 ng/μl), or they're all
similar lengths, then that makes creating an equimolar pool much
easier.

When creating the pools, I add at least 1 μl of the the sample that I
need to add the least for (might be more if the total volume is less
than 11 μl), then add the corresponding amount of other samples to
create an equimolar pool. I then take 11 μl from the pool to be used
for rapid adapter addition.

**If samples are equal concentration:**

Add amounts according to the length of the plasmid divided by the length of the shortest plasmid. For example, if there are two plasmids, one with length 3kb and another with length 35 kb, then add 1 μl of the 3kb plasmid, and 35/3 = 11.7 μl of the 35kb plasmid.

If plasmids are roughly equal length (i.e. less than ~10% length
difference between plasmids):​

Add amounts according to the concentration of the
highest-concentration sample divided by the concentration of the
plasmid. For example, if there are three plasmids, one with
concentration 50 ng/μl, one with concentration 35 ng/μl, and one with
concentration 20 ng/μl, then add 1 μl of the 50 ng/μl plasmid, 50/35 =
1.4 μl of the 35 ng/μl plasmid, and 2.5 μl of the 20 ng/μl
plasmid. The total volume of this pool will be less than 11 μl (1 +
1.4 + 2.5 = 4.9 μl), so in this case I would triple these volumes (3
μl; 4.2 μl; 7.5 μl) to create a pool of > 11 μl.

**If samples are different concentrations and different lengths:​**

Make the sample prep easier. Use multiple flow cells for different
plasmid length ranges. Dilute higher-concentration samples down to the
lowest-concentration samples. I don't recommend trying to do both
calculations at the same time to determine added volumes because
there's a much higher chance of getting added amounts wrong, leading
to wasted samples or wasted flow cells.

**If you have a sufficiently-accurate pipetting robot, a sample sheet,
and someone who is comfortable with equations:​**

Pre-calculate amount to add assuming 12 μl total pool volume:

*ratio* = *length* / max(*length*) * max(*conc*) / *conc​*

*volume* = *ratio* * 12 / sum(*ratio*)

[That's my guess at the right equations; please let me know if there's
an error]

## 2021-Aug-11

### pore blocking

My own experience is that blocking starts becoming a problem when
reads are over ~20kb. Nuclease digest and reloading is essential for
getting decent reads once there are reads over 100kb.

If long reads aren't necessary, but they exist in the sample, pipette
or needle shearing can be used to reduce read lengths prior to adapter
ligation (e.g. 1000 μl pipette tip, 50 times).

My current hypothesis is that the physical length vs the sequencing
well size plays a role in this. Sequencing wells are 100μm across,
which works out to about 300kb as a stretched-out linear strand (or
150kb from edge to centre). Any reads longer than that need to bunch
up just to get inside the sequencing well, let alone navigate into the
pore. I've noticed that DNA that is more repetitive, especially when
there are lots of reverse-complement repeats, tends to be more prone
to blocking the pores.

## 2021-Jun-15

### cDNA sequencing

If it's your first cDNA run, you want it done yesterday, and have
extracted RNA sitting on the bench waiting to be converted for
sequencing, it might be worth having a think about that. Note that
it's typical for yield and quality from the first nanopore runs of any
new sample to be lower than what is obtained after gaining more
experience in sample prep; it'd be worth it to give yourself some
breathing space for testing out cDNA sequencing prior to jumping
all-in.

Even if none of that's the case, downstream bioinformatics will be
easier and faster when using one of the ONT cDNA kits - I recommend
the PCR-cDNA barcoding kit (and associated external reagents), because
it allows strand-specific analysis of multiple samples from one MinION
flow cell.

If you're not using the nanopore cDNA kits, you'll still need at least
whole-transcriptome amplification reagents. Assuming you have those,
you could probably get away with feeding the amplified products from
each sample into the rapid barcoding kit, bearing in mind that this
will exclude short transcripts, will probably have isoform detection
issues due to shorter sequences, and may have other systematic errors
that will be difficult (and time-consuming) to work around during
downstream analysis.... And you'll probably need to create your own
data analysis pipeline, because long nanopore reads don't work well
with short-read workflows, and most long-read workflows expect
full-length transcripts.

Another alternative would be using a ligation kit on the amplified PCR
products, which would substantially reduce length-based issues. As
with the rapid barcoding kit (or any non-cDNA kit, for that matter),
you won't be able to do any stranded transcript analysis, which makes
it more difficult to distinguish between sense and anti-sense RNA.

## 2021-Apr-20

### Indigenous management of data

While I do agree that open sharing of data is a good ideal, I don't
think this should be a hard requirement.

It's not great to take control away from people who don't have much
control to start with. Many indigenous groups consider themselves to
be guardians of endangered species, and have been historically given
little control over what is done with the outputs of research on the
things that they want to protect.

To further demonstrate the issues around this, perhaps it's helpful to
point out the apprehension that ONT has around releasing their sample
preparation protocols publicly (especially including the release of
old, obsolete protocols), or the concerns Clive Brown has had in the
past about releasing his own genome and/or draft data without
restrictions.

There is some more information about indigenous management of
materials on the Local Contexts website. This is centred around the
idea that there should be metadata tags applied to objects to
recognize that there could be accompanying cultural rights, protocols
and responsibilities that need further attention for future sharing
and use of materials:

https://localcontexts.org/

ONT have responded to this critique by commenting that exceptions to their rules would be considered on a case-by-case basis. The problem with such an approach is that subjective filters tend to favour privileged people, possibly because they have the familiarity with how other privileged people expect the world to work. By stating that public release of data is a 'must have' requirement, people who don't feel comfortable with open sharing of their data are less likely to apply.

Saying something along the lines of "you can ignore some of the rules, and we'll work through that" is not a helpful response to the issue. It creates another additional class of applicants that could be pulled into the project (i.e. those who don't mind ignoring rules in order to achieve their goals) but I'm not convinced they wouldn't have applied anyway.

I liked the approach ONT took with the first MinION Access Program of, essentially, anyone who could write an abstract ended up with a MinION (unless they were a competitor). It would have been nice to know that in advance, in which case it would have been a great example of an ideal application filter: a low bar with consistent, declared requirements.

Questions that are asked on the registration form for Org.one seem like informational questions rather than application filters, which is preferred; this nicely matches my remembered experience of other Nanopore project applications (e.g. the initial MinION Access Project, where pretty much anyone who could write an abstract was accepted into the program).

## 2021-Mar-06

### Modified bases

The fields from the modified base table in the fast5 file correspond
to the likelihood of a modified base, according to the methylation
model specified in the fast5 file.

"The contents of the table are integers in the range of 0-255, which
represent likelihoods in the range of 0-100%"

https://community.nanoporetech.com/protocols/Guppy-protocol/v/gpb_2003_v1_revv_14dec2018/modified-base-calling

Note that these might be inverted from what you would expect,
depending on how you interpret "likelihood". The explanation for
methylated As might help:

"Given that an A was called, the likelihood that it is a canonical A
is ~25% (63 / 255), and the likelihood that it is 6mA is ~75% (192 /
255)."

Also note that these are conditional likelihoods; it's necessary to
look at the called base (which isn't included in the table) in order
to properly interpret the fields.

FWIW, I've written a script to convert the probabilities from FAST5
files into modified sequences. Using the new 5mc model, it will
convert methylated Cs into a lower-case 'm'. It will check to make
sure the predicted modification matches the called base, and has a
probability threshold above which a methylated symbol is emitted:

https://gitlab.com/gringer/bioinfscripts/-/blob/master/multiporer.py

[Note: this python script needs h5py and numpy]

Usage:

    ./multiporer.py ~/scripts/multiporer.py fastq reads.fast5

or

    ./multiporer.py ~/scripts/multiporer.py fastq read_directory

## 2021-Jan-27

### Rapid Barcoding

If you're sequencing from purified mtDNA or long amplicons (e.g. 8kb *
2, or longer than 1kb, for that matter), the rapid barcoding kit works
really well.

The rapid barcoding kit produces a higher yield with longer sequences
(i.e. >10kb), but the yield doesn't matter as much when sequencing
amplicons, plasmids, or other similar short regions. I've found the
transposase fragmentation to be essentially random for amplicon sizes
down to 600 bp (possibly shorter than that as well).

Reads will be fragmented (because that is required for the adapter
attachment to work), but I haven't found read fragmentation to be an
issue when looking at assembly and/or variants. As long as the yield
is sufficient, the main difference (from a downstream analysis
perspective) will be fewer chimeric reads formed during the sample
preparation process.

## 2020-Dec-13

### Bubbles

The flow cell is effectively an electrolytic cell, where a voltage
applied to a current-carrying solution will cause the breakdown of
molecules in the solution (including the breakdown of water into
gaseous hydrogen and oxygen). There is also some release of dissolved
gases in the solution due to the flow cell heating up.

ONT have previously done all they can to reduce the dissolved gases in
the flowcell, but given that this is a normal chemical process, I
don't think there'll be a way to completely prevent it from happening.

As long as the bubbles stay small and float above the sensor array,
there shouldn't be any loss of capability. If they get bigger and
stick to the bottom of the array, then there'll be an obvious drop in
available sequencing channels where the bubble is (on the channel view
in MinKNOW). The best thing to do in that case is to leave the bubbles
where they are until sequencing is finished; bubbles that move around
the flow cell will destroy more pores.

## 2020-Dec-01

### Plasmid sequencing

Plasmid sequencing is very easy. I ask people to prepare / purify
their plasmid DNA to a concentration of at least 20 ng/μl, and run
that product through the rapid barcoding kit.

I recommend that people get a rapid barcoding kit rather than a rapid
sequencing kit - I find it a bit odd that ONT still offer the rapid
sequencing kit. The preparation process is identical, but you get
12-sample multiplexing for six runs for $50 more.

I like running until I get 200X coverage in the least-covered sample,
which usually happens in under half an hour on a MinION flow cell, or
2 hours on a Flongle flow cell.

Here's a video of me attempting to do this demultiplexing protocol for
my LinuxConf talk using a 200μl pipette:

[Sequencing DNA with Linux Cores and Nanopores](https://www.youtube.com/watch?v=CHCAb-PAqUI&t=19m56s)

### Flongle reloading

Flongle reloading is theoretically possible, but the flow cell
construction makes it challenging to do.

The Flongle flow cell had essentially a combined priming / SpotON
port, and two waste ports around the outside. The input port is close
enough to the flow cell matrix that positive pressure from that port
has a good chance of damaging the flow cell matrix. For cycling liquid
through the flow cell, it needs to be dropped onto the input area to
make a large meniscus, then pulled through the waste ports (one waste
port might need to be taped up for that to work).

## 2020-Nov-20

### cDNA Sequencing

RNA-seq is a slower process compared to cDNA-seq (about 1/4 of the
speed), can't be amplified, and is a bit more fiddly to work with
(reads are sequenced backwards, RNA modifications are present in the
signal).

1-2M reads for one sample would be fine, although on a single flow
cell it's a little expensive compared to what *could* be possible.

I prefer the PCR-barcoded cDNA sequencing kit. Our first cDNA runs
were 1-2M (a few years ago now), but we're getting more reasonable
yields now, and I expect yields will get better once the adapter ATP
fix moves through the rest of the kits.

My aim is >1M reads per sample; we can usually get that for most, if
not all samples, especially if we do a 20min run with half the library
at the start in order to gauge barcode distribution, then rebalance
samples based on those results.

I prefer doing around six samples per run, ideally with similar sample
types; that seems to be a good sweet spot for high yield. If there are
12 samples prepared as one sequencing library, my recommendation is to
run the six best samples first, stop the run when the yield is
sufficient, do a nuclease flush, then run the remainder. This ensures
that the pores are kept occupied, which increases the overall yield.

## 2020-Jun-12

### Research paper - metagenomic sequencing

Our multi-continent river metagenomic sequencing paper is finally
published, see here:

https://doi.org/10.1093/gigascience/giaa053

This is international research (involving river sampling from three
continents) that has been over four years in the making. A lot of time
was spent on adapting to a rapidly-changing technology, organising and
distributing kits and protocols, basecalling and shifting data around,
and finding a good way to report results from a diverse range of river
systems throughout the world.

If you're wondering why there's something we didn't do, the most
likely answer is, "Because the paper was big enough as it is." We
enthusiastically encourage others to explore our dataset and carry out
additional follow-up work; raw signal FAST5 and FASTQ files are
available from ENA via accession [PRJEB34137 / ERP116996](https://www.ebi.ac.uk/ena/data/view/PRJEB34137).

Thank you very much to the amazing international team who has helped
with river sampling, sample preparation, data analysis, manuscript
writing, and additional editorial work to get this marathon research
over the finish line. And also, many thanks to Oxford Nanopore for
supporting this project (in particular, Rosemary Dokos and Alina Ham),
to the Cloud Infrastructure for Microbial Bioinformatics (CLIMB)
service in the UK for facilitating the upload and transfer of raw
FAST5 files, and to the Oxford Nanopore community for the many and
varied discussions around sequencing techniques, troubleshooting, and
technological frustrations. It's a big relief to finally be able to
say, "We did it!"

For those interested in replicating our process (which will hopefully
take a few weeks, rather than a few years), here's the method blurb
from our paper (paraphrased / slightly shortened):

> Water samples were taken at 0.5–1 m depth during daylight hours at a
time when neither drought nor recent excessive precipitation events
occurred within 1 week preceding sample collection. River water (2–4
L) was collected for filtration in sterile collection bottles and was
processed immediately or stored at 4°C for prolonged transportation
time or until ready for filtration.

> The water samples were subsequently processed through a GF/C filter
to remove suspended solids, particles, etc. (size retention: 1.2
µm). The water recovered after GF/C filtering was subsequently
filtered through a 0.22-µm Durapore filter to capture microorganisms
present. Upon completion of all filtering, nucleic acid was recovered
using a modified procedure combining enzymatic lysis and purification
using a DNeasy PowerWater DNA Isolation Kit.

> Subsequently 10–50 ng of DNA was used in conjunction with the Rapid
Low Input by PCR Barcoding kit (SQK-RLB001, Oxford Nanopore, Oxford,
United Kingdom [now Rapid PCR Barcoding kit, SQK-RPB004]) in
accordance with manufacturer's protocols to prepare WGS libraries for
use with a MinION device, with minor alterations
[2.5μl FRM; 20 cycles PCR; PCR reaction volumes doubled to 100μl]. The
prepared library was then loaded into the MinION flow cell (R9.4
[now R9.4.1]) in accordance with manufacturer's guidelines and the
unit was run for a full 48 hours of sequencing.

> Whole Genome Sequencing reads were processed for basecalling and
quality control filtering. Reads were processed using Kraken2, and
also uploaded via the command-line API and processed using
MG-RAST. Pavian plots of the representative taxa for each metagenome
were constructed using the Kraken2 output. Using both MG-RAST and
Kraken2 taxonomy results and the Bray-Curtis distance matrix among
normalized family counts, PCA was implemented. To evaluate putative
ecosystem-related functions from the reads, the MG-RAST server was
used to compare data sets to 3 controlled annotation namespaces:
Subsystems, KO, and COG proteins. Normalized function data for each
river sample were compared using PCA in MG-RAST.

Ngā mihi nui [thank you very much],

 - David Eccles

### Pore Depletion

I consider pore depletion over time to be normal flow cell
behaviour. The way it was explained to me is that the flow cell works
a bit like a battery, in that it becomes less efficient over time.

Nanopore sequencing depends on the ability to measure a flow of ions
moving through the pores that are embedded in a polymer
membrane. Given that there's no way to get ions back into the loading
side of the pores, the difference in ions between the two sides will
reduce over time. An increase in the magnitude of the voltage applied
across the membrane can compensate for this a little bit, but
eventually the difference will be so low that ions won't budge
regardless of the applied voltage. And without a flow of ions, there's
no sequencing.

Assuming this, the best way to extend the life of the flow cell during
sequencing is by reducing the amount of small ions that cross the
pores, and the way to do that is to keep the pores occupied. The more
actively sequencing pores, the slower a flow cell will deplete,
because the flow of "carrying ions" is reduced while long polymers are
moving through the pore. Every time a pore unloads and loads again
with a new sequence, there'll be another rush of carrying ions, so
reducing the number of pore reloads by using longer template polymer
sequences will also increase the life of the flow cell.

A flow cell starting with about 65% loading of actively-sequencing
strands at the start of the sequencing run means that about 35% of the
available pores are unoccupied, and are acting as big holes for lots
of carrying ions to move through. When this is noticed, increasing the
initial library concentration, or loading more concentrated library,
might help to reduce the depletion of the flow cell in such a
situation.

**Follow-up comment**

My knowledge of this has been acquired from discussions with ONT staff
during London Calling, and also a presentation that Divya Mirrington
gave during a workshop in Wellington last year (who explicitly
mentioned and clarified the ion flow model, which led to an "aha"
moment for me). I think the battery concept may have been discussed in
Clive Brown's presentations as well, but unfortunately I haven't
written it into any of my notes.

... there's also this, mentioned in the Wash kit announcement:

> Please also note that the common voltage drifts in the course of a
> run due to the depletion of the redox chemistry in the nanopore
> array. If you are reusing a Flow Cell that has already run for a
> certain amount of time, it is advised to adjust the starting
> potential to maximise the output.

And, looking again now, some further mentions on the community of the
flow cell being a bit like a battery.

The main purpose of refueling is to provide energy (in the form of
ATP) for the helicase to unwind and ratchet the DNA. It might provide
a few more ions, but given that the trans side is not accessible,
there will always be an increase in electrical resistance over the
course of a run.

With regards to the statement about inactive pores, it really depends
on how they're inactive. A decrease of the bright green to dark green
proportion over the course of a run might be associated with a loss of
available energy, in which case refuelling might work to improve the
sequencing rate. If that light-green:dark-green ratio remains
constant, but the number of "Unavailable" and/or "Out of Range" pores
increases, then it would suggest to me that the pores are getting
blocked, in which case a nuclease flush (and library reload) could
improve sequencing rates. However, sometimes neither of these
situations are happpening, and the apparent issue is an increase in
the count of "No pores from scan". This could also be happening due to
blocked pores, but I expect it will also happen if there's no current
flow for other reasons (e.g. ion buildup on the trans side, pore
damaged by chemicals). Sometimes the membrane will get damaged as well
and let too much current through, in which case the pore reading
becomes "Saturated", and is also unusable.

If you're looking for a DOI to cite, I had a go at summarising and
explaining that part of Dr. Mirrington's presentation in an internal
talk I gave at our institute in June last year:

[DOI: 10.7490/f1000research.1116830.1](https://doi.org/10.7490/f1000research.1116830.1)

I did try getting this into the introduction of a research paper, but
one of my co-authors thought it was too simple for an academic text.

## 2020-May-16

[from [reddit](https://www.reddit.com/r/bioinformatics/comments/gk91mp/what_are_the_drawbacks_of_oxford_nanopore/)]

### Accessibility

Sequencing-by-service is not possible on cassava farms in Africa. The
turn around time for service-based sequencing is too long to be useful
for saving crops:

https://cassavavirusactionproject.com/

### Accuracy

Nanopores can produce similar or better results to Illumina for most
applications, at a similar or cheaper cost. This doesn't mean that the
individual reads have exactly the same per-base quality, just that the
end result after downstream data analysis can be similar.

With the new Guppy v3.6 basecaller, it's looking like Illumina
polishing is no longer needed. I'm eagerly awaiting an update from
[Kat Holt's lab](https://github.com/rrwick/Basecalling-comparison) to
confirm that.

There's a 1D^2 kit that encourages template DNA to be followed shortly
after by its reverse complement pair, and other amplification
protocols that pre-amplify and ligate copies of template DNA prior to
sequencing for getting very high-accuracy reads
([R2C2](https://dx.doi.org/10.1101/gr.257188.119) is one example).

My current belief still remains that accuracy is a *software*
problem, rather than a *pore* problem. The recent improvements in
accuracy demonstrate that (i.e. I've been able to re-call reads from
2017 and get 96% mean accuracy).The base calling will always get
better over time, because we will never have a complete model of the
physics of a DNA sequence.

Whether or not the current base calling accuracy is *good enough* is
situation-dependent. Where possible, older reads can be re-called
using new basecallers (or custom-trained basecallers) to improve
accuracy, assuming fast5 files are kept.

The biggest issue for me with colourspace is/was that representing
mapping errors is a challenging problem (i.e. how do you demonstrate
in an easily-understandable way that **ACTCTGTCT*CGATCGATC*** is
very similar to **ACTCTGTCT*ATCGATCGA***). Related to this, most
software tended to map in base space, rather than
colourspace. Nanopore sequencing has a similar issue - mapping would
be better in signal space rather than base space - but I think it's
a more solvable problem because the underlying system (i.e. ion flow
signal) contains a lot more information.

Nanopore reads are a single molecule. Pile that up 100 times, and the
accuracy is better than 99%. Nanopore error is mostly random, although
within that randomness there are a lot of deletion errors. Minor
variants can be found / quantified from nanopore sequencing, although
I concede that the sensitivity of Illumina for minor variants is
higher (e.g. maybe 1 in 10,000 vs 1 in 100 for nanopore) - this is one
of the applications where billions of reads are more useful than long
reads, and can be important for cancer variant analysis.

Illumina sequences are derived from a consensus sequence of hundreds
to thousands of individual molecules; that consensus approach gives
the output reads high accuracy. Illumina sequencers don't have
sufficient optical resolution to capture fluorescence from individual
molecules. Sanger sequencing measures fluorescence of all the DNA
molecules that have progressed through the gel and ended up at the
same final distance at the end of the run. IonTorrent clusters measure
local proton counts of multiple molecules, in an electronic fashion
that is similar to a single-channel optical measurement.

Sanger sequencing is also a consensus sequencing method. I would
expect that anyone who's attempted to sequence regions containing
heterozygous INDELs via Sanger might be comfortable with considering
Sanger sequencing to be a consensus sequencing method. When I use an
alignment program (such as
[Seaview](http://doua.prabi.fr/software/seaview)), align sequences and
carry out the operation "Consensus sequence", it will create a
consensus sequence from the component sequences that has "a bunch of
noise" [i.e. 'N'] where there is no obvious consensus at that base
location. This matches the results that I get from Sanger sequencing
(and, for what it's worth, from the automated consensus generated from
2D nanopore sequences back when ONT first commercialised their sequencer).

PacBio *can* generate consensus reads by repeatedly sequencing the
same individual molecule, but so can Nanopore (via linear
amplification or rolling circle prior to sequencing), with similar
improvements in accuracy. Linear amplification was announced by
Nanopore last year, and I expect it will eventually make its way into
their official protocols. There are already published protocols about
using rolling-circle with nanopore sequencing
(e.g. [R2C2](https://doi.org/10.1073/pnas.1806447115)).

The output from each ZMW in a PacBio sequencer is a single synthesised
molecule; if the polymerase falls off, then the sequencing
stops. Consensus reads are generated by PacBio sequencers in software,
optionally, after the sequencing of each molecule has finished. It's
possible to retrieve movie files from each ZMW, and re-call to get the
individual components of each circular consensus read.

A consensus sequence generated from individual nanopore reads is
higher quality than the individual sequences. We can't ever know the
true quality of the individual reads (unless called reads get to 100%
accuracy), but the basecalled quality is currently lower than Illumina
reads and PacBio circular consensus reads.

The current requirement for multi-read consensus for high-quality is
not as much of a limiting factor as it appears, because for most
applications where high accuracy matters (e.g. variant analysis,
de-novo sequencing), pileup is carried out even for Illumina
reads. Where that's not done (e.g. cDNA sequencing + gene counting,
metagenomics), nanopore accuracy is usually sufficient to produce
similar or better results than Illumina sequencing due to the long
reads.

### Yield

A typical yield for well-prepared sequencing on current technology is
10-15 gigabases on a MinION flow cell, and 70-100 gigabases on a
PromethION flow cell.

For a 6 Mb bacterial genome, MinION sequencing would give 1,000-2,500X
coverage. For a 3.3 Gb human genome, PromethION sequencing would give
20-30X coverage.

Unlike flow cells for other sequencers, runs on nanopore flow cells
can be stopped, and the flow cells flushed and reused. There's still
generally an upper limit of total sequencing time of 48-72 hours, and
sequencing yield reduces over that time, but it allows for a couple of
small-genome runs to be carried out on used flow cells.

### Turn-around Time

The fastest possible time from sample to results is about 20
minutes. That's for a presence / absence test for high-concentration
plasmid DNA, prepared using a rapid barcoding kit.

A typical run time for large nanopore runs is 2 days, but because
nanopore sequencing is a serial process (pores are recycled; reads are
available shortly after the templates exit the pore), data can be
analysed during the run, rather than having to wait for the run to
finish. Illumina sequencing is parallel (all clusters are extended at
the same time), so it's usually not useful to analyse the data
mid-run.

### Cost

A typical well-prepared MinION run might produce a yield of 10-15
gigabases, for a cost of $1000 USD (down to half that price if buying
48 flow cells in bulk). Assuming a read length of 10kb (e.g. for a
small genome assembly), that works out to around 1-1.5M reads - a cost
of $600-$1000 per million reads.

An Illumina HiSeq run might produce a yield of 120Gb and reads of
length 100bp, or about 800M reads per run. Assuming a run cost of
$5000 USD (and I have no idea about Illumina run costs any more), that
works out to be about $6 per million reads.

Per-base, costs are more similar. Assuming you're ignoring capital
expenditure (which is effectively zero for MinION / GridION, and much
higher for Illumina), price-per-base for MinION is similar to MiSeq,
and price-per-base for PromethION is similar to NovaSeq. The read
length is a big advantage in terms of specificity (e.g. a 1kb read is
worth about a hundred 100bp reads in terms of covering genomic
sequence, and that's ignoring the ability to span repetitive
sequence).

That's for the first run on a flow cell. It's possible to squeeze out
more use from the flow cells using a nuclease wash kit, and those
subsequent runs are a lot cheaper.

The minimum run cost, although a bit more per base (i.e. $90 for a
flongle flow cell + $650 for a rapid barcoding kit that can process 12
runs of 12 samples) makes it more accessible to people who don't have
much money.

### Base Calling

Illumina sequencing depends on a cluster formed from hundreds to
thousands of individual molecules being synthesised in tandem.

Illumina sequencing is possible because most of those individual
molecules are lighting up in the same way, and consequently amplifying
a fluorescent signal. Phasing / stoichiometric issues mean that those
molecules don't all light up in the same way, which puts a limit on
the length of the sequence: once synthesis of molecules gets too
out-of-phase, the sequencing cannot reliably continue.

Nanopore sequencing works at the single-molecule level. There's no
duplication happening; what goes through the sequencer and gets
sequenced is a direct sampling of what's put on the flow cell
(excluding a few control reads that are used for internal QC).

This allows nanopore sequencing to directly measure the template DNA
itself, without any assumptions required around what bases pair with
what (as is needed for any synthesis or ligation-based
techniques). This near-model-free sequencing* means that the sequencer
can capture oddities in the sequence (e.g. from abasic sites,
non-standard bases, or methylation), which can be discovered post-hoc
and incorporated into base-calling models, increasing base calling
accuracy over time.

\* near-model-free, because it assumes that the composition of the
sequence can be fully-represented by ion flow, a bit like trying to
guess the 3D structure of a person based on their shadow, and
understanding what people ordinarily look like. A method that would be
more model-free would be ultra-high resolution microscopy (e.g. using
negative-refraction lenses, optical mirror nanoadjustments, or
tunneling electron microscopy).

### Data Analysis

For data analysis from called reads, the closest open-source
user-friendly-ish thing is probably NanoGalaxy:

https://nanopore.usegalaxy.eu/

For basecalling from raw signal (i.e. generating called reads), I'm
not aware of any user-friendly programs, but there are a few
command-line programs. The one that seems the most promising to me is
[Chiron](https://github.com/haotianteng/chiron).

Nanopore is now much better on the Open Source aspect of their code,
although they could still do more on giving it a proper Free Software
license. The code for their newest basecaller, bonito, is available on
GitHub:

https://github.com/nanoporetech/bonito

### Commercial Use

MinION commercial use is a contractual / legal issue, not a technical
issue. The T&Cs that users agree to include a condition that MinION is
only used for research, and not for commercial services. People who
want a commercial / service license need to purchase a GridION ($50k
USD, with ~$50k of included flow cells and sequencing kits to be used
in 4 months), or PromethION ($165k USD, with ~$130k of included flow
cells and sequencing kits). To add more complication, they can *then*
use a MinION to provide sequencing services.

### Aptamers

Aptamer sequencing is definitely an area that needs work. ONT claim
that the sequencer will handle very short sequences fine (i.e. below
50bp), but there are software issues with that: the signal scaling
issue that affects normalisation for basecalling (i.e. what you
mentioned), and that the MinKNOW control software will skip over and
not output any reads that are too short (this is, IMO, a more
concerning issue, because the reads can't be recovered and re-called
in the future).

One way to deal with the length issue for aptamers is to include a
random ligation step prior to sequencing. There are also various
methods to pre-amplify along a similar vein to PacBio's HiFi reads
(e.g. linear amplification, rolling circle) which can be used to
increase template accuracy.

Alternatively, short reads can be annealed to longer adapter
sequences. This is the approach taken in [this
recent paper](https://dx.doi.org/10.21203/rs.3.rs-27205/v1).

### Cancer

Generally, nanopore is a lot better for large structural variations
(e.g. deletions of hundreds to thousands of bases, or chimeric
recombination events) because of the longer read
length. Single-nucleotide changes have reasonable accuracy with
nanopore, but the applications matter, and software that distinguishes
between forward and reverse-complement mapping (important for nanopore
variant discovery) is rare.

Looking for cancer needles in whole-blood haystacks is likely to be
challenging with long-read nanopore sequencing; that's one situation
where massive numbers of reads are actually helpful. This issue can be
reduced a bit via cell-free sequencing, or shorter reads, or targeted
sequencing, but that involves a loss of information (i.e. you need to
know what you're looking for before you can do the sequencing).

### cDNA sequencing

Nanopore devices can definitely be used for cDNA sequencing (and, for
that matter, true native RNA sequencing). Accuracy concerns are a moot
point for cDNA gene/transcript counting once you get above 90% (and
Nanopore is near 95% with their most recent base callers). All that
really matters is whether or not a sequence can be reliably assigned
to its originating transcript.

It's my opinion that Nanopore cDNA sequencing runs have comparable (or
possibly better) sensitivity and specificity than Illumina, with a
lower cost, faster turnaround time, and true isoform-level
results. This is because there's a lower noise floor (i.e. at most 2-5
reads for Nanopore vs ~100 for Illumina) which compensates for a lower
read count (e.g. 1M reads for Nanopopore vs 40M reads for
Illumina). In addition to that, the longer reads make it more likely
that mapped reads will uniquely hit an isoform, and more likely that
mapped reads cover the entirety of a transcript. I've got a graph
showing different Illumina read lengths vs Nanopore here:

https://twitter.com/gringene_bio/status/1047698591987322882

### Install Base

The entirety of the Sequel II install base has a comparable throughput
to a single P24 PromethION. According to [Albert Vilella's
chart](https://docs.google.com/spreadsheets/d/1GMMfhyLK0-q8XkIo3YxlWaZA5vVMuhU1kg41g4xLkXc/edit?usp=sharing),
there are 45 PromethIONs out in the wild, and 4500 MinIONs (compared
to 125 PacBio Sequel IIs).

### Drawbacks

* The amount of effort I have to put into convincing people to give
  them a try. Oxford Nanopore rely mainly on word of mouth to market
  their products, which means they rely a lot on the unskilled
  marketing ability of research scientists (such as me). Their staff
  will go to places to give talks, but generally only when they've
  been invited. I get money by analysing other people's data, so it's
  beneficial for me to put in that effort.

* Heated discussions with critics who keep spreading misinformation
  like, "it'll never be used in a clinical setting", despite
  [years-old published
  research](https://dx.doi.org/10.1186/s13073-015-0220-9)
  [contradicting](https://doi.org/10.1101/2020.02.06.936708) those
  statements. I tend to assume that those statements are personal
  opinions / expectations, so generally try to avoid directly refuting
  or bringing attention to those statements.

* People tend to equate "cheap" with "easy". It's a very different
  process from other sequencing, even ignoring that labs tend to get
  sequencing done as a service, rather than doing it
  themselves. Nanopore offer training, but it's very expensive, so
  most people try to muddle through themselves (hence the tendency for
  sequencers to collect dust).

* Their sample prep protocols are not properly versioned, and have
  lots of traps for naive users (especially users who aren't used to
  working within a sequencing service facility).

* Software for working with nanopore reads is very new, tends to be
  command-line based, and hence is not very user-friendly.

* MinION sequencers (the $1000 ones) can't be used to provide
  commercial sequencing services to other people [Nanopore's response
  is that other people should just buy their own MinION].

* The cost for GridION and PromethION is so low that it sets up "too
  good to be true" suspicions for funding (the commercial cost of flow
  cells included in the sequencer purchase covers the cost of the
  sequencer).

* Technology updates. By the time a particular technology gets into a
  published paper, it's a common occurrence that Nanopore has moved on
  and created a better thing (and has "archived" / deleted their
  protocols for the published technology).

* Data storage / data transfer, especially for PromethION. The raw
  signal for a read is 5-10 times the size of the associated called
  fastq file. A fully-loaded PromethION has a theoretical limit of a
  few terabases of sequence per day, so storing the raw signal for
  that sequence is not a trivial exercise. Keeping that raw signal is
  important, because it allows for recalling old runs with much higher
  accuracy and/or incorporating additional base change models
  (e.g. methylation).

* ONT is a bit over-eager in their estimations of sequencing yield. I
  think they should be reporting median user yields, rather than their
  own internal yields.

## 2020-Apr-23

[from [reddit](https://www.reddit.com/r/science/comments/g5t4i5/first_confirmed_detection_of_sarscov2_in/fo6825s/?context=3)]

### PCR

A while ago, a group of people discovered that there was a particular
bacteria, [*Thermus
aquaticus*](https://en.wikipedia.org/wiki/Thermus_aquaticus) that
could survive in hot springs that were around 70°C. This bacteria,
naturally, needed to be able to copy its own DNA in order to
survive. The protein to do that, *Thermus aquaticus DNA polymerase*
(usually shortened to *Taq DNA polymerase*) was isolated in 1976 by
[Alice Chien (and two other
researchers)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC232952/). This
high-temperature protein (or, more correctly, a modified form of it)
is a key part of the polymerase chain reaction (PCR) that is used as a
core element of the most common COVID-19 diagnostic tests.

In the cells in our body, DNA is usually copied by the following rough process:

1. A group of proteins latch onto the DNA and unzip it, locally
separating apart the two strands that are stuck together
1. Primer sequences bind to the DNA
1. *DNA polymerase* latches onto both the primer sequence and the DNA
strand to be copied, then extends the primer by adding additional
blocks that are complementary to the blocks on the original DNA strand

That first step (i.e. unzipping and breaking apart the DNA, also
called *melting*) can also be done by high temperature, as I've
already explained. This high-temperature melting idea, combined with a
high-temperature *DNA polymerase* forms the functional loop of the
[polymerase chain
reaction](https://en.wikipedia.org/wiki/Polymerase_chain_reaction):

1. Heat (about 95°C) is used to melt the DNA *template* to be copied,
which is also in solution with a mix of primers
1. The solution is cooled down (to about 65°C), which allows the
primers to attach to the DNA template
1. The solution is heated up again to the ideal temperature for the
*DNA polymerase* (about 72°C), which latches onto the primer sequence
and the DNA strand to be copied, then extends the primer by adding
additional blocks that are complementary to the blocks on the original
DNA strand

This technique is called a *chain reaction*, because it can be carried
out multiple times. Each heat/cool/extend cycle [assuming there are
enough available primers and building blocks, and the primers can find
DNA template in time] will double the amount of template DNA in
solution. Starting from a single viral particle, 32 cycles of PCR (in
optimal conditions) will create (or *amplify*) about 4 billion copies.

More recently, researchers have worked out how to attach
*fluorophores* to DNA primers (also known as *fluorescent tags*, or
*probes*), which release photons of specific light wavelengths when
they're exposed to light of a different wavelength. A commonly-used
fluorophore for the COVID-19 diagnostic tests is a [*fluorescin
amidite*](https://en.wikipedia.org/wiki/Fluorescein_amidite) (usually
shortened to *FAM*), which absorbs cyan light of [wavelength
494nm](https://en.wikipedia.org/wiki/Visible_spectrum#Spectral_colors),
and emits green light of wavelength 518nm.

These fluorophores are combined with *quenchers* on the DNA primers,
which chemically interact with the fluorophores so that they won't
light up in solution. When the DNA polymerase tries to extend the
primer sequence, it chops off the quencher, which allows the
fluorophore to activate. By blasting the solution with light of the
absorption wavelength, and measuring the amount of light produced at
the emission wavelength, it's possible to get a pretty accurate idea
of how many tags have been incorporated into the amplified DNA.

This is how most of the diagnostic PCR tests work for COVID-19: when a
sample doesn't have any viral sequence, no amplification happens, and
no green light is observed. When a sample has even a small amount of
viral sequence, that gets amplified to billions of copies, producing a
very obvious green light.

### Primer Design

DNA is a very long stringy thing. It's sticky, *really*
sticky. Ordinarily it sticks to a shadow copy of itself (which we call
reverse-complement DNA; "reverse" because there's a physical inversion
that happens with the chemical structure).

*[There's a similar structure called RNA which is even more sticky, so
ordinarily sticks to itself, rather than to a reverse-complement
shadow copy. For simplicity I'll just write about DNA, but the general
ideas here apply to both]*

At a molecular scale, DNA is made of long strings of four building
blocks, which we symbolise as A, C, G, and T. The shadow copy, or
complements, of these bases are, respectively, T, G, C, and A
(i.e. the sharp, straight letters stick together, and the roundish
letters stick together).

If you heat it up in a solution, it gets less sticky and breaks apart
into single strands. If you put some shorter reverse-complement DNA
into that solution and cool it down again, then there's a chance that
those shorter pieces will stick to the DNA (instead of, or in addition
to the shadow copy). We call these short sticky pieces of DNA primers.

DNA sequences are mostly random, but not quite. We call DNA sequences
high complexity when they look random (e.g. CTGTCTATCCAGTTGCGTC), and
low complexity when they don't (e.g. AAAAGGAAAAGCTAAAAAA). It just so
happens that low complexity sequences, if they exist at all within a
really long DNA sequence, tend to exist in many different places. If
you're designing a primer to stick to a particular region of DNA, and
that region includes low complexity sequence, then the primers will
probably also stick to lots of other different regions as well.

One of the difficulties in designing primers is that the composition
of DNA sequences changes how sticky they are at particular
temperatures. It is generally, but not always, the case that longer
sequences will start sticking at lower temperatures (you could see
this as the longer sequences have more attachment points). For
example, TGGAGCTAAGTTGTTTAACAAGCG and TTCTCCTAAGAAGCTATTAAAATCACATGG
have very similar sticking temperatures (but quite different lengths),
whereas AGTGAAATTGGGCCTCATAGCA and CGCAGACGGTACAGACTGTGTT have quite
different sticking temperatures (but are both of length 22).

The chemical reactions we use to carry out genetic research work best
at a specific temperature, so there's a balancing act in choosing
primers that are sufficiently long that they stick to the specific DNA
sequences of interest, but short enough that they will stick and
unstick at a high enough temperature.

The polymerase chain reaction (PCR) that is used as a core element of
the most common COVID-19 diagnostic tests (which I'm choosing to *not*
explain here for the purpose of brevity), requires two primers to work
properly (one at each end of the target region). The ideal
temperatures for PCR mean that these primer pairs are often too short
to be specific enough for a really accurate detection of the
existence/abundance of a target region, so one or more additional
tagging primers can be included as well.

*[by the way, all of the sequences I've used as examples here come from
the SARS-CoV-2 viral genome]*

## 2020-Jan-16

### Short 16S fragments

The shorter a fragment is, the less likely it is to be able to
distinguish different organisms from similar phylogenies. The benefit
for 16S vs Illumina is demonstrably better with 1.5kb reads (e.g. see
[this paper](https://doi.org/10.12688/f1000research.16817.2)). But
between 250bp (the maximum useful read length for MiSeq, or arguably
300bp) and 1.5kb, there will be a range where MiSeq will be better due
to the higher accuracy of a 250bp read balancing out the long read
advantage.

When fragments are shorter than 250bp, and cost/time-related factors
are not considered, MiSeq should always be better at classification
due to having higher single-base accuracy for cluster-amplified
templates.

### Short tagmentation

Why would short (100-500bp) amplicons not work for Nanopore
sequencing? I can think of at least two reasons:

* The tagmentation may be sequence-specific, so for short amplicons
  there is an increased chance that the target sequence is not present
  in the amplicon.

* Base calling is more difficult on shorter sequences. The current
  basecallers require a reasonable length of sequence to determine the
  current range / distribution for scaling the electrical signal. When
  the sequence is short, that range might be skewed and throw off the
  basecaller. This is a software problem and should be fixed some time
  in the future. One thing that Clive Brown mentioned in the December
  Community Meeting was the possibility of scaling based on adapter
  sequence signal (which should have a much more consistent electrical
  profile).


This will be dependent on the template DNA; it's not a guaranteed
failure. I think the best thing to do is just try it (e.g. with a
washed, used flow cell) and see if it works.

## 2019-Dec-04

### Shotgun Metagenomics

The costs are easier to calculate for nanopore sequencing because it's all publicly available on their store. The [rapid PCR barcoding kit](https://store.nanoporetech.com/sample-prep/rapid-pcr-barcoding-kit.html) [$650 USD] is probably the best for quantitative shotgun metagenomics, because it gives a reasonable yield in terms of number of reads, but still has a simple sample preparation process (~15 minutes hands-on time + PCR).

For metagenomic *assembly*, longer reads are better, so the non-PCR [rapid barcoding kit](https://store.nanoporetech.com/catalog/product/view/id/226/s/rapid-barcoding-kit/category/28/) [$650 USD] is a better choice. Reads are fewer, but longer (e.g. 15~50kb, vs ~2kb with the rapid PCR kit). Sample prep is fast: I can do it in about 10 mins in the lab, and in about 30 minutes when I'm nervous because Linus Torvalds and Jonathan Corbet are watching.

The biggest benefit of nanopore sequencing is the low per-run cost. The cost of both rapid kits are similar, because the flow cell and kit costs are identical. Yields are also similar; I'd expect 1-5 Gb from the first few runs with these kits, and maybe 5-10 Gb with a bit more experience. The main costly external reagents are Ampure XP beads (or a [cheaper](https://bomb.bio/) alternative) and Tris-buffered saline (10 mM Tris-HCl pH 8.0 with 50 mM NaCl). Excluding those, it's a sample kit cost of $9/sample for a fully-loaded run (12 samples). Standard MinION flow cells range in cost from $500 to $900 USD each (depending on how many are purchased in bulk), so the maximum per-sample cost for a fully-loaded run of 12 samples from ONT reagents is about $85 USD (run cost of $1010). But....

* One of the many neat things about nanopore sequencing is that you can monitor the run when it is sequencing and stop when you've got enough data. It's sometimes the case for metagenomic studies that a few hours of sequencing (or maybe even a few minutes) will be enough. In which case the run can be stopped, flushed with a [nuclease wash kit](https://store.nanoporetech.com/catalog/product/view/id/122/s/flow-cell-wash-kit-r9/category/28/) [$100 USD for 6 flushes], and re-used with another sample.

* Lower-yield "flongle" flow cells are also available. These are $90 each, and with currently-seen yields might be able to squeeze out enough reads over a day if a few minutes on a standard MinION flow cell is good enough. With 12 barcodes per run, that'd be a per-sample cost of $17 from ONT reagents. At the moment, it usually works out cheaper flushing a MinION flow cell that has been used for something else, but that might change in the future.

* Higher sample multiplexing is possible with longer sample preparation workflows. Those workflows also give higher yields (5-15 Gb, possibly up to 30Gb depending on the cleanliness of the sample, and the nano-fu of the person doing sample prep). There's a ligation-based kit that works with up to 48 barcodes (as of 7 December 2019), and a PCR barcoding kit that has a plate full of 96 barcodes. One of the newest things at the moment that people are trying out is dual-barcoding with inner and outer barcodes, which gives 4608 combinations when the PCR barcoding kit is paired up with 48-barcode ligation.

## 2019-Sep-17

### Sequencing Dynamics

Nanopore sequencing preferentially sequences short reads, so these
will turn up in excess of what is observed via gels or similar
quantitative methods.

Adding a bit of DNA ladder into the sample (where templates have a
known molarity) could be used to generate and apply a standard curve
to read counts.

The problem with small molecules is more than just molarity; physics
has a role in sequencing as well. The mass (or equivalently the
momentum) affects how quickly a polymer can be sucked into an open
pore. Smaller sequences are more agile, so will be more quickly
affected by rapid changes in current (e.g. just after a sequence is
ejected and it is in the "open pore" state). Imagine a 100m drag race
between a 50-wagon train, a 18-wheeler truck, and a hatchback car. The
hatchback car will usually get to the finish line first because
there's a lot less mass to get moving in order to pick up speed.

If a sequence is really long, then the physical environment plays a
role in sequencing as well. The sequencing wells are 100μm in
diameter, which works out to about 300kb of linear DNA sequence. Long
sequences either need to coil up to fit into the well (increasing the
chance of blocked pores), or be draped across multiple wells
(increasing the likelihood of the sequence snagging on the edges of
the wells).

### Base Calling

The most recent guppy works well for R9.4 data (i.e. from about
January 2017). The biggest difficulty is converting the single fast5
files into bulk files. I expect that it will also improve base calls
on R9 data (which has a similar electrical profile), although I
haven't tested that yet.

### Amplicon Sequencing

As far as I'm aware, the current 1D2 kit would not be a good idea for
amplicon sequencing (including 16S), because sequence similarity and
length are used to determine if one read is linked to the next
read. If all the reads are almost identical (as would be the case for
amplicon sequencing), then this linking would erroneously associate
every read that has a previous reverse-complement 1D sequence.

ONT's proposed solution to this issue is to add a Unique Pair
Identifier (UPI) tag sequence to the 1D2 adapters, and detecting /
matching up that sequence when pairing, but I don't think those kits
are available yet.

## 2019-Sep-16

### Read Errors

The error profile for nanopore sequencing is not consistent (unless
you want to count deletions on long homopolymers, and methylation),
and it's getting more random as ONT improves their algorithms. That's
a good thing, by the way, because it makes it more likely that
single-molecule errors will be smoothed out on consensus.

Here's an example of that, where I'm showing all the single-molecule
variation for reads mapped to the mitochondrial genome of
Nippostrongylus brasiliensis. The black lines indicate the read
coverage, purple and cyan dots represent insertions and deletions
respectively (adding or removing from the coverage at that location),
while the blue, yellow, and red dots near the concentric axis are the
transition, transversion, and complementary variants respectively from
the reference genome. Dots get bigger as they get further away from
their baseline, and their radial location relative to the distance
between the concentric axis and the coverage line represents the
proportion of variation contributed by that variant. Reads mapped to
the forward strand appear on the outer chunk of the donut, and reads
mapped to the reverse strand appear on the inner chunk of the donut:

<img src="/pics/stompPlot_2x_Nb_mtDNA.png" alt="Mitochondrial genome variant plot for Nippostrongylus brasiliensis"
title="Mitochondrial genome variant plot for Nippostrongylus brasiliensis" width="512"/>

I haven't been able to see any consistent patterns of variation within
this genome. That doesn't mean they're not there; maybe I just haven't
looked hard enough.

## 2019-Sep-13

### [More] User-friendly Software

Galaxy has a
[nanopore analysis section](https://nanopore.usegalaxy.eu/). Here's a
tutorial for using nanopore + Illumina data together for creating a
bacterial assembly in Galaxy:

https://galaxyproject.github.io/training-material/topics/assembly/tutorials/unicycler-assembly/tutorial.html

You'll probably find that interesting programs will hit the
command-line first (because that's the easiest to write code for), and
bubble up to Galaxy when a developer gets interested enough to add
them to the available tools.

## 2019-Sep-12

### Rapid Barcoding

The rapid barcoding kit (SQK-RBK004) can be used with a single
sample. The protocol is identical to the rapid sequencing kit (i.e. no
bead cleanup), except you use one of the barcoded fragmentation mixes
instead of the non-barcoded fragmentation mix.

## 2019-Feb-19

You can't avoid transposome-based fragmentation with the rapid kit (as
found in the fragmentation mix); that's the whole point of the rapid
kit.

The rapid adapter and helicase protein is clicked onto the
incorporated adapters from the transposome. See the 'workflow' tab for
the rapid adapter kit in the ONT store:

https://store.nanoporetech.com/kits-248/rapid-sequencing-kit.html

ONT provides a protocol selector in the ONT community forums with a
drop-down for kits. If you choose the native barcoding expansion
[EXP-NBD104] from the drop-down, it comes up with a protocol for 1D
Native barcoding of gDNA.

Yes, this requires "a bunch of additional NEB products". Any other
barcoding you're doing will probably also require similar
reagents. It's a pain to source them, particularly if you live in New
Zealand or another remote country. However, sequencing yield is much
higher with the ligation-based kits, which can substantially offset
the cost, depending on your application.

There's no ONT-specific barcoding protocol for SQK-RAD004 because it's
not a barcoding kit, and hasn't been designed to work with any
barcoding solutions. If you want rapid barcoding without external
reagents, get the rapid barcoding kit (SQK-RBK004), bearing in mind
that it has a lower yield:

[SQK-RBK004 Rapid Barcoding Kit](https://store.nanoporetech.com/catalog/product/view/id/226/s/rapid-barcoding-kit/category/28/)

## 2019-Jan-25

If you want to use the MinKNOW software without pinging ONT, I
recommend sending an email to support@nanoporetech.com with reasons.

The MinKNOW software pings ONT before the start of every run, and
won't start a sequencing run unless it gets a valid response
back. There are other alternatives: buying MinIT, or waiting 6 months
and getting a MinION Mk1c.

I don't think using MinKNOW *requires* an account with ONT, but it does
require an internet connection with access to non-standard ports. In
addition to pinging, I think it will also download run scripts and
firmware prior to sequencing (both of which are necessary to run the
sequencer). At the end of the day, flow cells and kits still need to
be ordered from ONT, so there's not really any practical way to do
nanopore sequencing without talking to them.

## 2018-Nov-21

### Active Channels / Mux

QC information is available by looking at the bream log files:

    $ grep -e 'group 1.*active' -e 'bulk file name' bream-*.log

The group 1 count matters the most to me, rather than any particular
mux. The pores are loaded by flowing them onto a chip according to a
poisson distribution; some wells get none, some get more than one. ONT
tries to load the flow cells to ensure that they have the best chance
of getting 512 channels with only one loaded pore in any of the four
muxes.

There's an ideal proportion of active channels in each mux that
corresponds to this. Based on pictures I've seen (for flow cells that
look good), I'm going to guess that it's about 75% in any one
mux. There will be a mathematical formula to work this out precisely.

*I've since found out that ONT has a clever trick involving
electrical current that allows them to do better than a poisson
loading of the flow cell. Pores won't load into the membrane unless
the electrical conditions are favourable, and loading a pore changes
the electrical profile of the sequencing well. By disabling the
"pore goes here" signal when a pore load is detected, they can more
reliably fill sequencing wells without overloading them.*

### Flow Cell Storage

ONT claims a shelf life of at least 2 months for the flow cells. There
have been people on the ONT community running unused flow cells that
were in their fridge for over a year. Just make sure you keep them
above 4°C (i.e. not a frost-free domestic fridge), because the pores
get destroyed by ice crystals.

If in doubt, store in a cool cupboard; they are designed to handle
room temperature for reasonably long periods of time.

## 2018-Oct-27

### RNA / cDNA read mapping

We're getting what looks to me like good results from anything over
200k cDNA reads per
sample. [Here's](https://dx.doi.org/10.17504/protocols.io.smuec6w) my
approach for doing stranded mapping to a transcriptome using LAST.

As far as I'm aware, LAST won't recognise the 'U' from RNA reads, so
they'll need to be converted into 'T's first.

## 2018-Oct-26

### Linearising DNA

Our experience is that plasmids will sequence fine with the rapid
prep. The transposase seems to work on circularised DNA as well as
linear DNA. However, PCR is more efficient on linearised DNA. If you
have a low quantity of starting DNA that needs to be amplified, cut it
first.

### Identifying a single sequence in a set of reads

I do this sequence searching frequently, and use either
[minimap2](https://github.com/lh3/minimap2/releases) or
[LAST](http://last.cbrc.jp/) for it.

LAST is trainable and allows a bit more control over alignment
scores. Minimap2 is easier to use, because it doesn't require an index
to be generated first, can handle both compressed and uncompressed
files, and doesn't care about whether references or queries are fastq
or fasta format. Assuming minimap2 is installed (and in the search
path), you've got sequenced reads in a file 'reads.fq', and your query
sequence in a file 'query.fa':

    minimap2 reads.fq query.fa

This requires your query sequence to be in fasta format, with a header
line starting with '>' containing a description of the sequence, and
the next lines containing the sequence. Line breaks are allowed in the
sequence, but every line except the last should be the same length:

    >tig00023107:400944-401709#500-760
    ATAGAAAAGATGAAACGGTAAAGTAAAGGCGATTTCGATACAAAGAAAGA
    TGAACAGAATAGTCAGGTACAATCTCGGTAGTCTTAGAGTTACTGTTAAC
    ATAATTAAattaaaataattagcataattaaactAGTCATATTCAGTCGA
    ACGACTGAACAGTTCAAGACTACTGTAACCTCGACACTGAAAAATTCCAA
    CAAAGTGATGATAATTCCAGAATAGGCAATAATCCTAGTCGTTCGAGGTG
    ATTTCGCAAA

If you're searching for a 20-40nt sequence, LAST will probably work
better. You can extract out sequences from a BAM file as fasta using
samtools fasta. The samtools help suggests removing supplementary
alignments (presumably so that sequences are not duplicated):

   samtools fasta -F 0x900 in.bam > reads.fa

From there, generate a LAST index for the reads. Because it's a fastq
file, the '-Q 1' option is needed:

    lastdb -Q 1 reads.fa reads.fa

Then mapping the prepared fasta sequence can be done as before:

    lastal reads.fa query.fa

LAST will produce output with '#' at the start of lines, even when
sequences are not present, but nothing else will be produced if there
are no matches.

## 2018-Oct-25

### Flow Cell Washing

It is possible to wash the flowcell and use it to sequence several
times. Information about how much of the previous run is carried over
in an ideal situation can be found on the
[wash kit page](https://store.nanoporetech.com/catalog/product/view/id/122/s/flow-cell-wash-kit-r9/category/28/)
of the store.

Note that the flow cell probably won't get better after a wash. It
will keep sequencing, at best, at the same rate it did prior to the
wash. Due to this, I wouldn't advise rewashing a flow cell used for
whole-genome/transcriptome human sequencing and then trying to use it
again for whole-genome/transcriptome sequencing.

If you're doing transcriptome/genome studies on large genomes, either
barcode the samples and run together or use separate flow cells.

### Starter Kit

The starter kit includes two flow cells in the initial $1k
purchase. It's recommended by ONT that you first do a 6h run with
lambda DNA to get used to the sample preparation and sequencing
process. While you don't strictly *need* to do lambda on your first
run, it's a good idea to get acquainted with the process of
sequencing.

If your own sample is not precious, go ahead and use it on your first
run, but be prepared that you'll probably have a low-yield run and
won't know why. If that happens, one of ONT's first suggestions will
be that you run some lambda to demonstrate that your sample
preparation process is okay.

### Barcoding

RNA can't be barcoded at the moment, as far as I know.

There are two rapid barcoding kits, one uses PCR and one uses
transposase fragmentation. It depends on your application which one
will be best.

For gDNA, I recommend the transposase-based rapid barcoding kit, if
you can handle a lower yield. I haven't observed any sample-prep based
ligation with that kit, which makes the bioinformatcs side of things
much easier & more robust.

For cDNA, we're using the PCR rapid barcoding kit (using rapid
attachment primers that click into the adapters). Unfortunately the
cDNA kit only includes PCR amplification primers for six samples,
whereas that barcoding kit can be used for up to 72 samples (i.e. 6
runs of 12). We're still trying to sort out ordering / using our own
custom primers for that.

The ligation-based barcodes will give you a higher yield (particularly
with fragmented / sheared DNA). I expect that most of that increase is
due to cleaning stages in the sample preparation process. However, the
adapter ligation steps used after barcoding can create physical
chimeric reads, which can be a pain to deal with in downstream
analysis.

## 2018-Oct-06

### cDNA Sequencing and Run Yield

Our last cDNA sequencing run produced 7 million reads with an average
length of about 800bp; more than enough coverage to get good
transcript depth for three bulk samples. For doing single cells, you'd
probably be able to do up to about 1000 cells on a single MinION flow
cell and still get reasonable results.

The highest yield (outside ONT) so far from a MinION is 19 Gb, and the
new Series D flow cells (which give more consistent output over longer
periods of time) should see that approaching 30Gb with no additional
chemistry changes.

While the usual recommendation for human is from 10M to 25M reads per
individual sample, that's with short Illumina reads. Nanopore gives
you full-length sequences with fully-resolved isoforms. Also, the
standard Illumina sequencing protocol will mix in unprocessed
transcript, which means that intronic sequence also comes through in
the reads (and is wasted / confounding).

The long read advantage is substantial. From the work that I've been
doing with cDNA, we've been getting reasonable results for the mouse
transcriptome from anything over about 150k reads per sample. I've
just put up a [one-sample example
dataset](https://doi.org/10.5281/zenodo.1419086) on Zenodo, run in
August last year. I think it's about 1M reads (which can be
sub-sampled to see if anything changes).

Both Illumina and long-read data have issues with low-expressed
transcripts. The only way you'll be able to reliably pick up
low-abundance transcripts is with a microarray, because then the
high-abundance transcripts won't swamp out the hits for other
transcripts.

There's a lot more noise in Illumina SBS reads which means that it's
difficult to make reliable calls for fewer than about 100 reads for a
given transcript. The noise floor for nanopore is a lot lower: not
more than 5 reads from the data I've been looking at, and it could be
as low as two reads. As an example, here are two genes that are at the
lower end of expression on our MinION runs, one (Adh1) that is not
expressed. To suggest that this is expressed in the Illumina samples
would require justifying why the unannotated regions outside the gene
are expressed at a similar level:

[Adh1](https://i.imgur.com/8teaVSA.png)

And one that has two reads oriented in the same direction as the
gene. I have trouble convincing myself that these reads don't
originate from the annotated transcript:

[Ankrd39](https://i.imgur.com/48q3ELC.png)

When that is taken into account, together with the already mentioned
confounding factors, 10-25 million reads on Illumina isn't that far
off 1M reads on nanopore.

And that's ignoring the long read advantage for transcripts and
isoforms (i.e. "bases covered"), a lack of short-transcript bias, and
gene network correlations that can be used to adjust zero-count data
(commonly used for single cell sequencing).

There's only one transcript per read, if you're using the standard
strand-switch method for cDNA sequencing. There are approaches like
R2C2 which incorporate multiple passes of one transcript. Compared to
PacBio, nanopore can produce more reads per flow cell, especially for
shorter reads of the order of 1kb (e.g. cDNA) - even the new flow
cells have an upper limit of 8M reads; we got 7M reads from our last
cDNA run.

Barcoding RNA comes under "feasible, but ONT haven't implemented it
yet". Given that there's already a chunk of custom sequence in the
adapters, it's not much of a stretch to imagine that a barcode could
be added to that.

A particular downside of native RNA sequencing is that the sequencing
is slower, so it'd be worth thinking about whether you really want to
do RNA. You also have to concern yourself with the quirks of RNA: it
degrades faster than DNA, and has a habit of folding up and twisting
into knots that make sequencing a little bit more difficult.

Yield will be higher, and sequencing will be more reliable with
cDNA. I'd recommend only using native RNA if you need it (e.g. for RNA
modifications).

### Read Errors

MinION can detect if a base is repeated multiple times in a row. When
the number of repeated bases is 5 or greater, the base caller can both
over and underestimate the reference sequence. For what it's worth,
PacBio also has problems with long homopolymers, which makes me wonder
how much of it is a representation of the true sequence, and how much
is introduced by the caller.

Up to a year or so ago the basecallers collapsed any homopolymers
longer than 5 bases into 5 bases, but the reads can be re-called using
more recent base callers to recover the missing bases. Nanopore's most
consistent / systematic error now is inaccurately calling the length
of long homopolymers (over 5bp), especially when methylation-aware
consensus calling us used (which gets rid of most of the systematic
base call error due to methylation).

Nanopore sequencing is an observational method of sequencing; there's
no degradation of error based on length. If this were the case, then
it wouldn't be possible to match a [2.3Mb sequenced
read](https://www.biorxiv.org/content/early/2018/05/03/312256) to the
human genome. PacBio read length is limited; even 100kb would be
pushing it on a 10h run.

### Flow Cell Re-use

To determine if a read was a residual molecule from a previous run or
the current library, wash the flow cell to get rid of most of the old
reads, barcode the reads, and dump any reads that have no barcodes,
old barcodes, or multiple barcodes. It's not great to re-use a flow
cell for high-throughput analysis (e.g. large whole genome or whole
transcriptome sequencing). Stick to bacteria, viruses, and amplicons
for re-using flow cells.

Even ONT admits that washing won't completely remove the previous
reads.

The problem with re-using flow cells is that many people think a flow
cell can be fully-recovered by washing, and try to use it again for
high-throughput sequencing. The washing [only] removes most of the old
bound sequence, so that it interferes less with additional library;
washing can't recover dead pores.

I frequently re-use flow cells for sample QC and low coverage /
amplicon sequencing. If you're only interested in read length
distribution, a few thousand reads is plenty. When you're sequencing a
10kb plasmid or 16kb mitochondria, only getting 10-100 Mb out of a run
is plenty.

## 2018-Feb-19

### Chimeric Reads

I've seen chimeric reads in every nanopore run I've done. In some
cases, they are created *in-silico*, and can only be identified by
looking at the raw signal associated with the sequenced reads. This
open pore signal is what the raw signal looks like when there is no
DNA or adapter engaged with the pore. "Open pore" is ONT's
terminology. Between each read, there is a brief period of time when
the pore needs to change from one adapter to another. At this time the
pore is "open" and electricity requires a much higher current in order
to bridge the gap.

When the raw signal contains a break, it suggests that the sequencing
software didn't properly break up the sequence into two separate
reads. There's a threshold for the dissociation time, and if it's a
very quick reloading then two reads can end up in the same FAST5 file,
with the basecaller ignoring the stall signal and calling as a
chimeric read.

Particularly for the R9 pores, the motor protein will sit on the pore
for a short while before processing the DNA. That's the stall signal:
a situation where there's DNA in the pore, but it's not moving. If
there's a long stall region in the middle of a read, it's likely to be
a chimeric read that has formed *in-silico*. If that's the case there
should also be a spike at the start of each read as well. Sometimes
DNA will also stall for a bit within a sequence if the pore current
has been reversed (and the DNA/adapter has refused to eject). The
current reversals are very obvious: they have a huge spike, then a
huge drop in the other direction directly afterwards, possibly
followed by an incorrect base current level.

## 2018-Jan-30

### How I began in Bioinformatics

I talk about my early research history
[here](https://www.researchgate.net/publication/308026084_The_Maori_Difference_Exploring_the_life_of_a_Maori_genetics_researcher)
[[talk
script](https://www.researchgate.net/publication/308026087_Talk_script_for_presentation_2016-Sep-09)].

But my desire to look at things, see how they work, and change them
started in my childhood. I learnt how to use DOS batch files when I
was about 8 years old, and had a memory sniffing program that allowed
me to cheat when playing computer games. That led to a bit of
tinkering with save game files, and messing around with different
binary file formats.

I started teaching myself how to program in Pascal when I was about
14, and a friend helped me out with setting up a website a year or so
later.

I've sort of always been tinkering, but it was helped greatly by
formal education in computer science at University. I took up biology
to challenge myself (it was my worst subject at school), and have been
fond of genetics because it has a lot of similarities with mathematics
and computer programming.

## 2017-Oct-17

### Working as a Bioinformatician

I've always had a somewhat stable job I could fall back on, which
generally comprises about 40-60% of the work I do. I'm also struggling
a bit in finding additional work, because everyone wants full-time
employment, which doesn't fit in with my mode of work. I'm much more
productive if I'm able to work on a broad range of projects, because
there are the occasional flashes of insight where I can use a solution
from one project in a completely different domain. I haven't needed to
advertise my services all that much, with most of my jobs obtained
through word of mouth.

However, time management for project-based work can be very
challenging. It has meant that I've had some nail-biting situations
work-wise in being overloaded with work (and not being able to
complete jobs on time), and some times where I've had to go for a
month or so without income (hunting out and prospecting for work).

Doing work on software projects (e.g. Trinity, Canu) has been
incredibly beneficial in increasing my knowledge and capability, but
not so great in getting more paid work. That's partly because some job
contracts are biased against sole-trader businesses (e.g. requiring
$10M liability for damages).

t's frustrating that English doesn't distinguish between
free-as-in-facebook and free-as-in-freedom. It's the second one that
specifically applies to free and open source software, although most
FOSS developers choose to do both. I ask for payment for development
time and bug fixing, rather than use. If I'm not doing any work
myself, it feels wrong for me to charge money.

## 2017-Sep-29

### Alignment

[LAST](http://lastweb.cbrc.jp/) is quite good for showing differences
between individuals nanopore reads and a reference sequence, or
between two different nanopore reads. The HTML output can be loaded up
in [LibreOffice
Writer](https://www.libreoffice.org/download/download/) and exported
to a PDF file.

## 2017-Apr-29

### Ribosomal DNA repeat regions

A couple of years ago the MinION had less than 1/100th the yield it
has now, and max read length was forced to about 35kb. It's
substantially better now (read length limit is effectively removed);
but will still be difficult getting 600kb reads due to sample prep
issues.

However, we have had some success in assembling some ribosomal repeat
regions using Canu. I don't expect it would be any different from
other long-read sequencing. Just be careful with sample prep, don't
use PCR and avoid pipetting as much as possible.
