---
title: "Free Software"
author: "David Eccles"
date: 2018-04-07
---

I am an [eLife
ambassador](https://elifesciences.org/inside-elife/912b0679/early-career-advisory-group-elife-welcomes-150-ambassadors-of-good-practice-in-science)
for open research, but am also a strong advocate for free
software. I've previously written up [a
post](/2018/03/23/open-research) about the links between open research
and free software, and would recommend reading that to understand why
I think free software is important for good research.

# What is Free Software?

Free Software is software that gives the *users* of that software the
freedom to do anything they want with the software. A proper
definition of what is required to give this freedom [is
complicated](http://gplv3.fsf.org/rms-why.html), but those
complications are mostly about how free software is *distributed* to
other people. If a person just wants to *use* free software, they
probably don't need to be concerned about those issues.

# What is Free Software?

Isn't that the same question?

Well... yes. But my answer is different.

The answer I just gave was something of a dictionary definition, but
here I give a practical definition: a definition by demonstration. It
is my hope that by demonstrating the wide range of free software that
is available, I can better show what it is.

All the software that I present here is available on at least the
three major classes of operating systems: Windows, OSX, and Linux. All
these software programs also have code available for download that can
be inspected, so that curious people can see exactly how it works.

The programs are broken up into rough categories, with a bit of a
blurb about each program (once I get around to writing them) and links
to the program websites for downloading the programs. Note that the
descriptions here are frequently paraphrased from multiple sources
(including other tweets and software web pages).

## **Office**

### Open Broadcaster Software (OBS)

https://obsproject.com/

OBS is a favourite among streamers for helping them manage the audio
and desktop that is presented to the world. It includes many different
tools that help deliver a clean, interesting experience, and can also
be used offline for pre-recording presentations.

### Jitsi

https://jitsi.org/

Jitsi is a set of open-source projects that allows you to easily build
and deploy secure video conferencing solutions. At the heart of Jitsi
are Jitsi Videobridge and Jitsi Meet, which let you have conferences
on the internet, while other projects in the community enable other
features such as audio, dial-in, recording, and simulcasting.

### LibreOffice

https://libreoffice.org

LibreOffice is an office productivity suite for letters, spreadsheets,
presentations, and databases.

### Notepad++

https://notepad-plus-plus.org/

Notepad++ is a text and source code editor.

### WinSCP

https://winscp.net/eng/index.php

WinSCP is an FTP/SCP client for transferring files between local and
remote computers.

### Colour Oracle

https://colororacle.org/

Color Oracle is a color blindness simulator that shows in real time
what people with common color vision impairments will see.

### PeaZip

https://www.peazip.org/

PeaZip is a file compression / decompression utility.

### JASP

https://jasp-stats.org/

JASP is a spreadsheet-style statistical analysis package.

### Zotero

https://www.zotero.org/

Zotero is a free, easy-to-use tool to help collect, organize, cite,
and share research.

### Sozi

https://sozi.baierouge.fr/pages/10-about.html

Sozi is a zooming presentation editor and player. Unlike in most
presentation applications, a Sozi document is not organised as a
slideshow, but rather as a poster where the content of your
presentation can be freely laid out. Playing such a presentation
consists in a series of translations, zooms and rotations that allow
to focus on the elements you want to show.

### eLabFTW

https://www.elabftw.net/

eLabFTW is a free and open source electronic lab notebook, designed by
researchers, for researchers, with usability in mind.

I used eLabFTW when I was writing up experiments for [COVID cDNA
testing
protocols](https://dx.doi.org/10.17504/protocols.io.bebujanw). It was
useful for helping me keep a record of prior experiments, and the PDF
export function made it easy to print out, scribble, then update as I
worked through and improved my protocols.

## **Graphics**

### Inkscape

https://inkscape.org/en/

Inkscape is a vector graphics editor for diagrams, icons, and
logos. Inkscape opens Adobe Illustrator and PDF files, to edit them,
facilitating transitioning away from the expensive subscription-based
Adobe tools. It is outstanding software for manuscript figures and
scientific posters. Inkscape is trivial to use as a point-and-click
program, yet also has a comprehensive scripting ability.

### Scribus

https://scribus.net

Scribus is a press-ready desktop publishing program for posters,
brochures, and magazines, which I've used for all my research posters
for the last ~10 years.

### ImageJ

https://imagej.nih.gov/ij/

ImageJ is an image processing and analysis toolkit. ImageJ (and its
spin-off [FIJI](fiji.sc/#download)) is fantastic for viewing, editing
and analysing large CT datasets. And the software doesn't need to be
installed; very useful when admin privileges are hard to come by.

### GIMP

https://www.gimp.org

GIMP is a raster/pixel graphics editor for photo retouching and image
manipulation.

### Blender

https://www.blender.org/

Blender is a 3D renderer and movie maker. I've known about Blender for
a while, and have enjoyed watching the [short
movies](http://archive.blender.org/features-gallery/movies/) that have
been made with it.

My first foray into Blender was in 2017, creating a [can of
worms](https://www.thingiverse.com/thing:2497729).

### OpenSCAD

https://www.openscad.org/

OpenSCAD is a programmatic 3D modeler for creating solid 3D CAD
objects (for 3D printing and animation). OpenSCAD has helped me a lot
for my various 3D-printing adventures. I think my most complex object
so far would be a [vaccuum filter
device](https://www.thingiverse.com/thing:2341007) for helping me to
filter river water from the local stream for subsequent nanopore
sequencing.

### FreeCAD

https://www.freecad.org/

FreeCAD is a multiplatform software that allows you to design and
modify 3D models with parametric constraints. It supports many file
formats, features and workbenches for various engineering needs.

### Krita

https://krita.org/en/download/krita-desktop/

Krita is a digital painting tool, including wet paintbrush / blend
effect.

### Darktable

https://www.darktable.org/about/screenshots/

Darktable is a digital photo manager for both processed and raw
photos, a virtual lighttable and darkroom for photographers. It
manages digital negatives in a database, provides a zoomable
lighttable and allows for the development and enhancement of raw
images.

### QGIS

https://www.qgis.org/en/site/

QGIS is a Geographic Information System. Vector and raster data can be
overlaid in different formats and projections without conversion to an
internal or common format.

### MyPaint

http://mypaint.org/

MyPaint is a nimble, distraction-free, and easy tool for digital
painters. It supports graphics tablets made by Wacom, and many similar
devices. Its brush engine is versatile and configurable, and it
provides useful, productive tools.

## **Sound**

### Audacity

http://www.audacityteam.org/

Audacity is a multi-track sound recorder and editor. Audacity is
hugely useful for cleaning up background noise in audio interviews
prior to transcription.

### VLC

https://www.videolan.org/vlc/

VLC is a multimedia player and framework that plays most multimedia
files as well as DVDs, Audio CDs, VCDs, and various streaming
protocols.

### MuseScore

https://musescore.org/en

MuseScore is a sheet music notation / composition program, with music
entry via a mouse, a computer keyboard, or a MIDI keyboard.

### Lilypond

http://lilypond.org/text-input.html

Lilypond is a text-based music engraver and lyric notation program.

### LMMS

https://lmms.io/

LMMS is a sound generation system, synthesizer, beat/baseline editor
and MIDI control system which can power an entire home studio. Sounds
and tones can be generated, played and artfully arranged to create
entire tracks easily, giving you the opportunity to create music from
beginning to end. LMMS can also connect to a MIDI keyboard and play
music live.

## **Programming**

### R and RStudio

http://rstudio.com

R is a programming language: a statistical computing / bioinformatics
kitchen sink, generating statistics and associated graphs using short
script files. RStudio is a user interface that helps people write R
programs, and create related documents. This document was written
using Rstudio.

### Anaconda

https://www.anaconda.com/

Anaconda is a distribution of the Python programming language for
large-scale data processing, predictive analytics, and scientific
computing, that aims to simplify package management and deployment.

### Atom

https://atom.io/

Atom is a customisable text editor with a great selection of plugins.

### Octave

https://www.gnu.org/software/octave/

Octave is a matlab-compatible programming language.

### SageMath

https://www.sagemath.org/download.html

SageMath provides a Mathematica/Maple-like experience by combining a
whole bunch of different Free and Open Source packages (including
Octave).

## **Psychology**

### Orange

https://orange.biolab.si/

Orange is a python-based app for data mining, with a clean-nice GUI.

### BORIS

http://www.boris.unito.it/

BORIS is an easy-to-use event logging software for video/audio coding
and live observations.

### Open Sesame

http://osdoc.cogsci.nl/

OpenSesame is a program to create experiments for psychology,
neuroscience, and experimental economics.

## **Games**

### Simon Tatham's Portable Puzzle Collection

https://www.chiark.greenend.org.uk/~sgtatham/puzzles/

Simon Tatham has developed a collection of small puzzle games, for the
purpose of giving people short breaks from their regular work.

### Enigma

http://www.nongnu.org/enigma/

Enigma is a top-down marble labyrinth / memory game.

A marble game? Is that all?

Well, yes, but it's got a tetris simulator as one of the puzzles that
you need to solve to advance through the game. It's surprising how
many different worlds can be created from a fragile marble and a bit
of imagination.

### Godot

https://godotengine.org/

Godot is a cross-platform engine for 2D and 3D game development. It
supports GDScript, C#, C++ and other languages, and offers a dedicated
2D engine, a powerful 3D engine, and a simple deployment process.

I've started writing [a couple of games](https://gringene.itch.io/)
using Godot, and like how much freedom it gives me to play around with
the code. The godot website includes many excellent tutorials to help
get you started with programming games.

## **Other**

Have I forgotten something? Want to know more?

Check out my [Twitter
thread](https://twitter.com/gringene_bio/status/960668027711733760) on
free software, or [email me](mailto:bioinformatics@gringene.org).

For a more comprehensive view of the diversity of free software, check
out [this
list](https://en.wikipedia.org/wiki/List_of_free_and_open-source_software_packages)
on Wikipedia.

# Summary

For most of the computer things people want to do in their everyday
life, those things can be done using free and open source
software. The gaps are getting filled as more people use free
software, and the landscape of free software is improving all the
time.

An important thing to remember is that most of the developers of free
software are creating software on a voluntary basis. Be nice to them,
use their programs, support them, and encourage them, because they are
helping to create the future.
