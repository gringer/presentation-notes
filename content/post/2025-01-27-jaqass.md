---
title: "The January JAQass Incident"
author: "David Eccles"
date: 2025-01-27
---

### Disclaimer

This is not a scientific case study. I am not a psychologist, nor an expert on conflict in social media. I am reporting this from my own armchair perspective as a bioinformatician. The source of my information is almost entirely based around the reply skeets of a Bluesky user from 10th January 2025 to 20th January 2025. I am aware that this user ascribes to the "all publicity is good publicity" idea. I don't expect this to ever end, and am probably stoking the flames by doing this, but it is cathartic for me to get my perspective on this out of my system. My own life experience relating to misrepresentation of my own research has demonstrated to me that there can be benefits in doing such things. In particular, I have found that it is useful to have a pre-written record of common ideas, allowing me to spend substantially less energy on constructing answers on the fly.

This post is incomplete, and may stay that way. I have no current desire to progress beyond what is found below, because I'd rather just shut this door on that part of my life and move on to other, actually exciting things.

## Summary

This is a little story about a Category 6 [Reply Guy](https://bsky.app/profile/sbarolo.bsky.social/post/3k6jaykvdoi2d), subsequently referred to as "the sealion" or "the JAQass", because of [this explanation](https://bsky.app/profile/sbarolo.bsky.social/post/3k72hjeh4jk2g) and [this comic](https://wondermark.com/c/1062/). The JAQass claims that they are writing a paper on this discussion event, so if you like you could consider this to be a pre-bunking publication. Based on their responses, they love the attention they're getting, and would probably enjoy the chance to respond to such a discussion... but only if they get the last word, and are able to exhaust everyone else into silence.

As a short summary, the sealion was presented with a research paper that found some research papers contained detectable image duplications and/or deliberate image manipulation, and then proceeded with classic sealioning tactics:

1. Ask an abundance of bad-faith questions
2. Exhaust other responders into silence
3. Claim that silence is equivalent to agreement
4. Complain about being misinterpreted
5. Complain about questions not being answered
6. Ignore or discard answers that don't fit the presented narrative
7. Demand other people disprove the bad-faith claims
8. Demand tolerance and civility, despite breaking a social contract of tolerance and civility

Sealioning most often occurs as a form of harassment where a man is harassing a woman. The claimed [false] justification of this is that the sealion is "Just Asking Questions", where the more likely underlying intent is that they just enjoy being a jackass (i.e. acting contemptibly foolish, stupid and ignorant). It is typical for sealions to state that labelling them as sealions is an "ad hominem" attack of character, and they should instead be attacked on the substance of their arguments. Clearly, this does not make sense; a sealion spouting bullshit and asking for proof against that bullshit should be called out for the bullshit generator itself, rather than for what comes out the other end. There are an infinite number of false things in our world, and the effort of refuting a false statement is far greater than the effort involved in its creation.

This was explained better by someone else in the many interactions with the sealion:

"""
Telling someone "your argument is bad because you're a dick" is ad hominem. It's not when you simply tell someone they're being a dick. Common misconception.
""" - 3lfklkowgm22y

In addition to the usual exhausting sealion strategy, this sealion also acts as if discussion on social media is instantaneous, just like conversation. At the same time, this JAQass claims that a sequence of multiple separated skeets produced in quick succession (but not simultaneously) should be treated as a single cohesive idea. They complain about comment threads being derailed by a response in the middle of a sequence of multiple separated skeets, rather than at the start or end (e.g. "a summary that [she] is intentionally cutting off with her replies" - 3lfwupmepes2c). There are a few obvious reasons for this, e.g. the particular skeet that is replied to is particularly relevent, or the skeet was the most recent available skeet at the time that the commenter began typing out their response. Along the same vein, the sealion refers to temporal events as if they are a reason for non-response, or a response that is less measured than normal (e.g. "I was literally eating my breakfast when you came in my digital door with your nonsense" - 3lfnb7op7zk27), and also that if someone hasn't replied, then the discussion is over and there are no further points of disagreement.

At best, the responses of this sealion are from an ignorant fool that has trouble remembering anything. However, given what [3lfkgmou5mc2k] is implying, the JAQass is deliberately misleading and manipulating science experts on social media - with a clear understanding of what he is doing and the types of responses it will lead to (as evidenced by their 2023 paper) - in order to create a polarising story for a research paper. In other words, participating in academic misconduct and fraud.

## Inception

The sealion was introduced to Dr. Elisabeth Bik via Jay Patel:

"""
Hmmm, I think @elisabethbik.bsky.social and @statmodeling.bsky.social  are great sleuths and civil. I see a fair bit of professionalism and humility in many sleuths. 

I agree that others could tone down their language, but can you share (DM?) Examples of true harms? False accusations?
""" - [3lffmlri4hc2s]

The JAQass's initial response was adversarial. In other words, they started off with bad-faith suspicion, rather than genuine interest:

"""
I don't know either of them or their work, unfortunately. but if they engage in the same sorts of stuff i see others doing, I would say it is problematic.

maybe they can say how they pick their targets or what % of tests they run turn out negative & how they report those neg tests?
""" - [3lffnnt56ys26]

In response to this, Dr. Maria Zalm replied with a link to Dr. Bik's paper:

"""
@elisabethbik.bsky.social et al actually published a paper (pubmed.ncbi.nlm.nih.gov/27273827/) describing their assessment and findings.

They scanned 20K articles from 14 OA journals and found image concerns in 3.8% of articles assessed.
""" - [3lfg3durask2l]

Note the careful wording here from Dr. Zalm, "image concerns". Here is the actual paper abstract:

"""
Inaccurate data in scientific papers can result from honest error or intentional falsification. This study attempted to determine the percentage of published papers that contain inappropriate image duplication, a specific type of inaccurate data. The images from a total of 20,621 papers published in 40 scientific journals from 1995 to 2014 were visually screened. Overall, 3.8% of published papers contained problematic figures, with at least half exhibiting features suggestive of deliberate manipulation. The prevalence of papers with problematic images has risen markedly during the past decade. Additional papers written by authors of papers with problematic images had an increased likelihood of containing problematic images as well. As this analysis focused only on one type of data, it is likely that the actual prevalence of inaccurate data in the published literature is higher. The marked variation in the frequency of problematic images among journals suggests that journal practices, such as prepublication image screening, influence the quality of the scientific literature. 
""" - [https://doi.org/10.1128/mBio.00809-16]

Dr. Bik uses the terms "problematic [figures / images]" and "deliberate manipulation". These are also careful words that are observational statements, rather than a hypothesis of author intent.

In the discussion of their 2016 paper, Bik et al. present five hypotheses relating to reasons behind these problematic figures, and an increased frequency of duplication since 2003, which I will paraphrase here:

1. Older papers often contained figures with lower resolution, which may have obscured evidence of manipulation.
2. The widespread availability and usage of digital image modification software provides a greater opportunity for both error and intentional manipulation.
3. Images directly prepared by authors instead of by professional photographers has removed a potential mechanism of quality control.
4. Increasing competition and career-related pressures may be encouraging careless or dishonest research practices.
5. Electronic manuscript submission facilitated submissions from researchers in countries who were previously discouraged to submit because of high postal costs.

Of these, hypothesis 4 suggests an intent of "dishonest research practices", which could be considered to include an intent to deceive (without being explicit about it). Other hypotheses either refer to errors, an increase in frequency, or merely extend intent to the act of the manipulation, rather than a deeper underlying reason behind that. In other words, it is only in the speculative discussion section of the paper where hypotheses about author intent are propsed. This is is a paper about observations of image manipulation that affect the interpretation of a paper; it is *not* a paper about author intent.

## Knowledge / Experience

The JAQass launched into an attack on this observational research with no prior knowledge about the study, the authors, or biological research papers involving Western blots*.

\* "... have you seen the types of images? they are nothing that anyone is really looking at and interpreting in a meaningful way. they are tiny images in arrays of images ..." - 3lg73pizupk2q

Despite that, the sealion built up a persona of perceived belief in their expertise over the course of the next few days, and rejected perspectives presented by other people more familar with the study author's investigations and biological research. By asking more and more questions to override the explanations that the experts were giving, the JAQass broke the social contract of tolerance, and was labelled as a sealion.

When learning about a new investigation, it is a rational response to ask a few questions out of curiosity. However, after a demonstration of substantial support for the investigation, it is also rational to accept that the investigative domain is foreign enough that more learning is needed. In such a situation, it is expected that people do their own learning, without wasting the time of experts that have plenty of other important work to do.

## Illuminating comments

"it is not a reasonable burden to expect me to recode by eye 20,000 articles" - 3lfivxquygs2t [true, but no one is expecting SL to do that]

"they did not test at all whether the image is important to the paper" - 3lfgayscox22t [yes, they did; an image needed to be presented as separate conditions]

"ive said many times i am writing a paper on this and testing arguments on average ignorant readers (i.e., social media)." - 3lfkgmou5mc2k [suggesting SL is faking interest and ignorance... but that's worse]

"the study is about 'the prevalence of duplicated images'" - 3lfivp7ekcs2t ... > [reply] > "in my mind this necessitates in some way knowing the total number of images" - 3lfivq5vtqs2t [SL came so close to making a statement that was demonstrably correct]

"Barge, Cuddy, the thing about harassment is the harasser thinks they didnt do anything" - 3lffaohowl22x [... not all men, but definitely this fucking guy]

"... according to her example 50 of them per paper. can you not imagine making an error or someone making an error across 20,000 papers...." > [reply] > [reply] > "they highlight the different types of errors and how often they are in the paper" - 3lfiibpgnas2d [another close call to being demonstrably correct, but it was tacked onto direct misrepresentation]

"the title of the paper is, 'The Prevalence of Inappropriate Image Duplication in Biomedical Research Publications' so the level of analysis is images. thx bye" [the level of analysis is "Research Publications", not "images"]

## Skeet thread

So... the sealion is fighting on his own turf / research area: social media and psychological manipulation.

Let's see what I can uncover on my own turf: programming, scripting and data analysis.

Helpfully, Bluesky provides examples for downloading all user posts:

https://docs.bsky.app/blog/repo-export

As of just a few minutes ago, the sealion had 1365 feed posts:

    /app.bsky.feed.post$ ls | wc -l
    1365

1295 of these were reply posts:

    $ grep '"parent": {' *.json | wc -l
    1295

Let's copy those:

    $ grep '"parent": {' *.json | perl -pe 's/:.*//' | while read fname; do cp ${fname} reply_posts/; done

The majority of those posts were created this year:

    $ grep createdAt *.json | awk '{print $3}' | perl -pe 's/..T.*//' | sort | uniq -c | tail -n 15

     68 2024-01
     43 2024-02
     33 2024-03
     21 2024-04
     16 2024-05
      4 2024-06
      9 2024-07
     33 2024-09
     20 2024-10
     10 2024-11
     53 2024-12
    985 2025-01

[these have been manually cleaned up]

... since January 10th:

    $ grep 'createdAt": "2025-01' *.json | ... | perl -pe 's/T.*//' | sort | uniq -c | tail -n 15
      1 2025-01-06
     95 2025-01-10
    166 2025-01-11
    319 2025-01-12
     96 2025-01-13
    115 2025-01-14
     35 2025-01-15
     46 2025-01-16
     11 2025-01-17
     14 2025-01-18
     28 2025-01-19
     59 2025-01-20

It's clear that things started happening from January 10th onwards, but just to check...

    $ grep -A 50 'createdAt": "2025-01-06' *.json | grep text

    "Did you get funding? if it doesn't report any pilot testing, or if we didn't do it in one place, does it make sense to try it in 20+ places?"

How ironic:

    $ cat 3lffaohowl22x.json
    {
    ...
      "createdAt": "2025-01-10T12:59:05.237Z",
    ...
      "text": "Barge, Cuddy, the thing about harassment is the harasser thinks they didnt do anything"
    }

Ah, *this* is the post I remember:

    $ cat 3lffauq4zkc2x.json
    {
    ...
      "createdAt": "2025-01-10T13:02:35.413Z",
    ...
      "text": "I want to know what happens to all the tests that these data police or whatever conduct that do not show fraud. do they just put them in the file drawer?..."
    }

So, I'll do another subselect starting from January 10th.

    $ grep 'createdAt": "2025-01-[12][0-9]' *.json | perl -pe 's/:.*$//' | while read fname; do cp ${fname} file_drawer; done

3 posts before the "file drawer" post, which I'll delete:

    rm 3lffaohowl22x.json 3lffas4vrw22x.json 3lffauq4zkc2x.json

Now for some text analysis. I find the 'text' element, and remove the surrounding cruft:

    grep '^  "text": ' *.json | perl -pe 's/^.*?"text": //;s/^"//;s/"$//' > text_corpus.txt

    $ perl -pe 's/[^a-zA-Z\n ]/_/g;s/ +/\n/g' text_corpus.txt | perl -pe 's/^_+//;s/_+$//;' | grep '[a-z]' | sort | uniq -c | sort -rn | head
       1150 the
        680 is
        661 to
        581 you
        557 of
        535 it
        480 that
        472 and
        439 not
        385 a

... manual filtering for common words needed.

This is word-level - or possibly phrase-level - analysis, rather than post-level analysis or author-level analysis. I hope that's okay.

    $ head wordfreq_noCommon.txt
    357 i
    301 they
    209 paper
    204 she
    203 but
    165 about
    163 what
    163 we
    156 images
    154 if

It appears these are words from a narcicisst.

xkcd provides a list of the ten hundred most common words (and what appears to be their derivations); I'll do an exclusion filter:

https://www.xkcd.com/simplewriter/words.js

[interesting trivia: "fuck" is included in this list]

Some additional cleaning of this list is needed to replace odd characters.

I seem to need to do this in two stages:

    $ comm -2 -3 <(awk '{print $2}' wordfreq.txt | sort -k 1b,1) sorted_tenhundredwords.txt > uncommon_words.txt

    $ join -e - <(awk '{print $2,$1}' wordfreq.txt | sort -k 1b,1) uncommon_words.txt | sort -rn -k 2,2 > wordfreq_notenhundred.txt

    file_drawer$ head -n 20 wordfreq_notenhundred.txt 
    data 147
    science 99
    errors 85
    scientific 80
    fraud 79
    level 67
    *** 58
    analysis 57
    dataset 49
    etc 47
    ethical 46
    duplicated 45
    e_g 43
    dont 43
    results 42
    evidence 40
    error 38
    thread 36
    random 33
    basically 33

[this person don't like apostrophes]

Apparently word clouds are the popular thing to do with word frequencies:

    library(wordcloud2);
    set.seed(750); # most common non-trivial number
    data.tbl <- read_table("wordfreq_notenhundred.txt");
    wordcloud2(data=data.tbl, fontFamily="Pointilised");

<img src="/pics/jaqass/cloud_all.jpg" alt="Word cloud from the reply posts of a sealion who leapt onto discussion about a paper quantifying the degree of image duplication within research papers, excluding the ten hundred most common English words. The ten most prominent words are data, science, errors, scientific, fraud, level, ***, analysis, dataset." width="512"/>

This person really likes their errors and fraud.

The next thing I want to do involves looking through these replies manually to identify common themes. I have ideas, but minimal experience.

It's manual work, so will take some time, but not looking-for-image-duplication-in-thousands-of-papers time.

Also, this is treading on the sealion's turf.

Anyway, time for LibreOffice Calc.

Given how often the sealion is insulting other people, I have chosen to categorise reponses about being insulted himself as "Thin skin", rather than harassment.

Someone who is intolerant of other people's perspectives has broken the social contract of tolerance.

https://bsky.app/profile/strandvogel.bsky.social/post/3lcwwf6svk22p

Oh, also, because the question was asked, and the sealion has trouble remembering answers...

JAQass: Jackass who Just Asks [bad-faith] Questions

https://bsky.app/profile/sbarolo.bsky.social/post/3k72hjeh4jk2g

I've now added a Category Dictionary to help me out a bit with remembering my categories.

Still going - I'm about half-way through, and will be stopping for the night - but I wanted to share what I've tagged so far as "Nuh uh! [Vacuous or disparaging comment that is only used to fill space]"

I had a category reserved for "Interest - First question on a subject, or other suggestion of genuine interest", but after going through about 2/3 of the replies and looking back at the inception, my conclusion is that such a response doesn't exist. The sealion was adversarial right from the start.

Maybe an argument could be made for genuine interest in an alternative randomised study of image manipulations, but that interest is coloured by the large number of prior responses disputing the magnitude of the problem of image manipulation.

100 more comments to go, but I now need to shift my computer downstairs so that the builder can rip the windows out of our upstairs office / study / bedroom.

When going through the responses, I made a discovery [3lfkgmou5mc2k] that both surprised and didn't surprise me.

It seems to be confirming my hypothesis that the sealion could be faking interest and ignorance for kicks, deliberately misleading and manipulating science experts on social media.

I've now realized that I've got enough written up in my "notes" document that this would be better as a separate, proper, dated post, rather than another entry into my "Two lines" blog.

That approach also allows me to write a different disclaimer, which I think is important here.

Okay, all done. Now I think I should probably review my categories and examples.

In particular, there are a few "Nuh uh!"-tagged replies that would be better placed in the "Filler" category, because when context is lost, the disparaging interpretation is also lost.

FWIW, here are my category counts after reclassifying replies that were in a category with a count of one:

[Theme Categories, counts, and demonstrative examples for the "sealion incident". The most prevalent categorisation is "Reproducibility" [159 replies]; the lease prevalent categorisation is "Demonstrably correct [and non-disparaging]" [2 replies].]

After looking at surrounding context again, I have reclassified the two "Demonstrably correct" replies as "Asked and answered" [they followed up with an answered question about other data] and "Direct misrepresentation" [they were previously talking about "copy-paste errors"].

That's enough messing around in LibreOffice Calc. Here are the post-review category counts (1 of 2):

Direct misrepresentation 74
Huh? 7
Interpretation 13
Lack of knowledge 43
Reproducibility 159
Approval / preregistration 58
Negative tests / file drawer 74
Problem magnitude 82
Reskeet only 19
Response farming 37
Silence / blocking 19
Study resolution 43
Thin skin 60
Tolerance 8
Asked and answered 39
Nuh uh! 59
Leading / hypothetical 19
Filler 27
Paper writing 26
Science Police 39

Okay, now for the word clouds and category explanations.

I have deliberately named some of these in a particular way due to the sealion's bad-faith behaviour: responses that would be fine under normal circumstances, but not with the context of a person that keeps shoveling out more shit.

Word Clouds (1 / 20)

<img src="/pics/jaqass/cloud_thinSkin.png" alt="Word cloud for the 'Thin skin' category. Prominent words are profile, app, comments, oink, instance, bullying, pig, behavior, ad hominem, arguments, science." width="512"/>

... might as well start off with those. The sealion was annoying to many people, and occasionally complained when people told the sealion that the sealion was being a dick.

I have labelled these replies "Thin skin".

Word Clouds (2 / 20)

<img src="/pics/jaqass/cloud_nuhUh.png" alt="Word cloud for the 'Nuh uh!' category. Prominent words are sealion, wikipedia, argument, troll, dont, oink, wasting." width="512"/>

The JAQass disagreed... a lot. When the substance of the comment seemed to be nothing more than disagreement, I labelled this as "Nuh uh!"

Word Clouds (3 / 20)

<img src="/pics/jaqass/cloud_responseFarming.png" alt="Word cloud for the 'Response farming' category. Prominent words are social, thread, science, mistake, respond, mistake, comments, science, post" width="512"/>

Asking questions for more clarification is part of the usual, normal discourse on social media. Until a JAQass "Just" Asks too many simple Questions, then keeps on asking them after they're told to stop.

I decided to label seemingly innocuous questions as "Response farming".

Word Clouds (4 / 20)

<img src="/pics/jaqass/cloud_leadingHypothetical.png" alt="Word cloud for the 'Leading / hypothetical' category. Prominent words include intentionally, errors, duplicated, fraud, trial, ethical, editor, correlation, copy, article." width="512"/>

What if the world existed without sealions? Isn't it weird how some people sometimes just keep saying rubbish false things and demanding you prove their falseness?

Leading statements, or questions about intent got filed into the "Leading / hypothetical" category.

Word Clouds (5 / 20)

<img src="/pics/jaqass/cloud_paperWriting.png" alt="Word cloud for the 'Paper writing' category. Prominent words include social, ive, topic, science, paragraph, published, behavior, public, testing, ill, ignorant." width="512"/>

The academic sealion likes to tell everyone that they're writing a paper, or that they've written a paper on this very subject (or, at least the *argument* component of social media discourse).

Trumpet toots [ordinarily would be fine] got filed in the "Paper writing" category.

Word Clouds (6 / 20)

<img src="/pics/jaqass/cloud_filler.png" alt="Word cloud for the 'Filler' category. Prominent words include beep, fantastic, argument, lol, brilliant, fantastic, thats." width="512"/>

Okay, I think that's the potentially controversial definitions done. Now a bit of a breather with [relatively] neutral statements.

Things that didn't add much to the conversation, but were not obviously caustic, got filed into the "Filler" category.

Word Clouds (7 / 20)

<img src="/pics/jaqass/cloud_huh.png" alt="Word cloud for the 'Huh?' category. Prominent words include Publications, research, generalizable, analyzable, Biomedical, Duplication." width="512"/>

Sometimes I just couldn't work out what the sealion was on about, even when reading through the surrounding context. These replies were filed under the "Huh?" category.

[FWIW, the 'n's that are scattered around in the words are due to line breaks; which appear as '\n']

Word Clouds (8 / 20)

<img src="/pics/jaqass/cloud_reskeetOnly.png" alt="Word cloud for the 'Reskeet only' category. Unsurprisingly, the common words include the sealion's profile name, bsky, app, and profile." width="512"/>

The sealion occasionally replied with a link to another skeet or video, with no other substantial surrounding context.

These got filed in the "Reskeet only" category.

Word Clouds (9 / 20)

<img src="/pics/jaqass/cloud_tolerance.png" alt="Word cloud for the 'Tolerance' category. Prominent words include science, focus, disagreement, weak, identifying, disagree, uncomfortable." width="512"/>

Now onto annoying anti-social behaviour, of the type that I don't think can be excused as something that would be considered acceptable in moderation.

Claims that everyone should tolerate arguments as part of normal discourse were filed in the "Tolerance" category.

Word Clouds (10 / 20)

<img src="/pics/jaqass/cloud_silenceBlocking.png" alt="Word cloud for the 'Silence / blocking' category. Prominent words include tweet,tyrants, tweet, fools, resurgequality, cant, generally, im, type." width="512"/>

What does it mean when someone doesn't reply, or blocks you?

This JAQass would like everyone to know what silence and blocking always means. Even if no one else agrees with them.

I filed claims about the meaning of silence or blocking as "Silence / blocking".

Word Clouds (11 / 20)

<img src="/pics/jaqass/cloud_lackOfKnowledge.png" alt="Word cloud for the 'Lack of knowledge' category. Prominent words include dont, basically, results, discussion, experience, arguments, audit, opinion, indicate, general, judging." width="512"/>

A common complaint of the sealion when they hit an obvious disagreement is that the other person doesn't know enough, or hasn't read all the threads, or is just saying things that are wrong.

Anti-social replies like these were filed as "Lack of knowledge".

Word Clouds (12 / 20)

<img src="/pics/jaqass/cloud_askedAndAnswered.png" alt="Word cloud for the 'Asked and answered' category. Prominent words include data, dataset, available, errors, articles, thread, list, raw, etc, dont." width="512"/>

And then, of course, the classic JAQass behaviour of asking things that have already been answered numerous times.

These were filed in the "Asked and answered" category.

Word Clouds (13 / 20)

<img src="/pics/jaqass/cloud_interpretation.png" alt="Word cloud for the 'Interpretation' category. Prominent words include disagree, editor, scientists, errors, ethics, basically, majority, PROBLEM." width="512"/>

The next two categories are a mix of antisocial behaviour and a slide towards falsifiable arguments. First up are claims about how something is, or should be interpreted.

These got filed under the "Interpretation" category.

Word Clouds (14 / 20)

<img src="/pics/jaqass/cloud_directMisrepresentation.png" alt="Word cloud for the 'Direct misrepresentation' category. Prominent words include fraud, errors, error, level, copy, paste, evidence, duplicated, scientific, thread, data, example, estimated." width="512"/>

The sealion also made statements that are a direct misrepresentation of presented research.

In other words...

JAQass: "This is what the paper demonstrates!"

* checks *

Narrator: Reader, the paper did not demonstrate that

These were filed under "Direct misrepresentation".

Word Clouds (15 / 20)

<img src="/pics/jaqass/cloud_fileDrawer.png" alt="Word cloud for the 'Negative tests / file drawer' category. Prominent words include file, drawer, scientific, results, positive, negative, science, data, report, investigations, errors." width="512"/>

And then, finally, we get onto claims (or pleas) relating to the subject of the paper itself.

Starting with a demand to see the negative results for an observational, best-effort screening study.

These were filed under "Negative tests / file drawer".

Word Clouds (16 / 20)

<img src="/pics/jaqass/cloud_approvalPreregistration.png" alt="Word cloud for the 'Approval / preregistration' category. Prominent words include ethical, pilot, approval, data, preregistration, oversight, materials, labs, investigations, piloted, testing, scale." width="512"/>

The sealion also started complaining about pre-registration, ethics approval, and pilot testing for a mostly-done-at-home observational study.

These complaints were filed in the "Approval / preregistration" category.

Word Clouds (17 / 20)

<img src="/pics/jaqass/cloud_sciencePolice.png" alt="Word cloud for the 'Science police' category. Prominent words include science, data, etc, scientific, negative, positive, scientists, integrity, ethical, standards, tests, results." width="512"/>

The JAQass also started complaining about watchmen, or science police, or something like that, claiming that... er... people who are looking for image duplication in papers should be careful about... image duplication in their own papers?

[FWIW, the JAQass didn't *actually* claim this. What they claimed was more demonstrably weird, that people who are looking for fraud (which, to be clear, was *not* the target of the observational study) should have a full, public list of annotations for all the papers they looked at.]

Filed as "Science police".

Word Clouds (18 / 20)

<img src="/pics/jaqass/cloud_problemMagnitude.png" alt="Word cloud for the 'Problem magnitude' category. Prominent words include duplicated, duplicate, level, fraud, errors, science, problematic, overall, rate, random, million, error, basically, estimation, difference, prevalence." width="512"/>

The sealion also decided it was important to point out that the magnitude of the problem generally in scientific research was different from that observed in the observational study of specific research journals across a specific time frame.

Filed as "Problem magnitude".

Word Clouds (19 / 20)

<img src="/pics/jaqass/cloud_studyResolution.png" alt="Word cloud for the 'Study resolution' category. Prominent words include level, total, duplicated, errors, analysis, random, journals, articles, publisher, prevalence, summarize, sample, statistics, sensational." width="512"/>

The JAQass also decided that the observational study carried out on specific journals across a specific time period, looking for a specific thing in journals, should have actually been looking for something else.

Replies like this were filed as "Study resolution".

Word Clouds (20 / 20)

<img src="/pics/jaqass/cloud_reproducibility.png" alt="Word cloud for the 'Reproducibility' category. Prominent words include data, dataset, analysis, replication, scientific, errors, available, original, reproducible, scripts, etc." width="512"/>
	
Finally, The sealion devoted the majority of their complaints to the difficulty of a psychologist to quickly reproduce the work of a microbiologist who spent months looking for image duplication in tens of thousands of biology research papers.

Filed as "Reproducibility".

There were two categories of responses I had hoped for, but couldn't find when I included the surrounding context of parent and child replies:

* Questions of genuine interest [without malice]
* Demonstrably correct statements [without side-loaded insults]

In other words, they were *always* acting as a jackass.
