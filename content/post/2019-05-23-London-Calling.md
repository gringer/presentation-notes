---
title: "London Calling 2019"
author: "David Eccles"
date: 2019-05-23
---

# Day 1 - Gordon Sanghera

# Zoe McDougall, housekeeping

* Should we do the run again?
   * Money was raised for universify
* Download the App; if you did that last year, please uninstall and reinstall
* Please use social media #NanoporeConf

# Eva Maria Novoa [[video](https://vimeo.com/337887032) / [interview](https://www.youtube.com/watch?v=O9MenBNmxWA)]

* First London Calling conference
* Interested in modifications in general
* Central dogma of molecular biology: a very simple linear process
  * But each step is reversible
  * RNA translation is less characterised
  * There is a difference between transcriptomes and translatomes
  * RNA and protein are not equivalent
* Why such a large difference?
  * Different regulatory elements
  * RNA modifications
    * Chemical modifications at base positions, there are 117 known
    * tRNAs are entities that are capable of regulating translation
    * 2011 study found that m6A modifications in mRNA were reversible
      * Why reverse something that is just structural?
      * 2012 - first method for discovery, coupling antibodies with NGS
      * Boosted the field of publications, now that it could be measured
* Did a survey of the literature - how many RNA modifications were associated with disease?
  * About half of them, but only six of them can be measured
  * Prior to nanopore sequencing, needed antibodies or chemistry
    * Some problems: required a customised protocol
    * The majority of modifications cannot be mapped
* Moved to the Garvan Institute, met Martin Smith
  * Trying to get RNA modifications with nanopore
  * First run was a disaster
  * Getting better
* Theoretically possible to detect these modifications
  * Problems associated with resquiggle
  * Need a training set, so tried to do that
  * Created synthetic sequences containing all possible 5mers
    * CURL CAKEs
    * The idea was to sequence both modified and unmodified sequences
  * m6A modifications created a massive amount of error
    * Might not need the current trace, just look at the error profile
    * Found an increased number of mismatches, deletions, and insertions (not frequent)
    * Errors were replicable
    * The most replicable features were modified A vs [unmodified]
* Trained machine-learning algorithms
* Overall, used a combination of raw features and base-called features
* Aim to validate in-vivo, which is much more challenging, because we don't know what to expect, and assessing the accuracy is difficult
  * For example, yeast m6A vs yeast delta-1ME4 (unmodified)
  * Visual inspection of Illumina results, see a mismatch error
* Will pursue further modifications
  * pU (pseudouridine) caluses massive problems for error rates
* How do we reduce sequencing costs?
* How do we reduce the minimum required input?
* Could barcode on direct RNA
  * Incorporate adapter into ONT's adapter
  * Use deep learning to classify squiggles - 98.8% accuracy, 80% recovery

# Nicola Hall [[video](https://vimeo.com/337887055) / [interview](https://www.youtube.com/watch?v=RJ76uiTm34k)]

# Rob James [[video](https://vimeo.com/337948897)]
# Amanda Warr [[video](https://vimeo.com/337948910)]
# Mahesh Dharne [[video](https://vimeo.com/337948925)]
# Breakout panel discussion

# Lightning talk - Jeff Nivala [[video](https://vimeo.com/337955206)]
# Lightning talk - Sophie Colston [[video](https://vimeo.com/337955188)]
# Lightning talk - Yutaka Suzuki [[video](https://vimeo.com/337955170)]
# Lightning talk - Silvia Carbonell Sala [[video](https://vimeo.com/337955156)]
# Lightning talk - Ned Peel
# Lightning talk - Devin Drown [[video](https://vimeo.com/337955113)]

# Anna Schuh [[video](https://vimeo.com/337887077)]

# Dan Turner [[video](https://vimeo.com/337887091) / [interview](https://www.youtube.com/watch?v=EtcbBena774)]

# Data Breakfast manel - Stephen Pascoe

* An idea of the scale of data
  * Bases to bytes: 1Gb of data representes 20 GB on disk
  * Most of that is the fast5 raw signal, 9% fastq, summary is 1%
  * Easier if you can get by with just fastq [DE - I don't like this idea]
  * Total data produced from a single run is just below, or just above the availablile storage space on disk
    * Need to get data off the device before the run is finished

* Data transfer time
  * Modern networks don't go much faster than 10 Gib/s, more standard is 1 Gib/s
  * Quickly get into many hours of data transfer
  * Consider whether you really want fast5
  * Need to write to SSD to keep up with the device

* Since December, multi-read fast5
  * A huge boon for data management
  * Recently improved directory structure
  * Write summary into termination files, guaranteed to be written at the end of the run

* More Coming
  * NAS transfer (should be in the next MinKNOW)
  * Signal compression, about 30-40% of the gzipped signal (see Chris Seymour)
  * A lot of people want to write to cluster, or be directly writing to mounted storage
  * Developers can use the MinKNOW API
  * ONT has an internal analysis automation using custom asynchronous http (a whole new talk)
  * Expect to be presenting a non-developer API

* Questionnairre
  * More people are anylysing on a compute cluster
  * Surprisingly few doing analysis or storage on the cluid
  * Most prefer analyses at the end of the run, not during the run
    * For those that want analyses at the end of the run, should re-think this

* Questions
  * ONT has a script for slicing and dicing fast5, should be available soon
  * Would be good to have an initial file
  * Transferring data during the run - can mount storage on GridION
  * File transfer is not currently configurable, relying on the OS (e.g. mounted S3)

# Data Breakfast manel - Mike Vella [[video](https://vimeo.com/341513659)]

* Using GPUs for genomics or assembly
   * Have started to become more significant since the last ten years

* CPUs started to plateau about the mid 2000s

* GPUs are meeting or exceeding Moore's law
  * CPUs are optimised for latency (lots of different things at the same time)
  * GPUs are optimised for throughput
  * GPUs have thousands of cores, and are significantly simpler
  * GPUs have a very high memory bandwidth
  * There is a nice new feature (NVLink)
    * Increased speed interconnect
    * Can pool a large amount of memory (14 TB/s) memory into compute

* Why Genomics?
  * Parallel workflows
  * Deep learning (Guppy / DeepVariant / Medaka)
  * Edge compute - Increase in sequencing throughput is vastly increasing over networking bandwidth
  * Trend probably isn't going to stop

* Why *de-novo* assembly?
  * Pro: more species are not assembled, no reference genome
  * In humans, increased resolution
  * Cons
    * difficult to analyse
    * very computationally expensive; even with ONT, and 10M human reads

* Two types of *de-novo* assemblers: de-bruijn Graph (DBG) versus Overlap-consensus (OLC)
  * OLC are good, but computationally challenging
  * DBG is faster, but doesn't have great assemblies

* Consensus / alignment algorithm - partial-order alignment
  * Expresses sequences as a graph
  * Consensus called by Machine learning, [approximately] through a graph
    * Tricky to parallelise
    * GPUs have "warp shuffles"; allows inter-thread communication
    * When updating gap stores, can do it in a semi-parallel way

* Acceleration Results
  * Racon, or polishing on a 100Mb simulated genome
    * 40 minutes on an Azure V32 CPU
    * With banded, or adaptive banding, 1 minute on DGX-1, 34s on DGX-2
    * Can start to think about hours or days for workflows down to minutes

* Future Work
  * Accelerated racon: [developer.nvidia.com/Clara-Genomics](https://developer.nvidia.com/Clara-Genomics)
  * API: Python and C++
  * Long-read mapping
  * Deep-learning-based consensus
  * Guided by what the community wants
  * Would like to see *de-novo* being done in real-time on the device
  * NVidia will have DirectGPU - can put data directly onto GPU

# Data Breakfast manel - Ewan Birney [[video](https://vimeo.com/341513722)]

* Play with [github.com/EGA-archive/ont2cram](https://github.com/EGA-archive/ont2cram)

* EBI promises to store all data if you are an academic, but wants us to use data compression

* ONT's fundamental measurement is current over a fixed time
  * Change in current is critical
  * Improved basecallers will work off the signal
  * Signal contains additional information [beyond what is encoded in the called bases]

* CRAM overview
  * Same data model as BAM
  * Column-oriented compression, hooks for custom compression
  * [identified] DNA sequence compression against the references
  * Can handle unaligned data, but optimised for alignment-based storage, optimises for the largest use case

* Mapping everything in HDF5 to CRAM
  * Can do lossless mappings between HDF5 and CRAM
  * Preserve the content, not the structure
  * Some technical aspects on HDF5 tags
  * Prototype is written in Python, uses h5py and pysam [as external dependencies]

* CRAM2ONT coming
  * Old event plus signal hdf5, able to compress 50-fold [by removing events]
  * We should not be storing events
  * With modern ONT files, get about a 30% compression

* Might be a stage when it is natural to flip from fast5 to CRAM

* Realistically can do 30% reduction
  * Are there better compression schemes for the signal?
  * How much precision do we need to store (implicit auto-smoothing)
  * Base qualities are 50-fold smaller than the raw signal
    * Maybe there is an intermediate between signal or PHRED
    * Can be more aggressive with PCR-based sequence
    * But different for native DNA, different again for RNA

* Reference - Ewan recommends GRCh38

* Some implementations of CRAM allow parallel processing, sharding across the genome
  * There's no lock, because you don't write into CRAM
  * Usually map, then convert to CRAM

# Day 2 - Zoe McDougall

* At Marco Island in 2012, Clive talked about PhiX and GridION
* He spent the last two minutes on the MinION
* The ONT tweet about the MinION went out on the 17th February 2012, they had a massive response from people who wanted to take the sequencer to the sample
* Feedback from ONT has helped to hone our philosophy
* Anna-Lise is not able to be here today; she is sequencing in the ocean

# Karen Miga [[Video](https://www.youtube.com/watch?v=Mdh3tS2LeB4)]

* [walk-on music - Everything Is Awesome]

# Christopher Oakes

# Daniel Depledge [[video](https://vimeo.com/338852545)]
# Monca Keyhoe [[video](https://vimeo.com/338852554)]
# Heather Drexler [[video](https://vimeo.com/338852566) / [interview](https://vimeo.com/338044102)]
# Breakout panel discussion

# Lightning talk - Tomasso Leonardi [[video](https://vimeo.com/338064340)]
# Lightning talk - F. Veronica Greco
# Lightning talk - Susan Engelbrecht [[video](https://vimeo.com/338064277)]

* Retroviruses have reverse transcriptase + integrase
* Primate retroviruses are very different from each other
 * HIV / SIV was extracted 1984-1990
* Stored DNA for about 30 years, waiting for the MinION to arrive
* Used LSK-109 + GridION
  * Consensus assembly was blasted
  * Had about 890,000 reads, 13 of them were lentiviral reads
* Also looked at a Chacma baboon

# Lightning talk - Charles Kayuki [[video](https://vimeo.com/338064256)]

* Using genomes to fight plant pathogens
* Optimal yield of Cassava is 10 tonnes per hectare
  * The most threatening factors are the brown streak virus, and the mosaic virus
* Illumina and other technologies are under-utilised in the developing world
  * High cost
  * High maintenance
* Used MinION + PDQex to minimise the sequencing effort
* After 30 mins, got a good extraction
* MinION + MinIT produced useful results in two hours (reaching the limit of the battery)
* In most developing countries, power is a struggle
  * I don't recall a 48-hour run that has run to completion before the power died

# Cheryl L Ames [[trailer](https://vimeo.com/329333085)]
# Emma Langan [[video](https://vimeo.com/338183720)]
# Shaili Johri [[video](https://vimeo.com/338183706)]
# Plenary panel discussion

* Most surprising incidental discovery
   * Getting a large chunk of the shark genome [were only sequencing at low coverage]; shark gut microbiome had all kinds of things
   * *Melinaphis sacchari* (sugar-cane aphid) in the middle of the ocean
   * Got insects picked up in the water column
* Often times specieas that are poached are sent to us for analysis
  * For permits, etc. generate a map of poaching hotspots
  * But that's six months apart, by which time everyone had vanished
* A large part of MinION is capacity building, making the process easier and less harmful

# Jeroen deRidder [[video](https://vimeo.com/338183687) / [interview](https://www.youtube.com/watch?v=5M-xxwNMGcY)]

# Clive Brown [[Video](https://vimeo.com/338137948)]

## Novel statements selected by David Eccles

* **New regeneration chemistry - separate module, turns ADP back into ATP**
* **Credit for returned used flow cells**
* **Small ASIC - A new one, very small, about 400 channels, at least as good as MinION**
* **Plongle - plate-format flongle with small ASICs**

## Introduction

* Music from movie that came out in 1969 - Casino Royale, original
  * 3 different James Bonds, at least 5 directors
  * still remains one of the most popular Bond movies in the franchise

* Going to change the format for the talk. Too much detail, always make mistakes

* Sharing the pain this year.

* Getting people to focus on what they're doing every day

* What is Plongle? Whenever I think of these crazy names, turns out to
  be something rude in Australia

* A couple of surprises at the end, and a WTF moment, will leave you
  thinking like you've been slapped in the face with one of those
  jellyfish you heard about earlier

* A wealth of talks, a lot of background information, all on the website

* Our mission - enable anybody to sequence anything anywhere
   * A lot of good examples of that this year
   * Long reads add value
   * It doesn't have to be done in the lab

* Explain slide - I do it anyway, because it gets me warmed up
  * Entire system can be very real time
  * All enabled by some fancy electronics

* Full product range, across entire spectrum of user types
  * MinION, GridION, PromethION - Out of beta, fully enabled, selling and shipping around the world

* A slide that shows what we've achieved
  * Last year - incremental software - don't underestimate
  * Flongle out - about a month ago
    * Enormous interest, way more than MinION
  * Voltrax V2 is now out, that's the fully-featured version. Not static, will get better and better over time
  * Huge improvements to MinKNOW
  * Upgraded base calling
  * Own consensus caller, SNP, and mutation detection
 * Latest MinKNOW enables PromethION to work on all the flow cells all the time

* PromethION
  * Can run all 48 Flow cells at the whole time
  * 8,000 whole human genomes per year
  * Have had as high as 180Gb per flow cells
  * Have topped at over 7 Tbp
  * Have had customers with similar performance
  * 24 / 48 flow cell version
  * One customer got 5.5 Tb out PromethION
  * Best flow cells (if they were all that good - just an optimisation problem - would have 9 Gb, head-room to double that

* PromethION applications
  * Telomere-to-telomere
  * China services

* GridION
  * Some technical issues with the power supply
  * has been revamped

* Mk1c
  * A mobile phone - contains a phone OS, has an ESim, can be used on mobile networks; has been available to buy since March
  * Ship late July / August
  * Should be useful for applied markets (Doctors, dentists)

* Flongle (er... pretty impressive)
  * Array of metal connectors, much cheaper disposable flow cell
  * Less than $100
  * Still headroom for improvement, can prob get 4x improvement
  * Have had customers getting 2Gb - better than ONT
  * Works in field
  * Just build new factory in Oxfordshire

* Will let other plongles come up now, I think they will do a better job

## Stuart Reid - Oxford Physics Graduate

* Clive told me we were sharing the glory, not the pain
* MinKNOW
  * The piece of software running your sequencer
  * Doing a balancing act
  * Piling in the new features to make things work better
  * Have received feature requests by the community - we do read them
  * New released on run management / control
  * Aggregate for PromethION / GridION
  * Next release pushing on data management
    * Compression features
    * Fold in integrated basecalling
    * Barcoding
    * Pause run - interact with the flow cell in a safe state
    * Integrate basecalling of existing data - no more command line
  * Operate across all the boxes, a few PromethION-specific
  * 24 runs in January
    * multi-read fast5 format - helped out to manage data
    * Enabled all kits
    * PromethION can now do everything other platforms can do
  * New version - enabled flip-flop high-accuracy
  * Can run DNA and RNA on the same box
  * Dramatically lowered the CPU load, enabled 48 flow cells, now out in the field and working
  * Introduced choice in UI (fast vs high-accuracy)
    * Fast base caller will keep up on our platforms
    * High-accuracy, a bit more computationally intensive (best consensus)
    * Excellent pipeline - basecall everything in fast model, choose reads to re-call using high-accuracy
  * GridION is the box with the most headroom, will be able to keep up

* Accuracy - 3 types
  1. Raw read - 1D, single pass (not multi-read)
  2. Consensus accuracy
  3. Variant calling - is this different from the reference

* Raw read accuracy
  * Started with HMM, was all right for us, every year have made a major change
  * Every change, made fewer and fewer assumptions of how the data is treated
  * Delivers good improvements, but coupled with chemistry changes
  * Using the same R9.4.1 data now, improvement from 90% to 95% modal accuracy
    * 10% error down to 5% error using just algorithms
    * We don't think we've finished yet
  * Base calling - flip-flop
    * None of this kmer stuff
    * Discovered when we were doing development, algorithm got a bit confused
    * Algorithm flips and flops to count repeated bases
    * Gives a few advantages, pretty simple decoding
    * Hopefully human-interpretable trace output
    * More accurate basecall

* How are we doing?
  * Flip-flop basecallers
  * High-accuracy model giving 95% modal accuracy
  * Datasets are fairly representative, mixture of native and PCR
  * Much bigger improvement on RNA, have really closed that gap
    * High accuracy - 94% modal accuracy
    * All with the data you have

* How do you train your base callers?
  * Ignore / call modifications
  * Use Taiyaki tool - same training set that ONT uses
  * Can train / call modified bases

* Modified bases
  * Will fold approach into core product
  * Expanded the alphabet - from 4 bases to 5 bases
  * That doesn't necessarily scale (e.g. for RNA)
  * Categorical approach, additional dimension
    * 4 bases + modification
    * Single model for 5mC and 6mA - more is a data-set challenge
    * Calling modified A, and modified C
    * Integrated into Guppy and MinKNOW from July onwards

* Improving 1D base-calls
  * Algorithm - run-length encoding
  * probability distribution to run length
  * Any similarity to 454 is entirely coincidental
  * Flip-flop consensus - gives imrpovement in consensus accuracy
  * Closes a significant amount of error gap
  * Can have a chat about early access
  * Can use raw basecalls without platform-specific knowledge

* Consensus - Medaka
  * Don't need to keep the raw signal
  * 1 minute for 10Mb of draft on GPU
  * Epi2Me workflow
  * Does a pretty good job, averages out most of the errors
  * Don't know of any sequencing technology that has bias
  * If your definition of random error is that they average out in consensus, then ONT errors are random

* ONT consensus
  * Where we were this time last year, signal-polished q mid 30s, now q mid 40s
  * An improvement of q10 in a year from algorithms, using the same data
  * Not finished, still improvements
  * Information from the basecaller that we're not using

* Variant calling
  * Have adapted medaka to do variant calling
  * A community tool doing better currently
  * Medaka consensus - fairly easy to phase reads - use WhatsHap
    * Don't have to call heterozygous variants
    * 98% recall
    * Clair - successor to Clairvoyant, gets 99% recall, 99% precision (competitor)
    * Can do SNPs with Nanopore data to 99%
  * Structural variants - really need long reads to do this
    * Epi2Me workflow - uses Sniffles, similar authors to Clair
      * 96% recall, 96% precision

* Everything here is R9.4.1 data, what you've had for about a year
  * 95% DNA, 94% RNA, q40 consensus

## Lakmal Jayasingle

* Head of nanopore research
* Develop new nanopores, engineer precicely
* Release of new nanopore, will usually make at least 300 versions
* Will show you how much further improvements we can do in accuracy by improving the nanopore

* Current nanopore
  * CsgG from E. coli
  * Approximately 5 bases dominates the signal
  * When there is a homopolymeric region, there is a problem
    * Flat regions in the signal, affects accuracy badly
  * R9.4.1, accuracy drops down significantly when homopolymer length > 5
    * The main source of errors for current chemistry

* R10 - Two read-head pore
  * R10 has homopolymer length where unpolished accuracy that does not drop down significantly
  * Still really high along homopolymer length
  * Can expect very high consensus accuracy
  * Talked about in London Calling last year
  * Polished R9 data with R10
  * Clive talked about two-pore system, could bring both together with complement accuracies, so errors with one
  * Since then, had improved R10 so much, R10 was hitting q42 in consensus
    * Let's give up R10 as high-accuracy pore
    * Only a few people have tested R10
    * Have set the bar quite high to release R10 (Clive wouldn't let me)
      * Raw read accuracy low
      * Throughput very low
      * Some sensitivity issues

* Throughput
  * R10 has more places to block sequence - needed to be fixed
  * Improved blocking via mutations
  * Improved pore sample quality
  * Still some blocking, needed to change software
  * Have improved thoughput, getting 26Gb in MinION flow cell, 100Gb in PromethION flow cells
  * Also investigating other conditions

* Improvements
  * All made massive improvement to raw-read accuracy for R10
  * Currently hitting 95% raw read accuracy for R10

* Have trained medaka on R10
  * Even at 25X coverage, comfortably above q40
  * at 100X coverage, hitting q50, significant improvement over other accuracies

* Nanopores and Accuracy
  * Also making direct RNA-specific improvements to R10
    * Some issues with enzymes in pores, can also have RNA-specific imrpovements
  * Also looking for new pores, R10b, R11, also long read head pres
  * Some of pores have different signal, even in homopolymer region
    * Could bring back two-pore system, high consensus combined

* Beyond q50
  * Have good path to improve accuracy even more
  * Also have new base callers
  * Also talked about 8B4 - randomly modify bases in DNA
    * Working on that
  * In different configurations, can have really high accuracy
  * R10 is worth the wait

* R10 releasing
  * MinION June-July
  * Prom July- August
  * Flongle September

## Andy Heron

* Like to adapt nature's machinery
* The heart of sequencing itself, function at very high speeds, stable and storable
* For high accuracy, need to move DNA though nanopores hundreds of bases per second, in metronomic, step-wise manner
* Just need to get those on your genomic sample
* Currently bring molecular molecules onto DNA via adapters
  * Stalled, ready to go on the nanopore, will keep running for hours (?days)
  * Will only stop when they encounter damage they can't get around, or at end
* Leaders / tethers as well

* Have different kit offerings
  * Field criteria - simplicity and speed
  * Portfolio and options that suits that criteria, but not too complicated
  * RNA world - worlds current only way of sequencing RNA directly
  * Inputs are moderate, typically only 1µg needed
  * Many people are offering more barcoding options
  * Consolidating sequencing kit options, improving robustness and performance

* Key offering improvements since last year
  * Want sequencing to be a fast and simple process
  * Rapid - transposase pre-loaded onto an adapter, provided in liquid form
    * Seeking to simplify this further, lyophilised version of the rapid kit
    * Will continue to deliver liquid forms for those that want them
    * Fits into ubik portfolio of products - sample to sequencing, with minimal extra work
    * Integrated solutions, might look difference

* cDNA sequencing
  * A full revamp in software and chemistry
  * ONT cDNA is the complete package - full length, and quantitative (for differential gene expression)
  * Throughputs were not great
  * Can expect excess of 20M reads for cDNA, PromethION 100M reads
  * Targeting for cDNA 200M reads on a PromethION
  * Barcoding, will allow multiple samples, e.g. 6 barcodes, each with >10M reads
  * Isoform detection

* Direct RNA
  * Particularly passionate amout
  * So much RNA that we don't know what it does
  * We are the only company in the world that can offer this
  * A molecular motor that can function on RNA
    * Motor not as good as DNA, only runs at 70 bases/s
    * Can sequence any RNA
    * Will create a revolution in the RNA world

* Customers crying out for more
  * Now at about 94% accuracy
  * Base modifications are incredibly important and biologically relevant
  * Over 70% involved in human diseases
  * Training base modifications into the base caller
  * Labelling up categorically the modification
  * Have upgraded the motor, now runs at 120 bases/s, effectively doubling the throughput
  * Now seeing in the region of 5-6M reads from new RNA3 chemistry
  * Total RNA - don't need difficult & lossy steps, same fantastic outputs
  * RNA coming to PromethION
    * Currently getting about 6M reads from RNA2
    * Targeting 15M reads from average library on a single flow cell

* Fuel depletion
  * Everyone likes to see the green screen
  * Overloading flow cell is consuming fuel in a futile manner, slowly depletes system
  * Exxagerated case - overloaded sample has significant speed drop
  * Addressed with a variety of updates
  * **New regeneration chemistry - separate module, turns ADP back into ATP**
  * Much improved yield
  * Quality of data will go up

* VolTRAX
  * Your one-stop shop
  * Has additional features
    * Heating elements
    * Magnetic elements
  * Out there, performing very well
  * In our hands, we have experienced the same
  * Libraries that come out of VolTRAX are the same or better than pipetting
  * Want to update it for a range of kits
  * Coming soon - WGA, 16s, rapid barcoding, direct RNA
  * Want to do direct from sample on the VolTRAX - system will do lysis as well
  * Customer customisation of own library preps

* Continually improving portfolio, will see improvements in yield,
  performance, and robustness
  * Goal in the short term to enable 10ng inputs to saturate flow cells, eventually 1ng
  * Watch this space, plenty more to come

* Single-molecule accuracy
  * Is this platform capable of high-single molecule accuracy
  * Raw read accuracy has now increased to 95%
  * There's more to be had in the signal
  * There's plenty more to be had in the chemistry
  * Should be able to get to 99% and above
  * Some applications where each individual molecule is important
    * Some people asking for q30 and beyond
  * Options
    * Pile up multiple reads

* Unique Molecular Identifiers
  * Not aware of anyone using it for high single-molecule accuracy
  * Previously just talked about for 1D^2
  * Tag up moleculees with random barcode, put 30mers or so, informatically stitch it together
  * With raw-read accuracies can make very high accuracies with both R9 and R10

* UMIs are not for everyone
  * Rolling circle amplification is most established
    * Can be difficult to circularise both fragments
  * Circular with dumb bells
    * No requirement for additional step to read complement
  * Linear consensus
    * Keeps one copy of native strand

* What does the data look like?
  * Linear or circular consensus - template and complement data is shifted
    * A large reason why 2D was abandoned previously
    * Building new structures
    * Developing new algorithms that will call though the molecule
    * Accuracies are not quite as good, because complement is not quite where we need them to be

* Rolling circle amplification
  * Very high accuracies
  * Want to get complement data - it adds additional information
    * Complement looks a lot like temporary data
    * If only template-only, getting to high q20
    * Getting to q30 with template + complement
  * More to be had from other methods
    * 8B4 is another option

* Will finish up there before the lawyers get to me

## Rosemary Dokos
  * When, How, How Much
  * Continuously release products
    * Will release a lot more
    * We gather feedback
    * We released something onto the store that caused an absolute sensation
      * A $5000 pack wasn#t the right way to go
      * Now have $1860 flongle pack (12 flow cells, including adapter)
    * Have made single individual flow cell now ($90)
  * A really clear upgrade path for Mk1c
    * Full credit for MinIT
    * Have thought about Mk1b
      * **Credit for returned used flow cells**
      * Mk1b belongs in your pockets
  * P24 / P48, same thing

* Sequencing upgrades
  * Price per Gb, with a scalable platforma
  * R10, q40 at 25X
    * Coming out soon, can start ordering now
  * If dependent on R9, will still have access

* VolTRAX
  * Can now buy cartridges / kits ($200-$330 each)

* Analysis
  * All of research tools on github
  * Working hard to get bioinformatics tools

* Next 12 months
  * Shopping list, measuring for next conference
  * PromethIONs shipping, Mk1c
  * Aiming for MinKNOW software release every month
  * Chemistry - a slew of kits coming, continuously updated

## Clive Brown - And Another thing

* Note to self - be more tyrannical

* What is plongle?
  * Can start thinking about products for next 18 months or so
  * A lot of interest in SmidgION - perfectly feasible to make, have been waiting for a new ASIC
  * If there is a barrier to entry, ASIC is the key thing
  * **A new one, very small, about 400 channels, at least as good as MinION**
    * Very cheap, disposable
    * SmidgION would have ASIC in flow cell, still disposable
  * Can't proceed with SmidgION, because still can't put a drop of blood on it, waiting for a direct-to-biology interface

* New product - Plongle - plate flongle
  * It's too big, it will get smaller
  * A plate format sensor, each well has one of these small ASICs, can parallelise what you're already doing with flongle
  * Not shown here - a plate that will connect to ASIC
  * Parallel 96-way ubik, want to encapsulate that in a parallel format
  * Plate-based workflow, very very cheaply
  * Next year, not this year

* Droplet platform, combine VolTRAX TFT with MinION electronics
  * Droplets tiny, potentially down to 1nl
  * Still working on this, still moving on
  * We know we can smash cells
  * Can catch what you sequenced
  * Not going away, not this year
  * Fully-functioning bread boards

* Another interest - a lot of talk about DNA storage
  * Can't let another year go by
  * Audience - Jeff Nubale - about storage of DNA
  * Spoke to Intel and others, use DNA as a storage medium
    * Challenge is in synthesising DNA
    * Envisaging a cloud-compute shed, another running parallel phosphoramidide sequences, another shed with NovaSeqs, etc.
    * Carbon footprint will be absolutely horrific
  * Now a project with nanopore, a differnt scheme
  * Crazy thing is that they're synthesising the storage medium
    * In nature, have 10^47 natural bases of DNA being synthesised for you every time
    * Idea is to take natural DNA (know what the sequence is), use nanopores to modify the DNA, write on it like a tape
    * With nanopores, can see the modifications being made, can write digital information on a pre-existing backbone

* Proposal
  * Get very cheap DNA with an enormous chromosome
  * Thinks we can do this, write gigabits per hour
  * Making the opposite of what they're doing
  * A nanopore device
  * Very tiny chip loaded with DNA
  * Overnight / every week, archives dropbox folder
    * Take chip, put it away
  * Read DNA back using the same technology used to write as to read

* The advantage of scaling down is that we can scale it up again
  * Thousands of little computers on the back of a Google cloud
  * No nasty chemistry
  * Can slide DNA up and down
  * Key thing - isolated bit we want to modify is encoded
  * Crazy idea - one file per molecule
    * Currently fragment file across multiple sequences
  * Only objection (apart from technical information) is density
    * Even at ten fold loss of density, DNA pellet 2.5mm to store Encyclopedia Britannica
    * Way to implement archive-based storage
  * I think it might be commercially viable

## Questions
* Do additional questions via the Application
* What percentage of reads will pass QC
  * 90/95% are passing QC, we think tightening of the chemistry will be fine
* Synthetic pores
  * e.g. DNA origami - will be a good one to look at
  * Don't underestimate protein pores

# Day 3 - Gordon Sanghera

* First thing's first - there's a competiton to guess Matt Loose's real age

# Min Sung Park

# Lightning talk - Jungwoo Lee
# Lightning talk - Sara D'Andreano [[video](https://vimeo.com/338845319)]
# Lightning talk - Mohamed Sater
# Lightning talk - Katie Doroschak [[video](https://vimeo.com/338845345)]

# Spotlight talk - Irina Chelysheva

* Looking at tRNA sequencing with ultra-long reads
  * Using fonts
* 600 hats are even more better

# Spotlight talk - Rebecca Richards

* Crime with biological evidence, gold ring in the middle of the stream
* Someone cuts their hand on broken glass while stealing the ring
* Imagine that sequencing can be done within a matter of hours
* Also works part time at ESR, how sequencing is changing
* Who stole the golden ring?

# Spotlight talk - Samuel O'donnell

* New Zealander as well, working in Paris on Fenovar
* Shifting the paradigm to the reference assembly panel
* Can start to look at [...?]
* Developed a script [mamarco], which compares an assembly panel with a reference

# Spotlight winner - Rebecca Richards

# Spotlight runner-up - Irina Chelysheva [[video](https://www.youtube.com/watch?v=rT0rSFa2MH8)]

# Mark Ebbert [[video](https://vimeo.com/338855198)]
# Martin Frith [[video](https://vimeo.com/338855207)]
# My Linh Thibodeau [[video](https://vimeo.com/338855231)]
# Breakout panel discussions

# Yue Wang

# Rachael Tarlinton [[video](https://www.youtube.com/watch?v=Ohujk17Og2A) / [interview](https://www.youtube.com/watch?v=lxkfS-fotJo)]
