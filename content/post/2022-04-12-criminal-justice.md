---
title: "Criminal Justice"
author: "David Eccles"
date: 2022-04-12
---

***"In a society in which close to 30 per cent believe that women are
partially responsible for being sexually assaulted (against their
will) if voluntarily drunk, obviously, if a woman agrees to sex, she
must be held responsible for her decision???even if, being drunk, she
lacked the capacity to make it. This view, it is submitted, should
pass from the world along side other prejudices."*** - Wallerstein,
S. (2009)

## Prelude - How to Seek Help

If you have been hurt by sexual harm (any form of unwanted physical, verbal or
visual sexual contact), remember that you are in control of the way that you are
healed. You have the right to determine what is best for yourself and your own
body.

Resources are available to help you through the healing process. If you are
feeling overwhelmed, consider contacting your local rape crisis centre. Here is
some information from the centre nearest to where I live:

https://wellingtonrapecrisis.org.nz/information-for-survivors/

# Rape Trials And Our Criminal Justice System

Trigger Warning: rape, justice system, jury trials

TLDR: I was on a
[jury](https://www.justice.govt.nz/courts/jury-service/how-a-jury-trial-works/jury-trials-in-depth/)
for a rape trial. My recent experience left me in a state of shock. I'm writing
this in the hope that other people can know and understand what to expect, and
maybe help change our racist, sexist system.

For those who were curious about my expectations for jury service, I
didn???t expect, two weeks ago, that I would have a traumatic experience
from my jury service. It was me exercising my responsibility as a
citizen. It would be fine... except it wasn???t.

*Disclaimer: I have been told that I can talk about most things, but
should stay away from anything that could identify people who were
involved, especially the jury (apart from me, obviously), the
defendant, and the complainant. Due to these concerns about breaching
privacy, I???ll keep things as general as possible to get my point
across. This will hopefully also help others to understand the
magnitude of the problem: these events are happening all the time. The
problem is not this single trial.

## Harassment In Court

Witnessing and participating in a rape trial has traumatised me. It
brought back memories of my past trauma, of unrelated times when I've
felt that I have encountered a huge injustice, and am powerless to do
anything to change it.

During the trial, I watched in shock and horror as a woman was
harassed and abused by a rich, greedy, powerful man. I watched as an
entire roomful of people sat by and also silently watched this, forced
to bear witness, and unable to do anything to stop it.

I watched, knowing that another man was watching the woman while she
was harassed; a man who had previously done something to the woman
that, in the very least, she was horrified by a day later. This
woman???s experience of the trial will have been much worse than mine.

I watched as another woman was ripped apart and broken by the inner
workings of a sexist system. I watched as her and others chose -
against their own conviction - to follow the letter of the law: they
were human, and hand an ounce of doubt amongst a tonne of belief.

It broke me as well. I allowed it. I didn't speak up about it. We had
a stated, explicit threat hanging over all our heads that a hung jury,
or a wrong verdict, would lead to even more pain and suffering in the
future for everyone involved. My knowledge let me down. My words
didn???t help; my silence was worse. My natural hesitation and
reluctance towards conflict led to a belief that things would be
better if I let go. I bear witness to yet another tragedy, and can't
help feel that I am to blame for it.

## Systemic Failure

I???ll give some details later, but I wanted to start with general
statements, because this injustice is pervasive and common. Everyone I
have spoken to about my concerns has essentially acknowledged the
problem, that it is systemic. The problem is not this single trial.

A rape trial should not be a person???s first experience as a juror. I
didn???t expect this to happen to me, I *know* I have been manipulated,
both in my emotions and my thoughts, and I can't help shake the
uncomfortable knowledge that the trial was this way by design.

The lawyers, those "learn-ed friends", debated with each other in
closed session about which evidence should be presented to the jury,
and I am led to assume that the trial was such a farce by design. I
assume that the trial was designed to be difficult because the
alternative - that the prosecuting lawyer didn't think about what
experts would be useful in a rape trial, or expected that their case
would be open-and-shut easy - is even more unbelievable to me.

## Some Advice

Bearing in mind the disclaimers above, here is some advice for people
who are not familiar with the justice system:

* Past traumatic experiences, no matter how minor you think they are, are good reasons to exclude yourself from a potentially traumatic trial
* Believe women
* Repeat what women say, *especially* if you agree with it
* If you???re unsure about something, ask a question about it
* If you have a concern, no matter how minor, speak out about it as early as possible
* Don???t rape people
* If there???s *any* doubt, ask ???Do you want to have sex????
* As a juror, sometimes you can ask questions too. However, you should save
these until the prosecution, defence and judge have all examined the witness.
You can then write your question down and pass it to the foreperson to give to
the judge. The judge decides if they will ask the witness your question.

## Common Elements of the Trial

*Disclaimer: I am, for various reasons, giving you incomplete
information. The jury is presented with all the evidence that the
lawyers claim is important to reach a verdict, and small bits can
influence judgements. Please don???t tell jurors that their judgement
was wrong, or suggest that you have a better understanding of the
case. Jurors do what they???ve been asked to do, and make their own mind
up about how to do that. I accept the judgement of the other jurors in
this case.

My core complaint is about the system. It is broken. It breaks
people. It is unfair. It is often unhelpful.

The rape trial I judged on involved a man and a woman who were
drinking at a student party. They had sex; that point was
undisputed. The issue was around consent.

For a rape conviction to be laid down, there are three key points to
be proved, which I will attempt to explain in my own words:

1. Penetration [specifically penis / labia] must have happened, no matter how minor
2. Consent was not given; consent does not need to be verbal
3. The defendant believed that consent was not given, or (alternatively) a reasonable person would not have believed that consent was given

In every criminal trial, there should be a presumption of
innocence. Complainants in rape trials are told that they will be
believed, that they are not on trial, but the reality is that they
effectively *are* on trial. The jury needs to decide who is telling
the truth.

In order to keep this more generic, and to preserve a bit more
privacy, I'm not going to mention publically the verdict for the
particular trial I was involved with. If you really want to know, feel
free to DM me on [Twitter](https://twitter.com/gringene_bio).

The woman (the complainant) had access to more alcohol than anyone
else at the party. The man (the defendant) was less drunk.

In a rape trial, the prosecuting lawyer does *not* represent the
complainant. This surprised me. They are not there to support the
complainant. The prosecuting lawyer is there to present evidence that
they believe will prove that rape has happened.

The woman claimed that she did not consent: she tried to push away on
multiple occasions, and said she told the man to stop (in various
different ways). The man claimed, although there was no verbal
consent, the reciprocal actions of the woman demonstrated consent.

We were told that drunk people cannot consent. We were also told that
consent for other acts does not indicate consent for sex, and other
related things. We were also [confusingly, in my opinion] told
explicitly that a person disinhibited by alcohol can provide consent.

Both the prosecuting lawyer and the judge seemed to be concentrating
entirely on the alcohol consumption aspect of consent. This [to me]
meant that they did not believe the woman, who said that consent was
not given. This seemed wrong to me. Why not believe women?

Another thing that annoyed me about the case was that there were
essentially no expert witnesses. For a case with a central argument
about alcohol consumption, why was there no expert witness on alcohol
metabolism, inebriation, or consent?

## Memories, All Alone

As evidence, we were shown a recorded interview from the woman,
created about a day after the incident. We were also shown a recorded
interview from the man, created about a month after the incident. The
man was asked leading questions, the woman was not.

I understand from my psychology education that memories are more
reliable when they are recalled closer in time to when the event
happened. Memories change over time, and a person's confidence in a
memory bears no particular relation to its correctness. Asking leading
questions can change both a person's recollection of a memory, as well
as their confidence in a memory. For people who are in a situation
where something serious has happened, the advice that was given to us
as students was to not talk to anyone until writing down as much as
could be remembered, then signing and dating the recorded
memory. Treat this record as the most reliable source of evidence: if
there's a disagreement between a recalled memory and the written
statement, trust the written statement more.

There were no expert witnesses to talk about memories, witness
testimony, and stressful events. The woman claimed directly after the
event that she had no memory of it, but her memory came back in
patches over the next day. She remembered bits and pieces, some
clearly out of order, and included specific and highly-accurate bits
that were seemingly insignificant. The man claimed essentially full
recollection, with a consistent, gapless narrative that described a
near-perfect sexual encounter.

[There's other understanding I have with regards to the reliability of
narrated memories, but I'll save that for when I can find the source]

Shortly before the sex, the woman had asked the man if she could be
taken home; her friends had left the party at that time, so *I*
assumed that she would have felt lonely and wanted to get away from a
place she didn???t know. The man assumed otherwise. He admitted that he
assumed that she wanted them to go to her house to have sex.

The woman admitted that her memory was unclear. She had blacked out,
and experienced a gap in her memory. There were witnesses to her
confused state afterwards. She didn???t know where she was, or where she
had been. I don???t want to get into details, but there was very
convincing evidence (both in appearance and by what witnesses said she
said) that she was confused after the event. The woman also claimed
that she was still drunk the next morning, and she took a few more
hours to clear up.

## The Aftermath

I could write about more, but I feel it???s pointless to do so. The
details of any particular case don???t matter when the system is broken.

I left that trial jittery, in a rage, knowing that I was powerless to
do anything about what had happened.

I???m livid; I was still shaking a week after the trial. Writing this
out has helped me recover, but I know that others will be in a worse
state than me. I don???t want anyone else to go through that. Our system
of criminal justice is broken, and there are people who are happy to
keep it that way.

## Legislation and Legal Precedent

I looked at the legislation around consent to see if there was
anything that could be made clearer. There isn't, from what I can
tell. It is a problem that we weren't shown this (or allowed to see it
during our deliberations):

https://legislation.govt.nz/act/public/1961/0043/latest/DLM329057.html

Of note is that the law includes a non-exhaustive list of things that
should *not* be considered consent.

We had to rely on legal interpretations. This is where the judges and
lawyers attempt to explain the law to us in a way that we can
understand... except they didn't. For some reason, the judge chose to
state to us that "a person disinhibited by alcohol" could still
provide consent. That was, in my opinion, irrelevant and misleading.

I think I understand why the judge did that now. There is existing
precedent around the idea that "drunken consent is still consent",
even though such a position is inconsistent with the actual
legislation.

I wouldn't be surprised if the lawyers leapt to this precedent, even
though it didn't apply.

Regardless of whether it is applicable in the case I was asked to
decide on, I've found an article which seems to give a great summary
of the legal perspective, and supports my own views on the matter:

https://doi.org/10.1350/JCLA.2009.73.4.582

Some snippets from this article:

> In a society in which close to 30% believe that women are partially
> responsible for being sexually assaulted (against their will) if
> voluntarily drunk.... This view, it is submitted, should pass from
> the world along side other prejudices.

> Subsequently, evidence of gaps in memory should not play against the
> victim as in Dougal and Bree or be at best ignored, but rather count
> in her favour as providing a further proof of the high level of
> alcohol in the victim???s blood.

> Whereas the court in Bree held that ???a drunken consent is consent???,
> it is proposed that the default position should hold that ???a drunken
> consent is not consent???

> At the end of the day, the court appears to legitimise public
> practice of having drunken sex, rather than recognise the harm that
> may be caused to drunkenly consenting women and warn against it.

> This article has shown how the existing legislation may be suf???cient
> if more guidance to juries is given by presiding judges, and with
> further expert evidence regarding the effects of alcohol on a
> drunken woman???s mental capacities.

Given this existing well-presented argument in a published paper, I decided to
write a letter to MPs, suggesting that the existing law be clarified. I have not
yet had a response to this letter, as of about a week after sending it.

I also submitted a complaint relating to this about judicial misconduct. If a judge is advising the jury based on legal precedent that is inconsistent with the law, I think there are reasonable grounds to suggest that
such advice is misleading.

## The Letters

Here are the letters that I wrote. They have been abridged / adjusted to exclude personally-identifying information.

### Judicial Misconduct Complaint

...

My complaint relates to an element of legal precedent that does not seem to be
consistent with our legislation, and that the judge’s spoken advice relating to
that precedent was misleading: leading the jury to a distracting thought process
that likely caused the jury to ignore other evidence relevant to the case.

I am aware that errors of impunity are an accepted consequence of our legal
system working as intended, and do not expect (or wish) that any outcome of the
trial be changed. However, I do ask that my complaint be considered and
reflected on as it relates to future cases, especially as it relates to legal
precedent, and therefore should impact advice given by other judges.

#### My complaint is as follows:

In the defence lawyer's initial summary, they stated, "Semi-drunken consent is
still consent."

Later during the trial, the jury asked a clarifying question to confirm that
statements made from lawyers could be considered correct from a legal
perspective, and that if any legally-incorrect statements were made, they would
be corrected by the judge. Given this, it was my understanding that the
statement "Semi-drunken consent is still consent" was legally valid.

The judge's concluding spoken statement confirmed this viewpoint, including a
phrase suggesting that a person disinhibited by alcohol could consent to sexual
activity. Following a request from the jury to clarify the judge's concluding
statements, the judge repeated this phrase about disinhibition.

After the trial concluded, I looked at the legislation around rape and consent,
and now believe that the judge's concluding advice was leading at best, but
likely misleading.

#### Details of complaint:

From what I can find in the 1961 Crimes Act legislation (as at 6 November 2021),
consent for sexual activity does not seem to be defined. I would interpret that
as meaning that it is the job of the jury to determine whether consent was
given, bearing in mind their life experiences. Furthermore, the words
"disinhibited" and "disinhibition" do not appear in the text of that act:

https://legislation.govt.nz/act/public/1961/0043/latest/DLM329057.html

To clarify, there is nothing in the legislation to indicate that someone who
would ordinarily not consent to sexual activity can be considered to have
consented after the consumption of alcohol. In fact, the legislation has
*exclusions* for consent, and explicitly states in Section 128A:

"(4) A person does not consent to sexual activity if the activity occurs while he or she is so affected by alcohol or some other drug that he or she cannot consent or refuse to consent to the activity."

It also states:

"(7) A person does not consent to an act of sexual activity if he or she allows the act because he or she is mistaken about its nature and quality."

The New Zealand legislation also makes explicit that the listing of situations
of non-consent is not exhaustive:

"(8) This section does not limit the circumstances in which a person does not consent to sexual activity."

In my opinion, these exclusions are not ambiguous in how they relate to the
consumption of alcohol. Given this text in the legislation, I would now conclude
that if alcohol alters a person's perception of the nature and quality of sexual
activity - such that the person would consent to sexual activity after consuming
alcohol in a way they wouldn't prior to consumption - any such consent (if it
existed) should not be interpreted as consent according to the New Zealand
legislation.

In summary, the judge's statement included advice that was inconsistent with the
New Zealand legislation, and it is my belief that this advice both led the jury
to concentrate on the alcohol consumption aspect of the case, and also
encouraged them to ignore other presented evidence indicating that the
complainant had not consented.

Following on from my discovery about this aspect of law, and the idea of drunken
consent, I was confused as to why the judge and defense lawyer would have
commented on this. A subsequent search I carried out for legal precedent
surrounding the idea of drunken consent uncovered a legal academic paper
discussing the issue. This paper supports my perspective, explains the issues
and arguments in further detail, and demonstrates that the existing law *should
already be sufficient*, but that more guidance in this regard should be provided
by presiding judges:

Wallerstein, S. (2009). ‘A drunken consent is still consent’ – or Is It? A Critical Analysis of the Law on a Drunken Consent to Sex Following Bree. The Journal of Criminal Law, 73(4), 318-344. https://doi.org/10.1350/JCLA.2009.73.4.582

The paper additionally argues (relevant to the case that I was a juror for) that
gaps in memory can be another indication of a level of drunkenness that is high
enough that a person’s ability to validly consent is impaired. Here are some
supporting quotes from the paper:

"This article has shown how the existing legislation may be sufficient if more
guidance to juries is given by presiding judges, and with further expert
evidence regarding the effects of alcohol on a drunken woman’s mental
capacities."
  - P343; L36-39

"At the end of the day, the court appears to legitimise public practice of
having drunken sex, rather than recognise the harm that may be caused to
drunkenly consenting women and warn against it."
  - P343; L11-14

"Subsequently, evidence of gaps in memory should not play against the victim as
in Dougal and Bree or be at best ignored, but rather count in her favour as
providing a further proof of the high level of alcohol in the victim’s blood."
  - P334; L12-16

“In a society in which close to 30 per cent believe that women are partially
responsible for being sexually assaulted (against their will) if voluntarily
drunk, obviously, if a woman agrees to sex, she must be held responsible for her
decision – even if, being drunk, she lacked the capacity to make it. This view,
it is submitted, should pass from the world along side other prejudices."
  - P343; L21-27

To reiterate and conclude, I believe that the judge's statements (and
non-corrections) around the idea of disinhibition and drunken consent were
misleading and not legally valid according to our New Zealand legislation, and
would like for the concept of drunken consent to be removed from future legal
advice provided to juries. Where this idea is introduced by lawyers, it should
be identified by judges as being inconsistent with the legislation.

In addition, it would be helpful if advice on consent considered what a person
would be expected to consent to if they were not affected by alcohol or some
other drug. I note that this approach is already applied to the third "belief"
part of the legal requirements for a rape conviction, where the belief can be
either considered from the perspective of the defendant, or of a reasonable
person in the same situation as the defendant.

### Crimes Amendment Letter

I would like the legislation around rape to be clarified.

I'm writing to you after my first life experience of being a juror, which left
me in a state of shock. I didn’t expect, before it happened, that I would have a
traumatic experience from my jury service. It was me exercising my
responsibility as a citizen. I thought it would be fine... except it wasn’t.

What that jury service demonstrated to me was that our current legal system
surrounding rape cases is broken. In many different ways. This is known and
understood by many people, but it seems to me that very little has been done
about fixing any of the problems.

During the trial, a lawyer stated "semi-drunken consent is still consent", which
jarred with my own understanding of the rules around consent.

I wasn’t allowed to carry out further research on this during the trial, nor be
provided with the relevant legislation during the trial (which is one of the
many concerns I have about it). I have looked at the existing legislation in the
Crimes Act since then, and believe this legal position is not correct.
Regardless of whether or not the case I was involved with depended on this
interpretation of the law, it is clear to me that the existing legislation is
not clear enough for lawyers and judges to properly interpret it, and therefore
could be changed to improve their understanding.

I also found [a paper](https://doi.org/10.1350/JCLA.2009.73.4.582) written by a
legal scholar which supports my perspective, explains the issues and arguments
in further detail, and demonstrates that the existing law *should already be
sufficient*, but that more guidance in this regard should be provided by
presiding judges.

Please find attached a draft of amendments to our current legislation that I
believe may help with a proper interpretation of our existing law.

For further information, please see my blog post about this:

https://gringer.gitlab.io/presentation-notes/2022/04/12/criminal-justice/

Please feel free to pass this email onto whoever else you think would be
interested in my concerns, or could help with making changes (including outside
parliament, if desired).

---

#### Crimes Amendment Act

#### Purpose of this Act
During a recent jury trial about rape, it was discovered that there is existing
legal precedent around the idea that "drunken consent is still consent", even
though such a position is inconsistent with our legislation in Aotearoa. Of note
in the current legislation [i.e. Crimes Act 1961 No 43 (as at 06 November
2021)], it states:

“A person does not consent to sexual activity if the activity occurs while he or she is so affected by alcohol or some other drug that he or she cannot consent or refuse to consent to the activity.” [section 128A (4)]
and  
“A person does not consent to an act of sexual activity if he or she allows the act because he or she is mistaken about its nature and quality.” [section 128A (7)]

The idea that a person could be in a state where they cannot refuse to consent,
suggests that they are disinhibited by alcohol (or some other drug), are
affected by it to such a degree that they have a mistaken idea about its nature
or quality, and would have refused if they were not under the influence of
alcohol (or some other drug).

There is [an existing legal paper](https://doi.org/10.1350/JCLA.2009.73.4.582)
supporting this perspective. The paper additionally argues that gaps in memory
can be another indication of a level of drunkenness that is high enough that a
person’s ability to validly consent is impaired. Some supporting quotes from the
paper are included here for completeness, but people interested in this
perspective should read the entire paper, or at least the concluding remarks:

* "This article has shown how the existing legislation may be sufﬁcient if more guidance to juries is given by presiding judges, and with further expert evidence regarding the effects of alcohol on a drunken woman’s mental capacities." - P343; L36-39
* "At the end of the day, the court appears to legitimise public practice of having drunken sex, rather than recognise the harm that may be caused to drunkenly consenting women and warn against it." - P343; L11-14
"Subsequently, evidence of gaps in memory should not play against the victim as in Dougal and Bree or be at best ignored, but rather count in her favour as providing a further proof of the high level of alcohol in the victim’s blood." - P334; L12-16
* “In a society in which close to 30 per cent believe that women are partially responsible for being sexually assaulted (against their will) if voluntarily drunk, obviously, if a woman agrees to sex, she must be held responsible for her decision—even if, being drunk, she lacked the capacity to make it. This view, it is submitted, should pass from the world along side other prejudices." - P343; L21-27

In line with the arguments presented by Wallerstein in 2009, the following changes to the Crimes Act are recommended, in an attempt to make the existing law more clear:

#### Clarification on Drunken Consent

Section 128A (4) of the principal Act is amended by replacing with the following:  
(1)	A person does not consent to sexual activity if the activity occurs while he or she is so affected by alcohol or some other drug that he or she—  
(a)	cannot consent or cannot refuse to consent to the activity; or  
(b)	would not have consented to the activity prior to being affected by alcohol or some other drug

#### Memory Loss Indicating Cognitive Impairment

Section 128A of the principal Act is amended by inserting the following:  
(2)	A person does not consent to sexual activity if he or she is so affected by alcohol or some other drug that he or she—  
(a)	cannot remember the sexual activity after it has occurred; or  
(b)	cannot remember the consenting process after it has occurred  
(3)	A person does not consent to sexual activity if he or she is so affected by trauma that he or she—  
(a)	cannot remember the sexual activity after it has occurred; or  
(b)	cannot remember the consenting process after it has occurred

## The After-aftermath

I sent my letters, and have had responses from them. The summary of responses is
basically this:

Our system is working as intended, and the judge's opinions and decisions are
untouchable.

The responses were about what I expected... but at least I tried. The act of
trying to do something helped somewhat to calm my nerves.

With regards to my jitteryness (which lasted for about a week and a half), I was
prescribed some heart beat lowering medication. I took one pill, and my body
calmed down, so I left it at that.

I have not encountered anyone from the jury since my time in court. A couple more
consent and rape events have come up in my social media feeds since then, and I
have linked this thread, in the off-chance that someone else might be able to
read it and do better to fix this than I was able to do.