---
title: "Ten people who were wrong"
author: "@1goodtern, David Eccles"
date: 2023-05-18
---

{{< toc >}}

# Overview

This is a categorised version of @1goodtern's thread on Ten people who were wrong:

https://twitter.com/1goodtern/status/1676122922207916032

The quickest way to get the attention of the world [is to say something wrong on the internet](https://xkcd.com/386/). For each statement, I will attempt to find the following information:

* An example of someone saying the thing, or something similar
* An example of evidence demonstrating the thing's wrongness

Because Twitter is broken, or in the process of breaking, I will copy out entire tweets, and what I find relevant in any other references.

*Disclaimer: this list primarily represents the opinions of @1goodtern, not me. In some cases, I don't consider the wrongness to be complete. I might take the time to explain this, or I might just let it slide. We'll see how it goes on the day.*

# FAQ

* **Who?** My name is David Eccles. I am a neurodivergent multi-disciplinary bioinformatician.

* **What?** A categorised version of [@1goodtern's thread on Ten people who were wrong](https://twitter.com/1goodtern/status/1676122922207916032).

* **When?** Using my own date format and timezone, 2023-Jul-04 / 2023-Jul-05 (the night between).

* **Where?** Me? I am working mostly at home (because COVID) in Te Whanganui-a-tara, Aotearoa. This stuff blew up on Twitter, and @1goodtern talks mostly about UK current events, but the statements are almost always applicable worldwide.

* **Why?** Because I care about human health, and would like to do what I can to encourage people to care about their own health and the health of others. Understanding the magnitude of a problem is one of the first steps in solving it, and we have a *huge* problem with misinformation around the current COVID pandemic. Let me repeat that again, for those who missed it the first time: the *current* COVID pandemic, present tense; it's still with us, and we still need to fight against it. Twitter threads are not the best for organising stuff, and I wanted to organise it for my own reference (initially to help me with identifying similar or identical statements).

* **How?** Manually copy-pasting from [ThreadReader](https://threadreaderapp.com/thread/1676122922207916032.html), and when that failed, directly from Twitter. Plus lots of web searches.

* ***All* of them?** Yes, eventually, if I can.

# Trivia

* Birds have trouble counting large numbers. There are more than ten people who were wrong.
* #19 is not present.
* #45 and #46 are duplicated.

# Categorised Tweets

For now, I've been binning statements into single categorisations. Given the work and confusion resulting from duplication, I probably won't do multiple categorisations.

For uncategorised tweets, see [here](#uncategorised-tweets).

## Timing

* [1)](https://twitter.com/1goodtern/status/1676122924070100992) The people who said it was the exit wave in April 2020, October 2020, February 2021, July 2021, October 2021, January 2022, May 2022, September 2022, December 2022, January 2023, and March 2023 are also wrong when they say it's the exit wave in July 2023.
* [12)](https://twitter.com/1goodtern/status/1676122948481036288) The people who said that Covid is over, they were wrong.
* [16)](https://twitter.com/1goodtern/status/1676122957033230337) The people who said that it was seasonal, they were wrong.
* [73)](https://twitter.com/1goodtern/status/1676144469165461505) The people who said that we overreacted in 2020, they were wrong.

## Immunity

* [2)](https://twitter.com/1goodtern/status/1676122926087634946) The person who said that when you catch Covid you're then protected for 30 years, he was and is wrong.
* [4)](https://twitter.com/1goodtern/status/1676122930915205120) The people who said that everyone had already caught it in Spring 2020 and that we were all immune to it by June 2020, they were wrong.
* [10)](https://twitter.com/1goodtern/status/1676122944177676289) The people who said you can't get reinfected, they were wrong.
* [11)](https://twitter.com/1goodtern/status/1676122946291507200) The people who said that there wouldn't be any reinfections, and then when there were reinfections said the repercussions of them would be milder, they were wrong.
* [21)](https://twitter.com/1goodtern/status/1676122966461931520) The people who said that covid infections don't damage your immune system, they were wrong.
* [22)](https://twitter.com/1goodtern/status/1676122968714354692) The people [who said](https://twitter.com/ollysmithtravel/status/1471560067262656513) that Omicron was nature's booster, they were wrong.
    * @ollysmithtravel - "Got myself some omicron. Nature's booster."
* [57)](https://twitter.com/1goodtern/status/1676138080959340545) The people who said that covid would not mutate to evade immunity, they were wrong.
* [78)](https://twitter.com/1goodtern/status/1676154905462026240) The people who said that covid infections couldn't dysregulate the immune system, they were wrong.
* [82)](https://twitter.com/1goodtern/status/1676156025110446080) The people who said that unvaccinated people generally have better outcomes from infections than vaccinated people, they were wrong.
* [92)](https://twitter.com/1goodtern/status/1676160037746626563) The people who said that herd immunity would end the pandemic, they were wrong.
* [93)](https://twitter.com/1goodtern/status/1676161003212484609) The people who said that hybrid immunity would end the pandemic, they were wrong.
* [94)](https://twitter.com/1goodtern/status/1676161004718239745) The people who said that we had achieved herd immunity, they were wrong.
* [104)](https://twitter.com/1goodtern/status/1676162823670439936) The people who said that vaccine efficiency would not wane, they were wrong.
* [110)](https://twitter.com/1goodtern/status/1676164158360788993) The people who said that a covid infection guaranteed that you were safe from covid for months, they were wrong.
* [121)](https://twitter.com/1goodtern/status/1676169492202762240) The people who said that you had to catch Covid to strengthen your immune system, they were wrong.
* [123)](https://twitter.com/1goodtern/status/1676169495277182976) The people who said that you couldn't be infected by two different variants at the same time, they were wrong.
* [133)](https://twitter.com/1goodtern/status/1676170906916323328) The people who said that immunity by infection is better than immunity by vaccination, they were wrong.

## Children / babies / foetuses

* [3)](https://twitter.com/1goodtern/status/1676122928772009984) The people who said that kids don't catch it, they were wrong.
* [6)](https://twitter.com/1goodtern/status/1676122935726055432) The people who said that covid infection didn't harm kids, they were and are wrong.
* [26)](https://twitter.com/1goodtern/status/1676123792001708034) The people who said that kids couldn't catch it, they were wrong.
* [37)](https://twitter.com/1goodtern/status/1676126324287479809) The people who said that repeat Covid infections for kids would be harmless, they were wrong.
* [43)](https://twitter.com/1goodtern/status/1676127621636136961) The people who said that we had to infect healthy children with it, they were wrong.
* [50)](https://twitter.com/1goodtern/status/1676136651125321728) The people who said that the vaccine was a greater risk for kids than the virus, they were wrong.
* [67)](https://twitter.com/1goodtern/status/1676140077771104260) The people who said that pathogenic infections are good for children, they were wrong.
* [76)](https://twitter.com/1goodtern/status/1676154469027917824) The people [who said](https://twitter.com/DokMdm/status/1309653103893864448) that adults couldn't catch Covid from children, because adults are taller than children and stand above their coughs and sneezes, they were wrong.
    * @DokMdm - "It could be that children are shorter than their adult counterparts, that’s why they are less prone to the aerosolozing/airborne spread of the Coronavirus. Remember the observation that taller individuals are more prone to getting a Covid-19 infection?"
* [96)](https://twitter.com/1goodtern/status/1676161008182648832) The people who said that covid infection doesn't affect developing foetuses, they were wrong.
* [98)](https://twitter.com/1goodtern/status/1676161011739512833) The people who said that Covid won't affect infants developmentally, they were wrong.

## Severity

* [5)](https://twitter.com/1goodtern/status/1676122933054275584) The people who said that Omicron was mild, they were wrong.
* [42)](https://twitter.com/1goodtern/status/1676127619597598720) The people who said that it would attenuate, they were wrong.
* [44)](https://twitter.com/1goodtern/status/1676127623301287937) The people who said that covid infections were finite and did not persist inside you, they were wrong.
* [65)](https://twitter.com/1goodtern/status/1676139330081832961) The people who said that covid is no longer causing serious illness, they were wrong.
* [68)](https://twitter.com/1goodtern/status/1676143740728090628) The people who said that the severity of the ongoing repercussions of a Covid infection depends on the severity of the initial infection, they were wrong.
* [86)](https://twitter.com/1goodtern/status/1676157455259033608) The people who said that covid infections were only dangerous because the virus was novel, they were wrong.
* [111)](https://twitter.com/1goodtern/status/1676164971602821121) The people who said that people were just being hospitalised with Covid, they were wrong.

## Lockdowns / Isolation

* [7)](https://twitter.com/1goodtern/status/1676122937751896064) The people who say that people are prone to illness now because of lockdowns three years ago, they're wrong.
* [8)](https://twitter.com/1goodtern/status/1676122939878514688) The people who say that working from home is worse for your health than working in an office, they're wrong.

## Hygiene

* [9)](https://twitter.com/1goodtern/status/1676122942101495813) The people who said the best way to stop the spread of Covid is to wash your hands, they were wrong.
    * *[Concentrating only on hand washing is not a good idea because hand transfer is not the primary mode of infection. Handwashing is good for other reasons, and soap will kill the virus.]*

## Comparisons with other diseases
* [13)](https://twitter.com/1goodtern/status/1676122950582321152) The people who said it was just a flu, they were wrong.

## Prevalence / Herd immunity
* [14)](https://twitter.com/1goodtern/status/1676122952750837761) The people who said that everyone had to get it, they were wrong.
* [32)](https://twitter.com/1goodtern/status/1676125207176822785) The people who said that most people don't get seriously ill from it, technically they're right, but morally they're wrong.
* [83)](https://twitter.com/1goodtern/status/1676156163572768770) The people who said that covid infections aren't anyone anymore, they were wrong.

## Genetics / variants / evolution
* [15)](https://twitter.com/1goodtern/status/1676122954894065664) The people who said that variants would be rare, they're wrong.
* [89)](https://twitter.com/1goodtern/status/1676159293861638144) The people who said that covid wouldn't mutate past antiviral treatments, they were wrong.
* [132)](https://twitter.com/1goodtern/status/1676170905221844992) The people who said that covid was evolving into a dead end branch that would end the pandemic, so far they're wrong.

## Long covid
* [17)](https://twitter.com/1goodtern/status/1676122960208224256) The people who said that long term repercussions of Covid infections would be rare, they were wrong.
* [53)](https://twitter.com/1goodtern/status/1676137412362108930) The people who said that Long Covid doesn't exist, they were wrong.
* [70)](https://twitter.com/1goodtern/status/1676144112532086789) The people who said that covid infections couldn't be chronic, they were wrong.
* [80)](https://twitter.com/1goodtern/status/1676155413593464832) The people who said that Covid infections couldn't cause permanent disruption to metabolic processes, they were wrong.
* [95)](https://twitter.com/1goodtern/status/1676161006723035137) The people who said that all the damage of Covid infections is evident at the time of the initial infection, they were wrong.
* [102)](https://twitter.com/1goodtern/status/1676162820646289408) The people who said that most people fully recover from long covid, they may be right, we've yet to find out, but they're wrong to claim that yet.
* [109)](https://twitter.com/1goodtern/status/1676164156297297920) The people who said that the cure for Long Covid was graded exercise, they were wrong.
* [122)](https://twitter.com/1goodtern/status/1676169493788147715) The people who said that covid infections couldn't cause POTS, dysautonomia, tachycardia, they were wrong.

## Vaccination
* [18)](https://twitter.com/1goodtern/status/1676122962196418560) The people who said that vaccines would end the spread of Covid, they were wrong.
    * *[Vaccines should not be the only protective measure applied for protection. Some people can't have vaccines; some people will refuse vaccines, even when they are minimally invasive and offered for free. The level of vaccination that would protect the entire population and suppress outbreaks before they become a problem is likely to be similar to the level of vaccination required for measles (95-99%).]*
* [51)](https://twitter.com/1goodtern/status/1676136893447020545) The people who said that you should vax and relax, they were wrong.

## Transmission / masks
* [20)](https://twitter.com/1goodtern/status/1676122964440260609) The people who said that it wasn't airborne, they were wrong.
* [25)](https://twitter.com/1goodtern/status/1676122975173595137) The people who said that masks don't work were technically right if you're talking about masks that don't work, but if you're talking about masks that do work worn properly, they're wrong.
* [101)](https://twitter.com/1goodtern/status/1676162819258023938) The people who said that covid particles couldn't hang in the air for hours and then cause infections later, they were wrong.
* [103)](https://twitter.com/1goodtern/status/1676162822215028737) The people who said that singing, shouting, aerobic activity, breathing, coughing, sneezing are not aerosolising activities, they were wrong.
* [129)](https://twitter.com/1goodtern/status/1676170899412729856) The people who said that there is no such thing as superspreader events, they were wrong.

## Causes of Covid
* [23)](https://twitter.com/1goodtern/status/1676122970903683072) The people who said that 5g was causing the pandemic, they were wrong.

## Covid causing other things
* [33)](https://twitter.com/1goodtern/status/1676125209194119169) The people who said that Covid infections weren't causing the increased RSV waves, they were wrong.
* [52)](https://twitter.com/1goodtern/status/1676137203192168451) The people who said that frequent testing made people vulnerable to Strep A, they were wrong.
* [69)](https://twitter.com/1goodtern/status/1676143949671550976) The people who said that covid infections don't leave you vulnerable to other opportunistic infections, they were wrong.
* [79)](https://twitter.com/1goodtern/status/1676155144470233094) The people who said that Covid infections couldn't cause or trigger autoimmune disorders, they were wrong.
* [106)](https://twitter.com/1goodtern/status/1676163520440696833) The people who said that covid infections do not increase your risk of death from other causes, they were wrong.
* [107)](https://twitter.com/1goodtern/status/1676163523569676288) The people who said that covid infections do not make you vulnerable, they were wrong.
* [115)](https://twitter.com/1goodtern/status/1676167316596305920) The people who said that it was impossible for covid infections to cause or accelerate cancer, or that covid infection couldn't damage the body's ability to fight cancer, they were wrong.
* [116)](https://twitter.com/1goodtern/status/1676167318278217728) The people who said that covid infections couldn't reactivate latent viruses, they were wrong.
* [118)](https://twitter.com/1goodtern/status/1676169486947303424) The people who said that covid can't trigger, cause, or worsen epilepsy, they were wrong.
* [119)](https://twitter.com/1goodtern/status/1676169488402726912) The people who said that covid infections couldn't cause appendicitis, they were wrong.

## Ending the pandemic
* [24)](https://twitter.com/1goodtern/status/1676122973009334272) The people who said that the way to end the pandemic was to stop testing, they were wrong.

## Safe areas
* [27)](https://twitter.com/1goodtern/status/1676123793662640128) The people who said that restaurants and cafés were safe, they were wrong.
* [38)](https://twitter.com/1goodtern/status/1676126953084968960) The people who said that standing two metres apart meant you were safe, they were wrong.
* [39)](https://twitter.com/1goodtern/status/1676126954922348544) The people who said that schools were not super spreaders, they were wrong.

## Safe countries
* [29)](https://twitter.com/1goodtern/status/1676125201552089088) The people who said that it would only affect China and the Far East, they were wrong.
* [56)](https://twitter.com/1goodtern/status/1676137945508503553) The people who said that most of the effects of Covid were felt in the aging west and not in middle income and developing countries, they were wrong.
* [117)](https://twitter.com/1goodtern/status/1676167320060805120) The people who said that countries with x climate wouldn't experience waves of Covid infection, they were wrong.

## Safe people
* [34)](https://twitter.com/1goodtern/status/1676125211253518336) The people who said that Covid infection was safe for healthy people, they were wrong.
* [49)](https://twitter.com/1goodtern/status/1676136409076232192) The people who said that only people with pre-existing medical conditions would be at risk were technically right, except we all have pre-existing conditions, so they were wrong.
* [74)](https://twitter.com/1goodtern/status/1676153833016250369) The people who said that there was no asymptomatic infection, they were wrong.
* [75)](https://twitter.com/1goodtern/status/1676154022594502656) The people who massively overestimated asymptomatic infection, they were wrong.
* [91)](https://twitter.com/1goodtern/status/1676159818963320833) The people who said that you can only catch Covid from strangers, they were wrong.
* [105)](https://twitter.com/1goodtern/status/1676163518960107520) The people who said that you can tell if you're infectious, they were wrong.

## Systemic Damage
* [40)](https://twitter.com/1goodtern/status/1676126957153443841) The people who said that covid was solely a respiratory infection, they were wrong.

### Systemic Damage - Heart
* [28)](https://twitter.com/1goodtern/status/1676123795591946241) The people who say that Covid isn't causing heart disease, they're wrong.

### Systemic Damage - Brain
* [47)](https://twitter.com/1goodtern/status/1676128994121461760) The people who said that covid did not cross the blood brain barrier, they were wrong.
* [71)](https://twitter.com/1goodtern/status/1676144465872932865) The people who said that covid couldn't damage your brain, they were wrong.
* [77)](https://twitter.com/1goodtern/status/1676154669096226817) The people who said that covid infections couldn't accelerate dementia progression, they were wrong.
* [99)](https://twitter.com/1goodtern/status/1676161013480128512) The people who said that covid infection doesn't affect cognitive function, they were wrong.

### Systemic Damage - Nerves
* [97)](https://twitter.com/1goodtern/status/1676161010036555777) The people who said that covid infections don't cause neurological issues, they were wrong.

### Systemic Damage - Blood
* [84)](https://twitter.com/1goodtern/status/1676156384738439170) The people who said that covid doesn't cause strokes, they were wrong.
* [114)](https://twitter.com/1goodtern/status/1676167315036012544) The people who said that covid infections didn't cause blood clots weeks and months after the initial infection was over, they were wrong.

### Systemic Damage - Reproductive system
* [125)](https://twitter.com/1goodtern/status/1676169498628431872) The people who said that covid infections couldn't affect male or female fertility, they were wrong.
* [126)](https://twitter.com/1goodtern/status/1676169500348014592) The people who said that covid infections couldn't cause erectile dysfunction, they were wrong.


## Societal Management / Inevitability
* [30)](https://twitter.com/1goodtern/status/1676125203158450177) The people who said that western hospitals and healthcare would be able to cope with it, they were wrong.
* [31)](https://twitter.com/1goodtern/status/1676125204920115201) The people who said that we should just let it rip, they were wrong.
* [41)](https://twitter.com/1goodtern/status/1676127617966039041) The people who said that we just have to get it over with, they were wrong.
* [45)](https://twitter.com/1goodtern/status/1676128130837168129) The people who said that we have all the tools, were technically right, but they didn't share or use the tools, so they were wrong.
* [45)](https://twitter.com/1goodtern/status/1676128162063822848) The people who said that we have all the tools, were technically right, but they didn't share or use the tools, so they were wrong.
* [48)](https://twitter.com/1goodtern/status/1676136073158701058) The people who said that letting covid spread would be good for the economy, they were wrong.
* [54)](https://twitter.com/1goodtern/status/1676137531857936384) The people who said that covid infection was unavoidable, they were wrong.
* [55)](https://twitter.com/1goodtern/status/1676137681753972736) The people who said that we're all in this together, they were wrong.
* [59)](https://twitter.com/1goodtern/status/1676138459704991744) The people who said that the best thing to do was to make covid endemic, they were wrong.
* [66)](https://twitter.com/1goodtern/status/1676139675516379138) The people who said that we need to learn to live with Covid were technically right, but they meant just catching it repeatedly forever, so they were wrong.
* [85)](https://twitter.com/1goodtern/status/1676156527386714114) The people who said that covid mitigations were just delaying the inevitable, they were wrong.
* [87)](https://twitter.com/1goodtern/status/1676157669927788544) The people who said that it should be personal responsibility about whether you use mitigations, they were wrong.
* [88)](https://twitter.com/1goodtern/status/1676158375179026432) The people who said that the people who died from covid were going to die then anyway, they were wrong.
* [90)](https://twitter.com/1goodtern/status/1676159390913626114) The people who said that covid would just go away, they were wrong.
* [108)](https://twitter.com/1goodtern/status/1676164154866925568) The people who said that waves of Covid infection no longer put again on healthcare systems, they were wrong.
* [131)](https://twitter.com/1goodtern/status/1676170903128776708) The people who said that talking about the pandemic makes the pandemic continue, they were wrong, except for the case of when someone with Covid is talking to someone else without covid about the pandemic, thereby giving them covid.
* [134)](https://twitter.com/1goodtern/status/1676174232718307329) The people who said that covid infections would not have a significant effect on the economy because only a small proportion of people become sick straight away, they were wrong.

## Benefits of Covid
* [81)](https://twitter.com/1goodtern/status/1676155830838648834) The people who said that covid infections were beneficial, they were wrong.

## Gaslighting
* [35)](https://twitter.com/1goodtern/status/1676126320399380482) The people who said that Long Covid was a psychological problem, they were wrong.
* [36)](https://twitter.com/1goodtern/status/1676126322047827969) The people [who said](https://twitter.com/MichelleSairsel/status/1675890922322632707) that Long Covid was caused by deconditioning, they were wrong.
    * @MichelleSairsel - "I was told today my POTS is due to deconditioning after Covid. Why would one decondition from a ten day virus, and not return to pre virus condition?! Do they even believe what they’re saying?!"
* [46)](https://twitter.com/1goodtern/status/1676128132477198343) The people who said that covid was a hoax, they were wrong.
* [46)](https://twitter.com/1goodtern/status/1676128163544309762) The people who said that covid was a hoax, they were wrong.
* [112)](https://twitter.com/1goodtern/status/1676164973578387456) The people who pretended that covid didn't exist, they were wrong.

## Protective measures
* [58)](https://twitter.com/1goodtern/status/1676138230226329600) The people who said that we didn't need to lockdown, they were wrong.
* [60)](https://twitter.com/1goodtern/status/1676138584141619200) The people who said that HEPA doesn't help, they were wrong.
* [61)](https://twitter.com/1goodtern/status/1676138690198884353) The people who said that ventilation doesn't help, they were wrong.

## Detection / contact tracing
* [72)](https://twitter.com/1goodtern/status/1676144467353501697) The people who said that testing was pointless, they were wrong.

## Incubation / infection period
* [62)](https://twitter.com/1goodtern/status/1676139325115834374) The people who said that it's safe to end isolation at five days, they were wrong.
* [63)](https://twitter.com/1goodtern/status/1676139326415962112) The people who said that there was no such thing as a paxlovid rebound, they were wrong.
* [100)](https://twitter.com/1goodtern/status/1676161468289490944) The people who said that it takes fifteen minutes to get infected with covid, they were wrong.

## Treatments
* [64)](https://twitter.com/1goodtern/status/1676139328378925057) The people who said that molnupiravir wasn't triggering mutations, they were wrong.
* [113)](https://twitter.com/1goodtern/status/1676164975436480518) The people who said that the cure for Covid was ivermectin/hcq/butthole sunning/prayer, they were wrong.

## Pets
* [120)](https://twitter.com/1goodtern/status/1676169490420097026) The people who said that your pets couldn't catch or be harmed by Covid, they were wrong.

## Symptoms
* [124)](https://twitter.com/1goodtern/status/1676169496850059265) The people who said that your sense of smell always comes back if you lost it during a covid infection, they were wrong.

## Death / excess death
* [127)](https://twitter.com/1goodtern/status/1676169501996457984) The people who said that covid infections aren't causing massive excess deaths, they were wrong.
* [128)](https://twitter.com/1goodtern/status/1676169503770570752) The people who said that covid infections wouldn't lower life expectancy, they were wrong.

## Religion
* [130)](https://twitter.com/1goodtern/status/1676170901329522688) The people who said that you just need to have faith to not catch Covid, they were wrong.

# Uncategorised tweets

* [135)](https://twitter.com/1goodtern/status/1676174234362454017) The people who said that covid infections couldn't cause permanent damage to the immune system, they were wrong.
* [136)](https://twitter.com/1goodtern/status/1676174236149317632) The people who said that covid infections couldn't present like a stomach bug, they were wrong.
* [137)](https://twitter.com/1goodtern/status/1676174237952884736) The people who said that Long Covid is caused by lockdowns or masks or anxiety or anything other than covid infection, they were wrong.
* [138)](https://twitter.com/1goodtern/status/1676174240075087873) The people who said that covid infections couldn't dysregulate your gut microbiome, they were wrong.
* [139)](https://twitter.com/1goodtern/status/1676174241761304577) The people who blamed other viruses for the damage done by Covid infections, they were wrong.
* [140)](https://twitter.com/1goodtern/status/1676174243350847493) The people who said that Long Covid doesn't cause fatigue, they were wrong.
* [141)](https://twitter.com/1goodtern/status/1676174245112557568) The people who said that covid infections couldn't damage the autonomic nervous system, they were wrong.
* [142)](https://twitter.com/1goodtern/status/1676174246731546624) The people who said that covid infections couldn't cause endothelial dysfunction, they were wrong.
* [143)](https://twitter.com/1goodtern/status/1676177970593951744) The person who told me that covid particles couldn't travel across a room because they had to obey the laws of physics, he was wrong.
* [144)](https://twitter.com/1goodtern/status/1676177971982262275) The person who said I can't wear this mask forever, they were wrong.
* [145)](https://twitter.com/1goodtern/status/1676177973567803392) The people who said that we should take it on the chin, they were wrong.
* [146)](https://twitter.com/1goodtern/status/1676178333833261056) The people who said that the mental health consequences of lockdown were worse than the consequences of COVID, they were wrong.
* [147)](https://twitter.com/1goodtern/status/1676216012671270912) The people who said that covid variants couldn't spontaneously combine to form recombinant variants, they were wrong.
* [148)](https://twitter.com/1goodtern/status/1676216300287283203) The person who said that you could inject people with bleach and/or light, he was wrong, and impeached twice (for something else).
* [149)](https://twitter.com/1goodtern/status/1676216920146624513) The people who said that mask wearing only protects the people around you, they were wrong.
* [150)](https://twitter.com/1goodtern/status/1676247132813049857) The people who said that you couldn't catch or transmit Covid after you were vaccinated, they were wrong.
* [151)](https://twitter.com/1goodtern/status/1676247373360558081) The people who said that the t-cells would protect us, they were wrong.
* [152)](https://twitter.com/1goodtern/status/1676248435484180481) The people who said that surface contact was the primary mode of transmission, they were wrong.
* [153)](https://twitter.com/1goodtern/status/1676248437535264769) The people who said that we need to keep a constant rolling wave of infection to prevent peaks that overwhelm healthcare, they were wrong.
* [154)](https://twitter.com/1goodtern/status/1676248439548530695) The people who said that covid infection doesn't cause vasculitis, they were wrong.
* [155)](https://twitter.com/1goodtern/status/1676250026576625666) The people who said that asthmatics were at less risk from Covid, they were wrong.
* [156)](https://twitter.com/1goodtern/status/1676250776602042368) The people who said that Covid infections couldn't fuse brain cells together, they were wrong.
* [157)](https://twitter.com/1goodtern/status/1676252007273824260) The people who thought that pharmaceutical treatments would be a panacea for the repercussions of Covid infections, they were wrong.
* [158)](https://twitter.com/1goodtern/status/1676252728958361600) The people who said that Covid infections couldn't cause embolisms, they were wrong.
* [159)](https://twitter.com/1goodtern/status/1676252933799682049) The people who said that the only way to prevent infections was to get infected, they were wrong, obviously.
* [160)](https://twitter.com/1goodtern/status/1676253046664306693) The people who said that the repercussions of covid infections were rare, they were wrong.
* [161)](https://twitter.com/1goodtern/status/1676253583250014209) The people who endlessly made a fuss about the difference between dying with covid and of covid and from covid and by covid weren't so much wrong as just trying to confuse things, but they were also wrong about loads of other things, so they were wrong.
* [162)](https://twitter.com/1goodtern/status/1676253741266137088) The people who said that a Covid infection couldn't affect every part of your body, they were wrong.
* [163)](https://twitter.com/1goodtern/status/1676254590902099970) The people who said that Covid infection couldn't be behind a massive increase in the number of people suffering atrial fibrillation, they were wrong.
* [164)](https://twitter.com/1goodtern/status/1676254593045413890) The people who said that the people trying not to catch covid were prolonging the pandemic, they were wrong.
* [165)](https://twitter.com/1goodtern/status/1676254595415171072) The people who compared SARSCov2 with a cold rather than with SARSCoV1, they were wrong.
* [166)](https://twitter.com/1goodtern/status/1676254597415948288) The people who said that patients need to see their doctors' smiles, they were wrong.
* [167)](https://twitter.com/1goodtern/status/1676254599433314311) The people who said that Covid infections couldn't hurt athletes, they were wrong.
* [168)](https://twitter.com/1goodtern/status/1676255872132218880) The people who said that everyone who needs to can take action to protect themselves, they were wrong.
* [169)](https://twitter.com/1goodtern/status/1676255874170355713) The people who said that their right to experience pleasure was more important than other people's right to life, they were wrong.
* [170)](https://twitter.com/1goodtern/status/1676255876036931584) The people who said that Covid didn't cause brain fog, they were wrong.
* [171)](https://twitter.com/1goodtern/status/1676255877970509825) The people who were baffled why they kept on catching an endless string of infections, they were wrong to be baffled.
* [172)](https://twitter.com/1goodtern/status/1676268251490263060) The people who said that hybrid immunity was robust and long lasting, they were wrong.
* [173)](https://twitter.com/1goodtern/status/1676268253641941002) The people who said that each variant would be the last variant, they were wrong.
* [174)](https://twitter.com/1goodtern/status/1676272062850113554) The people who said that plastic screens would be a suitable protection against a virus that moves like smoke, they were wrong.
* [175)](https://twitter.com/1goodtern/status/1676272236783706116) The people who said that covid did not persist in children's lymph glands including tonsils, they were wrong.
* [176)](https://twitter.com/1goodtern/status/1676272638602215424) The people who said that Covid infections didn't cause microclotting, they were wrong.
* [177)](https://twitter.com/1goodtern/status/1676273976740388875) The people who said that Covid couldn't infect cartilage and persist in it, they were wrong.
* [178)](https://twitter.com/1goodtern/status/1676280970020790274) The people who said that Covid had been defanged, they were wrong.
* [179)](https://twitter.com/1goodtern/status/1676282431299526665) The people who said that hospitalisation with Covid was trivial, they were wrong.
* [180)](https://twitter.com/1goodtern/status/1676490816401948673) The people who said that vaccines would stop people developing Long Covid after a covid infection, they were wrong.
* [181)](https://twitter.com/1goodtern/status/1676493164482371584) The people who said that mass infection would not be a problem after vaccines because only a small percentage of people would be seriously ill, were right about the small percentage, but about the overall effect they were wrong.
* [182)](https://twitter.com/1goodtern/status/1676493167766523904) The people who said that they were doing everything they could to protect the elderly and vulnerable, they were wrong.
* [183)](https://twitter.com/1goodtern/status/1676493169511350272) The people who said that we just needed cotton masks to protect against transmission via droplets when transmission was actually via aerosols, they were wrong.
* [184)](https://twitter.com/1goodtern/status/1676493172115947520) The same people who then went on to say that 'masks' (cotton masks, not designed to stop aerosols) don't work, they were right about cotton masks, but wrong about ffp2/n95+ respirators.
* [185)](https://twitter.com/1goodtern/status/1676493176507449344) The people who said that we don't need to worry about Covid infections any more, they were wrong.
* [186)](https://twitter.com/1goodtern/status/1676493181733462016) The people who said that covid would not evolve to become more transmissible, they were wrong.
* [187)](https://twitter.com/1goodtern/status/1676493186498273281) The people who said that covid would not evolve, they were wrong.
* [188)](https://twitter.com/1goodtern/status/1676494249292865537) The people who said that you can't catch Covid outdoors, they were wrong.
* [189)](https://twitter.com/1goodtern/status/1676494643276513280) The people who said you can only catch Covid face to face, they were wrong.
* [190)](https://twitter.com/1goodtern/status/1676494646904537093) The people who said that you could only catch Covid in the winter, they were wrong.
* [191)](https://twitter.com/1goodtern/status/1676496198767718401) The people who said that you don't need to assess risk or employ mitigations outside, they were wrong.
* [192)](https://twitter.com/1goodtern/status/1676496710426607616) The people who said that Covid couldn't destabilise the cells that manage your bones, [they were wrong](https://doi.org/10.1002/jor.25537).
    * "Using the molecular biological approaches, ***a more significant decrease in the level of the expression of the TGFB1 and FOXO1 genes was shown in patients with osteoarthritis after COVID-19*** compared to the patients with knee osteoarthritis against the background of a more significant decrease in both superoxide dismutase and catalase activity (which indicates an impairment of the redox status of cells and possible silencing of TGF-β1-FOXO1 signaling) in patients with osteoarthritis after SARS-CoV2 infections. This may be associated with an increase in systemic inflammation due to the response of the body to the viral invasion. At the same time, ***a more significant decrease in the expression of the COMP gene was found in patients with osteoarthritis after COVID-19*** in comparison with the patients with knee osteoarthritis against the background of a more intense increase in the concentration of COMP in patients with osteoarthritis after SARS-CoV2 infection. ***Such data indicate a more significant activation of destructive processes in cells*** after an infection and further progression of the disease." - [https://doi.org/10.3103/S009545272302010X](https://doi.org/10.3103/S009545272302010X)
    * "Utilizing a humanized mouse model of COVID-19, this study provides the first direct evidence that SARS-CoV-2 infection leads to acute bone loss, increased osteoclast number, and thinner growth plates." - [https://doi.org/10.1002/jor.25537](https://doi.org/10.1002/jor.25537)
* [193)](https://twitter.com/1goodtern/status/1676530999545479168) The people who said that covid couldn't travel round corners, she was wrong.
* [194)](https://twitter.com/1goodtern/status/1676531656247721988) The people who said that only people with underlying conditions were dying, they were wrong.
* [195)](https://twitter.com/1goodtern/status/1676531946048872448) The people who said that the severity of acute infections was the principal metric for the progression of the pandemic, they were wrong.
* [196)](https://twitter.com/1goodtern/status/1676532499223130114) The people who said that covid infections were only 'burning the dry tinder', they were wrong.
* [197)](https://twitter.com/1goodtern/status/1676532732250275840) The people who said that they had put a 'ring of steel' around the elderly and vulnerable, he was wrong.
* [198)](https://twitter.com/1goodtern/status/1676563416930975747) The people who said that facemasks could increase the risk of catching Covid, she was wrong.
* [199)](https://twitter.com/1goodtern/status/1676563419191816199) The people who said that children were more likely to die from a lightning strike than Covid, she was wrong.
* [200)](https://twitter.com/1goodtern/status/1676563421205090306) The people who said that a Covid death would be "a relatively peaceful end", they were wrong.
* [201)](https://twitter.com/1goodtern/status/1676564724379516928) The people [who said](https://www.dailymail.co.uk/news/article-8657713/Jenny-Harries-Pupils-likely-hit-bus-catch-Covid.html) that kids were more likely to be hit by a bus than catch Covid, she was wrong.
    * "Dr Jenny Harries, the Deputy Chief Medical Officer, today said children were more likely to be hit by a bus than catch coronavirus at school... 'Every time a parent sends their child off to school, pre-Covid, they may have been involved in a road traffic accident – there are all sorts of things.... That risk, or the risk of seasonal flu, we think is probably higher than the current risk of Covid.'"
* [202)](https://twitter.com/1goodtern/status/1676564864075014145) The people who said that Covid had become just a cold, they were wrong.
* [203)](https://twitter.com/1goodtern/status/1676678676144848896) The people who said that there was no transmission by asymptomatic infections, they were wrong.
* [204)](https://twitter.com/1goodtern/status/1676678678279798788) The people who said that there was no need for boosters, they were wrong.
* [205)](https://twitter.com/1goodtern/status/1676683846367035395) The people who said that Omicron infection would not cause Long Covid, they were wrong.
* [206)](https://twitter.com/1goodtern/status/1676965487853576194) The people who said that Covid infection could not trigger programmed cell death, they were wrong.
* [207)](https://twitter.com/1goodtern/status/1676965885649768448) The people who said that we should copy Sweden, they were wrong.
* [208)](https://twitter.com/1goodtern/status/1676966128147652610) The people who say we should all just trust our unprimed immune system to defeat covid infections, they were wrong.
* [209)](https://twitter.com/1goodtern/status/1677034324980379648) The people who implied that death is the only bad outcome of Covid infection, they were wrong.
* [210)](https://twitter.com/1goodtern/status/1677743001974472704) The people [who said](https://twitter.com/totaltravelskhi/status/1246460569374392320) that it was safe to fly on aeroplanes, [they were wrong](https://twitter.com/1goodtern/status/1677196010441830400).
    * @totaltravelskhi - "Is airplane cabin air #safe during #covid? YES, it is! The risk is low, HEPA filters installed on modern aircraft clean the air up to 99.97%"
    * @1goodtern - "Remember when I said that now they've got rid of Covid mitigations, they would stop caring about other potential transmissible diseases too? ✈️🔺 A plane drenched in blood after a passenger haemorrhages, and they don't even clean it."
* [211)](https://twitter.com/1goodtern/status/1679061573942345729) The people who said that you were more likely to die of flu, they were wrong.
* [212)](https://twitter.com/1goodtern/status/1679594404074356739) The people who said that respirators and hepa couldn't trap airborne virus particles because the virions were too small, they were wrong.
* [213)](https://twitter.com/1goodtern/status/1679594711789494272) The people who demanded a randomised controlled trial for engineered solutions like respirators, they were wrong.
* [214)](https://twitter.com/1goodtern/status/1679595138849337345) The people who equated anti-infection with anti-vaccine, they were wrong.
* [215)](https://twitter.com/1goodtern/status/1679595305409413122) The people who were pro-infection, they were wrong.
* [216)](https://twitter.com/1goodtern/status/1680249467197353984) The people who said that Covid is a seasonal virus so you should catch it repeatedly, as opposed to other pathogens like HIV and TB, they were wrong.
* [217)](https://twitter.com/1goodtern/status/1680249640795398144) The people who said you should be exposed to Covid by infection regularly so that you don't lose your immunity to it, they were wrong.
* [218)](https://twitter.com/1goodtern/status/1683960844181438464) The people who thought that one way systems in schools would be a great way of preventing covid infections, [they were wrong](https://twitter.com/Sandyboots2020/status/1400584195773276163).
    * @Sandyboots2020 - "“schools now a major source of transmission.” Thank god we have “catch it, kill it, bin it;” one way systems; and red tape on the floor so covid knows where it to stop 👍🏻👍🏻👍🏻 Oh and hand gel!! Perfect for stopping an airborne virus in its tracks. - [The Guardian](https://www.theguardian.com/education/2021/jun/03/india-covid-variant-spreading-in-englands-schools-and-colleges-data-shows)"
* [219)](https://twitter.com/1goodtern/status/1684994123458138112) The people who said the covid pandemic would not affect life expectancy, [they were wrong](https://twitter.com/denise_dewald/status/1692516404623245408).
    * @denise_dewald - "I will consider letting my guard down once life expectancy returns to its 2019 level." [Image of US life expectancy, which is at 76.1 years, similar to its 1995 level].
* [220)](https://twitter.com/1goodtern/status/1692215208830251083) The people [who said](https://twitter.com/LCsucksbigtime/status/1690831084215271425) that it's ok to go into work while sick if you wear a surgical mask, they are wrong.
    * @LCsucksbigtime - "Same as my local hospital ! Staff are being told to come in whilst positive if well enough, wear a mask and wash there hands. Apparently their vulnerable high risk patients don’t need to be told their clinician is positive !" [Image of NHS poster, directing people who are "well enough to attend work" to wear a surgical mask whilst symptomatic]