---
title: "Stop Competitive Funding"
author: "David Eccles"
date: 2022-03-13
---

***When you make something a target, people will fire arrows at it,
regardless of how small the target is. Competitive funding will always
reward cheaters and privileged researchers (including reviewers'
friends) more than anyone doing things that follow the spirit of the
rules.***

## The Problem - Competitive Funding

I believe in an academic future that is diverse, open, and
non-competitive. The core issue with competitive funding is that it
requires an increasing amount of effort, year on year, for people to
get the same amount of money. As that effort is increased, less
privileged people (who can't afford to spend more money/effort) lose
out. I will demonstrate this by considering the most lucrative fund in
Aotearoa.

The Marsden Fund is a competitive fund in which the "top" researchers
are given money to do proposed research.

In 2021, $82,345k was awarded to 120 projects. That's a mean of ~$230k
per project. When considering 100% institute overheads, that ~$230k
over 3 years ends up as enough for 1.5 researchers at [Massey's Step 1
lecturer salary](https://www.massey.ac.nz/massey/about-massey/careers/salary-scales/academic-salary-scales_home.cfm) ($74k, or about $35/hr). Unfortunately those 120
projects had 394 named investigators, or around 3.3 investigators per
project. In other words, the awards are not sufficient to cover
researcher salary, let alone additional research costs.

However, Marsden is a competitive fund, which pretty much means
researchers need to do pre-research in order to even have a chance at
getting those funds. So, the cost of research should be factored into
the award when considering net gain from the fund. In 2021, the
Marsden Fund had 1152 Expressions of Interest... and 1032 of those
weren't funded this year. Assuming 3 researchers per unfunded
application working 100 hours as a Step 1 Lecturer, that's $11,140k
wasted.

The Marsden Fund does involve a two-round process, so alternatively I
could consider 20h work at L1 lecturer salary for 1032 applications
for round 1, and 100h work at $200/h for 108 applications: $2,450k
round 1 $7,130k round 2. This comes out to a similar amount of wasted
money ($9,580k vs $11,140k).

Those researchers still need to get their research funded from another
source in order to continue as a researcher. An award success rate of
10.4% for the Marsden Fund means researchers will need to spend ~10
times that amount in order to get funded (assuming their only source
of income was Marsden Fund-like sources... and non-Marsden tend to
have smaller payouts). Even at the Step 1 Lecturer salary, when
accounting for multiple applications, once again the wasted
pre-research comes out as higher than the award money allocated
regardless of which calculation is used.

Summary: it does not seem to be worth a researcher's time, effort and
money to carry out work towards applying for the Marsden Fund.

*A common counter-argument is that this pre-research time is not
wasted; because it improves chances for other grants. However, I still
consider it wasted time because it's targeted specifically for a
project that has not been funded: researcher time could be better
spent doing something else, e.g. science communication to the public.*

## A Proposed Solution - Equal Distribution of Funds

My proposal to reduce wasted money and time does not require any
funding increases. It can be done using the same systems already
available, with less administrative overhead.

My proposal is that we have a low, fixed threshold for acceptance
(i.e. constant from year to year), followed by universal distribution
of available funds to everyone who passes that threshold. This makes
the targets predictable (allowing people to know in advance whether
they have a good chance of getting funded), and creates a much fairer
distribution of funds that has no preference for white men,
established researchers, peer review bias, or "safe" research (all of
which are a problem with the current peer-reviewed system, see
[here](https://doi.org/10.38126/JSPG180105)).

Suppose, instead, that every researcher who applied were provided with
an equal proportion of the $82,345k award money. That's ~$21k per
researcher, for an input effort of 100 hours. Instead, 394 of the
"best" researchers get $209k each for a similar effort.

A common counter-argument is that giving out money evenly to all
"fundable" applications means projects will never be given what they
need. However, consider that with the current system the majority of
researchers get no money, and have also put in additional effort for
that non-reward.

It's a numbers game. Are there more researchers who can do great
research with less money [than researchers who need lots of funding to
do any research]? Are there researchers who can pool their money to
collaborate in larger projects?

Success / reward simulations suggest the benefits are greater when
funding is more universal. This simulation research indicates that
when working out how to implement universal basic funding, the
"universal" part is more important than the "basic" part:

https://doi.org/10.48550/arXiv.1802.07068

[Note: this post is essentially an unrolling of this Twitter thread:
https://twitter.com/gringene_bio/status/1456130900689756169]
