---
title: "London Calling 2023"
author: "David Eccles"
date: 2023-05-18
---

{{< toc >}}

# Pre-tech Interview

## Abdul Karim Sesay

* Need local capacity to do sequencing, need to be prepared to respond to any outbreak
* Important to engage community to do genomics; personal medicine might be AMR; important to have facility to do genomics, any delay may mean people losing their life
* Looking at diversity in Africa; would be great if Clive could announce trying to increase the number of genomes in Africa; building bigger datasets for Africa
* Important to have a reference genome in Africa
* Thinking about high-output data locally
* This meeting brings together people to interact and discuss answers to global issues
* Research around pre-term diagnosis is interesting

## Rich Scott (Genomics England)

* Medic
* Diversity of talks has been inspiring
* Really enjoyed Hanlee's talk
* NHS / national scope; working through individual sites to make sure sample prep is okay

# Technology update from Oxford Nanopore Technologies

## Novel/interesting statements selected by David Eccles

* Duplex rates up to 85-90% (over 50Gb native; over 100 Gb duplex when using PCR)
* Changing to 5 kHz sampling model; getting 99% accuracy HAC, 99.5% accuracy SUP
* RNA04, 2x faster motor, really boosted accuracy to 95%
* Changing flow cell buffers on the trans side, 50% boost to throughput
* SmidgION ASIC (400 channels, no MUX low power consumption) working; getting q20+ as well as duplex
* MinION MkII - cheap; 5-10 Gb per flow cell
* Grongle - 8x MkII flow cells sequencing in parallel
* New MinION ASIC coming; will be swapped out for existing MinION flow cells
* VolTRAX / TraxION upgrade - reagents included in pack; integrated with sequencer; just drop sample in

**Announced by Rachel Rubinstein [Some of those at work forces...; RATM]**

## Clive Brown

**[can't pick walk-on music]**

* A lot of the talk will be themed around near-future improvements
* Public company, no promises, don't buy stocks based on what I'm saying

### Our Technology

* Goal: enable anybody to sequence anything anywhere
* Pretty far along with that, but still a lot to do
* Have spoken previously about protein sequencing; won't talk about that today
* Outy chemistry; all going really well, won't talk about that today
* FET array, new kind of sensor, thousands or tens of thousands measuring voltage; also won't talk about that
* All these things are ongoing R&D areas

### Brief introduction of nanopore sequencing

* Membrane system, proprietary technology
* Pore + motor (helicase; couldn't get polymerase to work)
* Strand sequencing: motor in flow cell, DNA goes into pore, fragments can be very long

### Duplex

* Various attempts to sequence both strands; hairpin approach was abandoned
    * Some artefacts in the signal; movement of helicase on complementary strand to it
    * Algorithms didn't work
* One day, pulled senior researchers into office - what if complement follows; about 1.5% of time, complement followed naturally; we tried to optimise that
* Initially, fiddled with adapters, got to around 30% duplex
* First strand goes through, second strand will follow it; better second strand read than with hairpin
* Have been developing ML methods for basecalling; very strong ML team now
* Stereo - method to base call both strands
* Have spent the past ~9 months to get the numbers up
* Huge amount of work getting duplex to be very competitive
* Releasing duplex at this conference - high-duplex / super-duplex
* Can get around 85-90% duplex rates now, maybe can get a bit more; rates right up from where the prototypes were; this is the high-water mark
* Got a group of naive users, had never run duplex; one got 85-90%; two got 80% duplex, #3 didn't do so well.
* Incremental improvement, have now got duplex accuracy to around q32, at 5 kHz, went out yesterday in MinKNOW; most have been at 4 kHz
* 5 kHz gives a little boost
* Plot of kit 10, Kit 14, Kit 14 duplex; substantial improvements
* No relationship between length and quality, not using light-based chemistry; if you can deliver the fragment to the pore, it will work
* Simplex rates, modal simplex around 99.5%, getting close to q30, at 400 bases/s
* Longest perfect read 40 kb
* Longest Q40 read 130 kb
* This is double ligation, but wants ultra-long duplex, maybe doing something that doesn't require ligation
* Native human, almost 150 Gb from one PromethION flow cell, of that over 50 Gb duplex, left with another 45 bp simplex (q20+ simplex, great for mutation detection)
* Natural DNA; all other platforms amplify during or before sequencing
   * Inter-strand cross-links, backbone breaks
   * Natural DNA is difficult
   * All of the damage is going to be in there
* Now can go onto the shop and obtain this
* Software-wise, a new version of dorado which is very capable of doing duplex calling, very performant, quick enough to be useful; should be folded into MinKNOW by July
* MinKNOW - can turn on various features, will be able to turn on duplex
    * Can work from pod5
* Won't tell you how to do your day jobs; lots of utility, don't need to sell that
* Available right now; soft commercial launch with cheaper introductory pricing
* Flow cell is marked; duplex is a special flow cell, marked with HD
    * If you don't want duplex, need an ordinary flow cell
* Did a pre early access; talks from T2T using alpha version with pre, ropey version
* Version you're getting is a big step up
* Lots of ways to get more duplex
    * Need to get both ends on; naive users are pretty good
    * A lot down to blocking (a bit of a misnomer)
    * Very rarely, something goes wrong, something will foul up, and you lose a channel
    * A lot of headroom to fix that
    * Currently only get about half the available data from a flow cell
    * Want to take the existing system and uplift
    * A lot is about natural DNA, some related to extraction
    * It turns out that buffers have quite a large impact on behaviour, can measure very small effects
    * Changing buffers on trans side and cis side are having quite an effect
* Illumina missed 8% by smashing DNA up
* ***PCR, all the weird stuff goes away, can get over 100 Gb duplex + 50 Gb simplex; we think this has crossed the threshold where it's usable***
* Don't need to change the machine, can continuously and incrementally improve

### Simplex
* ***Pretty good, moving to 5 kHz sampling***
* Super accuracy milks all the data we can get, but is computationally intensive
* HAC caller does 99% of sup accuracy, does in 1/10th the time
* Very high yields of native
* Simplex is actually very good
* 85% of applications are about mutation detection
* Indels are what we mostly get criticised for; mostly from edges of long HP
    * Indel performance is still quite good on human
* SV work is pretty good
* Simplex does a lot of work, a lot of heavy listing; don't underestimate
* If you look at hac/sup called, only really INDELs that are slightly worse
* Improving
* Simplex performance; we currently get about half out of the flow cell, can probably run faster
* Best run R9 was 250 Gb, R10 target (currently 190 Gb) is to get to 400 Gb
* A lot of headroom in the current system without changing the instruments; capacity is there to get to a $200 genome, mostly by optimising chemistry and flow cell (major target in the coming year)
* Nanopore system is an investment in the future; modular system / chemistry; more and more value to mine out
* 2012 talked about run until; time estimates aren't that good
* More and more common for people to do sequencing until
* Adaptive sampling now available in MinKNOW for P2

### Base calling

* Dorado team has done a huge amount of work
* HAC now keeps up on-device for a P24; much better for running all flow cells all the time
* Can now run 24 PromethION flow cells with HAC
* Have a software package - EPI2ME; lets you do automated bioinformatics tertiary analysis; more and more useful workflows, mostly open source unlike competitors (??? Galaxy)
* Making small, cheap devices; most people don't have the bioinformatics skills of large labs
* Version coming for a clip workflow for clinical

### Modifications

* Huge progess made
* We started off comparing to the brass standard - CpG bisulphite
* Can get 5mC, 5hmC, now baked into MinKNOW
* Can look at natural DNA and natural RNA; why wouldn't you try and get everything
* Believe that Nanopore 5mC is better than anyone else; getting there with 5hmC
* 2.5 years of work, making synthetic randomer, modifying one position, synthetic validation and training sets, often things that won't work in nature
* Models trained on synthetic DNA, as close as we can get to de-novo
* Can now call many H. pylori modifications
* Adrian has a list of modifications; more and more getting added to the basecaller
* Latest dorado has the latest models; better than [soto]

### Homopolymers

* Technology can't be completely useless, you're all sitting here
* Work in progress
* On a duplex system, can do a synthetic complement, can spike in special reagents that will modify homopolymers (proprietary)
* Basic problem with HP on nanopore is that it's flat; can disrupt on the complement; basecaller can latch onto that and give a base sequence
* Ambition for the rest of the year is to come back with a plausible fix for at least consensus homopolymers
* Target - T2T assembly, nanopore only

### RNA

* Have done a huge upgrade of RNA
* A really little tight-knit fan base of fanatics (about 5%); we think it's going to grow
* Now people use modification to tell the tissue of origin for cancer
* RNA junkies; have an upgrade going up now
* ***RNA04, 2x faster motor, really boosted accuracy to 95%***
* RNA is quite heavily modified, basecallers are probably miscalling mods as errors
* More value to be extracted from calling RNA modified bases
* Currently at least 147 modifications in RNA
* Adrian has a list that reflects research priorities
* We are using the same synthetic method to knock down RNA bases
* kit available now
* We have a custom pore for this; it's complicated having multiple pores
* Looking at signal intensity, standard variation, and dwell time
* No modifications in RNA, can see in the signal; all the modifications
* First 8 modifications they looked at can be seen
* Looks really promising, a cracking RNA with modifications
* Same hardware, different pore, different flow cell, no new box to buy
* Pseudouridine - the one used in RNA vaccines; boosts expression of antigen protein
* Quite key to be able to sequence it
* Can see this very nicely on the new pore
* RNA004 available / shipping now; it's pretty good (both platforms)

### Nanopore Platforms

* You know what we sell
* P2 is out
* Some of the very early P2 solos had a hardware bug; all fixed
* Now shipping the other P2 (wanted to call Skywalker); fully-integrated compute
* Adding new software features for walk-away analysis
* First one went out a few weeks ago to Josh Quick's lab
* P2 Solo has been used in the field, some nifty skins; all looking quite tasty, a released product
* How else can we boost output?
    * Have found other motors that work quicker, up to 1000 bases per second
    * Quite likely that motor will be swapped out, potentially more data from the current system with at least the same accuracy

### Buffers

* Light has a small effect on what goes on in the flow cell
* By changing buffers on the trans side, can have improvement in flow cell
* ***From 80 Gb to 120 Gb by changing buffers***
* Possibly G-quadruplex
* A really easy change to make; just swap buffers
* Some analytes are worse than others
* People who have never run a sequencer before it are doing it; we're not just smashing up human genomes
* Fish, chicken are difficult
* Getting more than 70% improvement by changing buffers


### Ultra-rare sample

* Very irate user used an ultra-rare sample, thought they had lost the sample
* Nanopore only sequence a tiny fraction of the molecules, can take it out again and run it on another flow cell
* Get about 80% of the yield on the second flow cell
* If library prep didn't work, can pull it out

### New boxes

* They're better
* Mk 1D, replaces current MinION (after 10 years); looks like communicator from Star Trek
* Good temperature control, backwards compatible with current flow cells, USB-C
* Have made a system that docks the Mk1D to an iPad
* New Apple processors have good GPUs on them
* 5G and WiFi, can stream data to the cloud; final device will have a ruggedised cover
* Will replace Mk1C

### New ASIC

* I could talk about ASICs for another hour
* All other platforms rely on a 3rd party CCD, little or no control over the characteristics of that device
* We design our own sensor, the ASIC; making ASICs isn't easy, takes about 6 years to develop, takes about 1 year to fix mistakes
* We now have finally some new ASICs coming; this requires new devices; called previously the SmidgION ASIC
* Actual board with an actual functioning ASIC
* ASIC is the heart of the machine
* No chip, no product, no job; that is everything
* Different architecture; connected to a board
* 400 channels, single MUX (MinION has 4 MUX)
* Very low power consumption; need to heat it up to make it work, really suitable for portable devices; this is the thing we should have made in the first place
* ***End-to-end run... it does work, get q20+ and duplex; tiny mobile phone-style power consumption***

### MinION MkII

* MinION in 2012 was meant to be a completely disposable USB stick
* Huge flexibility on pricing
* Can really think about pushing MinION to new places
* Small dongle for interfacing to USB
* Could be made fully disposable, but pretty unethical to throw everything away
* Coming shortly
* ***Thinking of 5-10 Gb per flow cell; it can be cheap***
* Low power, all from USB-C

### New platform - Grongle

* ***GridION as a dongle***
* Can array these things (8 per dongle)
* Box contains a heater, collects data; current M2 laptop can keep up with basecalling

### Another MinION ASIC

* Cost 200 Million to make the current ASIC
* New ASIC based on the new architecture; different MUX
* Front-loads the output, same amount of data as a MinION in half the time
* ***Will swap out the ASIC on the MinION flow cell, compatible with the new device outside***
* When you buy a certain number of flow cells, will get the new device
* Opens up new ideas
* It's so cheap

### Revamping VolTRAX device - TraxION

* Inclusion of this thing
* Same base unit, enhanced optics
* Comes ready to run, all reagents pre-loaded in the bloody flow cell, don't need to do anything except load the sample
* Can automate pretty complex workflows
* ***Key thing: have incorporated the new sequencer into the device; plug in the reagent cartridge, and will run as a sequencer; improved VolTRAX and sequencer***
* Hope to have on the stand this time next year
* Can sequentially load flow cells, put it into the Grongle

### Summary

* Main message: things that we dangled in front of you last year, most are out of the door
* Next challenges: modifications and RNA (out the door as well)
* Thing I want to fix is homopolymers; nanopore-only - why would you buy anything else

## Rosemary Dokos

* Clive has talked about an enormous amount of work
* P2 is with developers now, Mk1D coming out in summer
* Terbit will be coming out later this year
* R10.4.1 have gone from early access to release over last few days
* High duplex, early acces now available
* Kit 14 early access to release
* cDNA sequencing kits September
* Cyclomics EA October
* On software side, MinKNOW 5 kHZ pod5 out now; dorado updates out now
* Everyone working really hard on dorado integration
* EPI2ME team; please see tomorrow
* Will continue with MinKNOW; duplex integrations, workflow integrations

### Timeline slide:

* Now - MinKNOW 23.04 with pod5 output and 5kHz basecalling
* June - High-duplex early access; rapid PCR barcoding Kit14; Dorado basecalling speedup
* July - RNA04 developer access; multiplex ligation kit
* August - Cas9 multiplex kit14; MinION Mk1D developer access
* September - cDNA sequencing kit14
* October - P2i early access; cyclomics early access
* November - TurBOT developer, 384-barcode native barcoding
* December - iPad case developer, 16S kit V14

### Summary

* Raw read accuracy 99.5% (q22)
* Duplex accuracy > 99.9%
* Now aligning against T2T, getting >Q50 for assembly
* Raw-read methylation accuracy > 99.7%

## Questions

* Cost of new flow cell will be cheap; will not cost you $1000
* Can we call methylation from duplex strand-specific - yes
* Do you need SUP for duplex - no

# Helen Gunter (Mini Theatre)
*Using nanopore sequencing to understand the manufacture, delivery, and action of mRNA vaccines*

## Summary

* mRNA vaccines are both safe and effective
* a number of different therapeutics
* siRNAs for clinical therapeutics
* How we are using ONT sequencing to investigate synthetic mRNA

## Structure

* 5' cap
* 3 and 5' UTR
* Synthetic mRNA synthesised in-vitro, translated into protein using intracellular machinery

## BASE

* Innovations in mRNA therapeutics
* manufacturing mRNA
* leading Australia for pre-clinical manifactured mRNAs

## Process of manufacture

* 1st - plasmid template synthesised (including T7 promoter)
* plasmid linearised
* in-vitro transcribed
* purified
* formulated in lipid nanoparticles
* quality monitoring is of central importance
* current reccomendations use a roomful of technology, takes about half of Pfizer's production time
* Used ONT to develop a comprehensive test, using different tests, from plasmid template, in-vitro RNAs, and after lipid manufacture
* can use both cDNA and direct RNA sequencing to investigate quality

## QC
* Integrity of plasmid template can be investigated via plasmid sequencing
* cDNA aligned to reference sequence
    * Majority aligned to reference sequence
    * Some unintended sequences, run-on transcription leading to full plasmid-length mRNA
* Test (vacseq) does tail length analysis and other QC
    * Information is helpful for refining processes
    * Plan to refine and benchmark against the existing QC recommendations

## Applying vacseq at the BASE facility
* Used for routine monitoring
* Propagation of mRNA can introduce spontaneous deletions
* ONT sequencing can confirm plasmid integrity
* Results of ligation sequencing of plasmid template; IGV plot indicates low coverage at the area of the polyA tail
    * confirmed by tail finder; length significantly shorter than intended 113nt length
    * polyA tail length is important for vaccines
* Used to validate formulation
    * Process of formulation exposes RNA to degredation condition
    * Compared formulated vaccine to unformulated controls
    * Sequenced using direct RNA
    * Observed slight increase in abundance of short mRNA fragments (<500bp)
    * More detailed analysis, ~1% increase in 5' degredation
    * 5' UTRs appear to be similar, despite degredation

## Summary

* Comprehensive and streamlined vaccine test, using ONT sequencing
* Test used in mRNA vaccine manufacture at the base facility

# Jade Forster (Mini Theatre)
*Identifying m6A RNA modifications in neuroblastoma cell lines using nanopore sequencing*

## Epitranscriptomics
* Modifications of RNA molecules
* over 150 types
* Most common m6A (role in mRNA splicing, translation and stability)
* Regulated by combination of readers, writers, erasers
* Methyl groups read by readers (can be location specific), can impact application of mRNA (e.g. alternative splicing, translation, stability, decay)
* Erasers remove methylation
* Process is highly dynamic
* Dysregulated in cancer, possibly related to evasion

## Neuroblastoma
* Childhood solid cancer
* Heterogeneous process
* 50% spontaneous regression, other half <40% survival with chemotherapy, radiotherapy and novel immunotherapy
* Lack of recurrent somatic alterations, nowhere global
* Finding novel treatments
* Recet studies suggest m6A is important

## ONT to study
* Has been difficult to study in the past; cDNA loses modifications
* ONT offers chance to detect on native molecules
* Unmodified and modified base goes through nanopore
* From one experiement can get transcriptomic information, alternative splicing, differential gene expression, polyA tail, analyse data for base modifications
* Expect as basecaller becomes more sensitive, deconvolution will be easier

## ONT to study epitranscriptome of neuroblastoma
* Protocol - used GridION, RNA002, R9.4.1 flow cells
    * grew LA-N-1 and IMR-32
    * cultures extracted with Trizol
    * Used total RNA (5µg), split across 4 flow cells, sequenced for 48h
* MinKNOW basecalling, minimap2, m6anet, nanopolish

## Results
* 2M reads for LA-N-1
    * 11,371 m6A sites, high confidence 143
* 2.6M reads forIMR-32
    * high confidence 489 sites

## Example
* Location of m6A, most at 3' end of mRNA, some in UTR, some exonic
* Have found some intronic modifications
* 143 sites in NA-N-1 across 88 genes
* IMR-32 489 sites across 148 genes
* shared: 55 genes
* gene-ontology analysis identified relevant processes
    * 55 genes, process was histone H3-K36 methylation

## Conclusion
* use direct RNA sequencing to identify m6A modifications
* gene ontology identified H3K36 methylation
* Ongoing analysis using other m6A tools
* Further analysis needed to understand the biology

# Data for breakfast - George Kolling
*Making read processing faster*

## Base Calling
* Interpreting the read signal is hard
* Universal approximation theorem - a deep neural network is able to approximate a complex function closely
* PromethION can generate up to 720M read samples per second, more than a quadrillion operations per second
* GPUs - graphics processing unit; evolved from graphics processors
    * Motivation: make video games look good
    * highly parallel, initially couldn't be used for anything else
    * eventually, the things to make graphics better became useful for science and neural networks
    * from that point onwards, NN became massively successful
    * GPU companies have now redesigned cards to be good for NNs

## GPU Evolution

* V100 produces 1.25 peta operations
* 2 years later, 6 times improvement in peak performance
* Ampere architecture 2 years later again, another 6 times improvement
* Now using 8bit integers in Ampere architecture, another 5 times peak performance

## Dorado and GPUs
* Use Torch library, many reasons
* Torch isn't great for performance in specific cases, need to use hand-optimised code (e.g. LSTM layers)
    * LSTM - the core component, the most computationally expensive
    * capable of making sense of long-distance relationships
* Implemented a custom type of layer in CUDA
* Top execution trace / timeline, initial Torch built-in LSTM takes about 159 ms
* Improved version using optimised matrix layouts and Cutlass, improved to 103 ms
* Used Cutlass library from Nvidia; matrix multiplication and gate calculation combined; combine thousands into a single block
* Using quantisation (16 bit floating to 8bit integers)
* Achieved 2.4x performance improvement, some great help from Nvidia

## Custom Metal code (e.g. for M1)
* Dorado implementation predates Torch-supported Apple
* 90% Apple ALU utilisation, currently exploring other options

## Batch processing
* Need a lot of work to be done in parallel
* Take multiple reads, chunk into fixed-size chunks into batches (can be thousands of chunks)
* Getting the numbers right is important for high-performance
* Default dorado does automatic batch-size detection

## What's New and what's next
* Duplex calling on GPU, uses same code as simplex, so simplex improvements propagate to duplex
* Will widen the quantisation used
* Structured sparsity
* As we get faster, will investigate whether we can do more

# Data for breakfast - Katherine Lawrence

## Why 5 kHz
* More measurements sounds great, why not higher?
    * Each measurement is noisier
    * Intrinsic features of samples mean that noise increases as rate increases in a non-linear fashion
* pod5 file format makes it easier to work with increased data
* Can make tweaks to data
* Increasing stride (step size for convolution)

## Read accuracy histograms
* Both high-accuracy and super-accuracy have substantial improvement
* HAC is now at q20

## Duplex
* Can get to q30 for duplex accuracy
* 1 end by chance will start sequencing first
* Other strand, with some probability, will follow its mate through the pore

## Why duplex?
* Second strand sequence has different bases, moving in a different direction, so sees a different electrical signal

## Stereo duplex basecalling
* Take two input signals, base call independently, do complement and alignment on second strand
* Take alignment features, feed into stereo basecall model, including multi-feature
* Produces high-quality output

## Duplex accuracy
* Vs first strand accuracy
* HAC condition have modal accuracies close to q30 at 5 kHz
* Have high-quality q30 reads up to hundreds of kb

## Applications
* Some cases where q20 is enough
* q30 important for T2T genome assemblies
* Want a source of long, high accuracy reads at high accuracy
* Add ultra-long reads to span the difficult repetitive regions

## Implementation
* Code has been in Dorado, but had upstream methods to identify pairs, some duplication of effort
* Now have an all-in-one integrated workflow
* Input pod5 model; dorado begins to call basecalls
* Also looks through basecalls to identify pairs
* Continuously outputting duplex basecalls as well
* Not only is this simpler than previous workflow, but it's also faster
* Any improvements to simplex translate to stereo basecallers

# Data for breakfast - Chris Wright
*Leveraging nextflow*
* Had a long history for Oxford Nanopore
* Now delivering bioinformatics for ON customers
* How do we package those up and get them to customers
* Want to make it simple for everyone to do bioinformatics

## Where we are
* Bioinformatics previously coming from left, right and centre
* Want to get from workflows to results in one seamless experience
* Demo - using JBrowse2
* Outside of bioinformatics, no one uses Linux, people use Windows
* Getting this stuff running on Windows is a challenge
* Focusing on getting it running locally, not on the cloud

## Process
* Creating a library of workflows, freely available, can take without using UIs
* Fully standardised, containerised, make heavy use of Conda
* All things come with demo datasets, produce QCs as well as primary outputs
* All freely available on Github, continuously tested
* Not really running on Windows (using WSL)
* A lot of bioinformatics code makes use of POSIX APIs

## How did we get here?
* Standardised on nextflow
* Needed something that worked on a wide variety of platforms
* Low resource countries
* Need to be able to deploy this to everyone
* Everything needs to be documented
* Need real time capabilities <- important
* Nextflow can process data in real time, can see files appearing on your file system and process them in real time
* Did demo something at LC 8 years ago trying to do assembly in real time, but usually not

## Reputation
* Nextflow is hard to learn
* People at Seqera - "It's not hard to learn, but there's always this sinking feeling that you're not doing something right, and that feeling never completely goes away"
* Written only by one person
* Has grown organically
* Lack of true expert knowledge in the community (see Chris' blog post)
* Not hard, just different
* Much more like writing code; you chain things together by calling functions
* Someone in Chris' group still doesn't like Nextflow; no one is going to use a workflow manager that ONT makes

## Real-time workflows
* Sequencing just enough
* As go though a sequencing experiment, will saturate how many taxa have been seen, but a long tail of low abundant things
* Can be done in nextflow using channel.watch path
* Can combine with adaptive sampling [progressive kraken2]

## Developing a new workflow
* Don't try to develop anything up-front
* Embrace feedback from users and external customers
* We are mirroring and trying to compile things for Arm (e.g. Mac); trying to rebuild bioconda for Arm
* Want people to use their own workflow in their UI

## Where are we heading
* Extending to the cloud, so things can run in the cloud
* All data starts on your device, compute will be done in the cloud
* Will have early access for free
* Will be able to run any analysis you wish on that platform
* Engage with us, try to break it

# Data for breakfast - Panel discussion
* Fast5; one of the limitations is that there's only so much data that can load off disk; bottlenecked by read loading speed
* 4/5 kHz - will they exist together?
   * MinKNOW 5.5, moving to capturing new data for 5 kHz; all tools continue to support 4 kHz calling
* Is dorado aligner going to be able to do spliced alignment?
    * We are working on it; it doesn't do it now
* Does dorado aligner use another tool for alignment?
    * Continuing to use minimap2; check if ONT preset is the right one to use
* It seems like dorado was designed for GPUs; what about very basic setup
    * Performance improvements, have been working on CPU running, and improving performance
    * Have also focused on making Dorado available on Apple devices, M1/M2 getting great performance
* Is it more accurate than guppy?
    * Set of models that are in guppy 5.7, and set of models in dorado are the same models; in terms of base call accuracy, getting basically the same results
* Why are you not using 8-bit integers across all layers?
    * Model architecture currently used some layers generate values in a wider range, can be difficult to determine range; doing a bit of extra work there
* When duplex calling, joint basecalling from more than 2 reads (e.g. UMI, PCR duplicates)?
    * Why not add in more strands? principle behind outy, pile-up
    * An active area of research
    * Stereo method is relatively flexible, can be adapted, but might not be the best approach
* Low-complexity samples?
    * Any dsDNA that can be paired up should behave relatively similar
    * Low diversity in general will make it difficult to pair up sequences
    * Dorado doesn't have support for using synthetic duplex pairs (which are not actual pairs); have looked at this internally; having two reads of the same strand is less beneficial than two reads from different strands
* 1.0 release roadmap?
    * Waiting for when it's finished and feature complete
* How do we develop our own models?
    * Canonical basecalling can be done in bonito
    * Modified bases are more difficult; can get access to software and lab protocols, all behind a developer agreement
* Do you have any Nvidia vs M1/M2 benchmarks?
    * Single A100 GPU (in a P2, 4 in PromethION); 60M samples per second, at about 1M samples per second on M1 basic (HAC)
* Does Dorado support RNA basecalling?
    * yes, RNA model in dorado
* EPI2ME - adding / sharing own workflows?
    * Can import any workflow, as long as it uses nfcore standards; don't follow everying they recommend because some is a bit silly
* Is my data safe and private when using these tools?
    * On your desktop, that's your concern
    * In the cloud, resources are strictly guarded (i.e. separate instances)
    * Not able to handle human data
    * A lot of people are from AWS, understand best practices

# Daily preview
*Tip, John ???, Matt Loose*

* Brain tumours - might need to know methylation status
* Want results within hours
* Low-power, low-cost ASIC

*Music - Like The Deserts Miss The Rain*

# Day 2 Introduction - Rosemary Dokos
*Music - London Calling*

* Already an epic LC; talks highly inspiring
* Gordon kicked off talking about Midlands, iron ore smelting
* All of innovation is always a journey
* London underground; first underground system on the planet, enabled by iron ore smelting
* 1860 first tunnel built using cut & cover, completely transformed London, incredible disruptive
* Only 7 years from cut & cover to great head shield with building out of iron
    * Wasn't perfect, wasn't seamless
    * A lot of firsts, first time done under without disturbing the world above
* Any innovation is always a journey
* This has been our journey so far
    * Started in 2014; MinION line, other lines
    * Key thins that drive the improvements are all the updates to chemistry, sofware
    * Investment keeps on giving
* What we have promised
    * Right in the midst of q20, all PromethIONs, short fragment
    * Will move to protein, aptamers, small molecules, voltage sensing
* Today
    * Sequencing anything, from cell-free DNA to largest genomes on the planet
    * Transcriptomics, seeing everything within those things; expression analysis, isoform detection
    * Anything, everything, all in one experiment
    * Can scale, from $1000 MinION to high-throughput PromethION
    * Launched new PromethION pricing, as P2 becomes more popular
    * Will start launching project packs
* Together, have developed all the approaches
    * SkimSeq - low-pass
    * Whole-genome sequencing; assembly (org.one samples: butterfly, spider monkey)
* What we've been missing really matters
    * Speed is important
    * Can get rapid assessment of genetic risk
    * Characterise TB, metagenomic profiles
    * Ability to deliver the answers in the place you need them
    * How, where, when is really important
* Products evolve, it's a journey, want to deliver impact
    * Need stability, robustness
    * Increased consistency in flow cells
    * Supported end-to-end workflows; field applications teams trained
    * Getting software download improved
    * Automation - Tecan; Beckman Coulter; Hamilton
    * Dorado integration into MinKNOW, not just base calling, want methylation and alignment; A100 tower should be able to keep up with 3000 flow cells / year
* Increasing number of questions
    * A lot in audience want to take it to the next step
    * Applied products
    * In life science, will continue to innovate
    * Products taken into Q-Line; software and consumables support for 12 months
    * Applied / market products locked for a single application
* Expert mode MinKNOW
    * Taking product into labs with technicians, things can become daunting and scary
    * Have MinKNOW workflow mode ready
* Building blocks
    * Upstream integrations coming
    * A lot of downstream pipelines
    * All want to start plugging in
    * TurBOT is going to be the first end-to-end solution
* Reminder for the next 12 months
    * Kit launches, etc.
    * An enormous amount in store

# Day 2 Introduction questions
* Community is now ready for novel technologies to fill gap. What about nanopore in the health-care system?
    * We are absolutely hearing this now from the community
    * [Post-COVID...?]
    * From our side, we need a product that is there, mature, ready
* Cost for TurBOT?
    * It is a robot, so will cost what a robot costs; cost-competitive
* Are you working in the direction of working with low input?
    * Every time there is a kit upgrade, sensitivity and input amount improves
    * Constantly active in our minds
    * In a lot of clinical applications, amplification is accepted practice

# Thomas Bray - Final day of LC 2023

* Welcoming all here today; moderating next session
* No scheduled fire drill, events team will help evacuate
* Wait for microphone, so can be heard here and online

# Fuchou Tang
*scNanoATAC-seq: a long-read single-cell assay to simultaneously detect chromatin accessibility and genetic variants*
* Set up lab in Beijing, looking at methylation patterns in one cell; methylome is quite expensive
* 2020 - lab developed single cell methylation sequencing method using ONT
    * Can detect over 4,000 different RNA isoforms, transcription pattern
* 2021 - first single cell genome sequencing; after you can sequence the genome, is possible to do de-novo genome assembly; as little as 30 single cells, can get 1.3 Mb long contig assembly
* Today mainly talking about single cell nanoteck
* 8 years ago (2015); single cell ATAC-Seq; both powerful and cheap
    * Open chromatin only 20-50,000 reads needed for discriminating cell types
* Open chromatin fragments are short, 100-300bp
    * Can use ONT to do single cell ATAC-Seq
    * Less than 300BP long, use TN5 to cut twice, isolate intermediate fragment
    * Adjust TN5 to cut once or twice within regions, and outside relatively randomly, around 30 kbp free
    * Can amplify several kb-long fragments
    * Can identify a lot of disease predictors
    * Compared to scATAC-seq can identify footprint of transcription factors
    * Can see around the motif, a valley around the peak, protein binding to the peak to make it less accessible
    * Can achieve similar power to Single cell ATAC-seq
* Apply measure to different cell types, can identify cell features and footprints
    * Minimal cross-contamination / doublets
    * Only about 20-50,000 reads, can clearly discriminate different types of cells
    * For cancer studies, can clearly discriminate cancer vs normal cells
        * e.g. K562 chromosome arms
    * Minimal background
* Important things for scATAC-seq is to identify alleles in open chromatin features
    * Sometimes the alleles can be different (e.g. maternal open, paternal closed)
    * Want to discriminate the two alleles
    * For our method, only need SNP within 8 kb surrounding region
    * Efficiency to detect alleles is better with ONT
* Applied method to cell line
    * Majority of features on X chromosomes, match expected feature / inactivation
    * A particular cell line, inactivation is biased to the paternal allele
* Some examples from imprinted genes
    * Paternal / maternal differentially silenced
    * Within the 300bp open region, no SNPs
    * When sequencing neighbouring 8k region, can easily identify alleles
* TRIM61
    * Maternal allele fully open, but open region again has no tagging SNP
    * With 8k region, can identify heterozygous SNP
* Some reads randomly fragmented from genome, so can detect single cell genome information
    * For structural variation, including deletions
    * Translocations are most difficult to detect; for our method it's very easy to detect; can clearly identify translocations and insertions, and 89kb deletion
* For scATAC-seq, can look at co-opening of different genomic regulating elements
    * Co-openings need to exist on the same allele
    * Directly sequencing the connecting fragment between promoter and enhancer
    * Can detect many co-opening features (e.g. SOX4 gene)
* After seeing method worked beautifully in cell lines, use method for embryonic development
    * From zygote to late blastocyst stage
    * Late blastocyst has already three different types of cells
    * See beautiful path of embryo, and separation of cell types
    * After path can be captured, can see different cell types
    * Most cells similar epigenetic state at the start; different states at even 8-cell state
* Can really identify the 'dark matter' in our epigenome
* Can clearly identify the structural modifications

# Questions - Fuchou Tang
* Where is the protocol?
    * PromethION can sequence up to 1000 cells, cost about 2 pounds, 20-50,000 reads
    * When merging 50-100 cells together, can see all the peaks
* Specific sequencing depth?
    * 20-50,000 reads per cell
    * If don't have so many cells, sequencing about 200-500,000 reads for each cell
* Have you looked at DNA methylation?
    * No, single cell needs amplification 1,000 to 10,000-fold

# Rachel Thijssen
*Single-cell long-read RNA sequencing reveals complex heterogeneity in leukaemia*

* Just started as an assistant professor
* All work from WEHI
* Bad blood - leukemic cells
* Why new methods?
* How you can use single cell long reads to detect isoforms

## Introduction to blood cancer
* Don't die from stress signal; sometimes due to upregulation of BCL2
* Molecular inhibitor Venetoclax
* CLL - most common adult leukemia
* Even though these patients were pre-treated, 80% responded to venetoclax
* Other clinical trials led to the approval, now used as up-front therapy for CLL
* Most CLL patients respond, but after years on the treatment, cancer comes back
    * Only 3 of original patients still on therapy

## Steps
* Apply bulk sequencing, found mutation in BCL2, reducing binding affinity
* Only see this mutation sub-clonally, some don't have this mutation
* Single-cell researchers love the smoothie analogy, want a fruit salad
* Looking at leukemic cells, want to look at high-throughput method
* If you really want to resolve cancer complexity, have to link the genome

## Samples
* Using peripheral bloods before and after relapsed therapy
* Using 10x, reverse-transcriptase with unique barcode and UMI
* Indexed cDNA, further amplified, only take a little bit for short read libraries; fragmented lose a lot of information with short-read sequencing

## 1 method - subsampling
* Developed a protocol to subsample, divide droplets into 80 and 20%, put into a different well
* Prepare short-read libraries from both
* Run 20% sample on a PromethION flow cell
* Can overlay whole transcriptome data

## 2nd method - gene-specific targets
* When looking at cancer, already know the targets
* Rapid Capture Hybridization sequencing
* Design a probe panel for genes of interest, then sequence it on Nanopore sequencing

### Probe capture
* 120bp probes, designed to tag exons of to 1kb
* Probes biotinylated
* Cultured together, overnight hybridisation
* Washes
* Amplify cDNA with specific primers including the ONT overhang
* Use nanopore cDNA PCR kit to clip on barcodes from ONT
* Can then pool multiple samples together

### Analysis - Flames
* Integrating short-read and long-read data
* First analyse short-read data (e.g. cellranger)
* Have the barcodes, Flame used to demultiplex long-read data
* Do QC step to remove artefacts; looking for PolyA + gene + barcode + TSO
* Align reads to genome
* Integrate all together

### Research
* Venetoclax only targets BCL2
* Designed a probe panel for genes, did nanopore sequencing on that
* For 17 genes, can combine 10 patient samples on one PromethION flow cell
* Sometimes capturing more UMIs in long-read data than short-read data
* DOing splitting protocol, can see some mutation
* Doing single-cell RaCH-seq, can nicely see mutation

### Data integration
* CLL2, only looking at leukemic cells; cells already don't cluster together
* Cluster on right side have BCL2 mutation
* Other cells, have a novel transcript of NOXA - novel pro-apoptotic transcript
* When looking at NOXA novel transcript, only found novel in cells, and not WT; both genes are located on Chr18
* Some allele-specific transcription going on
* When integrating short-read data, cluster expressing BCL2L1/BCLxL
* So 4 distinct mechanisms driving VEN resistance

### Isoform variations
* Patient 6 had complete deletion (out-of frame), nonsense-mediated decay of BAX
* Looking at isoform usage, now see another resistance method

### Further research
* Just looking at BCL2 family members, but still have stored cDNA
* Went back and designed another panel
* Looking at SF3B1, can overlay mutation calling
* Take cells with/without mutation, look at transcriptome profiling (doing this at the moment)

## Conclusion
* Capture multiple genes of interest, can just go whatever you like to do
* Can capture multiple genes of interes
* Can call mutations
* Can look at isoform usage
* Can identify the transcriptome profile of single cells
* Send email to get protocols

## Questions
* SF3B1 is a splicing factor mutation, can we look at splicing?
    * Yes, we did the splitting as well
* Can you use the same probes to capture cell-free DNA?
    * Probably
* Average read length with probe capture?
    * Capturing genes up to 8kb; can look at really long genes with more probes per genes
* Can we replace capture with adaptive sampling?
    * No; cost-effective because 90% of reads are on-target
    
# Sara Agee Le
* Spotlight session; champion research of ECRs
* Running a STEM event; students watching session from mezzanine above
* Yesterday heard 3 fabulous presentations
* Runners up will be presenting talks live during the next coffee break
* Winning pitch from Kilenka Sand Nutzen

# Kalinka Sand Knudsen
*Fantastic methanotrophs and where to find them*
*Music - First thing's first*

* Apologies for repeating from yesterday

## Methane
* Greenhouse gas, retains lots more heat than CO_2
* Limiting isn't just magically going to happen
* Bacteria are going to help us do this
* Most methane eaters survive and elevate, but some can reduce; we need to know these guys much better

## Gene-centric approach
* Functional genes that encode methane oxidase
* Translating all gene sequences into protein sequences, using model to check if a gene looks like a methane oxidase
* Can look at diversity
* Resolution depends quite a lot on the quality of the databases
* A lot of sequences are based on amplicon sequencing, biased against low-affinity oxidases
* Selecting samples that have an interesting profile
* Using nanopore sequencing, can only assemble genomes for most abundant samples
* Looking at reads that have not been assembled into contigs

## Translation
* Not full-length
* Using R10 chemistry, can use Diamond in frame-shift alignment mode to extract one consensus sequence
* Raw unassembled reads with correction, cluster very nicely around a contig
* Other group of methane eaters, cannot identify cluster, only able to identify when looking at raw / unassembled reads

## The tree of life
* Populating methanotroph tree of life with high-quality genomes
* Also finding completely novel methane eaters, e.g. atmospheric methane eater
* Does really work well by targeting sequencing effort by looking at shallow genomes

## What next
* Able to do predictive models, also if have necessary downstream pathways to handle this methane - enzymes have high sequence similarity to genes that have other functions
* Once have gone through these capabilities, can then look through 10,000 genomes and find sequences
* End-game - a giant encyclopedia of fantastic methanotrophs and where to find them

## Questions
* Using HMM on primary amino acid sequence; have you considered using AlphaFold?
    * Yes, this is something to look into; we are picking up novel sequences
    * Would be very neat to add on AlphaFold
* Are you looking at the MCR complex?
    * Methanogens and anaerobic... we are going to do that eventually, once I've handed in my Masters thesis
* 13 samples, any environmental characteristics?
    * Find in box and fins
    * Sequencing in forests and urban habitats, trying to target different things
* Are there any base modifications in the genomes of methanotropes?
    * Haven't looked

# Kimberley Billingsley
*Population-scale nanopore sequencing to further understand the genetics of Alzheimer's disease and related dementias*

*Music - insane in the membrane*

* Absolute pleasure of presenting CARD NIH initiative

## CARD
* Based in America / NIH
* Central mission is to initiate, stimulate, ...
* Not just enough to understand biology and identify target; need to dissect the disease
* Need to identify patients before they develop symptoms
* Right patient, right time, right treatment

## AD
* Most common type of dementia, genetic compononent
* Lewy Body Dementia
* Frontotemporal dementia
    * MAPT, GRN C9orf72 repeat
* All previous sequencing efforts have used short-read sequencing

## CARD LR sequencing initiative
* Variant dataset, 4,000 human brain samples
* Want to resolve HLA, MAPT, GBA1, APOE, etc
* Most have matched RNASeq
* Can start to look at methylation differences

## Challenges
* Need to develop wet-lab protocol
* Scalable computation pipeline
* Approach for data storage and access; 1-2 TB per genome
* Very fortunate for support from non-profit

## High-throughput wet lab protocol
* All protocols on protocols.io
* Around 30X coverage for R9
* Other projects for frozen blood and cell pellets
* DNA Extraction done on Kingfisher, using PacBio Nanobind
* DNA shearing
* Manual library prep with LSK110
* Sequencing on PromethION 48 with 2-3 loads
* No short-read enrichment

## Sequencing
* Have 2x P48 and P24
* How do we ramp up?
* Can sequence quite comfortably about 200 samples / month
    * Use Guppy in super-accuracy mode
* Can do basecalling on Google Cloud
* Basecalling is about $130 USD per sample on Google Cloud
* NIH HPC server, needs to be done in small batches

## Scalable computational pipeline
* Led by Mikhail Kolmogoriv
* Terra Workflow; can be accessed on Terra, or Github
* Input unmapped BAM
* Reference-based approach
    * Can align with minimap2
    * Sniffles for variants and Pepper-Margin
* Assembly-based approach
    * Shasta Hapdup
    * HapDiff to call structural variants
* Combine reference + assembly to create small variant VCF

## Benchmarking
* Regions of genome with high mappability + low mappability
* Comparing Nanopore to Illumina in the same sample
* Nanopore has reduced SNP error rate, especially in hard-to-map region
* Comparing with HiFi
    * Hapdup has similar F1 score to PacBio

## Haplotype-specific methylation profiling
* Can take methylation profile into modbamtools, can see haplotype frequency, reads and methylated region / unmethylated region
* Can visualise differences in methylation

## Paper
* Can detect SNPs with F1 better than Illumina
* Can detect structural variants with F1 similar to PacBio HiFi

## The first large-scale control brain cohort
* NABEC
* Sequenced over 222 cortex samples
* First 14 of brain samples are now on AnVil (Access via dbGaP)
* Pushing all 222 samples to AnVil

## Structural overview
* Detected over 80k SV, most INDELs
* Structural variants are mostly rare
* Looking at functional effects of variants, got matches short read Illumina RNASeq
   * 700 bp deletion at MAPT locus
   * Violin plot, genotype correlates with expression
   * Deletion of human retrotransposon
   * Validation via homozygous carrier, heterozygous deletion, reference

## Cell-type specific methylation
* Brain tissue is very heterogeneous, different neuronal cell types
* Have an in-house cell line that is differentiated into different cell types
* Sequencing data from neuron and microglia
* Flat line of hypermethylated at top, dip at TRIM2 in unmethylated site
* Can go back to bulk data, use cluster to pull out signals, and find signals from neuron and microglia

## Future
* Moving from R9 to R10
* Overall, R10 did better on almost every category
* Now in-line with Illumina, so moving forward doing everything with R10
* Optimised on Hamilton NGSStar
* Has been brilliant last couple of weeks
* Can library prep 48 samples in 4 hours
* A big win for high-quality, high-throughput

## More diverse populations
* Used NABEC because in-house... but Euro
* Second cohort HBCC is a Black American cohort, first disease cohort
* General requirement - need brain tissue, samples need to be broadly consented

## Additional work
* Somatic variation - also associated with AD
* Only issue is that usually need really high-depth data
* When developing wet lab protocol, ended up with 400X frontal cortex data
* Generated high-depth data for a number of different regions
    * Added 800X Cerebellum, 70X Primary visual cortex, 70X Parietal cortex
* Have been using Sniffles2
* Have 4 different brain regions, have found a low-frequency variant in the parietal cortex.

## Summary
* Generated long-read database for ~200 samples
* Lots of applications for this data

## Questions
* When preparing sample and shearing, used megaruptor 3, what were settings?
    * Brain specifically, needed DNA fluid plus needles, Speed 45, 2 cycles; a lot of trial and error
    * Pre-QC using shearing by hand
* DSCAM structural variant, did you find any function?
    * We haven't looked yet; still looking at parameters
* How fast is cloud-based basecalling?
    * Takes about 3 days
    * Cloud compute cost, about $100 for the entire pipeline, separate basecalling
    * Basecalling extra $130

# Studio / informal discussion from morning, day 2

## Fuchhou Tang, working in single cell field
* Very shallow sequencing; low coverage is enough
* Dozens of different types of cells mixed together
* Without single cell, can't see that variants on different chromosomes are happening in the same cell
* A lot of things unknown in our human genome, an exciting journey

## Rachel Thijssen, Amsterdam
* Single cell, often mutations are subclonal, have to look at a single cell level
* Looking at novel isoform, need long reads
* Single cell methods, most resistant mechanisms are epigenetics, went back over time; short time frame treatment will probably work better
* I like to do puzzles, to get paid is amazing

## Kimberley Billingsley, NIH
* Were still able to see some heterogeneity
* Interested in single cell; issue is that the brain RIN is not high enough; method is a loophole
* For last 2 years have focused on protocol development
* Everything is on [Tavern]; can go and access the workflow and data
* Parents said I was always asking "Why?"

# Mariana Corrales Orozco - Spotlight session #2
*Revolutionizing biodiversity research: Oxford Nanopore sequencing for the rapid and accurate identification of endangered species*

* PCR-based methods require specific primers that are not available for non-model organisms
* Traditionally, specific mt genes have been used, but genes have changed over time
* Reference databases are incomplete
* May find ourselves unable to compare with reference databases, or no available genes for the marker of amplification
* Approach: sequence entire mitochondrial genome without the need for PCR-based sequencing methods
* Can obtain data in the field
* Testing the protocol sequencing technologies
* Goal: new approach for accurate and rapid sequencing in-situ
* Conducted field work in northern Columbian andes
   * Tissue samples that did not require animals to be killed
* Species identification
* Mitochondrial genome via 2 approaches
* Genome WGS
* mt-filtered reads
    * Minimap2 + mtBlaster
    * Flye + Rebaler
* With Flongle flow cells, can generate enough data to assemble mitogenomes
* Percentage of genomes for each assembler
* Flye can assemble most if datasets >= 550 Mb, but poor for lower sequencing yield
* Rebaler good for moderate sequencing rates
* Although flye needs higher coverage, more accurate assemblies than rebaler
* Assembled mitogenomes enable reliable identification
* Developed a second pipeline for rapid species identification without the need to be an expert
* 3 different scenarios
* No information
* Sample identity known at order level
* Sample identity known at family level
* Could identify 21 mitochondrial genomes, including 4 novel
* Matter of hours or minutes, under field conditions, contributing to filling the gap between what we know and what remains to be discovered
* ONT may revolutionise the field of biodiversity research

## Questions
* Multiplexing multiple individuals?
    * No, using different Flongle flow cells, will multiplex on MinION in the future
* Reads that don't map to mitochondria?
    * A lot of bacterial reads, assembling them because reference databases are poor in those as well
* What is the advantage of Flye vs BLAST?
    * If databases lack the genetic data for the organism, assembly is needed
    * Also want to use the reads from species identification to compare against reference
* Can you get a more precise identification from assembly?
    * Identification is more accurate if doing a phylogenetic analysis for the whole mitochondrial genome
    * Want to allow non-experts to identify species
* Enrichment for mt genomes?
    * Could do it in the future, long-range PCR or adaptive sampling

# Patrick G. S. Grady - Spotlight Session #3
*Unravelling chromosomal evolution in marsupials: comparative genomics and methylomics with telomere-to-telomere precision*

## Why marsupials?
* An amazing model that substitutes for mammals
* Unique method of locomotion
* Dynamic forms of cancer (e.g. transmissible facial tumours)
* Can churn out rapid genomes

## Marsupial lineage
* Adding dasyurids
* Greater bilby

## Story
* Genomic diversity that complements the phenotypic diversity
* Karyotypic diversity across the carnivorous marsupials, chromosomes looks similar
* Kangaroos and wallabies have completely different chromsomes
* I set out to answer why this is happening - karyotypic flux, with a functional genome

## T2T consortium
* Allowed us to put together these genomes, very accurately with just two flow cells
* Some HiC/PoreC to scaffold together
* Credit to Nicole Pauloski to prepare samples
* Genomes assembled using Flye and Verkko, compared Male / Female

## Stats
* Quite a few levels of genome diversity; RN assemblies from thesis, near T2T level, accuracy a little lower (q35-40)
* R10 still have some gaps
* Some beautiful genomes

## Interested in comparing two groups
* Macropods have a lot of flux when aligning genomes together
* Carnivorous marsupials, everything stays in place
* What's going on? A really interesting answer

## What differs
* Splits at black bars (centromeres)
* In wallabies and kangaroos, centromeres acting as fusion and fission breakpoints
* Can get linear assemblies of the centromeres
* Chr5 from Tammar Wallaby is the favourite
* Aligned CENP-A to confirm centromere
* Sequence is a 6-letter simple repeat, TTAGGG; the telomeric central repeat
* Every centromere is the telomeric repeat
* A lot of FISH studies in the past have identified this
* Simple repeat is able to bind to itself

## Across genomes
* In places where it's a centromere, it has retained its identity
* Where it's no longer a centromere, it has lost the identity
* Interestingly, it still has a methylation dip

## Diving in
* Can answer more questions with the completed assemblies
* What's missing?
* Found all kinds of patterns
* Admixture, hybridization, important repetitive elements, unique gene features

## To Sum up
* R10, kit14 has allowed genomes very accurately and very quickly
* A major split defined by these repeat arrays
* Comparative genomics really powerful at this resolution

## Questions
* Is it known that telomeres and centromeres evolved from a common structure?
    * This is the first time it has been shown as an active centromeric element
* Sample loading on telomeric repeat; any other examples of this happening?
    * A whole lot of other questions about genome function; all kinds of genes that are blocking this from functioning
    * Looks like there has been a lot of suppression of telomere-associated genes

# Infectious diseases - Lukasz Rabalski
*Detection and differentiation of respiratory viral pathogens*

* Interested in co-infections, where more than one virus infects the same person
* Co-infections can increase the severity of diseases
* Can be problematic to treat; particular problem for children under 5y, or people undergoing chemotherapy
* Common approach - PCR; just detect the signal, not raw sequence; a lot of detection problems with false positive signals
* Can use nanopore sequencing to fight with it - cheap, small, can be used near point of care, designed to work with complex samples and coinfections, different dynamic sample levels
* Sequence of positive control allows easy comparison to almost completely remove false positive results
* Don't need to put multiple reactions per single sample
* After sampling, do total nucleic isolation, primers to amplify targets, rapid barcoding, real-time interpretation
* Target almost all main families of viruses
* Sometimes need multiple pairs of primers (e.g. for rhinoviruses)
* Didn't want to use published data, did everything from scratched
* Needed to optimise primer sequences, temperature, steps, concentration of primers
* We achieved / almost achieved it
* Able now to detect as low as 10 copies of target virus in the mix with other viruses
* Have done some research validation; clinical samples analysed by IVD samples, many freeze/thaw cycles; 92% of same results
* Also done validation of cultivated viruses, 100% hits

## Questions
* BioFire array
    * We got samples & information what they detect; 92% is all analyses together
    * We could detect some that BioFire couldn't detect, and vice versa
* Turnaround time
    * Nanopore turnaround was longer, but doesn't like to compare the two
    
# Infectious Diseases - Tiana Schwab
*Tuberculosis drug resistance profiling from native sputum using nanopore targeted sequencing: a field application study*

* Implementing targeted sequencing that is being developed by ONT

## Tuberculosis
* Drug-resistant TB is a major contributor to drug resistance worldwide
* A gap between diagnosis and infection
* Success only around 60%
* In order to appropriately treat, need to detect and diagnose

## Current methods
* Culture-based methods; stringent lab requirements (BSL3)
* Rapid molecular tests (GeneXpert); rapid, highly sensitive and specific; what you don't test for, you don't know
* High-throughput sequencing; requires technical knowledge
* With targeted sequencing is very sensitive

## Study setting
* Working in southern Africa
* Working with labs in Lusaka, Zambia and Johannesburg, SA
* Collecting sputum samples
* Hope to recruit 300 rifampicin resistant and 200 sensitive

## Assay
* Sequencing is a simple workflow
* DNA extraction done using Promega Maxwell
* 16-gene panel developed by ONT
* Rapid library preparation kit, real-time sequencing with MinION
* Analysis using EPI2ME labs (pretty reports), summary reports to clinicians
* Storing samples for off-site validation

## First insights
* Workflow is efficient and quick; results in 24 hours
* Recruitment speed in SA is a bit slower (2-4 samples per week); other 4-8 per week

## Comparison
* comparison to GeneXpert
* Most susceptible to rifampicin
* Resistance - two classified as multi-drug resistant, one only rifampicin
* Of samples already analysed, predicted susceptibilities and resistances all match

## Sites
* One already familiar with nanopore
* Other new, but 1 week training, now doing really well
* Most cost-effective batches of 22 samples, can do batches of smaller samples as well
* Seeing failed sequences, including human DNA
* Ongoing, we will see what we can get

## Future
* Updated 27-gene panel
* Library preparation even quicker
* Validation, will do off-site, validation and comparison with native vs decontaminated sputum samples
* Want to conduct a survey, see if willing or wanting to continue
* Look at treating clinicians to see how they like the approach

## Questions
* Multiplex PCR?
    * Yes, one tube
* Sequencing failures?
    * Based on report; we do want to look at it, but don't have anything properly set up to look at failures
    
# Infectious Diseases - Joep de Ligt
*The value of real-time‚ long-read sequencing for public health*

* Live and work from New Zealand
* Content warning: COVID
* Some of results are only possible with a comitted government

## What ESR uses genomics for
* Health, border protection, food health, forensics
* Focusing on public health

## COVID-19 pandemic in 5 acts
* Thankful to ARTIC
* When first case came across borders, could immediately sequence
* Had people at Massey University who created Midnight protocol (longer amplicons, cheaper)
* NZ challenge was to sequence every positive sample
* Nanopore hours, Illumina days

## Initial focus
* Every single case mattered
* Local discussions with public health - laundry room or elevator?
* Focused on communication of results (e.g. Microreact) with daily updates

## First approach
* Learning about how transmission was happening
* People with highly similar genomes, across from all over the planet
* Particular flight pattern
* Quite evident that cases transmitted on the airplane; not the first story, but important (Americans were saying plain transmision was impossible)
* Room / isolation facility
    * Genomics told a slightly different story, different cases, CCTV revealed footage of transmission across a hallway (aerosolised, corridors > 2m)
* We now think about airborne transmission of COVID
* Real kicker came when we were able to prevent a lockdown

## Lockdown prevention
* Process sample in seven hours (no longer world record)
* Could feed back that this was a contained cluster, public health measures could continue
* Easily offset any and all costs until that point

## Post-Vaccination
* Omicron came round, wanted to keep the finger on the pulse
* Could look at reinfections, could see what variants were inducing reinfections

## Difference in approach
* Until January 2022 could sequence all cases
* We now have capacity to sequence all at scale; now have 3 GridIONs (previously 3 MinIONs)
* Salmonella, came back as linked cases, risk factors of tahini, eating from a similar food box, recall, no subsequent cases
* Real time can work for bacterial and larger genomes

## NZ-specific bacteria
* Can introduce quite strong biases
* Long-read local references when putting into a pangenome (done with Neisseria meningitidis)
* Pangenome can improve mapping rates
* Graph of 131 genomes, final paper has pangenome of >1,000 genomes

## Questions
* GridION from Salmonella outbreak?
    * First quick analysis used GridION only, but SNP indications
    * Added on short read fragments for a hybrid approach
    * de-novo assembly

# Infectious Diseases - Panel Discussion / Questions

* That was Tiana's first ever conference presentation - a great job!
* Why do you have so few samples when incidence for TB is so high?
    * We don't really know; trying to find more; working at two clinics
    * For drug resistance, working options to try to find patients from other clinics
    * Consent is required
* Data sharing in public health - what is the best way to share data?
    * With more and more genomics in health care, there is a shift where there is controlled-access databases; still available for researchers to access; will probably see this happening in the infectious disease space.
    * Wondering about how to best approach this.
    * Best way is to use adaptive sequencing, so don't have data from the human genome.
    * Can also consider how to remove human (e.g. map reads to human genome and remove; can be an automatic process)
    * Some unmapped populations may appear in databases at higher frequencing
* Consent - even if not human DNA, there is an issue; not just about human sequences
    * Metagenomics is prone to incidental findings
    * Informed consent is important; local context needs to be taken into account
* Lukasz - is the panel publically available?
    * No. The panel is patent pending, not published
* Would it be possible to use Flongle instead of MinION?
    * Probably
    * Yeah, you could; MinION is recommended because it can be re-used
* Could you do 96 samples at a time on MinION?
    * Yes, you can
* Most approaches doing targeted sequencing; is shotgun sequencing on the radar?
    * Can do WGS from clinical samples, but it is trickier; very variable what you get for different samples. Even on GeneXpert is a high reliability, but still fails sometimes.
    * Choice to use targeted vs metagenomic is largely to do with cost
* Show of hands, are you using Nanopore in a clinical context? about 30%; good to see a lot of people on that road
* Could you elaborate on the Flongle issues?
    * Will be upgraded in the future; will be better
* Detecting alive vs dead bacteria from DNA modifications; can this be done?
    * Not planned yet; setting makes it difficult; diagnostic labs have their workflow
    * Targeted approach would lose a lot of that stuff
    * Depends when you get the patients; on treatment might be a clinical utility, but for diagnosis don't need to tell if live or dead; could target RNA instead of DNA
* Lukasz - what is your input volume?
    * 25µl, depends on isolation procedure
* What sort of samples do you do your tests on?
    * Mostly sputum; samples from upper respiratory tract

# Poster Summaries

1. Hagar Mor-Shaked et al. - ONT's straglr STR-identification software is able to type many different repeat expansion diseases.
2. ONT - ONT sequencing has been used for haplotype-resolved sequencing and variant typing.
3. Vahid Akbari et al. - prediction of Parent of Origin through Parent-of-Origin-aware genomic analysis is technically feasible in real world samples using T cells cultured from whole blood samples.
4. ONT - Multiplexed Adaptive Sampling  on the PromethION 2 Solo allows enrichment of pharmacogene targets.
5. Camille Mumm et al. - nanopore Cas9-targeted sequencing captures individual specific non-reference mobile element insertions, and can examine both global and locus-specific methylation patterns of both reference and non-reference repetitive elements across brain regions and samples.
6. Chris Kyriakidis et al. - discrimination of SMN1 and SMN2 based on 5 different nucleotides after reads are correctly mapped to the reference sequence.
7. ONT - nanopore sequencing with the bioinformatics pipeline wf-human-variation from EPI2ME Labs allows the detection and annotation of large repeat expansions in several genetic diseases.
8. Caroline Koch et al. - nanopore sequencing with barcoded nucleic acid probes, engineered to recognise a panel of biological targets (miRNAs, proteins, and small molecules).
9. Pedro Garrido-Rodríguez et al. - nanopore Long-read sequencing in RNA-Seq studies to define transcriptomic diversity of human liver, doubled diversity compared to conventional high-throughput sequencing methods.
10. ONT - native sequencing of individual telomeres on R10 nanopores using a bespoke 3’ adapter (CCCTAA; complementary to 3' telomere overhang).
11. Emilie Boye Lester et al. - long-read sequencing data enables fine mapping of breakpoints that are inaccessible by current methods, which enabled a more precise resolution of a broad spectrum of variants, including different complex chromosomal rearrangements.
12. Bradley Hall et al. - PCR enrichment, nanopore sequencing, and machine learning models accurately resolves multiple challenging variants across several classes for 11 of the most common gene targets associated with heritable disease.
13. ONT -
14. Koen Desarranno et al. -
15. Szi Kay Leung et al. -
16. ---
17. Marion Leblanc et al. -
18. Jihoon Yoon et al. -
19. Marinella Corbetta et al. -
20. Thomas Garcia et al. - 