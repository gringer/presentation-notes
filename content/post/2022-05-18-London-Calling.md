---
title: "London Calling 2022"
author: "David Eccles"
date: 2022-05-18
---

# Preparation: extracting high-quality DNA and RNA

*Vânia Costa, Applications Scientist, Oxford Nanopore Technologies*

* For cell lines, ONT recommends TRIzol extraction.
* For long-term storage, keep DNA and RNA at -20°C or -80°C
* Flick to mix DNA
* ONT usually uses Megaruptor 3 for shearing DNA (like a syringe; can
  change speed and alter fragment length)

## Q&A

* RNA storage
  * we would recommend to extract the RNA and store it at
    -80°C until ready to use
  * The best storage condition for extracted RNA is -80°C. A week at
    -20°C might not be too detrimental, but it is best to avoid
    storing it at -20°C for too long.
* DNAse treatment: we frequently treat our RNA extractions with DNAse
  and we have not observe any degradation


# Preparation: selecting the right library prep method for your experiment

*Tomasz Dobrzycki, Technical Applications Scientist, Oxford Nanopore Technologies*

* Nanopores embedded in an electrically-resistant membrane
* Passing DNA through nanopore creates specific peturbations into the current
* Ligation is optimised to produce the highest output
* Rapid has been further optimised recently for producing ultra-long reads
  * Ultra-long with rapid: 10-20 Gb yields on MinION flow cell, still
    greater than 50kb N50 libraries
* Direct RNA
  * Specific RT adapter is initiallly annealed to RNA
  * Reverse-transcription (optional) improves sequencing yield by about 30%
  * Adapter is slightly different, processes RNA at 100 bases per second
* cDNA sequencing
  * >20M cDNA reads from MinION, >160M cDNA reads from PromethION flow
    cells [with SQK-PCS111 kit]
  * Can use bespoke RT adapter to enrich for molecules of interest
* 16S barcoding kit
  * Quicker, more accurate than other 16S kits
* Adaptive sampling
  * Software-level
  * Hopefully future uses will enable [barcode?] balancing usage

# Choosing The Right Nanopore Sequencing Device For You

*Bryant Catano, Technical Applications Scientist, Oxford Nanopore Technologies*

* Flongle flow cells
  * Can use for pre-QC (load onto Flongle first, then other cells)
* MinION flow cells
  * 10-20 Gb ultra-long (>50kb) reads
* PromethION
  * 100-200 Gb
* Flongle / MinION
  * works in Mk1b / Mk1c / GridION
* MinION Mk1C
  * all necessary compute inside the device (+ touchscreen)
  * highly portable, field-ready
* GridION Mk1
  * higher-end GPU (V100)
  * Can run up to 5 individually-addressable flow cells (most flexibility possible)
* Flongle
  * TMO: 2.8 Gb [maximum output?]
* PromethION
  * P2: 2 individually-addressible; P24 / P48: 24/48 respectively
  * P48 up to 14 Tb per run
  * A100 acquisition unit
* Adaptive sampling
  * Coming soon to PromethION soon
* Genomic epidemiology
  * GridION being used to great use for large-scale SARS-CoV-2 viral genome sequencing

# Sequencing: how to load a Flongle Flow Cell

*Andrada Tomoni, Sequencing Development Scientist, Oxford Nanopore Technologies*

* Flongle - latest development in terms of sequencing; one-time use, cheap, disposable flow cell
  * Should expect 1-2 Gb data per run
* MinION case
  * Metal lip holds both adapter and flow cell
  * Need to be careful around posts, slots for adapter pins
* Flongle adapter
  * Contact pins where Flongle flow cell will slot in
  * Pins on the bottom - make sure they're not bent before inserting
  * ASIC chip should be completely covered by a heat pad
  * Insertion - rest it loosely under the lip, and let it guide you
* Flongle flow cell
  * Make sure no dirt or grease
  * Insert in the same way as the adapter; should get a satisfactory click
  * Do not disconnect it until the sequencing run is finished
* New Flongle design; cosmetic differences
  * Slightly bigger loading port area
  * Waste channels a bit clearer
* Use the lid; it ensures proper thermal control
* A little bit of liquid remaining on top of the Flongle flow cell
  array is fine; it will flow into the array eventually

## Q&A

* How do you deal with with bubbles underneath the loading port? What
  do you do if there is air in the loading port or pipette tip?
  * Hello, in order to avoid any air bubbles underneath the loading
    port, it is recommended to ensure that there is no air in the
    pipette tip when priming the flowcell.
* What do you do if the Flongle valve is blocked and resists
  pipetting?
  * Hello, can you please contact Technical Services via the
    conference platform? Thank you
* How do you deal with crystallised flow cell storage buffer in the
  Flongle loading port?
  * Hi, it shouldn't be a problem as long as is not blocking the
    loading port. Best course of action would be to clean with a lint
    free wipe.

# Analysis: basecalling your data and detecting methylation

* Basecalling
  * Raw read accuracy has been steadily increasing over the years
  * current Bonito CRF model
  * Using new training sets
  * Modification basecalling (e.g. 5mC) will include a supplementary
    table with modified base calling probabilities
  * Output as fastq-only is not recommended, as re-calling or
    alternative analysis is not possible
  * Fast5 files contain analyis information
  * Command-line tools offer more control over results
  * SUP model is 3x slower than HAC, which is 3x slower than Fast model
* Methylation
  * R9.4 better reproducibility and lower coverage needed in
  comparison to bisulphite
  * Remora with R10.4 is even better
  * Can call and phase long nanopore reads using methylation
  * methplotlib - visualisation of per-nucleotide methylation probability
  * Working with HTS, JBrowse & IGV to display methylation
* Native long read data enriches datasets, even at low coverage

# Welcome to London Calling 2022

*Gordon Sanghera*

* [... missed out the start]

* Translation of sequencing into community is a long process
  * Human, environmental, animal, plants
* Ceremonial gong to formally open LC 2022


*Emma White*

* Trying to integrate online and on-site experience; virtual audience
displayed in breakout rooms and auditorium

* Amazing lineup of speakers

# The potential of ultra-rapid nanopore genome sequencing for critical care medicine [[video](https://nanoporetech.com/resource-centre/video/lc22/the-potential-of-ultrarapid-nanopore-genome-sequencing-for-critical-care-medicine)]

*Euan Ashley, Stanford University, USA*

* Picture of Ferrari, used in place of Moore's Law graph
  * $0.01 if price had dropped as much as human genome sequencing had dropped
* Have focused on accuracy as well as cost, need great confidence in variants
* Sometimes speed is important as well
* Matthew (13 year old boy)
  * Dry cough, assumed he had COVID
  * Diagnosed with heart failure
  * Heart was barely managing to push out 10% of blood
  * In cardiogenic shock - heart not providing enough blood
  * Surgical implantation of an artificial heart
  * Possible myocarditis (reversible), but also possible a genetic cause
  * Normally would do a biopsy of the heart; they did that (it didn't
  show lymphocytes that were expected for myocarditis)
* 10 years ago, considered rapid sequencing process: 50 hrs (42 hours with process improvement); pushed down to 26 hours (GWR); down to 19.5 hours.
  * In 2021, 6-8 weeks is about the amount of time a genome assembly
  normally takes. Patients like Matthew don't have even 5 days.
  * Idea pitched to John Gorzynski - "Would it be possible to do it faster?"
  * First call to ONT; really only 1 machine that could do it; P48 1X
  genome in 2 minutes
  * Considered how to do it faster; maximising speed and quality, spreading 8 library preps over the whole PromethION
  * Compute was a problem
  * Process
    * Wet lab 4.2h
    * Sequencing 2.7h
    * Dry lab 3.6h
    * Sampling 62 mins
    * fragmentation 19 mins
    * Sequencing 1.26 Gb/min
    * Compute power at the time PromethION received, expected 80 more hours
    * Used a cloud method to optimise
    * Basecalling with Guppy GPU
    * minimap2 alignment
    * Reducing file size every few minutes
    * GPU 4x Tesla V100 per instance; 48 CPUs per instance
    * Moved from compute overhead of 16 hours to overhead of 30 mins
    * Alignment was 3.6h
    * Found very quickly a heterozygous duplication that indicated
      genetic abnormality in TNT gene; had shown it could not be
      myocarditis
    * Total processing time 11.2 hours
* Excited to bring team together, and apply this technology
  * Highly variable time on the curation side, depending how long to get variants
* Other optimising
  * Pore occupancy was better without barcoding
  * no change in accuracy (small amount of carryover, but still wouldn't affect numbers)
  * Removing barcoding step reduced genome to all under 12h
* Patients presenting with neurodevelopmental disorders
* Head of facility would like to sequence 2/3 of patients; direct benefit to patients
  * costs about $10,000 per day for ICU; test can save the healthcare system money
* Having beaten the GWR, were happy to be awarded a new GWR
* We need to rethink how we're doing genomics if shifting from weeks to minutes and hours
* If made Ferrari as much faster as human genome speed, would change from 199 miles/h to 1/20th speed of light

## Q&A

*Question hosted by Dan Garalde, who took the first conference question*

* What does it take to scale for hospitals around the world?
  * Tech is much more scalable than it initially appears
  * Will soon be limited by the difference between the patient and the sequencing machine (e.g. sequencing in hospital vs flying sample to a sequencing centre)
  * Everything shipped up to the cloud; anyone who wants to set this up could do it almost anywhere
  * Last mile (telemedicine) could also be done remotely
* How would you convince insurers and health care providers to take this on?
  * They are already convinced
  * Moves at California and federal level, and around the world, to specifically fund neonatal and intensive care genome sequencing
  * Cost saving; a few thousand dollars now, will drop further, reducing length of ICU stays
* Are there certain genetic conditions that make more sense for this testing?
  * Asked physicians first - any patients for who a genetic diagnosis would help
  * A lot of neurological conditions (42% diagnostic rate in pilot; higher after implementation)
  * Multi-system disorders
  * 2/3 of patients could benefit from this technology


# Identification of Novel genomic structures and regulation patterns at HPV integration events in cervical cancer

*Vanessa Porter, PhD Candidate, University of British Columbia*

* HPV necessary, but not sufficient for development of cervical cancer
* HPV infects basal layer through microabrasions in the cervix
* Cancer follows from integration of HPV into genome (70% of cancers), often E2 is disrupted or removed, can lead to increased expression of oncogenes
* Rearrangements can be simple, or more complicated
* In cancer, HPV integrates near oncogenes [image: circular plot, showing bars that stack into the centre]
* Project objective - investigate integration of HPV
  * Looking at genomes of 50+ Cervical cancer samples
  * Identify integrations
  * Assess methylations
* HIV Tumor Molecular Characterization Project
  * Sequenced 44
  * Performed Nanopore analysis pipeline: basecalling, alignment, SNV, methylation, phasing; added on other custom workflow
* Almost all samples; HPV integrates between regions where at least two double-stranded breaks have occurred - an integration event
* Used hybrid HPV/human genome, allowed to identify integration events as translocations
* Long reads can pair together breakpoints on the same read; can link chromosomes
* Sample reads subsetted to create assemblies; categorised integration events into an integration type
* 3 major resolutions
  1. Deletion of region
  2. Duplication of region, get alternating human/HPV structure
  3. eccDNA (extrachromosomal circular DNA); extra region excised as circular DNA, never see reads from circular region. Distance between breakpoints consistently longer, about 70kb (may need this to be stable in the cell)
* 1. eccDNA breakpoints can be simple (one region), or complex (multiple breakpoints)
* Content of inserted HPV sequence can be variable, all contain E6 and E7 (oncohenes), so probably the minimally-required genes
* Ran PCR to confirm circular contigs
* 2. Breakpoints on different chromosomes (chr4, chr12)
  * Copy number loss at the breakpoints
  * Proposed mechanism is an unbalanced translocation event
* 3. Multi-breakpoint integration events
  * Presents of multi-breakpoints leads to instability
* 4. Repeat region integration
  * Integration associates with an expansion of repeats
* Some samples have multiple integration events
  * HPV16 was more integrated than others
* Some integration event categories are more common
  * most common: multi-breakpoint
* Presence of categories can change depending on HPV type
* Using methylation for looking at haplotypes; found clear demethylation upstream of an integration region (100kb, up to 400kb away)
* Looked at HPV genome at sites of integration
  * Compared with non-integrated samples
  * Some samples have different methylation at different regions
  * Integrated HPV is hypermethylated vs unintegrated HPV

## Q&A

* How do you assess false positive and false negative results? Phenotype data for correlating events?
  * A spike-in chromosome to detect false-positive chimeric reads, high redundancy required (at least 5 reads); probably missing a lot of sub-clonal integrations
  * Survival data; found HPV clades associated with survival; are limited by sample size
* How are complex integrations detected?
  * Deletions / translocations can't be easily detected; had to get creative
  * Have been packaging up the process
  * Sniffles for HPV breakpoint detection

# Spotlight Session

*Chaired by Thomas Bray, Commercial Director Greater China*

* 2-minute pitch, followed by vote to see winning talk, presented in the auditorium
* Theme: how sequencing is changing your area of research

## The bacterial blackmarket: utilising sequencing to study the inter-cellular
*Richard Goodman*

* Star wars scroller - The bacterial black market
* Bacteria outwitted
* Mobile colistin resistance gene emerges
* Nanopore sequencing is helping in the fight against the AMR wars
* Bacteria evolve resistance, share through a mechanism analogous to a black market
* Used a decoy sequence to evaluate movement of sequences through the bacterial black market

## Whole mitogenome variation in domestic cats discovered via nanopore sequencing

*Emily Patterson*

* July 2012 - dismembered torso of a man; forensic scientists didn't
  find human DNA, but found cat hair; prime suspect had a pet cat
* Nanopore sequencing can be used for looking at cat DNA

## Unloicking the transcriptomic architecture of bacterial viruses with ONT-cappable-seq
*Leena Putzeys*

* Viruses - bad diseases & global pandemics
* But... not all virus are bad; some can cure disease
* Bacteriophages, can kill bacteria; have biotechnological potential
* Want to take advantage of nanopore sequencing platform to read out
  viral sequences

[sleep]

# Update from Oxford Nanopore Technologies
*Introduced by Rachel Rubinstein, Technical Product Manager, Software*

## Novel/interesting statements selected by David Eccles

* Smaller new ASIC; 5mm x 6mm, 400 channels, 1 MUX, expecting around 9
  Gb from a thumb-sized device, expecting 10 Gb output with deeper wells
* Adaptable sequencing from Kit 14: 400 bases/s at 99.2% modal
  accuracy (default), 260 bases/s at 99.6%, 520 bases/s at 99.0%
* Longest perfect accuracy read 72 kb
* New base caller - Dorado - coming out this year: open source, user
  extensible, apple + NVIDIA
* PCR tiling kit for VolTRAX (4 samples), 30 mins HOT, sample to
  sequencing for about $100
* RAD chemistry will be fully released Q4 2022; looking great,
  reducing input to about 100ng
* iPad case MinION will replace Mk1B
* Clive Brown's cell-free DNA data is now available

## Clive Brown

* Only 300 people at London, over 1000 people online watching, last
  done in 2019; too much stuff, got department heads to do most of it
* Clive will do a bit at the end; things I think are interesting
* Guys here do pertinent updates (latest changes), a lot of meaty detail not seen before
* Clive gave an update about a month ago, a lot of progress since then
* Online a long line of previous talks
  * Our new ability to measure modified bases
  * Relationship between speed of sequencing and throughput (explained
    by Lakmal, interesting progress; what Lakmal tells you is half
    what it needs to be)
* First James Clarke (chips)
* Each person 15 minutes, 35 slides, so might skip past a few slides for my time

## James Clarke

* The Platform - all the things you require to do nanopore sensing - the base:
  * Nanopore sensing
  * Nanopore sequencing
  * Applications
  * Your research
* 3 Different types of flow cell: MinION, PromethION, Flongle
* Devices: Mk1C, Mk1B, GridION, PromethION P24/P48/P2
* Got here by continuous improvement and agile development
* Some people want a firmer foundation - Q Line
* Clive gave a talk in March, a move to R10 series
  * Ability to go as fast as R9 series (this was holding R10 back a bit)
  * Simpler product offering
* R10 output, different ways to run
  * Default 400 bases/s, similar to R9 speed, 4 Versions of output
    1. PCR / E. coli (236 Gb)
	2. Native human, no interaction 10kb, 166 Gb
	3. 30kb, 155 Gb, with wash kit interactions
	4. 80kb N50, 120 Gb
  * Matt Loose got 170 Gb from his run
* Deeper wells on PromethION are helping (from Marathon Flow cell)
  * Like a battery: the more of it you have, more runtime, more run consistency
* PromethION is the best flow cell: most recent ASIC (best accuracy); more pores
* P2 can run 2 PromethION flow cells
  * John Tyson has the device
  * Out in the live lounge in London
* Lots of unfinished R&D business
  * Would like to change flow cell design
  * Larger flow cells could go to 4-chamber loading (on PromethION)
  * 600 microns of fluid above the nanopore, a bit of a hangover from early-access stage
    * Team will completely refactor this
  * Want to integrate library prem
  * Chemistry changes (e.g. silver chloride)
    * Can get accuracy over Q20 with silver
	* should go to Flongle first
* Manufacturing
  * Nanopore sequencing getting more popular; want quality to remain the same
  * Economies of scale - quality, output
  * Adding new features via a method of continuous improvement
    * Monitoring; characterising problems; making sure you capture that problem
* Where are we going?
  * Products enabled by ASIC; now have SmidgION ASIC
    * Consumes a lot of power; redesigned with low-end output, but as good as it can be from bottom up
	* Proof of concept - working properly; don't want to be eventually in Flongle format
	* Got enough data to basecall, looks pretty nice, enough to feel confident to moving to next step
    * 3 components for new flow cell; want to keep path to ASIC short, won't add much more to cost
	* Have a PCB which will be form factor of chips (about thumb size)
	* **5mm x 6mm, 400 channels, 1 MUX, expecting around 9 Gb, expecting 10 Gb output with deeper wells**

## Lakmal Jayasinghe

* Need to do many different things
  * Need to improve speed through pore
  * Need to get maximum number of pores
* Last year: R9 + Kit 10
  * Raw read accuracy about 98.3
* Introduced new nanopore, R10.4; new motor E8.1; new kit Kit 14
  * Raw read accuracies beyond 99% for first time
  * Speed initially 250 bases/second
  * Relationship between speed and accuracy is complex; important to improve speed and accuracy together (a hard job)
  * Want to keep accuracy about 99%
  * Teams screened different nanopores, 8000 nanopores, 300 (3000?) motor proteins
  * Were also changing run conditions: temperature, buffer conditions
  * At the end of the speed spectrum, accuracy was lower
    * Picked particular enzyme and nanopore wth 400 bases/s, changed models to push accuracy above Q20
	  * new pore 10.4.1
	  * Speed improved, better speed distribution
	* E8.2 accuracy has improved, better defined errors and fewer mis-steps
  * **Other nice thing about new chemistry, can tune using software by adjusting temperature**
    * 400 bases/s, 99.2% accuracy
	* lower temperature to 260 bases/s, 99.6% accuracy
	* higher temperature 520 bases/s, more output, 99.0%
    * Duplex mode Q30 accuracies
	* Released to new developers, most have pleasing experience
* Duplex chemistry
  * Working on very hard to implement
  * Capture second strand as well, 5% of normal experiments complement strand follows template strand
    * Accuracy increases
	* Needs perfect sample prep
	* Needs ends adapted
	* Needs no nicks in DNA
  * By June last year, rate improved to 20%
  * Now have 40%; target is 70%
  * Duplex process can be run on both R9 and R10 pores
  * Trying to make sure that kit14 is built around duplex
  * Q31 achieved with current duplex caller, going to improve much further, will be available soon
* Improving chemistry / accuracy further
  * R10 distance about 5nt between read head
  * New pore (Rx) has about 9nt between read heads, talked about at NCM last year
    * About 98% accuracy
  * Random substitutions to sample prep using PCR methods; breaks homogeneity of homopolymer regions
    * Homopolymer length accuracy improves
	* Consensus accuracy also improves
* Half-way through? Don't know what we'll do when we reach 100%

## Stuart Reid

* Data, where we are, where you can expect to be
* Changing temperature changes other things
  * e.g. current increases
  * We are not at the optimum condition
* A lot of headroom for ASICs
* Making changes deep in the software
* Have architecture, training
* Several different sorts of accuracy
  * Raw read accuracy; interested in single molecules, population diversity
  * Duplex, can call 2 reads
  * More than 2, consensus, the average of a population
  * The few errors that remain in raw reads are mostly averaged out in consensus
  * Have SNPs, structural variations, base modifications
* A few talks today showing really high test accuracies
* Where are we now?
  * Raw read, focusing on 400 bases per second
  * 230 Gb from PromethION flow cell, >Q20
  * Run condition choices - accuracy at 260 bases/s; output at 520 bases/s
    * Output 307 Gb
	* Accuracy 99 Gb (Q31 modal)
	* Longest Q30 duplex read 260 kb
	* Longest Q40 144 kb
	* **Longest perfect read 72 kb**
	* Probably quite a lot of headroom in the algorithms
* Accuracy progression
  * Duplex reads have very few errors
* Consensus / 400 bp
  * Complete, contiguous, accurate genomes with nanopore data alone
  * Recommend nanopore-specific tools
  * Optionally polishing with medaka; Flye still showing Q50 bacterial assemblies without Medaka
* Variants - Like Clair3 to call; LRA for variants
  * SNPs 99.9% accuracy
  * SVs 95% (hard for ground truth)
* Base modifications
  * All the base modifications are built into the nanopore signal
  * With Remora, built into MinKNOW, coming out in next release; visualisations just work, can see in popular genome browsers
* SV and methylation ground truth is hard to come by
  * Claim that nanopore is the most accurate technology for detection
* Highly contiguous, accurate human assemblies
* Duplex Q30 (99.92%)
* Flow cell record now at 307 Gb
* Read length remains at 4.2 Mb
* Software features
  * Short-fragment mode now built into the software; nanopore has read any length of DNA; lower length now configurable (down to 20 bases)
  * Accuracy is not a function of read length; applies to short reads to
* Adaptive Sampling
  * If you don't like what you're sequencing, can kick it out
  * 5-10 fold improvement
  * Now enabled on PromethION (3X improvement now, beta release coming out)
* Don't panic; most won't need to do anything
* New file format - pod5: pore open data
  * Thanks to slow5 format, working around many downstream analysis problems
  * Had to focus on the right performance; 48 flow cells on PromethION
  * 4-fold improvement in write performance, 10-fold improvement in read performance
  * available on github pod5-file-format
* **New base caller - Dorado - coming out this year**
  * Want rapid reach
  * Open-source and user extensible
  * Will support apple as well as NVIDIA
  * Should be live on github

## Rosemary Sinclair Dokos

* We have been building together a platform that can perform the full biology for an experiment
  * Any read length from 20bp up to millions of bases
  * Scalable, deliver data in real time
  * Want to scale platform to fit your biology
  * Flongle, MinION all the way up to P48
  * Made accessible through starter packs
  * Partnership with (Evantor?)
* Keeping platform open
  * Key features open to tweak: number of nanopores, run time
  * Ultra-long reads, multiplex
  * Aiming to generate data at really competitive prices
* Seamless upgrade paths
  * Most of platform upgrades are contained in flow cell, sequencing kits, software; have done 221 updates in last 3 years
* Latest release, have gone from research to customer in about 3 months
  * Rapid training, Cambridge compute has halved time from R&D
* Like to put proof-of-concept out quickly
* Miten: from flow cells to talk in 12 days
* Some people want more stability
  1. Developer
  2. Early access
  3. Released
  4. Fully-released
  5. Q-line
  6. Diagnostics
* Extraction - over 72 protocols; also extraction central on community
* Long-read sample preparation
  * Will be releasing short-fragment elimination kit
  * Also adding new ultra-long kit, as many extraction methods as possible
* cDNA sequencing (see talk by Dan Turner)
  * Barcoding kit (PCS111) now shipping
  * Working on size selection for isoform detection
* Single cell protocol
  * cDNA lab protocol simplified
  * Teams have released sockeye - nanopore-only analysis pipeline
* Direct RNA; a lot of questions
  * Need so much input material
  * Teams have reduced input requirements from 500ng to 50ng
* COVID-19
  * Continue to support community with ARTIC & Midnight kit, sequenced over 1M genomes together
  * Faces from LC2015, has been magical to see faces over zoom over last 2.5 years
  * As rolled out globally, a light-bulb moment: nanopore sequencing is good for pathogen detection
  * Teams actively working with community to provide a sample-to-answer workflow (like ARTIC and Midnight solutions)
  * Diagnostics team working on TB and respiratory metagenomics
* Voltrax carried on in background
  * **PCR tiling kit (4 samples), 30 mins HOT, sample to sequencing for about $100**
  * Now working on next product pipelines
    * Whole genome PCR
	* Ligation sequencing
  * Want to get VolTRAX into flow cell loading zone
* Kit 14
  * The best of all worlds - output from R9 + accuracy from Kit 12
  * Key thing: ligation kit, with developers today
    * Native barcoding kits very close - 24 / 96
	* New kits contain all sequencing components
	* Getting through 384 barcodes, in developer's hands (interested people can talk to us)
	  * Allows genome sequencing at $4 per sample
  * Rapid-based preps
    * **RAD chemistry looking great, reducing input to about 100ng**
	* Preparing for launch Q3 2022
	* An enormous amount to juggle
* Currently in development for Kit 14 for Ligation
  * R10.4 and Kit 12 being taken off the store Q4 2022
* Laser focused getting R10.4 and Kit14 into release phase
* Sequencing
  * Raising the bar on outputs
    * Flongle could be doing 3.4 Gb
	* 60 Gb on MinION
  * From Clive - needs to be doubled
  * They're not short or long reads, they're just reads
  * File formats
  * Raising the bar on accuracy
* All features making it through to smaller and portable devices
  * P2 enables people to start exploring PromethION-style experiments
  * Human WGS for under $1000 [assumes purchase of 96 flow cells, and one sample per run]
* Have insitutions with capacity for over 2000 genomes per week
* Sample to answer at under 8 hours
* Human at depth - dark regions and methylome
* Analysis
  1. Github research releases
  2. Tutorials - set of packaged tools
  3. Epi2ME lab workflows
  4. Epi2ME
* Simple interface to Epi2ME, no command line
* Anyone interested in replicating can look online
* Building a full picture

## Clive Brown - One Little Thing

* A complicated story
* MinION Mk1D - We are making this thing, an iPad case, wrapping a MinION/Flongle
  * Get 5G, cloud streaming
  * Low power, chance of running for 6h is quite high
  * Can register now
  * **Will replace Mk1b**
  * Module at the side to embed miniature VolTRAX
* Accuracy - nanopore basecalls have high confidence; errors tend to be confined to homopolymers
* Ability to call modifications; almost impossible not to in nanopore; a single atom change
  * Currently blank-over modified bases
  * A huge amount of [research] calling modified bases by default
  * Problem is ground truth; empirically-testable with synthetic sequences
* Basecaller, now synthesising DNA
  * Nanopore has a program to make a synthesiser
  * Can now make a synthetic set of random bases with 15th base mC, often sequences you wouldn't see in nature
  * Random training sets now outperforming bisulphite training sets
  * Proof of concept looks pretty good
* First data in 2012, nobody remembers, based on outy method
  * We run inny now: motor on front of the molecule acts as a break
* Can suck in DNA, motor drags DNA out of pore
  * Has some interesting features
  * Looking at scheme for re-reading molecule, more than one scan on a given molecule
  * Average single-molecule accuracy pretty much where we were on inny 18 months ago, no reason why it can't be improved further
  * Average overall accuracy is moving quite quickly
  * Per-base calibrated confidence scores can be moved quite high
  * Dwell-time distribution of steps is different, so should be okay for resampling
  * Asked to sequence lambda without amplification; they took cell-free DNA and measured a mutation at high confidence with a background - "Needle in a haystack"
  * Results are so good that they believe they can see PCR artefacts
* I get bled every so often, the substrate for nanopore research
  * Extract DNA, fractionated
  * Bottom red wine fraction; top white wine fraction - an old haematologist joke
* Sequenced my DNA, can call methylation patterns, can be correlated with tissue of origin, can sequence modified cell-free DNA
  * See patterns that match expectations
* With nanopore, can see longer cell-free fragments
  * Most platforms have a cap
  * Is the longer stuff newer?
* A whole number of applications for short fragments
  * Looks quantitative, can call to same accuracy, can call modifications
* **Cell-free run available**; ultra-long Kit14 version of me will be available as well, can play with those
* Would like to add a duplex version of me
* Will ultra-long cell-free catch on?
* David Cameron attended a flow cell loading class - everyone jams pipette in the hole
* New loading assistance
  * Have a little insert that fits into the loading hole
  * can squirt sample into the pan, or tip from Eppendorf, can put sample directly in
  * Quite sophisticated
* Interested in liquid biopsy, sample prep on the flow cell, everything occurs on the flow cell
  * Top chamber for swab with buffer
  * Barrier with a number of things including gel
  * Electrophoretically move DNA from swab to input chamber
  * Motors that will grab DNA, flow cell comes with chemistry
  * Outy sequencing system, from end-to-end swab or spit of blood
  * Proof-of-concept
  * Button on the flow cell can be pushed to flush the flow cell
  * All geared up towards moving the device out of the lab
* Vision of field sequencing
  * Think of ideal device, make a chemistry that works with the device
* Cell-free DNA, short fragments, sequencing multiply, brought
  together into a decentralised liquid biopsy, all in one device over
  the next year or two
* Early blood glucose, gave 20mL blood to get blood glucose measurement, moved onto strip tests
* Can now put device on your arm with needle that measures glucose
* Toothbrush - the ideal sampling device; twice a day, looking at 10-20 ml blood over a month
  * If see a molecule that looks suspicious, sequence it again, and again
  * Clive's fantasy product - trace over time; with infection / cold, accumulate enough to do something about it
* Protein sequencing
  * Very active development
  * Not the only chemistry we're looking at, but this is the one I'm talking about today
  * Can put a molecule through a nanopore as long as it's charged
  * Existing technology based on mass spectrometry, results difficult to interpret, quite posterior loaded
  * Think that same platform for DNA and RNA can be used for protein
  * Double-stranded DNA/RNA
    * use DNA motor for moving protein - conjugate to DNA
  * Generate signal; see DNA signal, then peptide signal
  * A lot of work on DNA-peptide concatamers
  * Took a flat/bland peptide backbone, added amino acids; trying to get reproducible peptide signal
  * Even a not very good sequence
  * In some ways the bar is lower than DNA sequencing
  * More efficient to look at concatamers
  * Starting to generate unique profiles, very reproducible, remarkable how tight the pair profiles are
  * Where does it lead to?
    * The value is in the hard bits
	* Trypsin digest
	* Make a conjugate
	* Sequence
	* Method can deal with partial digest - long peptides and overlapping peptides
  * very difficult, don't want to over-sell
  * Want any lab biologist to be able to run protein sequencing on the same hardware, no special box needed
  * Have a good concept for unique reproducible ...
  * We think we're on the first rung of the ladder
  * Outy based, can re-read peptide, can pull out of the pore, drop it back, pull it out again, then spit it out when done with it
* Summary
  * Trying to develop methods to look at single molecules from blood / water

## Q&A

* How does R10 perform on direct RNA?
  * Very enthusiastic, but small following, but scientifically a growth area
  * About to embark on refactoring to be compatible with 10.4; may become outy-based method
  * Expect a new version, may incorporate re-reading

* Does outy sequencing method have impact on read length?
  * Primarily focused on 3-5kb right now

* Duplex reads are awesome, but efficiency is low; what's up?
  * Seeing now upgrades to duplex, will get higher and higher yields
  * 5% -> 20% -> 40% -> 70%

* Peptide sequencing, what are the longest, does DNA motor slip?
  * 8-12 peptides, can increase using a longer pore
  * Some motors can move over the peptide, still active development
  * Longer is better

# Welcome Back To London Calling 2022
*Zoe McDougall, Oxford Nanopore Technologies*

* Final day of London calling talks
* 5,000 people registered to attend online
* Track from Bowie's best album, changes
* Conversation from Sarah (10x genomics)
* Growing, but want to retain foundational, deep values
* Community has developed new tools, provided feedback to improve
  whole user experience
* Anything, anyone, anywhere means enabling more scientists
* Goal is to grow without changing anything that we're doing in the community
* Remarkable work to drive single molecule raw read accuracy 99.6% simplex, 99.92% duplex
* What could you do with 72kb perfect reads?
* 6,000 Trillion bases sequenced on nanopore devices [in last year?]
* Surge of sequencing and publications from COVID
* Meaningful increase in cancer and metagenomics
* More than 3,000 publications; grass-roots movement
* Manufacturing building is built in the shape of a MinION
* New manufacturing centres 2022
* Particularly proud of new dots in Africa and South America
* How do we grow?
  * We think we need to listen; everyone wants to hear from you
  * Ask any nanopore person, they will find the right person to speak to you
* Platform that is so versatile that it can be used for all your needs

# Understanding cancer epigenetics, immunogenetics, and energetics
*Michael Dean, National Cancer Institute, USA*

[Pink Floyd - Another brick in the wall]

* I despise cancer; took the life of relatives; including wife's
  mother - the grandmother of my children
* Most of career on an army base - resources given to war on cancer
  * War is the wrong analogy
  * Current analogy - cancer moonshot
  * We knew where the moon was; it was a known problem
  * Cancer is not a known problem; it's a puzzle that is needed to be solved before finding a solution
  * Hundreds of puzzles, each need to be solved
* Since 1990, mortality of breast cancer cut in half, mostly from improved therapy
* Figured out nearly all causes of cancer
  * Chimney sweeps were exposed to chemicals
  * Genes
  * What do they all have in common?
    * NBCCS - part of international team that cloned a gene; chr9;
      human homolog involved in embryonic development (abnormal wings)
	* Found many genes critical for embryonic development
	* Any time a cell divides, it makes about 100 mistakes; stem cells accumulate mistakes
	* But that was the easy part
* Lab focused on cervical cancer; caused almost exclusively by HPV
  * Mortality climbs exponentially as income goes down
  * Studied in guatemala; tumour and blood samples collected
  * HPV is ds virus; often integrates into genome; have used MinION to study cancer
  * Last year, showed ultra-long concatamers of HPV sequence
  * Order arranged differently at different sites - HPV superspreading
  * Found category of HPV that was only extrasomal
  * Found other integrations with rearranged multimers
  * NCI did in-situ hybridisation; largest fragments of HPV ever seen
* "Nothing in life is to be feared, everything is to be understood" - Marie Curie
* Virtually all cancer patients die of metastatic disease, or therapy-resistant tumours
* New class of genes mutated in cancer
  * Modify histones, regulate splicing, regulate methylation
  * Theory is that cells with same DNA change into different cell types by regulating the epigenome
  * Parallel that onto cancer cell - ability to unlock all pathways of survival
    * Angiogenesis
  * All our cells are resistant to our own immune system; cancer cells are doing the same thing
* Did direct cDNA sequencing on MinION
  * Unprecedented detail on HPV
  * Only 1 of 6/800 copies was expressed, makes up majority of transcripts making E7 oncogene
  * Other of 1000 has 2 copies making oncogenes
* HPV can integrate inside genes (e.g. FOXE1)
  * Gene highly expressed near integration site
  * Can see that FOXE1 is overexpressed, mostly not full-length transcripts; alternatively spliced, region downstream that is normally not expressed
* Looked at how integration is affected by methylation
  * Can see every CpG site along virus
    * Caski cells have more methylation
	* FoxE1 has demethylation
* Direct RNA sequencing
  * RNA used to make DNA copy, just to stabilise molecule
  * Can determine size of polyA tail
  * Applied to HeLa cells (has NYT bestseller)
    * Soft-clipped fragments aligning to chr8, MYC oncogene overexpressed
* Would like to translate knowledge into the clinic
  * PI3K signaling pathway is frequently affected
  * PD-L1 inhibited by PI3K drug, turning off immunosuppressive signals, as well as reducing proliferation
  * Treated cells have RNA expression of HPV reduced
* Epigenome is not a linear thing, 3D interactions that we need to understand
  * Can use Pore-C to study
  * Can see that most of HPV-human interactions are happening in enhancer region
* Cancer cell energetics is dramatically altered
  * Warburg - switch from glucose metabolism to lactate
  * Full-length reads across whole mt genome
  * In general little methylation of mtDNA
  * Can count up transcripts from direct RNA
* See Rossi et al. BioRxiv 2021.10.22.465367v1
* Can we develop an inexpensive HPV point of care device?
  * Some conversations already

## Q&A by Lauren Sunda

* What are the pros and cons of cDNA vs direct RNA?
  * Results from both are highly comparable
  * More output from cDNA
  * Still exploring RNA modifications, don't know what we're going to find

* A lot of countries don't motivate women to get test before age 30;
  how do we deal with that information, it might create more fear?
  * Education is the key
  * A self-sampling model using a swab; hopefully will be able to apply that better
  * Women need to get daughters vaccinated

* What is the mechanism of hypomethylation around integration sites?
  * Is demethylation before or after integration?
  * Possibly enhancer brought in by HPV that leads to demethylation

* Stage experience - Wow.

* Third trip to London

* Be bold, curious to follow your instincts

* Working in Guatemala was challenging

# Spotlight Session; runners up will be presenting talks in mini theatre

* STEM students joining this session; part of full-day STEM event to
  inspire next generation of scientists

# Whole mitogenome variation in domestic cats discovered via nanopore sequencing [[video](https://nanoporetech.com/resource-centre/video/lc22/whole-mitogenome-variation-in-domestic-cats-discovered-via-nanopore-sequencing)]
*Emily Patterson, University of Leicester*

* Cat hair can be used as a forensic tool; used in July 2012, found
  wrapped in shower curtain; 8 cat hairs found, police wanted to know
  if they could be linked to the suspect
* Domestic cats are a very popular household pet
* Frequently found at crime scenes, but limited visual benefit
* Have to depend on mitochondrial DNA - higher copy number, greater stability
* Traditionally sequence 402bp sequence in control region, has repetitive sequences either size
  * Cats have quite a large NmMT
  * Once sequenced, assess profile in a population database
  * 12 universal mitotypes worldwide, 60% cats have 1 of 4 common mitotypes
  * Cat population database from 102 cats at Uni of Leicester
  * Cat from shower curtain matched rare profile, matched suspect's cats
* Evidence of increased discrimination by sequencing different mt genome regions
  * Wanted to build a new database from 93 cat blood samples, 2x
    overlapping amplicons avoiding NuMT
  * Also sequenced using Illumina sequencing, used variation
    identified to design primers for whole mitogenome
  * Used native barcoding, sequenced 8 cats per flow cell
  * Good agreement between Illumina and Nanopore; 10 INDELs that were excluded
    * 422 polymorphic sites outside repetitive regions
  * Aligned cats to sanger Sequence reference sequence, quite a long branch with 42 sites unique to reference
  * 3x unique profiles from control region, now have 60 unique
    mitotypes, reduced largest group from 20 cats down to 4
* Wanted to apply this to cat hairs, needed shorter amplicons
  * Average length 360bp amplicons (60 different amplicons); sequenced
    matched blood and hair samples
  * Successfully recovered complete mitogenome from blood and hair
  * Variants called in blood have NuMT prominent, found in several cat species
* Applied to missing cat case; tried to match jawbone to missing cat
  * Complete mitogenome sequence, found a match
* Allowed to distinguish 74 haplotypes, reduced frequency of largest
  haplotype from 26% to 4%

## Q&A

* Hardly any variation in 12S gene; any relevance?
  * domestic; quite recent; don't know why 12S

* How did you isolate DNA from cat hairs?
  * Qiagen mini kit; used Qiagen protocol for blood

* How many cat hairs do you need to isolate DNA?
  * We used around 10

* Stage experience
  * Quite intimidating; everyone is being really helpful

* What motivates you?
  * Very direct application, can lose sight of things

* No cats at home, but no parents

* Obstacle in research - COVID, out of lab for 6 months, had to adapt project; started out as a side project

# Animal conservation in the Era of genomics

## Genomic tools for gorilla population dynamics and conservation
*Etore Fedele, University of Leicester, UK*

* We are living through one of the worst extinction events in the
  history of the planet; gorillas are no exception
  * Poaching, habitat loss, diseases, political issues, bushmeat
  * Need to come up with an effective way to monitor changes, identify
    subspecies
  * Want to estimate population size
* Advent of HTS have allowed access to whole range of genetic markers
  for identitification
  * Single base changes, require only short fragments to be analysed
  * Want to reduce risk of spreading infectious diseases
  * Issue: very high startup fees for laboratory equipment; samples
    have to be shipped from Africa to sequencing sites; international
    regulation to limit transport of biological samples
  * MiniPCR + bento allow library preparation in the field
  * MinION enables researchers to conduct sequencing in remote areas
* Workflow
  * Utilised public data for constant SNPs + heterozygous SNPs
  * Designed primers
  * Assessed performance by matched blood and faecal samples
  * Used both MinION and Flongle side-by-size
  * Guppy -> Minimap2 -> BCFTools
Results from constant population SNPs + 28 individual SNPs
* Fragment length - really short bases
* Three patterns - western gorillas + 2 others
* Individual identification SNPs, each row different from the others

## Flongle sequencing reveals cryptic amphibian diversity and builds local research capacity in the Ecuadorian Amazon
*Zane Libke*

* 8 years old when caught first snake, filled with fascination about secrets, this fascination led me into the jungles of Ecuador
  * Originally tried looking at snake diversity, changed to frogs, found heaps of biodiversity
  * Current counts 140 species of mammals, 105 reptiles, 700 birds,
    149+ species amphibians, many more
  * Area is not a typical Amazon area; incredibly mountainous
  * Amazing network of researchers and conservation activists
* Read paper returning from first Ecuador trip
  * 1 table has all equipment we use - MiniPCR + MinION
  * Lab is product of the pandemic - what better to do than construct a lab
  * 1 solar panel powers all research; lots of didactic material
* Protocol adapted and simplified as much as possible, making the best out of every penny
  * Extraction for 7 cents
  * Barcoding from as many species as possible
* Expedition to 2 different mountains
  * River separating, found 2 different but similar frogs; both didn't
    have a BLAST hit (likely represent undiscovered species); didn't
    look like each other
  * Information passed onto collaborators
  * This kind of species resolution can inform conservation decisions
  * Diversity is unprecedented
    * Out of 44 different morphospecies, found 18 candidate species
	* How could we know how to best conserve them?
* Nanopore is a diverse device, allows many things
  * Have so far only looked at species we know
  * A need to create more labs like we're doing
  * Started conducting field genetics courses; 3-7 day intensive
    course; need more students; huge demand, need for this research
  * A lot of ideas and collaboration come out of the courses
* Things learned, will be touched on during plenary talk

## Genomics in the jungle: a field laboratory success story
*Mrinalini Erkenswick Watsa, San Diego Zoo Wildlife Alliance, USA*

* Can't be with us in London, pre-recorded talk
* Focus on field genomics - making it practical, affordable
* Living planet index - last 50 years 68% decline, higher in some areas
  * Want to say if conservation monitoring and program is successful
  * Monitoring is spread in a biased fashion across the world; samples
    get lost, degrade; exporting samples exports opportunity
* 2020 - consortium of researchers advocated for decentralised labs;
  nanopore technology's greatest advantage
* The In Situ Laboratory Initiative
  * First node in south-eastern Peru (team with decade of experience)
  * Other hubs emerging globally, Rwanda, Kenya
* Los amigos; enhanced solar capabilities, codified spaces, OpenTrons
  * BSL-2 lab in the middle of the Amazon rainforest
  * Case 1 - Every sample being DNA barcoded; have multiplexed 11
    plates onto one run using dual barcoding
  * Case 2 - Population monitoring; optimised for sample collection
  * Case 3 - Viral / disease surveillance; have identified one novel
    coronavirus strain

## Endangered European sturgeon detection through non-amplified eDNA sequencing
*Reindert Nijland*

* Getting full genome of European sturgeon
* The Netherlands have a number of species of sturgeon, most escaped
  from ponds and aquaria
* We have sturgeons, but not the species we'd like to see
* Pretty enigmatic, use both freshwater and seawater, would be nice to
  see these really big fish back in European waters
* Population living in France have been collected, hope to reintroduce
  before it goes completely
* Developing method; started with 3 swimming animals as part of
  reintroduction program
* Support from nanopore via org.one; isolated DNA with NEB Monarch,
  LSK110 + ULK kit
  * Draft genome sequence is helping us a lot
  * Have 1.8 Gb of sequence (whole genome), would previously only have
    marker genes
  * No more PCR needed to detect environmental DNA
* Teaching course; masters students decided to sample sturgeon tank
  * PCR on marker of sturgeon, found target
  * Collected DNA, 1.5ng in total volume
  * Thought maybe kit 12 could do something
    * It did work, throughput was very low, previously used flow cell; a
      couple of really nice reads in 1/2 hour, matched target and could
      tell apart from another sturgeon
  * Re-did experiemnt, about 1% of environmental genes matched sturgeon
  * Currently trying to sequence genome of other species
* Great proof of concept, hope to extend to other species of fish,
  then maybe extend to biodiversity as a whole
* Historical records showed 2x sturgeon alive in The Netherlands;
  could be that all the historical specimens have not been identified
  correctly; new genomes will help

## Q&A - Mrinalini joining from Zoom

* Museum specimens - can you sample the fluid?
  * Could possibly do it from the ethanol, but that has been replaced

* Extraction techniques, cost, sample limitations
  * Mrinalini - Too much sample to work with; use Open-source pipetting robot;
    have been using kits that use magnetic beads; make own beads
  * Ettore - wanted a kit that could be stored at RT; large used
    Qiagen kit + chelex - depends on quality of sample; faecal used
    Qiagen stool kit; Fire tissue kit
  * Zane - Don't need to purify DNA for simple PCR (thanks Mrinalini)
    * HotShot protocol, can be done in MiniPCR, works for most
      samples, at least for amphibian and reptiles
  * Reindert - Students collected 2L water; Reindert collected 5L;
    passed over a filter, used Qiagen kit. Don't like PCR for
    environmental DNA, likes to work with unamplified DNA. North sea
    try to filter 1.5-2L water (clogs filter); could filter 10L in
    zoo.

* Have you used adaptive sampling for eDNA?
  * Haven't tried it yet; not sure if it'll work with low concentration
  * Mrinalini, Ettore, Zane hasn't tried yet
  * Zane has project trying to detect possibly extinct species of
    toad; don't know where frog exists

* Adaptive sampling for eDNA for Kakapo - 0.01%, adaptive sampling
  didn't help; might work with 1%

* Challenges - in-situ lab; what does it take, how do those decisions get made?
  * Mrinalini - biological research station, variety of countries,
    lots of interesting ideas, incredibly supportive, very interested;
    making a blueprint for how to do this; it needs community buy-in,
    opens it up to people who could never do those projects, breaks
    down so many barriers; we need people to care about it; insects,
    mould, fungus are a problem (even in boxes of samples)
  * Zane - community involvement is key; courses given at research
    station; still a huge issue acquiring equipment, reagents; one
    distributor for all of South America

* Acquisition process for Nanopore stuff
  * Zane - first funding was $5k grant from university, bought device,
    brought on carry-on luggage. Funds can only be used to purchase
    equiment within the country (Equador), so can't get Nanopore
    stuff.
  * Mrinalini - have been walking around the issue

# Spotlight - The bacterial black market: utilising sequencing to study the inter-cellular and intra-cellular transfer of AMR genes in bacteria [[video](https://nanoporetech.com/resource-centre/video/lc22/the-bacterial-black-market-utilising-sequencing-to-study-the-inter-cellular-and-intra-cellular-transfer-of-amr-genes-in-bacteria)]
*Richard Goodman, Liverpool School of Tropical Medicine, UK*

* Less on the Star Wars
* AMR is a huge threat globally, UK government in 2016 reported deaths
  by 2050 would reach 50 million (if no intervention whatsoever)
  * 2019 alone, 4.9 million deaths from AMR, knocked estimates out of
    the water; carried mostly by low and middle-income countries
* How do bacteria acquire AMR? Point mutations, HGT - share pieces of their DNA between themselves - transformation, transduction, conjugation
* Humans have millions of bacteria on each surface, some contain antimicrobial resistance genes; picture that the environment is more complex than described
* Intercellular transfer - DNA moves between cells; intracellular
  transfer - DNA moves off plasmids onto genome; getting gene onto genome is more energy efficient
* Bacteria gets a resistance gene, give antibiotics, leaving one with AMR gene
  * patients in hospital are pushed towards a monoculture with lots of resistance genes
* Colistin: the last resort [after carbapenems]; a polypeptide that binds to LPS and solubilises cells
* First resistance was point mutation
* Then found AMR gene on plasmid; adds phosphoethanolamine to lipid A
* Looking at this movement of genetic elements
  * Typical approach - agar plate; looking at bacterial genome
* Developed entrapment vector that captures mobile elements
  * Antibiotic repressor gene; tetracycline gene
* Sequencing to look at what's happening
  * Initially looked at intracellular transfer; transposon with mcr-1 gene
  * Looked at different resistances
* Were able to check resistance on a plate
* Need sequencing because plating doesn't say where resistance is happening
  * Nanopore sequencing is rapid and accurate, can distinguish plasmid from genomic
  * Helps work out how they jumped into the chromosome
* Assembled with Flye, polished
* Found backpack plasmid jumped into the chromosome, mediated by ISApl1
  * Entrapment vector was captured itself; now defined as a transposon
  * Were also able to show the movement of this transposon, completely novel movement
  * Detected lots of different insertions
* Wanting to see if this can translate into therapy

## Q&A
 * Species - mainly *E. coli*
 * Research could be used in the field

# Unlocking the transcriptomic architecture of bacterial viruses with ONT-cappable-seq [[video](https://nanoporetech.com/resource-centre/video/lc22/unlocking-the-transcriptomic-architecture-of-bacterial-viruses-with-ont-cappable-seq)]
*Leena Putzeys, KU Leuven, Belgium*

* Phages are viruses that specifically infect and kill bacteria; a valuable source of inspiration for biotechnical applications
* They do it so efficiently; regulate gene expression; start transcription in a very systematic way
  1. Subset of early genes to take over host
  2. Replicate phage
  3. Viral assembly & lysis
* Trying to understand mechanisms
* Phage transcriptomics is mainly driven by Illumina, very compact
  genomes, very dense, difficult to analyse on a transcriptomic level
* Illumina coverage is very variable, very hard to make out what is happening; what is the original composition
* Classic RNA sequences do not discriminate between primary and processed transcripts
* Interested in primary transcripts, with original bindings, help in finding original transcription sites
* Cappable-seq methods; primary transcripts have triphosphate at end; can use 5'G cap
  * allows to sequence full length
* Calling approach ONT-cappable-seq
* Different stages of infection
* Implemented in-vitro polyadenylation
* One sample enriched, other as control
* Used second-strand cDNA synthesis
* Compared RNA profiles from Bioanalyser before and after
  * high peaks, high amounts of processed RNA; enrichment affects this; loses peaks
* Control sample - most mapping to rRNA
* Interested in finding out transcriptional boundaries
* Transcriptome for ONT-cappable-seq makes it much easier to see where
  transcripts start and end; initiation site, termination site
* Interesting patterns
  * Region with transcriptional readthrough with multiple terminators (can incorporate up to 5 additional genes)
  * Intergenic region with a lot of transcriptional activity, small RNA species

## Q&A

* Approaching this for the first time?
  * Lots of tweaking needed; give it a go and see
  * Make sure you have nicely intact RNA

* What kit used with PromethION?
  * PCB109; 12 barcodes with R9 chemistry

# Post-conference review - Long-read transcriptome sequencing reveals isoform diversity across human neurodevelopment and aging [[video](https://nanoporetech.com/resource-centre/video/lc22/long-read-transcriptome-sequencing-reveals-isoform-diversity-across-human-neurodevelopment-and-aging)]
*Rosemary Bamford, University of Exeter, UK*

# Post-conference review - Applying nanopore sequencing in the undergraduate classroom for environmental microbiome analysis [[video](https://nanoporetech.com/resource-centre/video/lc22/applying-nanopore-sequencing-in-the-undergraduate%20classroom%20for%20environmental-microbiome-analysis)]
*Ashley Beck, Carroll College, USA*

* Educator & scientist - research & tech to next generation
* Nanopore past couple of years seq into hands of undergraduate students
* Study sites - microbial communities; took samples from contaminated river
* Collected from locations along river, extracted DNA from sediments
* Next years, students designed own projects
  * Drinking water (good; filter should be changed)
  * Soil microbial commmunities around smelter site
* Streamlined workflow; 16S sequencing kit to multiplex on MinION Mk1B
* Examined samples via Epi2Me
* Challenge of exploring data later on in R later
* Scientific results - could get all way through project; distinct differences for smelter sites
* Got Preparation in being able to see project from design to presentation (1 presented at NCM)
* Survey assessment for students - creativity; discovery & relevance; science
  * scores relatively high
  * scores went up for second year
* "Exposure to new techniques and technologies helped me to feel less nervous and lost"

# Post-conference review - CyclomicsSeq: targeted and genome-wide detection of circulating tumour DNA using nanopore consensus sequencing [[video](https://nanoporetech.com/resource-centre/video/lc22/cyclomicsseq-targeted-and-genome-wide-detection-of-circulating-tumor-dna-using-nanopore-consensus-sequencing)]
*Jeroen de Ridder, Cyclomics & University Medical Center Utrecht, Netherlands*

# Post-conference review - Novel method for multiplexed, full-length single-molecule sequencing of human mitochondrial DNA using Cas9-mediated enrichment [[video](https://nanoporetech.com/resource-centre/video/lc22/novel-method-for-multiplexed-full-length-single-molecule-sequencing-of-human-mitochondrial-dna-using-cas9-mediated-enrichment)]
*Ivo Gut, CNAG-CRG, Spain*

# Post-conference review - Nanopore sequencing shows potential for personalised oncogenomics [[video](https://nanoporetech.com/resource-centre/video/lc22/nanopore-sequencing-shows-potential-for-personalised-oncogenomics)]
*Kieran O’Neill, Canada's Michael Smith Genome Sciences Centre / BC Cancer, Canada*

# Post-conference review - De novo assembly of immunoglobulin loci linked to full-length single-cell transcriptome of antigen-specific plasmablasts [[video](https://nanoporetech.com/resource-centre/video/lc22/de-novo-assembly-of-immunoglobulin-loci-linked-to-full-length-single-cell-transcriptome-of-antigen-specific-plasmablasts)]
*Christian Stevens, Icahn School of Medicine at Mount Sinai, USA*

* Subjects with MMR vaccine boost
* Day 6 - isolate PBMCs - B-cells single cell; non B-cells get germline DNA for IG loci
* Single cell transcriptomics with ONT only (sockeye)

* B-cell Development
  * Antibodies are built to stick to things
  * Variable region (sticks)
  * Constant - effector functions
  * Human DNA only have so many bases to work with, human capacity up to 1M - 1T different combinations
  * Need to recognise any non-self antigen that could exist

* MMR vaccine
  * Trivalent, 3 inactivated viruses

* Antibody
  * Heavy chain, light chain
  * All cells have germline loci
  * One B-cell makes only one antibody
  * mu-VDJ recombination (heavy), then light-chain (kappa or lambda) recombination

* A lot of V/D/J genes; Single B-cell selects one of each of those
* RNA splicing gives constant region

* Antigen comes in, B-cell waiting in the wings; isotype switching and somatic hypermutation

* Antibody effects include downstream function; e.g. neutralisation, opsonisation
* Lastly somatic hypermutation; each new antigen selects higher and higher affinity to antibodies

* Sorting
  * Different surface expression of cell proteins, flow sorting
  * Differentiate plasmablast and B cells, looking for memory B cells

* 10X sequencing for transcriptomics

* ONT genomic applications team - extracted DNA with IG loci adaptive sampling (50Mb targets) + immune genes, about 60X coverage of IG loci; assembling germline allows even more that can be done
* Can phase and get haplotypes
* Single phase-block haplotypes

* Assembly
  * Transcript from 10X -> find V/D/J
  * Found potentially novel new genes
  * novel V genes creates qualitatively different loci
  * Found 4 individual alleles associated with memory B cells
  * Subset of cells with heavy chain B allele as a novel locus

* Comparing Illumina vs Nanopore
  * ONT vs Illumina expression levels (7 cells), very homogenous cell population, cell barcode agreed between ONT and Illumina
  * Illumina and ONT are capturing the same thing
  * Illumina only, looking only at IGHG genes; IGHG1 vs IGHG2, no correlation
  * For IGH1/3/4, there is some overlap
* Why not Illumina?
  * Nearly identical isotypes, except at hinge region
  * hinge region is a way down from the read, so Illumina is not successfully picking up the difference

* ONT
  * Not seeing the same correlation for IGHG isotypes
  * ONT vs Illumina has poor correlation; demonstrates mis-assignment for Illumina
  * Can cluster plasmablasts based on IGH isotypes

* Why does seeing differences matter?
  * B4GALT1 - enzyme involved in glycosylation of antibodies; antibody that can affect ATCC activity
  * Didn’t expect to see different glycosylation factors as important
  * Significant enrichment in IGHG1 isotype
  * Can do differential expression analysis, comparing IGHG subsets

* Not Just any old gene, but does happen with others
  * B4GALT1 is the only 1 showing up
  * B4GALT1 is showing different effector functions
  * Being able to pull that out is an exciting part of this project

* Additional:
  * Pulled antibodies from specific plasmablasts
  * Synthesis in China; lockdown was a problem
  * 3 synthesised from potentially novel allele

* Can get not just VDJ recombination, but exact isotype

# Post-conference review - HLA typing using targeted third-generation sequencing methods [[video](https://nanoporetech.com/resource-centre/video/lc22/hla-typing-using-targeted-third-generation-sequencing-methods)]
*Steven Verbruggen, OHMX.bio, Belgium*

# Post-conference review - Portable real-time sequencing to safeguard critically endangered wildlife [[video](https://nanoporetech.com/resource-centre/video/lc22/portable-real-time-sequencing-to-safeguard-critically-endangered-wildlife)]

[walk-on music - NZ bush, with bird calls]

## Marissa

* NZ should sound like a land filled with diverse bird life
* Doesn't sound like that, due to mammalian predators, absolutely devastating birdlife; birds evolved without mammalian predators
* Kakapo has suffered severely
  * Flightless, nocturnal
  * Population down to 51 individuals in 90s; now 197 (down 2 from when slides were made)
  * Intensive management working really well
  * Slow-breeding species, every 2-4 years
  * Intensive management has made this a good system, could be applied to other species
* Every kakapo chick that hatches is precious
  * Weigh chicks every 2 days
  * Male / female have slightly different weight gains; need to tell female vs male in order to work out if weight gain is normal
* Kakapo don't have a lot of genetic diversity left
  * Artificial insemination used to increase representation
  * Many founders from 90s are still alive today, trying to make sure they still have offspring
* Aim: develop and deploy a rapid test for parentage and sex
* Doing with portable nanopore sequencing; portability was essential; affordability is also useful
  * Want protocol that can be left with the team after leaving
  * Protocol shown to rangers; they get excited about the technology
  * As few cold chain requirements as possible
* For low coverage sequencing, how much data is needed?
  * Downsampled data to various yields
  * Only need about 1.6 Gb to get good parentage information
  * Any yield tested generated useful results for sex determination
* Criteria: wanted to make sure regardless of samples, would be robust and give good results
  1. Went out to nests at night time
  2. Collected blood sample from chicks after mum had left (capillary)
  3. Qiagen spin column-based kit
  4. Bento lab
  5. Insulated plastic box + kettle water
  6. Rapid barcoding kit, MinION flow cell
  7. Magnetic rack
  8. Nvidia portable GPU
  9. MinION
* Setup was great
  * Could read off within an hour of sequencing
* Where used?
  * Pearl A2; initially doing well, but started to tail off
  * a couple of days later, found out chick was male; more concerning
  * Brought chick back from nest, was able to make a recovery with vetinary care
* Sex results for all kakapo chicks
* Parentage results for important birds; showed that artificial insemination was successful
  * Some chicks descendants of a founder that was getting very old

## Lara Urban

* Used in-situ & real-time component of nanopore sequencing to conduct portable wildlife epidemiology
* Picture: kakapo chick cuddling MinION sequencer
* Critically-endangered species is already at the brink of extinction
* Can try to support species and protect from disease, but need to understand disease risk
* Outbreak of aspergillosis last year; normally occurs everywhere in the environment; killed a few chicks (every one is important)
* Needed a fast in-situ monitoring of this strain, wanted to give techniques to ranges
* Needed to establish protocol using nanopore metagenomics from laryngal swabs
  * used Copan eSwabs
  * extracted DNA; needed to break fungal cell walls with handheld pestler & glass beads
  * found aspergillis, potentially quantitative
* Used rapid barcoding kit, useful due to transposase
* Sequenced libraries, 12 samples for around 12 hours; using Jetson Nano
* That's the theory
* Kakapo mum staring at screen, waiting for kakapo mum to leave, would get excited; need to wait for motion sensor to make sure mum had left (loud ring signal)
* Get chick out of nest, act like feeding chick until they reacted
* Miles Benton has made powerful embedded system work with Nanopore sequencing
* Jetson board AGS has powerful GPUs
  * Did live alignment of sequencing reads
  * Was able to try adaptive sampling in the field for depleting host
* Results - understand baseline; did a timeline of two different chicks (Pearl-A1, Pearl-A2)
  * There is a baseline load of aspergillis (< 0.5% of reads)
  * Drop between day 20 for Pearl-A2; mother decided to focus energy on other chick
  * Base load probably coming from mother's food
* Sequenced quite a few more samples, so far haven't detected any aspergillis reads in chicks
* Don't hope that this will happen, but needed to validate method
* Tried adaptive sampling, number of reads slighty reduced, number of base pairs about half; Aspergillis load approximately doubled
* Host depletion might allow detection rate to be increased
* This was a pilot study; generally very interested in using nanopore sequencing to improve planetary health
* Use for wildlife conservation
* Have previously used nanopore sequencing for monitoring environment
* Want to bring this all together, assess impact on the environment
* Currently setting up own research group at Helmholtz AI
* Thank Kakapo Aspergillosis Research Corporation; ONT (sent Mk1Bs + more consumables)
