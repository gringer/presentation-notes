---
title: "GPU Calling in MinKNOW"
author: "David Eccles"
date: 2021-10-08
---

This post documents my process of frustration and success in trying to
get GPU basecalling to work as-expected within MinKNOW. The ideal goal
is for someone to be able toload up MinKNOW, start a sequencing run
according to ONT's instructions, and finish it without realising they
were ever using a custom GPU basecalling service under the hood.

If you find this walkthrough useful*, please consider buying me a
[ko-fi](https://ko-fi.com/gringer). Or better yet, think of me when
you have a little bioinformatics project that I could help out
with. More details about what I do can be found here:

https://www.gringene.org/services.html

\* *[especially if you previously employed me, declared my work
redundant, and are trying to resurrect some semblance of a working
system after a fresh install]*

*Updates*

* 2024-Mar-28 - Basecall client v7.3.9 (MinKNOW 24.02.10) / Dorado models v4.3 (Ubuntu 22.04)
* 2024-Mar-22 - Basecall client v7.2.13 (MinKNOW 23.11.7) / Dorado models v4.1 (Ubuntu 22.04)
* 2023-Jul-13 - Guppy 6.5.7 (Debian testing 2023-06-12)
* 2023-Jul-11 - Guppy 6.5.7 (MinION / PromethION P2) [Ubuntu 20.04.6 LTS / focal]
* 2023-Mar-24 - Guppy 6.3.9 / MinKNOW 5.3.5-p2solo; PromethION P2 [22.07.7~focal]
* 2023-Feb-11 - Guppy 6.4.6 / MinKNOW 5.4.3 (Ubuntu 20.04)
* 2023-Jan-27 - Guppy 6.3.9 / MinKNOW 5.3.1 (Ubuntu 20.04) + PromethION P2
* 2022-Mar-01 - Guppy 5.1.15 / MinKNOW 4.5.4 (Ubuntu 20.04)
* 2022-Aug-28 - Guppy 6.3.2 (Debian testing 2022-08-10)
* 2022-Sep-26 / MinKNOW 5.2.4 (Ubuntu 20.04)

{{< toc >}}

# Before You Update

This is obvious to me, but just in case it isn't to anyone else....

Sequencing is often a critical part of the experimental workflow in a
research laboratory. Make sure you do your homework *before* upgrading
or replacing an existing working service. That means at least one of
the following needs to be true:

* The update will fix a problem that your laboratory is having
* The update will introduce a feature that is considered desirable

In addition, the following related things should be considered:

* The update should not introduce new, unfixable problems.
* The update should not remove features that are essential.

Some of this homework involves looking at update announcements and
community forum posts to identify potential issues, and making a plan
to work through solutions to those issues so as to reduce
disruption. In particular, I do not recommend blindly installing an
update because the software says that a new version is available.

The closest I've ever come to that was installing the very first Linux
version of MinKNOW (the morning after the night it was released) on my
own Windows/Linux dual-boot laptop, which allowed me to ditch my
dual-boot system and replace it with a Linux-only system.

# Getting GPU calling working in MinKNOW

Note: any lines starting with `$` are things that should be
typed/copied into the terminal. Copy the bit after the `$`
symbol. Sometimes these lines need to be modified before doing
that. Where the commands are too long to fit over a single line, they
are wrapped using a backslash at the end of the line. When typing out
this text into the terminal, do not type out the backslashes at the
end of the line (but copying them should be fine).

I use aptitude to help with dependency tracking, and nano for text
editing:

    $ sudo apt install aptitude nano

ONT suggests also installing wget and lsb-release:

    $ sudo aptitude install wget lsb-release

I'm using a computer running Ubuntu 22.04 / jammy...

    $ uname -a

```
Linux mimr-minion 5.15.0-101-generic #111-Ubuntu SMP Tue Mar 5 20:16:58 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux

```

    $ lsb_release -a

```
Distributor ID:	Ubuntu
Description:	Ubuntu 22.04.4 LTS
Release:	22.04
Codename:	jammy
```

And another computer running Debian 2023-06-12 / trixie...

```
$ uname -a

Linux musculus 6.3.0-1-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.3.7-1 (2023-06-12) x86_64 GNU/Linux

$ lsb_release -a

No LSB modules are available.
Distributor ID: Debian
Description:    Debian GNU/Linux trixie/sid
Release:        n/a
Codename:       trixie
```

As a preparatory step, add the ONT repositories to the APT database:

    $ sudo aptitude update
    $ export PLATFORM=$(lsb_release -cs)

LSB works for Ubuntu stable releases:

    ## adding via apt-key [as in Ubuntu]
    $ wget -O- https://cdn.oxfordnanoportal.com/apt/ont-repo.pub | sudo tee /etc/apt/trusted.gpg.d/nanopore.asc
    $ echo "deb http://cdn.oxfordnanoportal.com/apt ${PLATFORM}-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
    $ sudo aptitude update

For Debian, I needed to use the `focal-stable` repository:

    ## adding to keyring directory [as in Debian]
    $ sudo wget -O /etc/apt/trusted.gpg.d/nanopore.asc https://cdn.oxfordnanoportal.com/apt/ont-repo.pub | sudo tee /etc/apt/trusted.gpg.d/nanopore.asc
    $ echo "deb http://cdn.oxfordnanoportal.com/apt focal-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
    $ sudo aptitude update

## Preparation [If you are re-installing MinKNOW, or changing to a new Ubuntu version (e.g. from 20.04 to 22.04)]

Some version updates involve substantial system file changes. If you have an existing installation of MinKNOW,
then [ONT
recommends](https://community.nanoporetech.com/posts/software-patch-release-21-9359)
first purging installed packages and clearing out all existing configuration
files:

*[Note: this is still an issue, and still necessary, with MinKNOW as of at least 2024-Mar-22. If you're having software problems with MinKNOW, I strongly recommend trying a purge as a "have you tried turning it off and back on again" fix]*

    # clear out minion-nc and associated packages
    $ sudo aptitude purge minion-nc
    # clear out existing installations
    $ sudo aptitude purge 'minknow ~i' 'minion ~i' 'guppy ~i' 'dorado ~i'
    # clear out existing configuration files
    $ sudo aptitude purge 'minknow ~c' 'minion ~c' 'guppy ~c' 'dorado ~c'
    # remove the minknow and guppy installation directories
    $ sudo rm -rf /opt/ont/minknow /opt/ont/guppy
    # remove minknow and guppy/dorado service files
    $ sudo rm -i /etc/systemd/system/guppyd.service /etc/systemd/system/doradod.service /etc/systemd/system/minknow.service
    $ sudo rm -i /lib/systemd/system/guppyd.service /lib/systemd/system/doradod.service /lib/systemd/system/minknow.service
    # remove old p2 list files (if they exist) 
    $ sudo rm -f /etc/apt/sources.list.d/p2-testing.list /etc/apt/preferences.d/p2-testing
    # remove guppy overrides (if they exist)
    # sudo rm -f /etc/systemd/system/guppyd-cpu.service.d/override.conf
    # restart the computer
    $ sudo shutdown -r now

## Getting the video card working

Installing `ont-standalone-minknow-gpu-release`. This is a *huge* improvement as of this year; the new packages work with both P2 Solo and MinION sequencers, and install a GPU basecaller (that can, by the way, also call in CPU mode):

    $ sudo aptitude install ont-standalone-minknow-gpu-release

Check to make sure `nvidia-smi` can run:

    $ nvidia-smi

If this returns with an error after updating software, the computer may need rebooting:

    Failed to initialize NVML: Driver/library version mismatch

example output:

```
## Ubuntu 22.04
Fri Mar 22 15:37:26 2024       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 470.239.06   Driver Version: 470.239.06   CUDA Version: 11.4     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  NVIDIA RTX A4000    Off  | 00000000:01:00.0  On |                  Off |
| 41%   30C    P8     8W / 140W |   2144MiB / 16108MiB |      7%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|    0   N/A  N/A      1771      G   /usr/lib/xorg/Xorg                104MiB |
|    0   N/A  N/A      1944      G   /usr/bin/gnome-shell               76MiB |
|    0   N/A  N/A      2717      G   ...2/usr/lib/firefox/firefox      129MiB |
|    0   N/A  N/A      6275      C   ...in/dorado_basecall_server     1829MiB |
+-----------------------------------------------------------------------------+

```

## Getting dorado working

Check the currently-installed version of the dorado server, which works with the installed version of MinKNOW:

    $ dorado_basecall_server -v

```
: Dorado Basecall Service Software, (C)Oxford Nanopore Technologies plc. Version 7.3.9+b2176f600, client-server API version 18.0.0
```

Make note of the version before the '+' [in this case, 7.3.9]

Make sure this version matches that for the basecall client:

    $ ont_basecall_client -v

```
: ONT Basecalling Software, (C) Oxford Nanopore Technologies plc. Version 7.3.9+b2176f600, client-server API version 18.0.0
```

If you have pod5 files to test basecalling on, that will be more helpful, because it will allow checking the GPU part of dorado. If you don't have your own files, you can use my DNA ladder data here, which are R10.4.1 reads that were prepared with the ligation sequencing kit:

https://zenodo.org/doi/10.5281/zenodo.10020101

I recommend creating a single directory (e.g. `read_test`) which has a single pod5 file in it. This will reduce the time that it takes to verify that basecalling is working.

Once fast5 files have been found, calling can be tested with the basecaller (replace `#read folder#` with the location of the folder containing the fast5 files:

    $ test_read_folder="#read folder#"
    $ ont_basecall_client --use_tcp -s out_test -r -i ${test_read_folder} -c /opt/ont/dorado/data/dna_r10.4.1_e8.2_400bps_5khz_fast.cfg --port 5555

*[the _5khz bit is important; otherwise it won't use the newest models]*

On my computer, running this directly eventually (after about 2 minutes) led to a timeout error:

```
ONT basecalling software version 7.3.9+b2176f600, client-server API version 18.0.0
config file:        /opt/ont/dorado-models/dna_r10.4.1_e8.2_400bps_fast@v4.3.0
input path:         /mnt/P2_Storage/MinKNOW/read_test
save path:          out_test
minimum qscore:     7
records per file:   4000

Use of this software is permitted solely under the terms of the end user license agreement (EULA).
By running, copying or accessing this software, you are demonstrating your acceptance of the EULA.
The EULA may be found in /opt/ont/dorado/bin
[ont/error] basecall_service::BasecallClient::worker_loop: Connection error. [timed_out] Timeout waiting for reply to request: LOAD_CONFIG
[ont/error] apps::BasecallClient::create_basecall_client: Could not connect to server: [timed_out] Timeout waiting for reply to request: LOAD_CONFIG
[ont/error] main: Basecall client could not connect to server.
[ont/warning] main: An error has occurred. Aborting.
```

This is likely because permissions have been set in the configuration files to stop non-MinKNOW clients from connecting to the basecall server. I need to fix this, because I sometimes need to re-call reads outside of MinKNOW. To fix this, see the section below, titled "Patching MinKNOW to work a bit better with other systems". If it works, the output will look something like this:

```
ONT basecalling software version 7.3.9+b2176f600, client-server API version 18.0.0
config file:        /opt/ont/dorado/data/dna_r10.4.1_e8.2_400bps_5khz_fast.cfg
input path:         /mnt/P2_Storage/MinKNOW/read_test
save path:          out_test
minimum qscore:     8
records per file:   4000

Use of this software is permitted solely under the terms of the end user license agreement (EULA).
By running, copying or accessing this software, you are demonstrating your acceptance of the EULA.
The EULA may be found in /opt/ont/dorado/bin
Found 1 input read file to process.
Init time: 6 ms

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************
Caller time: 300 ms, Samples called: 1725471, samples/s: 5.75157e+06
Finishing up any open output files.
Basecalling completed successfully.
```

`<Ctrl> + C` can be used to break out of this once you have seen that
the GPU is being used (i.e. the progress bar moves along fast).

## Getting MinKNOW working

MinKNOW should already have been installed properly during the `ont-standalone-minknow-gpu-release` installation above, so there shouldn't be any issues reinstalling. Check by clicking on the MinION pore icon on the task bar (or search for MinKNOW in the "Show Applications" icon).

If there are no sequencer positions visible in MinKNOW, or they have "Software errors", there may be a file permissions issue. Try restarting the computer. If the problem still persists, see the section "Fixing MinKNOW file permissions" below.

Click on 'My Device' (if shown), then 'Host Settings' at the left, then 'Software' at
the left. There should be an 'MinKNOW' box containing something mentioning "Installed version" (which may be "unknown") hover over the 'i' symbol to see the installed versions:

*Note: if there is no 'Application Settings' or 'My Device' host displayed, you may need to give the MinKNOW settings files a spring clean. See the first steps (Preparation) for how to do this.*

```
[MinKNOW Installed version 24.02.10]
MinKNOW core:
5.9.7
Dorado:
7.3.9
Bream:
7.9.4
Script Configuration:
5.9.12
```

## Patching MinKNOW to work a bit better with other systems

*Note: previous versions of the workflow included a lot more tweaks in this step, but are no longer necessary now that ONT are bundling the GPU caller with their MinKNOW package*

The default installation of MinKNOW has the following issues on my system:

* Permission errors when basecalling
* Unable to basecall from the local computer outside of MinKNOW
* Unable to connect to MinKNOW from a remote computer
* Default write directory is on the wrong hard drive

These steps aim to fix this.

Create a backup of the configuration files. I like to use today's date, and store the copies in my home folder:

    $ mkdir -p ~/ont_conf_backups
    $ sudo cp /opt/ont/minknow/conf/app_conf ~/ont_conf_backups/app_conf.$(date +%Y-%b-%d)
    $ sudo cp /opt/ont/minknow/conf/user_conf ~/ont_conf_backups/user_conf.$(date +%Y-%b-%d)
    $ sudo cp /lib/systemd/system/doradod.service ~/ont_conf_backups/doradod.service.$(date +%Y-%b-%d)

Close the MinKNOW GUI (Right-click on the MinKNOW icon on the sidebar -> Quit), then stop the MinKNOW and doradod services (this may take a few minutes):

    $ sudo service minknow stop
    $ sudo service doradod stop

Update the MinKNOW settings to use GPU calling (for some odd reason this was disabled in the installed version) and TCP ports for the caller. This will allow the GPU calller to be used from another computer:

    $ sudo /opt/ont/minknow/bin/config_editor --conf application \
      --filename /opt/ont/minknow/conf/app_conf \
      --set basecaller.connection.use_tcp=1 \
      --set basecaller.server_config.gpu_calling=1
      
The default model file can be changed to fast mode at the same time, if desired. Defaulting to fast mode works best for the PromethION P2 Solo, so that the computer can keep up in real time with a sequencing run, monitor the output, and appropriately adjust temperature (and any other settings) to make sure sequencing performance and quality is the best possible:

    $ sudo /opt/ont/minknow/bin/config_editor --conf application \
      --filename /opt/ont/minknow/conf/app_conf \
      --set basecaller.config_file="dna_r10.4.1_e8.2_400bps_5khz_fast.cfg"

*[Actually, changing this doesn't seem to alter the behaviour of MinKNOW]*

Modify the doradod service configuration file; update the port to 5555, and set the `--use_tcp` option:

    # Note: this shouldn't change the file if it has already been changed
    $ sudo perl -i -pe 's#/tmp/.guppy/5555#5555 --use_tcp#' /lib/systemd/system/doradod.service

The CUDA device can also be changed here, e.g. by changing `cuda:all` to `cuda:0`:

    $ sudo perl -i -pe "s/cuda:all/cuda:0/" /lib/systemd/system/doradod.service

Check the file to make sure it all looks okay:

    $ cat /lib/systemd/system/doradod.service
```
[Unit]
Description=Service to manage the dorado basecall server.
Documentation=https://community.nanoporetech.com/docs/sequence

# Disable start rate limiting -- the service will try and restart itself forever.
StartLimitIntervalSec=0

[Service]
Type=simple
Environment="LD_LIBRARY_PATH=/usr/lib/nvidia-current-ont:$LD_LIBRARY_PATH"
ExecStartPre=/usr/bin/nvidia-smi
ExecStart=/opt/ont/dorado/bin/dorado_basecall_server --log_path /var/log/dorado --config dna_r10.4.1_e8.2_400bps_fast.cfg --ipc_threads 3 --port 5555 --use_tcp --dorado_download_path /opt/ont/dorado-models --device cuda:all
# Stop service based on exit status when restarts will not solve the problem
RestartPreventExitStatus=2
Restart=always
# Wait ten seconds in-between restarts, to avoid spam if the service starts crashing frequently.
RestartSec=10
User=minknow
MemoryLimit=8G
MemoryHigh=8G
CPUQuota=200%

[Install]
Alias=doradod.service
WantedBy=multi-user.target
```

*Note, specifically, the `port` and `use_tcp` options above. In my case, I didn't change the cuda device settings.*

### Fixing MinKNOW file permissions

For my computer, there's an issue with MinKNOW not being able to  access or create files on various different file systems. As a "nuclear" option, Miles Benton suggested changing the user and group for the minknow service to `root`:

    $ sudo perl -i -pe 's/(User|Group)=minknow/$1=root/' /lib/systemd/system/minknow.service

A better / safer option would be to change the user/group to the computer user that runs the MinKNOW application (in my case 'minion'). However, this approach doesn't seem to work properly (MinION devices aren't showing in the host manager):

    $ # sudo perl -i -pe 's/(User|Group)=minknow/$1=minion/' /lib/systemd/system/minknow.service

### Changing output directory [optional]

I also changed the default output directory to our large 8TB hard drive:

    $ sudo /opt/ont/minknow/bin/config_editor --conf user \
      --filename /opt/ont/minknow/conf/user_conf \
      --set output_dirs.base="/mnt/P2_Storage/MinKNOW"

### Enabling Remote Access [optional]

As of MinKNOW 5.2.4, settings need to also be changed for accessing the MinKNOW UI from another computer. To fix this, change the guest RPC and local connection settings to allow other connections:

    $ sudo /opt/ont/minknow/bin/config_editor --conf user \
      --filename /opt/ont/minknow/conf/user_conf \
      --set network_security.local_connection_only="all_open" \
      --set network_security.guest_rpc_enabled="enabled"

### Final Tidying up

Finally, it's helpful to create a backup of the modified configuration files as well:

    $ sudo cp /opt/ont/minknow/conf/app_conf ~/ont_conf_backups/app_conf.GPU.$(date +%Y-%b-%d)
    $ sudo cp /opt/ont/minknow/conf/user_conf ~/ont_conf_backups/user_conf.GPU.$(date +%Y-%b-%d)
    $ sudo cp /lib/systemd/system/doradod.service ~/ont_conf_backups/doradod.service.GPU.$(date +%Y-%b-%d)

Reload systemctl to register the configuration file changes, then restart the doradod and minknow services:

    $ sudo systemctl daemon-reload
    $ sudo service doradod start
    $ sudo service minknow start

## Verifying the configuration change

### Checking basecall_server logs

Check the system logs for the doradod service to make sure it's worked properly:

    $ sudo journalctl -u doradod.service -n 20

A good indication of this is if there's a `gpu device:       cuda:0` line:

    dorado_basecall_server[9229]: gpu device:           cuda:0 [GOOD]

This is what it might look like if the configuration file is set up to use a CPU caller, instead of the GPU caller:

    dorado_basecall_server[9229]:   what():  This build does not support GPU basecalling mode. [BAD]

This is what it might look like if the configuration file is set to use either caller in CPU mode:

    dorado_basecall_server[9229]: cpu mode:             ON [BAD]

### Checking the command-line caller

Unfortunately there's no noticeable output differences from a GPU basecall server and a CPU basecall server. However, the server *does* create a GPU process, which will be shown on `nvidia-smi`:

    $ nvidia-smi

example output:
```
Fri Mar 22 16:27:57 2024       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 470.239.06   Driver Version: 470.239.06   CUDA Version: 11.4     |
|-------------------------------+----------------------+----------------------+
...
|    0   N/A  N/A      7988      C   ...in/dorado_basecall_server     1829MiB |
+-----------------------------------------------------------------------------+
```

The `...bin/dorado_basecall_server` on the last line verifies that the GPU
is being used by the basecaller.

Check to make sure you can do basecalling using the system basecaller client, and that
it's fast (note: `--use_tcp` is necessary for command-line calling):

    $ ont_basecall_client --use_tcp -s out_test -r -i ${test_read_folder} -c /opt/ont/dorado/data/dna_r10.4.1_e8.2_400bps_5khz_fast.cfg --port 5555

*[the _5khz bit is important; otherwise it won't use the newest models]*

```
ONT basecalling software version 7.3.9+b2176f600, client-server API version 18.0.0
config file:        /opt/ont/dorado/data/dna_r10.4.1_e8.2_400bps_5khz_fast.cfg
input path:         /mnt/P2_Storage/MinKNOW/read_test
save path:          out_test
minimum qscore:     8
records per file:   4000

Use of this software is permitted solely under the terms of the end user license agreement (EULA).
By running, copying or accessing this software, you are demonstrating your acceptance of the EULA.
The EULA may be found in /opt/ont/dorado/bin
Found 1 input read file to process.
Init time: 6 ms

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************
Caller time: 300 ms, Samples called: 1725471, samples/s: 5.75157e+06
Finishing up any open output files.
Basecalling completed successfully.
```

`<Ctrl> + C` can be used to break out of this once you have seen that
the GPU is being used (i.e. the progress bar moves along fast).

If it doesn't get to the progress indicator quickly (e.g. within 2 seconds),
then there might be a port configuration error. Make sure that the `-p`
option is set to `5555`, and that the `--use_tcp` option is set.

### Checking MinKNOW

Open up MinKNOW and verify that devices are visible. If not, try restarting the computer. You may encounter screens indicating "Software error" for attached sequencers. In this case, click on "Restart position", and the problem should hopefully resolve itself.

Open up MinKNOW and verify that basecalling works:

1. Click on 'Start'
2. Click on 'Analysis'
3. Click on 'Basecalling'
4. Select desired parameters for calling via the tabbed interface (e.g. for Kit14, select "FLO-MIN114 DNA Kit 14 (400bps) - Fast" in the "Basecall settings" tab)
5. Click on '> Start'

In an ideal world, the run will display `State: Active (0%)`, then
eventually display `State: completed successfully`.

You might get an error, in which case it will say `State: Stopped with error`. Click through the `>` on the displayed interface to show the precise error reached during basecalling (e.g. ``boost::filesystem::status: Permission denied``).

Take a screenshot and report on [Bioinformatics Stack Exchange](https://bioinformatics.stackexchange.com/) or the Nanopore Community Forums with the error and any additional context you can provide (e.g. sample type, computer, GPU card). Feel free to post successful basecalling screenshots to the Nanopore Community Forums as well; it's great to hear when these steps are working for others.

Enjoy your faster basecalling!

# PromethION P2 Solo

We now have a PromethION P2 Solo! The User Manual can be found here:

 https://community.nanoporetech.com/to/p2solo

The P2 Solo software settings have now been incorporated into the standard MinKNOW installation; there's nothing more to do after following the steps above ^^^!

# Running dorado outside MinKNOW

As done previously during testing, dorado can be run outside MinKNOW, using the same basecalling server that MinKNOW uses; make sure that you include the port `-p 5555` and tcp `--use_tcp` commands:

    $ ont_basecall_client --use_tcp -s ${read_folder}/basecalling -r -i ${read_folder} -c /opt/ont/dorado/data/dna_r10.4.1_e8.2_400bps_fast.cfg --port 5555

# Troubleshooting

Potentially useful commands for troubleshooting issues:

    sudo journalctl -u minknow.service -n 100 # to check MinKNOW startup messages
    sudo journalctl -u doradod.service -n 100 # to check dorado startup messages
    nvidia-smi # to check GPU usage
    aptitude search 'nvidia ~i' # to check driver versions
