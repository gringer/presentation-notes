---
title: "Programming Notes"
author: "David Eccles"
date: 2020-06-22
---

I've decided that I need a section to collect my own notes on
programming. So here they are.

# List of Languages

I'll start off with a list of the programming languages that I have
used [i.e. I've written at least one line of the language to do something I want to do].

This is a long list, won't be chronological, and I will probably
forget a few
[taken from [this Twitter thread](https://twitter.com/gringene_bio/status/1235544158686736386)]
Yes, I'm aware that some of my stated "variants" are considered
additional languages in their own right. Due to their spiritual link
in my head, them be the breaks:

## BASIC (GW, and variants like QuickBasic)

My dad wrote a menu system in GW-BASIC, and showed me how to change
the data section of the code to add in new programs. I tinkered a bit
with BASIC code from magazines.

https://en.wikipedia.org/wiki/GW-BASIC

## DOS batch files

The BASIC menu system was purely graphical. The backend was a series
of DOS batch files that ran when the computer user typed the number on
the screen (e.g. 4 + \<Enter\> loaded Railroad Tycoon)

https://en.wikipedia.org/wiki/Batch_file

## Pascal (Including Borland TP7 and FreePascal)

The first programming language I was formally taught was Pascal. The
school teacher wasn't great, but the language had amazing help files
which found me very quickly writing quite complex code.

https://en.wikipedia.org/wiki/Pascal_(programming_language)

## x86 Assembly language

The Pascal help files included some embedded assembly language
(e.g. for making a click sound when a key was pressed), so it was a
natural supplementary language to Pascal.

https://en.wikipedia.org/wiki/X86_assembly_language

## C++

I think my first attempt at a C++ program involved creating 3D images
from random dot stereograms. I was later on formally taught it at
university, and it's been my go-to language when I need something that
runs fast. Really hard to debug, though.

https://en.wikipedia.org/wiki/C%2B%2B

## PowerBat

A batch file-like language which I tinkered with when I was trying to
improve the menu system. I used it to create a dynamic menu system
that would wait for keyboard entry. It was a memory hog, and needed
compilation.

[Can't find a web link for it, sorry]

## x86 machine code

My first and last attempt at writing raw x86 machine code involved
creating a file containing bytes that represented a computer soft
reset signal. I wrote it to the first two bytes of the hard disk the
system booted from. *\<facepalm\>* #DataLoss

https://en.wikipedia.org/wiki/Machine_code

## Java

I learnt Java in my first year of university computer science. This
was pre-eclipse, so we had to remember the "public static void
main(String[] args)" incantation. It used to be my go-to language
whenever I wanted a clicky graphical application.

https://en.wikipedia.org/wiki/Java_(programming_language)

## Excel

I've tinkered a bit with formulas in Excel. It was extremely useful
when I was doing bioinformatics that would fit into a 65,000-line
Excel spreadsheet. I wrote overly-complex formulas so that the formula
fill would do the right thing.

https://en.wikipedia.org/wiki/Microsoft_Excel#Functions

## Visual Basic for Applications (Word, Excel, Access)

When Excel didn't do what I wanted quick enough, I used VBA. My most
complex code was a hierarchical 2D bubble sort algorithm that I spent
a few months on for an Honours project.

https://en.wikipedia.org/wiki/Visual_Basic_for_Applications

## Microsoft Access

Access had a nice form designer for entering data into a binary
database. I discovered it when working on genealogy data for
Rangitāne, and used it for my first real bioinformatics project,
analysing chlamydia transmission data.

https://en.wikipedia.org/wiki/Microsoft_Access

## Warbots

It was an interesting exercise writing AI code to join in a
last-robot-standing fight. Warbots had radars, lasers, smoke, and
robots.

https://archive.org/details/Warbots

## Turing machines

I graduated with a maths degree, doing computational logic, with a
generous helping of computer science on the side. One exam required us
to write Turing machine code for adding two numbers together.

https://en.wikipedia.org/wiki/Turing_machine

## Register machines

I learnt Turing machines and register machines at about the same time
in both computer science and maths. I think my familiarity with
assembly helped for getting the hang of register machine concepts.

https://en.wikipedia.org/wiki/Counter_machine

## Prolog

Prolog was a *huge* breath of fresh air to me, lodging into my head in
a very comfortable way. It made sense! I entered "a = 3", and it would
reply "FALSE" or "TRUE" depending on what the value of 'a' was.

https://en.wikipedia.org/wiki/Prolog

## Haskell

I learnt Haskell at a similar time as Prolog, and got frustrated with
the particular interpreter we were forced to use for our course, which
wouldn't let me write a show function for a function. Lazy evaluation
gave me warm fuzzy feelings.

https://en.wikipedia.org/wiki/Haskell_(programming_language)

## R

I started learning #RStats near the end of my Honours year at
university. That's when I realised my hundred-line sort code could be
simplified into a short one-line command in R. I enjoy using R,
especially when I can use '->' and '%>%'.

https://en.wikipedia.org/wiki/R_(programming_language)

## Javascript

I'm not a great fan of the javascript flavour of the week / package
repository, preferring to stick with local, understandable code when I
write my own web pages. I will tolerate it as an end-user though. It
keeps Twitter chugging along.

https://en.wikipedia.org/wiki/JavaScript

## SQL (including Access, PostgreSQL, MySQL)

Knowing a bit about SQL helped a lot when translating medical codes
from one database into another. I don't use it much now; I generally
try to avoid using databases for anything that doesn't change much.

https://en.wikipedia.org/wiki/PostgreSQL

## LaTeX (including PDFLaTeX and TeX)

My Honours and PhD theses were written in [PDF]LaTeX, and I've used it
for a few research papers since then. Many people freak out over
formatting hints around text; I'd been yearning for them since
WordPerfect 5.1.

https://en.wikipedia.org/wiki/LaTeX

## PHP

I've tinkered a tiny bit in PHP, enough to get websites to do what I want, so that I can leave them alone and forget about the language.

https://en.wikipedia.org/wiki/PHP

## Perl (might as well include Raku)

My first memorable experience with Perl was hacking a friend's Perl
script to load picture messages from a different website to display on
a series of monitors. It's my go-to for line-by-line text processing.

https://en.wikipedia.org/wiki/Perl

## Dylan

I was challenged to write some Dylan code by a friend of a friend. I
didn't find it particularly interesting; it seemed like a mish-mash of
Haskell and C++, and the idea of compiling into a different language
turned me off.

https://en.wikipedia.org/wiki/Dylan_(programming_language)

## OpenSCAD

OpenSCAD is declarative in a similar way to Prolog, but has the
interesting idea that everything that is created is a representation
of a 3D object. My computer science knowledge has helped in finding
workarounds to some of its quirks.

https://en.wikipedia.org/wiki/OpenSCAD

## MATLAB (and Octave)

The idea of ultra-fast matrix manipulation, and succinct range
slicing, was pleasing to me. Less so was the idea that a programming
language could be commercialised.

https://en.wikipedia.org/wiki/MATLAB

## Python

Python reminded me a bit of MATLAB in its array notation; I like the
simplicity of array slicing. What I don't like is the way that
copy-pasted code creates unintended effects due to indentation
differences. I like my braces and semicolons!

https://en.wikipedia.org/wiki/Python_(programming_language)

## Maple

We learnt Maple as part of our university mathematics course, but I'd
learnt it a few years earlier as part of a high-school extension
course. I enjoyed watching our lecturer whip up some quick animations,
and demonstrate integration solutions.

https://en.wikipedia.org/wiki/Maple_(software)

## LilyPond

LilyPond is to musical engraving as LaTeX is to writing. I've written
/ copied a couple of songs into LilyPond format. It's a bit fiddly,
sometimes requiring a bit of web searching to get the right
incantation, but it makes nice pictures.

https://en.wikipedia.org/wiki/LilyPond

## Logo

I enjoy graphical languages, and was pleasantly surprised to discover
in high school (as part of formal teaching) that Logo could also be
used for knowledge-based systems (e.g. 20-questions).

https://en.wikipedia.org/wiki/Logo_(programming_language)

## Lisp

Most of my interaction with Lisp has been trying to get Emacs to work
better. I get easily confused by the parentheses, and generally
retreat to R or OpenSCAD, while reminding myself that I should really
try to get the hang of Lisp some day.

https://en.wikipedia.org/wiki/Lisp_(programming_language)

## C

I've written a couple of C programs, but most of my interaction
involves modifying someone else's bioinformatics C code, which is a
bit like walking on broken glass. It's mind-numbingly easy to write
crashable C code without realising it.

https://en.wikipedia.org/wiki/C_(programming_language)

## Brainfuck

Given that brainfuck is an esoteric / parody language, I feel fine
stating that my biggest program written in it was created while
playing the marble-rolling game
[Enigma](http://www.nongnu.org/enigma/).

https://en.wikipedia.org/wiki/Brainfuck

## Enigma

Speaking of which, I might as well mention that as well. I haven't
written any Enigma code from scratch, but I have tinkered a tiny bit
with levels others have written, mostly to work out how I can solve
them. It even has Tetris / EnigTris!

https://en.wikipedia.org/wiki/Enigma_(2007_video_game)

## Visual BASIC

It's a bit different from BASIC, and a bit more complex than VBA. I've
mostly tinkered with other people's code rather than written my own
applications, as I prefer Java and R/Shiny... and getting scratched by
a cat, for that matter.

https://en.wikipedia.org/wiki/Visual_Basic_.NET

## sh (including Csh, bash and dash)

One-liner shell scripts make up the majority of my bioinformatics
coding, but I've made and hacked a few longer script files. Shell
scripts are all over the place on Linux systems, so it's helpful to
understand them.

https://en.wikipedia.org/wiki/Bourne_shell

## HyperCard

I have vague memories of a card-like language that we had to work with
in high school, and I think it was probably HyperCard. Despite my
enjoyment of visual languages it didn't grab my attention enough for
me to remember it.

https://en.wikipedia.org/wiki/HyperCard

## QuakeWorld

I played a bit of Quake Team Fortress, and noticed that it's console
had some programming functionality which was *almost* turing
complete. I can't remember what I used it for, maybe timed messages.

https://en.wikipedia.org/wiki/QuakeWorld

## POV-Ray

Mostly I was peeking over the shoulder of my friend while they were
writing POV-Ray scenes. I did a couple of simple things (e.g. a ball
on a plane), but I never really felt a pressing need to do 3D
modelling until OpenSCAD came along.

https://en.wikipedia.org/wiki/POV-Ray

## Awk

Almost the entirety of my Awk experience has been in one-liners to
manipulate text fields for bioinformatics. Awk works well for quick
manipulation of tab-separated files.

https://en.wikipedia.org/wiki/AWK

## MIPS

We learnt a bit of MIPS assembly when studying computer science at
university. I liked the idea of a simple structured assembly, and have
memories of enjoyment around both learning (as a student) and teaching
MIPS (as a tutor).

https://en.wikipedia.org/wiki/MIPS_architecture

## Scratch (and BlockSCAD)

I had a play with Scratch for my first sino:bit program, and tinkered
with a Scratch-like variant for 3D modeling called BlocksCAD. I liked
the idea of a visual programming language with constrained, selectable
choices.

https://en.wikipedia.org/wiki/Scratch_(programming_language)

## What's Missing?

The most likely reason for me to have not used a programming language
is simply that I haven't needed it to solve a problem I had. I don't
consider myself to be much of a collector (although I do have trouble
throwing things out that others give me), and this philosophy seems to
extend into programming languages as well.

Here are some languages that people might expect me to have had
experience in, but I don't, yet:

* [COBOL](https://en.wikipedia.org/wiki/COBOL)
* [Erlang](https://en.wikipedia.org/wiki/Erlang_(programming_language))
* [Go](https://en.wikipedia.org/wiki/Go_(programming_language))
* [Rust](https://en.wikipedia.org/wiki/Rust_(programming_language))

# 2020-Jun-22

## Perl

I use it, love it, and write new code in Perl when R isn't appropriate
(e.g. when processing things line-by-line). I will reluctantly use
python when I have to (e.g. if there's a hdf5 library that I need to
use), or C++ when I have a deep concern about precisely *how*
something is done (usually due to a need for algorithmic speed).

One of the things that people struggle with in Perl is the way that
different types of variables are represented by symbols: single values
are represented by a `$` character; arrays of multiple values are
represented by a `@` character; maps / dictionaries are represented by
a `%` character; and file handles are... inconsistently represented by
either nothing, or a `*` character.

The other big thing that trips people up is the concept of a default
variable. For a lot of functions, if the function argument is not
specified, it is assumed that it operates on the single value default
variable, `$_`. Calling functions also loads a default array variable,
`@_`, with the called parameters of that function, so it's fairly
typical for functions to first do stuff with the default array
variable before doing anything else.

**On the other hand...** I am also currently debugging the Perl code
of someone else who just released a paper on a new method for single
cell sequencing. It's... an interesting experience, and has helped me
realise why so many people hate Perl.

Part of the issue is that the `use warnings; use strict;` incantation
is not very common, and it's a challenge to get people to understand
the idea that warnings and errors are helpful. The Perl interpreter
does not give much feedback (or helpful feedback) when things aren't
written "correctly", and will happily continue on doing *the wrong
thing*. The mixture of weird symbols and lack of warnings means that
it can be both difficult to understand what code is *meant* to do, and
difficult to demonstrate that the Perl interpreter does what the
programmer *expects* the code to do.
