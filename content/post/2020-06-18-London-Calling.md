---
title: "London Calling 2020"
author: "David Eccles"
date: 2020-04-18
---

# Gordon Sanghera

* 3500 people attending, 1500 are newbies
* In 2014, people all over the world gave 20,000 descriptions to ONT of what they would do with nanopore sequencing
* Just hit 1,000 publications this week
* This year, expect to ship 150,000 flow cells and 100,000 kits (same as total amount of shipments up to this point from previous years)
* Community members have sequenced a large number of COVID-19 viral genomes

# Oxford Nanopore Technologies Update

## Novel statements selected by David Eccles

* **ROC for COVID-19 virus LamPORE has a flat upper side: no false
    positives, one false negative at Ct=38**
* **Have distributed 10,000 sequencers in around 100 countries.**
* **Have started to see R10.3 achieving Q50 accuracy in consensus for some genomes**
* **Flongle robots can make 100,000 units per month, robot can be duplicated (some manual
    processes still remain)**
* **ONT have internally hit 8Tbases in one day from a PromethION, from a walk-away sample prep / loading**
* **Raw read accuracy now >97% modal accuracy (>Q15)**
* **Base calling model can now do 5hmC**
* **New MinKNOW UI allows live alignment + barcode demultiplexing**
* **Sample sheet upload ability for LIMS, will simplify downstream processing**
* **Adaptive sampling coming, about two MinKNOW releases away**
* **Potentially 4 times the runtime on MinION with deeper wells and Kit 10**
* **VolTRAX ARCTIC workflow in internal testing, 5 mins hands-on time**

## Dan Turner

Team of molecular biologists, displaying unique features of nanopore sequencing

**Talking about LamPORE**
* Rapid way of screening large numbers of samples for SARS-CoV-2
* Designed to be scalable
* Assay works by sequencing

**GenomeWeb - several assays designed**
* First based around RT-qPCR - can enrich for a region at the same time as detecting
* But... capacity of qPCR is limited, and samples typically run in duplicate or triplicate
* Sequencing is easier, with pooling - but how do we get to that?
  * Swab - extract total RNA, screen as a metagenomic
  * As soon as you *do* know what to look for, will spend most time looking at human reads
  * Not necessary to amplify for sequencing on Nanopore, but sometimes it can help
  * Amplification increases the chance that everthing sequenced will be on-target

**LamPORE**
* could take qPCR amplicons, barcode + sequence
  * this could be done without any help from ONT
* LAMP is finally getting its day: 10s of copies -> µg amounts
  * typically coupled with colour change
  * is a salmon colour a colour change (if not the correct colour)?
  * primers can interact with each other
* Sequencing helps to confirm the assay (becomes more specific)
* Works virtually in real time
* No fixed run time; a framework for a very quick end-to-end process
* Have rapid library prep - 10 minutes, or 15 with clean-up
* Rapid kit currently 12 barcodes, but need to do 100s, or thousands of samples

**LAMP**
* quick reaction, takes 1/2 hour
* Multiple copies of the target molecule joined together in long chains
* can put 10bp tag, which gets copied multiple times
* ONT rapid barcode on one end, plus other tags in the middle

**Assay**
* Target three different region of SARS-CoV-2 genome
* Target a non-SARS-CoV-2 gene (human), want to be able to tell true negative from false negative
* Added barcodes to loci, and actin control
* Can now deal with 20 or fewer copies
* Molecules consist of chains in the target
  * Repeating units are different length
  * Fish out reads that have both barcodes

**Caution**
* Not all LAMP barcodes work with equal efficiency
* Screen a large number, whittle down to set of best-performing 8
* Dual barcoding means the burden of testing is less

**Barcodes**
* In the process of expanding both LAMP and rapid barcodes

**QC**
* ROC curve, usual to accept some numbers of false positives
* **No problem with false positives at all**
* Where f1 value is at the maximum (400 reads), can confidently call positive

**Run Assay**
* Have run on lots of TWIST standards
* Have also run on 64 negative, and 20 positive samples
  * Ct values 19 up to 36, we get all of these
* **Have now done 80 samples, missed positive had Ct of 38**

**Next step**
* Assay verified by external collaborators
* Consider host genetics as well (adaptive sampling)
  * Sequence capture method without probes
  * E.g. reads aligned to ACE2 locus + 255 other loci that are known for COVID19 interactions
* Want to make LamPORE process as slick as possible
* Aim is to remove sample extraction bottleneck
  * E.g. use saliva directly - yes, we can work directly with it
  * LAMP reaction works perfectly well
* Future for LamPORE method, e.g. presence/absence of multiple viruses at once
  * Most extreme case - detecting all viruses at the same time
  * Might be possible to simplify this into something people can use at home

## Rosemary Dokos

Enormous number of people dialing in all over the world. Talking
through some of the updates from ONT. Share an office, so have a
social distancing bubble.

## James Clarke

Small stage by LC standards.

Role to talk through technology.

* Nanopore sequencing is quite new
* different to existing technologies
* but pretty well established now
* **Have distributed 10,000 sequencers in around 100 countries.**
* Recently passed landmark of 1000 publications

**Safety video - features**
* Nanopore is a really exquisite sensor, can distinguish small changes
  * Electode above / below nanopore
  * Different sequences give different currents
  * Electrical trace (squiggle) decoded to give sequence
  * Motor protein moves DNA through nanopore at 450 b/s
  * Multiple pores - R9.4.1 + R10 (greater contribution of more bases, better for homopolymers)
  * DNA double-stranded, will block pore
  * Single stranded DNA moves through about 1M bases / s
  * Have sequencing adapter to slow it down
  * Helicase with DNA + ATP + stalling chemistry, only activates when it docks with the nanopore

**Options**
 * Ligation
 * Rapid kit
 * Barcoding

**Other features**
 * Tether at the bottom, gives incredible increase in sensitivity (3-4 orders of magnitude)

**Layout**
* Nanopores are single-molecule sequencers
* Have well, each well has one nanopore
* Nanopore sits at the pinch point of green triangles in the centre
* Can array the well, create thousands, tens of thousands of wells
* includes ASIC
  * Can reverse potential on a well
  * Can do adaptive sampling
  * Allows to unblock ejected DNA

**Approach**
* Method is very simple
* A huge amount of bases contribute to the current (20)
  * A benefit - multiple chances to see a base before it's lost in the signal
* Decoded by recurrent neural network, trained on known sequences
* DNA/RNA can vary for different pore sizes
* Raw accuracy 97% modal
* Base caller + training methods made available

**Accuracy**
* 97% modal
* **R10.3 achieving Q50 accuracy in consensus**
* Platform has no dropoff in accuracy with read depth
* Users routinely getting lengths > 30kb
* Longer than this, need really good handling
* Current record - 2.44Mb
  * at 400-450 bases / s, would take an entire game of football + longer
  * Not everyone can get ultra-long reads

**What can it do?**
* Can see base modifications, no need to chemically modify the sample
* No need to PCR, can read naked strands
* Nanopore sequencing is the only way to sequence RNA directly
  * works in essentially the same way
  * needs different motor protein

**Data output**
* PromethION record 220 Gbases, MinION record 40 Gbases
* 200 million 400mer reads from a single flow cell
* Can barcode lots on a single flow cell
* Real time, can write analysis pipelines to process in real time
* Portable platform - can take your sequence to your sample

**Sequencing devices**
* MinION first released as part of MAP in 2014, 2048 well spread over 512 channels
  * Very accessible, works on a laptop
  * Deliberately priced to be accessible
  * High burden on the laptop, causing configuration problems
* MinION Mk1C
  * Standalone mobile sequencer
* GridION
  * also a standalone device
  * GPU, can keep up with calling
* Flongle
  * often people don't need high output
  * ASIC is in the adapter, so all the cheap bits in the flow cell
  * 126 channels, but can still generate 1-2 Gb of information
  * Really about a high-volume product, needs high-volume production
  * Factory on-line for about a year, built with flow cell manufacturing in mind
  * Some manufacturing parts are automated, some not
  * **Can make 100,000 units per month, robot can be duplicated**
* PromethION
  * Highest throughput
  * No batching
  * whole device + software built with this in mind - log into box / monitor
  * different flow cell, gives more output, 1200 wells, 3000 channels, outputs up to 180 Gbases
  * Comes in P48 / P24
  * GPU, fast SSD
  * A lot of work on the robustness of the platform
    * better temperature control
    * saturation detection (popped membrane)
    * electric calibrations - want to make it as tight as possible
    * flow cell check improvements

**PromethION Flow Cell Consistency**
* Catch a lot of problems before they leave the factory
* Marginal problems leave the factory
* Can have membrane that's too small, too big
* Poor flow cell failure can affect output
* Two issues (both resolved) - PromethION-specific
  1. PromethION built on top of ASIC; electrode chip mounts not properly adhered, some defects
    * have got really good at detecting these
    * a lot stricter at quality control
  2. Changing metal changed lithography
    * UV mask / dose changes resolution of well
    * Change in metal causes UV reflection, structures don't hold on fluids as well as wanted
    * Easy to fix by re-adjusting UV dose

**Repeated box benchmark**
* **Kit with fuel fix, run and walk away, 8 Tbases without interventions**
* Flow cells much more consistency
* 48 flow cells, median 170 Gbase, max 200 Gbase
* Accuracy hitting 96% modal accuracy on every flow cell
* An internal run; what matters is how they perform externally

## Stuart Reid

Technology development - where we are, where we're going

**How is Nanopore done?**
* Started community driven, need feedback to adjust what we do
* Times change, now have mainstream users who just want things to work

**Accuracy**
* What matters: can you get the answer you're looking for
* **Raw read accuracy now >97% modal accuracy (>Q15)**
* Recently upped the stride - steps through the data more, can squeeze more accuracy
* Going into live base calling
* Also testing on human datasets
* Can re-call R9 data (last few years)

**Additional research callers (on GitHub)**
* Run-length encoder
* Bonito, as assumption-free as possible
* Rerio will have models that are compatible with existing production base callers

**Barcoding**
* Attaching specific DNA sequence to samples
* Run on flow cell
* Work out after the fact which barcode it was
* about 24 bp
* Most stringent settings, can now get to 99.999% accuracy on barcodes (require both ends)

**Consensus accuracy**
* Add multiple reads to determine consensus sequence
* Use Medaka tool, which understands the data type
* Latest basecaller and Medaka models now at Q45 for metagenome
* R10.3 is now at Q50 (1 in 100,000)

**Single molecule accuracy**
* High single-molecule can be done with nanopore
* UMI - semi-random barcode + amplification
  * Can get up to consensus-levels
* Bonito basecaller with pair decoding and 2X, over Q20
* Can do depth-first consensus - multiple copies into single molecule
  * e.g. Cyclomics rolling circle
  * Get up to consensus-levels of accuracy

**Human whole-genome sequencing**
* 60X coverage - 180 Gbases from PromethION flow cell
* Using Medaka
  * Long reads, can phase SNPs, work out which copy a read came from
  * Changes - now have specific models for het / homozygous calling
  * Above 99% accuracy, a little bit behind on INDELs
* Add benefits with structural variants
  * possibly ten times more variation than SNVs
  * Sniffles used to call variants (workflow in Epi2Me)
  * Up to 96% accuracy, as good as anyone out there
  * works at 15X (e.g. 4 human on a PromethION flow cell)

**Assembly**
* Research RLE base caller + Flye / Shasta, Medaka
* Have looked at CHM13, pretty good contiguities
* BUSCO scores essentially complete (96%)
* SNPs + structural variants
* Nanopore looking directly at the DNA, can measure modifications
* 5mC in CpG context, see from the same data, comparing to ground truth (bisulfite)
  * Nanopore data is better correlated to bisulfite than bisulfite is to bisulfite
  * **Not just 5mC, now have model for 5hmC as well as other methylations**

**Software**
* New UI, coming with MinKNOW 4 (imminent)
* A lot of personalisation
* iPhone / Android versions
* **Live alignment - run sequence alignment from UI, can be combined with barcoding**
* Tutorials, guided tours in UI

**Behind scenes**
* Big changes to API
* **Planned changes to LIMS - sample sheet upload, will simplify downstream processing**
* Switching on compression by default

**Read-until / Adaptive sampling**
* Kick out molecules you don't wnat to look at
* Moved API out from behind developer licence to general release
* **Integrated solution coming, probably not this version of MinKNOW, but next one**
  * Select targets, and start a run
  * Matt loose has been doing this
  * Can do now, need to get hands a little dirty

**Analysis**
* Epi2Me - cloud-based
* Epi2Me labs - will have tutorials
  * Matching versions, runs on the device
* Open source Github

**Platform development**
* Consistency / ease of use
* Speed dropoff from enzyme using fuel - we've fixed that, coming out with kit 10, no more topping up
* Device life limitations - electrochemistry in the well
  * May be able to double the run time
  * Like a battery - can make a better battery, or a bigger battery (e.g. deeper wells)
  * **Potentially 4 times the runtime**

**What to put into the device?**
* Tethering chemistry - it sticks everywhere, but we want it to just stick to the membrane
* Have a factor of 10 improvement, aiming for 200X (picogram / femtogram)

**Futures (reminder)**
 * ASIC is a core component
 * working on a smaller ASIC
 * Well size is a key limitation
   * By changing way sequence is measured, can make wells smaller
   * Change to voltage
   * Could get up to 4 Tbases / day on MinION
   * Have proven 20µm, can get decent squiggle
   * ASICS designed, play later this year

## Rosemary Dokos

Taking lovely platform developments and packaging for release. Need to
consider all the different types of users we have. Jungle, ice, lab,
corporate - all using the MinION, want to establish an easy end-to-end
process.

**Preparation**
* Applications have developed protocols / kits, and targeted methods
* Last few months working with production-scale customers to increase genomes
  * XL kits, automation with Hamilton
  * Upstream processes in order to feed into automated preps

**Improved output**
* Can sequence more samples on a flow cell
* Will get 96 rapid barcodes
* 96 ligation barcodes
* Dual barcodes, gives barcodes and barcodes galore
* Will be releasing information on how to do custom barcoding in guppy

**VolTRAX**
* Designed for users to load sample, start protocol, and device takes care of the rest
* At start of this year, workflow devised / developed by ARCTIC team, challenge for ONT
  * **Video of ARCTIC prep running on VOLTRAX (4 samples)**
  * Use to exercise all features that are usable

**VolTRAX Features**
* PCR - needs cartridge upgrade
* Fluorescence - have software and hardware enabled
* Software worked on - live cartridge feedback, probably cross-platform by end of 2020
* Allow users to run own sample preps - opening July 2020
* Reducing amount of pipetting necessary

**Sequencing**
* Very scalable offering
* Chart designed by Clive in 2010, still relevant
  * Can massively increase by increasing speed and run-time
* GridION has similar output to single PromethION flow cells
* Might be able to get to human on MinION once this happens

**Future**
* New flow cells shipped with kit 10 to PromethION as a welcome home
* High modal read accuracy
* R10.3 - sent to KeyGene and Matt Loose
* New Guppy getting into MinKNOW in the next few days
* Speed - working with Nvidia to allow GPU on MinKNOW

**Epi2ME**
* Mechanism by which users can get to know analysis pipelines
* Group has been very busy, need to deliver clean, simple workflows
  * New agent, in beta testing, previous runs, multi-user support

**Core values**
1. Sequencing has to be accessible and capital free
2. Transparent pricing
3. Upgrade paths simple / easy
4. Community-backed development

**Capital-free devices**
* All include 12-month warranty

**Transparent pricing**
* Delivering incredibly prices per Gb without huge investments
* More multiplexing / barcoding, can access competitive price per sample

**Applying models**
* Come up with a few basic user cases
* Chemistry + improvements will allow 2 genomes per PromethION flow cell ($500 per genome)
* Smaller genomes, can run more, can get answers quickly

**Targeted Sequencing**
* Amplification-based
* Amplification-free (e.g. CAS9)
* Sample prep free, adaptive sampling
  * We are incredibly excited about this

**RNA Sequencing**
* Direct RNA

**Metagenomics**
* Metagenomic assembly
* River metagenomic - MARC group from 2015
  * Have made a lot of different projects
  * Sequenced 11 rivers across 3 continents
* Metagenomics in fight against COVID to detect co-infections

**Upgrade cycle**
* Have always provided users with simple / easy upgrade path
* Most upgrades happen in consumables, software, and kits
* Majority of upgrades happens behind the scenes

**Community-driven feedback**
* Thrive on getting product out of the door really quickly
* Important to connect to users very early

**Community**
* Growing and evolving
* A lot of people in translational, clinical, applied
* Will start giving a much clearer guide of where things are
* Research store - keep buying all products
* Q-Line, and LamPORe store
* Want to carry on innovating

**Summary**
* Will discontinue something for the first time - MinIT
* VolTRAX V2
* Kits too many to mention
* Software MinKNOW 4
* Clive will be doing a broadcast over summer

**Questions**
* Cut-off for size, around 50bp, tunable down in the code
* Flongle reaching flow cell reliability - being worked on (no gasket)
* Faster RNA still on the cards

# David Eccles [poster]

[--]

Kia Ora Koutou. My name is David Eccles, and I am a bioinformatics
analyst working at the Malaghan Institute of Medical Research in
Wellington, New Zealand.

While nanopore cDNA sequencing is most frequently used for isoform
discovery, we have used it for a robust differential expression
analysis from cDNA reads. [30s]

[--]

One of my projects involves investigating mouse 4T1 breast cancer cell
lines, and how they respond to mitochondrial DNA depletion.

The experimental setup involves bathing the parental cell line in
low-dose ethidium bromide for a few months to stop it from producing
more mitochondrial DNA, then injecting it into a different mouse and
seeing if it makes a tumour. It does, but only after acquiring
additional mitochondrial DNA from the new mouse host.

As a preliminary result, We discovered that when you stop a cell
population from producing more mitochondrial DNA, the level of
mitochondrial transcript expression drops to effectively nothing,
indicating that nuclear copies of mitochondrial transcripts are
completely inactive.

[--]

We also discovered that cells don't seem to have a system to deal with
mitochondrial loss.

Shown here are transcript differential expression results for all
proteins in the mitochondrial complexes, demonstrating that while the
mitochondrial transcript levels drop *way* down, the nuclear
transcript levels stay pretty much the same.

So, in other words, these cells will keep happily producing nuclear
mitochondrial protein transcripts even when there's nowhere to put the
proteins created from those transcripts.

 [40s / 70s]

[--]
https://pbs.twimg.com/media/DoosOiXUwAA-K2g?format=png&name=4096x4096

I want to finish up with something extra that I've only just started
exploring in my research, and that's base-level gene coverage.

In this image I'm showing beta actin gene coverage from three Illumina
runs in blue compared with a Nanopore run in green. As the read length
increases from top to bottom, the peaks and troughs of observed
expression get smoother, to the point where the nanopore reads are
almost completely flat across all the intermediate exons. This lack of
variability is great for identifying hidden isoforms, even for low-
expressed genes. [33s / 103s]

My poster outlines a pilot study to determine the effectiveness of
nanopore sequencing for differential expression analysis. The success
of that study means that I'm now a lot more confident about
recommending nanopore sequencing for gene and transcript expression
research.

Please feel free to ask me questions about either the biology or the
bioinformatics of our research. Thanks for listening. [20s / 170s]
