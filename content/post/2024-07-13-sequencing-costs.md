---
title: "Sequencing Costs"
author: "David Eccles"
date: 2024-07-13
---

Consider the following scenario: your research lab has been around for
a few years, and you are wondering whether all these sequencing
services you are paying for are worth it - should you invest in buying
your own medium-scale DNA sequencer?

## Prelude - Nanopore Sequencing

Disclaimer: I am a fan of Oxford Nanopore Technologies (ONT). ONT has
produced a sequencer that can do something no other commercial
sequencing company can do: carry out [relatively] model-free discovery
of novel polymers (e.g. DNA & RNA with modifications). Combined with
their relatively public disclosure of information (especially with
regards to pricing), I've been willing to work around
[numerous](https://www.reddit.com/r/bioinformatics/comments/1dhhcsy/why_are_people_still_wary_of_nanopore/l8x39oq/)
technical and operational issues because their "move fast, release
early" philosophy works well with my preferred way of doing research.

If you're just starting out, don't have much money, or just want a
taste of sequencing, then there's only one realistic option for an
in-house sequencer: Oxford Nanopore's MinION. A MinION Mk1b Starter
Pack with a 24-sample Rapid Barcoding Kit is available from Oxford
Nanopore for $2,000 USD ($3,300 NZD). For another $1,460 USD ($2,400
USD), you can get a Flongle adapter, which gives you access to Flongle
flow cells, which are the cheapest per-run sequencing flow cells
available.

But this post isn't about MinION sequencing (a device that has a yield
of 5-15GB for MinION flow cells, and 0.2-1 GB for Flongle flow cells);
it's about sequencing on a device that can realistically and
comfortably sequence a human genome in a single run.

## Cost Summary

There are three popular players in the low-end large-scale sequencing
market: Illumina, Pacific Biosciences (PacBio), and Oxford Nanopore
(ONT). Here is an approximate breakdown of the direct costs to get a
human-genome-scale system up and running from each company (costs are
approximate, mostly based on conversion from USD to NZD, and don't
include additional external costs):

### Illumina NextSeq 1000

* Initial system capital cost: $350,000
* Cost per run: $7,400 (P2, 600 cycle, 400M read kit)
* Maintenance: $35,000 / year (estimate, assuming [10% of system cost](https://sandiegomics.com/mgi-breaks-the-100-genome-barrier-barely/))
* Yield per run (using the above kit): 240 GB
* Marginal kit cost per GB: $31
* Read length: 600bp
* Read accuracy: Q30 (99.9%)
* Sequencing run time: 34 hours

### PacBio Revio

* Initial system capital cost: $1,300,000
* Cost per run: $1,650
* Maintenance: $75,000 / year (estimate, based on reported Sequel maintenance costs)
* Yield per run: 90 GB
* Marginal kit cost per GB: $18
* Read length: up to 20 kb
* Read accuracy: Q33 (99.95%)
*  Sequencing run time: 24 hours

### ONT P2 Solo

* Initial system capital cost: $38,000 (CapEx version)
* Cost per run: $1,650
* Maintenance: $1,650 / year
* Yield per run: 50-150 GB
* Marginal kit cost per GB: $11-$33
* Read length: up to 2 Mb
* Read accuracy: Q28 (99.8%)
* Sequencing run time: 5 mins to 72 hours

## Capital Cost Recovery

From my perspective, the marginal per-run costs for the platforms are
similar. In other words, a service centre with good cost recovery for
capital expenses and maintenance could choose any one of these systems
and get similarly cheap throughput.

However, most research labs are not service centres, so the
maintenance and capital costs should dominate purchasing
decisions. Here are the approximate maintenance costs taken from the
above summaries:

* Illumina NextSeq 1000: $35,000 / year
* PacBio Revio: $75,000 / year
* P2 Solo: $1,650 / year

Assuming none of the sequencing runs were done internally, with a
doubling of service cost to other people, here are the approximate
number of gigabases required to sequence in order to recover the
ongoing maintenance cost:

* Illumina NextSeq 1000 (P2, 600 Cycles): 2300 Gb (10 runs)
* Nanopore P2 Solo (PromethION): 100 - 300 Gb (2 runs)
* PacBio Revio: 8200 Gb (91 runs)

On the flip side, this is the minimum number of sequencing runs that
would need to get done externally (with overheads being 50% of the
service charge) over the course of a year as a service before it would
make financial sense to consider buying one of these sequencers for
internal use.

Given that the service market for sequencing is somewhat competitive,
that 50% service charge assumption may be invalid. If the service
charge were similar to (or lower than) the in-house reagent cost
(which happens for Illumina sequencing due to steep volume
discounting), it may not be possible to charge substantially more than
reagent costs and still get sufficient demand for cost recovery.

## Summary

For small volume sequencing (less than one sequencing run per month),
a purchase of the Nanopore P2 Solo makes financial sense, as recovery
of maintenance costs happens quickly. Due to the high maintenance cost
of PacBio Revio, it doesn't appear to make financial sense as a
purchase for individual labs at *any* scale.

There are some nuances depending on application, but my general
recommendation is the following:

* If you *only* want to do short-read sequencing (under 1000bp), then
  don't buy a sequencer; just continue to get sequencing done as a
  service from an established large-scale sequencing centre.

* If you want to do long-read sequencing (or a mix of short-read and
  long-read sequencing), then consider getting a P2 Solo. The ongoing
  maintenance costs are low enough that it can sit idle for almost the
  entire year and still be a financially-viable tool for discovery.

## Coda

I am the owner/operator of a sole-trader business - GrinGene
Bioinformatics - and can provide more specific advice about
high-throughput sequencing projects, from sequencing library
preparation through to downstream data analysis and result
presentation.

If you have a research project that you'd like help with, feel free to
contact me for advice. See here for more details:

https://www.gringene.org/services.html