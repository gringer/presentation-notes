---
title: "Presentation Notes and Other Things"
author: "David Eccles"
---

# Presentation notes (mostly ONT)
* [London Calling 2024](/2024/05/22/london-calling-2024/)
* [London Calling 2023](/2023/05/18/london-calling-2023/)
* [Nanopore Community Meeting 2022](/2022/12/06/nanopore-community-meeting-2022/)
* [London Calling 2022](/2022/05/18/london-calling-2022/)
* [Nanopore Community Meeting 2021](/2021/12/01/nanopore-community-meeting-2021/)
* [London Calling 2021](/2021/05/20/london-calling-2021/)
* [London Calling 2020](/2020/04/18/london-calling-2020/)
* [Nanopore Community Meeting 2019](/2019/12/06/nanopore-community-meeting-2019/)
* [Nanopore Community Meeting 2018](/2018/12/06/nanopore-community-meeting-2018/)
* [London Calling 2018](/2018/05/24/london-calling-2018/)
* [TEDxWellington 2017](/2017/06/18/tedxwellington-2017-perspective/)
* [London Calling 2017](/2017/05/04/london-calling-2017/)
* [Clive Brown's March 2017 Tech Update](/2017/03/15/gridion-x5-the-sequel/)

# Random musings

* [Covid papers](/covid-papers)
* [2024 Aotearoa Royal Commission COVID-19 Report Summary](/covid-report)
* [Painting and Bioinformatics](/2020/02/23/painting-and-bioinformatics)
* [Nanopore Notes](/2018/10/25/nanopore-notes)
* [GPU Calling in MinKNOW](/2021/10/08/gpu-calling-in-minknow/)
* [ONT Cost Summary](/2022/07/11/ont-cost-summary)
* [Programming Notes](/2020/06/22/programming-notes)
* **Two Lines on Everything**
  - [Part 2](/2022/06/27/two-lines-on-everything) 
  - [Part 1](/2018/10/03/two-lines-on-everything) 
* [My Life as a Freelance Bioinformatician (WIP)](/2018/04/13/so-you-want-to-be-a-freelance-bioinformatician/)
* [Open Research](/2018/03/23/open-research/)
* [Free Software](/2018/04/07/free-software/)
* [Chaos Fund](/2018/06/10/chaos-fund/)
* [My Jury Experience With Criminal Justice](/2022/04/12/criminal-justice/)
* [My Dream Bioinformatics Workplace](/2021/03/15/twos-a-company--my-dream-bioinformatics-workplace/)
* [Lab Handbook](/2019/06/16/lab-handbook/)
* [The Problem With Competitive Funding](/2022/03/13/stop-competitive-funding/)
* [The Problem With GWAS](/2022/03/06/genome-wide-association-scams/)
* [Ten People Who Were Wrong](/2023/05/18/ten-people-who-were-wrong/)
* [High-throughput Sequencing Costs](/2024/07/13/sequencing-costs/)
* [Diceomancer - Relics](../diceomancer/relics)

# Other tutorials

* [Genome Firework Plot](/2017/09/08/firework-plot/)
* [Repeat Spiral Plot](/2018/01/10/repeat-spiral-plot/)
* [Fonts](https://gitlab.com/gringer/pointilised)

# Web Profile Pages

* [ResearchGate](https://www.researchgate.net/profile/David_Eccles)
* [SeqAnswers](http://seqanswers.com/forums/member.php?u=17049)
* [Bioinformatics Stack Exchange](https://bioinformatics.stackexchange.com/users/73/gringer)
* [Wikipedia](https://en.wikipedia.org/wiki/User:Gringer)
* [GitHub](https://github.com/gringer)
* [GitLab](https://gitlab.com/gringer)
* [Printables](https://www.printables.com/@gringer)
* [Openclipart](http://openclipart.org/user-detail/gringer)
* [Reddit](http://www.reddit.com/user/gringer/)
* [Twitter](https://twitter.com/gringene_bio)
* <a rel="me" href="https://genomic.social/@gringene">Mastodon</a>

