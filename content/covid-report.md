---
title: "Whītiki Aotearoa"
author: "David Eccles"
---

# Whītiki Aotearoa - Royal Commission COVID-19 Report, Part One

**Lessons from COVID-19 to prepare Aotearoa New Zealand for a future pandemic**

https://www.covid19lessons.royalcommission.nz/reports-lessons-learned/main-report/

This is my attempt at a condensed summary of the report, taking
selected statements, with some modifications. This is presented as a
series of Bluesky skeet threads
[here](https://bsky.app/profile/gringene.org/post/3lby5rble2s2l). Usually
I'll delete flavour words and subclauses to fit into the space of a
skeet, but sometimes I'll also to add in my own bias. The text below
is closer to the original text (but often still not an exact copy);
skeets have been condensed to fit within the character limit that is
demanded by Bluesky developers.

If you find these notes useful, please consider buying me a
[ko-fi](https://ko-fi.com/gringer). Or better yet, think of me when
you have a little bioinformatics project that I could help out
with. More details about what I do can be found here:

https://www.gringene.org/services.html

---

{{< toc >}}

## Interesting / novel statements (picked by David Eccles).

In this section I will [eventually] put things that I think need
particular emphasis, due to being surprising, or more substantial than
the average tone of the report (which is a high bar).

* Given the expansive kāwanatanga powers exercised in this emergency
  and the need for agile decision-making by the Executive, the Crown’s
  obligation to actively protect tino rangatiratanga and partner with
  Māori is, in fact, intensified.

## Part One: Preliminaries | He Whakataki

The Aotearoa New Zealand Phase 1 Covid-19 Royal Commission report has
was released on 2024-Nov-28. Here are a few statements from it that
interested me (usually paraphrased to fit into the skeet limit), and
the occasional additional thought from me.

Excluded from terms of reference:

* specific clinical decisions
* the wider health system reforms
* decisions of the courts & oversight bodies
* private sector operations
* ...
* specific epidemiology of the COVID-19 virus and its variants
* vaccine efficacy

Te Tiriti o Waitangi is specifically mentioned as an important
contribution to Aotearoa's response.

## Part Two, Chapter 1: A snapshot of Aotearoa’s pandemic experience | He tirohanga ki ngā wheako o Aotearoa mō te mate urutā

1.2.1 Overall, Aotearoa’s COVID-19 response was effective. Aotearoa’s
COVID-19 response was effective at both protecting people from the
health effects of the virus, and minimising the potential impacts of a
global pandemic. The initial success of the elimination strategy
allowed the country to spend less time under strict lockdown
conditions than many other parts of the world, meaning daily life and
economic activity were able to return to ‘normal’ much earlier.

We begin this report with an acknowledgement of the success of
Aotearoa New Zealand’s response on a range of measures. It was praised
as an exemplar around the world, especially in the first two years.

[i.e. until protective measures were removed]

Supporting graph. The confirmed cases in Aotearoa were substantially
lower than other countries.

{Graph of COVID-19 cases (confirmed) per million people,
2020-2021. New Zealand is pretty close to a flat line along the bottom
of the graph, with Australia and South Korea also fairly similar; UK,
US, & Sweden have a gradual increase in cases up above 100,000 cases
by the end of 2021. Singapore has an initial rise, mostly flatline,
then a burst of cases in the last few months of 2021.}

Aotearoa New Zealand’s lockdown measures were strict, but New
Zealanders spent less time in lockdown than many other countries.

{Oxford stringency index for Aotearoa New Zealand and comparator
countries, 2020–2022 inclusive. New Zealand has two big peaks near 100
and two smaller peaks near 60, and a baseline level of 20; Taiwan has
a lower stringency index overall (but similar baseline level to
Aotearoa) with a blip in mid 2021; Australia, UK, US, and Sweden have
upper stringencies around 70, and not really any relief until mid
2022.}

Other demonstrations of the response effectiveness, relative to other
comparable nations:

* Economy recovered quickly
* Unemployment rates remained low
* Learners missed fewer school days

## Chapter 2 - All-of-government preparations and response

"Many reports, reviews and recommendations all saying the same thing:
the world is not prepared for a pandemic. COVID-19 has laid bare the
truth: when the time came, the world was still not ready."

A well-established civil defence emergency management system was in
place at the start of 2020, led by the National Emergency Management
Agency (NEMA). It was practised in dealing with disasters such as
severe weather events and earthquakes. The emergency management system
was not prepared for a pandemic of the nature and scale of COVID-19,
which required a prolonged response and had widespread and complex
national impacts. We were used to a few days or weeks; rarely months –
let alone years.

The New Zealand Influenza Pandemic Plan was useful at the start of the
response, but lacked a framework for reviewing the high-level response
strategy and adapting it over time as the situation changed. The Plan
emphasised the importance of engaging with Māori, but was largely
about communication and resource access. The Plan did not address the
role of Māori and iwi in decision-making, or in designing and
providing services (including healthcare services).

There is great value in developing strong relationships with
communities in advance ["ten thousand cups of tea now"], for improved
commissioning and delivery of services in the present as well as to
set the foundation needed to respond to a future crisis.

There was a change in organisation / management throughout the
pandemic response:

1. Lead agency + Officials Committee for Domestic and External
   Security Coordination
2. Bespoke All-of-Government
3. Refinement over time

Aotearoa used all three different response strategies: elimination
until late 2021, suppression briefly from late 2021 to early 2022,
followed by mitigation. The transitions from one strategy to the next
were fuzzy and not always well-signalled.

The report made their own graphs about flattening the curve, calling
them "now locally famous".

Here's the updated "Stop the spread" version of the actually locally
famous "Flatten the curve" graph, designed by @siouxsiew.bsky.social
and drawn by Toby Morris

{Stop The Spread cartoon, demonstrating that a strong, long-term
collective response produces the best results. This cartoon suggests
the healthcare system capacity is about half the curve height for an
unmitigated, whereas the actual capacity (as outlined in the graph in
the report) is far lower. Siouxsie acknowledged this at the
time. Aside: It's likely there was some artistic license employed to
get the point across to a naive audience. In the end it doesn't really
matter if the capacity is exceeded by 100% or by 1000%; they'll both
cause system collapse.}

The vast majority of decision-makers were not thinking of elimination
as a potential strategy before mid to late March 2020; reflecting
WHO’s advice not to use travel and trade restrictions as control
measures. Aotearoa elected to break with this advice. There was not a
consistent view that elimination was possible or even the goal in
Aotearoa. Strict border restrictions and stringent public health and
social measures were even more successful than anticipated;
eliminating the virus was a viable possibility.

2.4.4 Just as it was difficult to identify exactly when the
elimination strategy began, it is difficult to pinpoint exactly when
it ended.

I have my own ideas about when it ended: at a meeting on the 28th
September 2021:

[[see above](#aotearoas-descent-into-covid-madness)]

"By October 2021, Auckland had spent seven weeks in a lockdown that
had initially been signalled to last for at least one week, and
ministers and officials were aware that ‘social licence’ for
compliance was beginning to erode."

[That erosion of public support had government backing. From the 30th
September meeting Comms were trying to transition to a new framework
"despite overall support and comfort with the Alert Level Framework."]

The National Iwi Chairs Forum had wanted the transition to be delayed
on the basis that more time was needed to ensure adequate vaccination
among Māori. Vaccination levels continued to be substantially lower in
Māori and Pacific communities. The COVID-19 Protection Framework
(‘traffic light’ system) was widely viewed as less clear than the
Alert Level System. An expert health group reviewing a draft version
was ‘near unanimous in its skepticism about this framework in its
current form’.

In September 2022 Cabinet agreed to formally retire the minimisation
and protection strategy and move to a ‘long-term approach to managing
COVID-19’, together with the ‘traffic light’ system, signalling the
end of Aotearoa’s COVID-19 pandemic response.

The Unite against COVID-19 public information campaign was quickly
established as an effective brand achieving high levels of
recognition. It later received multiple awards for design and
communication.

The 1pm daily briefing became an important tool for conveying accurate
information, mobilising community support for Government measures and
generating public trust and confidence in the response. It was a
memorable feature of the pandemic experience. The Prime Minister’s
corralling of public sentiment to promote unity, together with the
Director-General’s factual information made the 1pm briefings work as
a public communications tool, with flow-on effects for the early
success of the elimination strategy.

Many community organisations worked to ensure the provision of
accurate, timely information to their members – translating and
sharing daily updates and critical information, actively dispelling
misinformation, and identifying providers who could meet needs.

We support something more like the approach taken in the
recently-revised Australia Government Crisis Management Framework,
with a lead agency model and whole-of-government coordination for
dealing with crises that have extreme impacts or complexity.

The creation of the Red Team within the Department of the Prime
Minister and Cabinet was an attempt to create space for alternative
thinking. There was agreement that a scrutiny mechanism is an
important but tricky function to put in place.

As the Minister of Finance, Grant Robertson, told Parliament on 17
March 2020: "Our first response is a public health one. It is our
fundamental duty. It is also the first and best economic response."

Key policies were developed by a small group of decision-makers and
advisers, with little scope for broader input. Administrative
processes meant health advice was prioritised (e.g. Ministry of Health
getting the last word), particularly in the early days. There were
many areas where outcomes could have been improved, had broader
perspectives been sought or considered, e.g. establishment of regional
boundaries, drafting of legislation and regulations, implementation of
PPE requirements, and the vaccine rollout.

Some elements of good practice were retained throughout the pandemic
response, including efforts to provide transparency in
decision-making. Cabinet papers were routinely released, as well as
continuing to meet obligations under the Official Information Act. The
innovation and dedication of individuals across the public service
enabled the delivery of New Zealand’s response, often at great
personal cost. This enormous effort has been well recognised – both
nationally and internationally.

From April 2020 until October 2021, Aotearoa had a strategic goal of
elimination. During most of this period the pandemic response was
viewed as coherent and effective, with a clear sense of the overall
public health goal and the actions needed to support it. Between April
2020 and September 2021, there was an implicit assumption that the
elimination strategy would remain in place until population-level
immunity could be achieved through vaccination.

[also explicitly mentioned in a couple of 1pm briefings]

When the tools supporting the elimination strategy were struggling to
eliminate Auckland’s Delta outbreak starting August 2021, there was no
‘Plan B’ or threshold at which to move to suppression.

[I stand firmly in the "keep the elimination strategy" camp]

Delta was still the dominant strain in October 2021, and the world had
not yet learnt about Omicron, with much higher transmission
rates. Despite this, the lack of well-integrated advice on, and an
agreed plan for, a post-elimination strategy is surprising. The
tendency to prioritise lowering risk of viral transmission to as close
to zero as possible made it difficult for decision-makers to consider
whether balancing health benefits and wider social and economic costs
continued to be appropriate.

"There was no bump free pathway to get from elimination to life as
normal.... We had everyone focused on ‘right now’. We really needed to
think beyond the horizon – we didn’t nail that." - Chris Hipkins

[There is no "life as normal" while the pandemic still envelopes us]

There was criticism of how quickly the approach changed once the
elimination strategy was abandoned, leaving many – especially those
with disabilities, chronic health conditions, and compromised immune
systems – feeling vulnerable. It was not well understood or
communicated that an elimination strategy was always going to be
time-limited, and that high levels of infection might be an
unavoidable part of the exit.

During the elimination phase of the pandemic, there was a tangible
sense of solidarity among many communities during the first lockdown,
and a high degree of compliance with its conditions. Aotearoa’s
‘empathetic communication’ during the pandemic and the Prime
Minister’s high degree of public engagement were highlighted as an
example of best practice by the OECD in 2021.

The fact that the All-of-Government Public Information Management Team
did not have specific responsibility for community engagement became
an issue as the response moved beyond the early days of the
pandemic. As time passed, government objectives shifted to a range of
restrictions and caveats to protect public health. This involved a
raft of new policy settings that changed frequently and were more
complex than the relatively blunt tools of the early lockdowns.

It was previously unheard of for government press conferences to
generate high public viewership. On occasion, journalists were
subjected to public anger or criticism for their questions, sometimes
for being too critical, sometimes the opposite. The lack of ethnic
diversity amongst the key spokespeople at the 1pm podium was seen as
problematic. Failing to reflect the diversity of New Zealand’s
demographics became an issue for communities, and whether they felt
included in ‘the team of five million’. Many Māori and Pacific
communities wanted to see their own leaders on the podium and felt
this would have helped their communities to feel more engaged, which
in turn would have had a positive impact on compliance.

Despite considerable efforts by many, the constantly changing
requirements and messages were hard to keep up with. This was
exacerbated by a lack of timely, accessible information resulting in
‘information voids’ that were filled by other sources. Conditions were
ripe for the spread of misinformation and disinformation going into
the pandemic. During the pandemic there was a marked increase in the
volume, diversity of topics and tenor of disinformation circulating on
topics related to COVID-19. Those who are already marginalised and
with low trust in government (including Māori) are most susceptible to
disinformation. Fostering and maintaining trust and social cohesion is
key to countering its impacts.

## Chapter 3 - Lockdowns

The first reported case of COVID-19 in Aotearoa was detected on 28
February 2020. This delay meant Aotearoa had an important opportunity
to assess what was happening in other countries before taking
action. The rationale for New Zealand’s lockdown – to break chains of
transmission – soon diverged from the rationale in most other parts of
the world, where lockdowns were used to keep transmission down to an
‘acceptable’ level that didn't overwhelm health services.

As the need for lockdowns had never been anticipated or prepared for,
Aotearoa had no apparatus in place for an all-of-society lockdown
ahead of the COVID-19 pandemic. The whole public sector was operating
without a playbook, as was everyone in Aotearoa.

‘Lockdown’ was used by Prime Minister Jacinda Ardern in a press
release announcing the first lockdown. It emerged in global use to
describe combinations of public health measures that heavily curtailed
people’s movement in the interests of stopping the virus. Levels 3 and
4 of New Zealand’s Alert Level System can be understood as ‘soft’ and
‘hard’ lockdowns, respectively, because they required people to stay
at home, closed schools and businesses, and involved heavy
restrictions on public gatherings.

Aotearoa spent 7 weeks in Alert Level 3 and 4 lockdown between March
and May 2020. The need for lockdown-like conditions was anticipated
when Prime Minister Jacinda Ardern announced the Alert Level System on
21 March 2020. From there, things moved quickly. At Alert Level 4,
everyone was instructed to stay at home in their household ‘bubble’
other than for essential personal movement. Gatherings were cancelled;
public venues were closed, as were all businesses other than those
recognised as essential services.

From a peak of around 1,000 cases two weeks after the first recorded
case, case numbers began to fall. Given the context and other
available options, lockdown conditions were a key component in the
success in eliminating transmission of the virus in Aotearoa. In the
last week of April 2020, the whole country moved down to Alert Level 3
– a ‘softer’ version of lockdown, with less stringent conditions than
Alert Level 4.

The decision to move was informed by the growing costs of Alert Level
4 restrictions. Also, the advice was that undetected community
transmission was unlikely, there was sufficient testing capacity and
capability, strong processes for managing outbreaks in high-risk
settings, robust border measures, and sufficient health system
capacity. At Alert Level 3, people expanded their immediate bubble to
connect with whānau, bring in caregivers, or support isolated
people. Schools re-opened with voluntary attendance. Businesses could
open if they could operate without physical contact with customers.

Case numbers continued to fall, supported by the public’s high
compliance with restrictions and the progress made in scaling up key
public health systems. The first lockdown ended on 13 May 2020 when
the whole country moved to Alert Level 2.

Responsible agencies generally took a principles-based, ‘light touch’
approach to enforcing lockdown rules where possible. Police had
significant enforcement powers, but prosecuted only the most serious
and persistent breaches. The ‘4 Es’ (engage, educate, encourage,
enforce) became the basis of a wider All-of-Government Compliance
Response to support a collaborative and consistent approach to
compliance and enforcement for the COVID-19 response. The dominant
messaging was to ‘be kind’, but there was high public interest in
perceived breaches of lockdown rules. Police launched an online tool
where people could report suspected rule violations. >80,000 reports
were received over the next month.

Iwi and Māori and many communities of different kinds stepped up
during the first Alert Level 3 and 4 lockdowns to provide essential
local leadership, support each other, and address local
needs. Community organisations, both formal and informal, were also
well placed to meet unseen needs such as for fellowship and
connection. Many people trusted community organisations to sort,
curate, and act as conduits for reliable information.

Iwi and Māori played a significant leadership role in mobilising their
communities and mitigating the negative impacts of lockdown. Iwi, hapū
and marae developed plans that adapted tikanga and kawa to the
challenges presented by physical isolation. Māori leaders told us that
iwi and Māori ‘policed themselves’ to follow the rules. While Māori
unfortunately are over-represented in Police enforcement action - also
the case during the pandemic - this happened at significantly lower
levels than usual.

Iwi and Māori in Tairāwhiti, Bay of Plenty, Taranaki and Te Tai
Tokerau stood up checkpoints to limit the movement of people and
control the spread of COVID-19, just as they had done a century before
to control the 1918 flu. Almost 50 roadside checkpoints were
established, resourced and led by iwi and Māori, staffed by
volunteers, and often operated in partnership with NZ Police. They
were also valuable for passing on information, and created to a sense
of trust between groups.

Cabinet adopted four principles to guide the process of identifying
essential services: prioritising public health, allowing the
Government to scale-up the response, ensuring the necessities of life,
and maintaining public health and safety.

Formal use of the essential services category ended when the country
moved to Alert Level 3 on 27 April 2020. It was replaced by a
requirement to meet specific, strict requirements for physical
distancing, contact tracing, and contactless delivery. Workers who
were not employed by essential services switched to working from home
during lockdown. Businesses, schools and community organisations
rapidly switched to remote ways of working and connecting. This was
critical to the successful use of lockdowns.

COVID-19 catalysed public sector agencies and workforces to switch to
remote working. Many thousands worked from home, and continued to
deliver essential public services. In some cases this involved a
near-total overhaul of their operating model. The experience across
the lockdowns varied greatly depending on individual circumstances,
temperament, family situation, income, safety and stage of life. There
were myriad experiences, both positive and negative.

Central government agencies worked quickly with community housing
providers, social services, iwi and other Māori organisations, and
local government to provide temporary and emergency housing support
for people who were in insecure housing or rough sleeping.

Case numbers continued to fall at Level 2 and by 8 June 2020 when
Cabinet met, there were no active community cases left. The country
moved to Alert Level 1 on 9 June 2020 based on the high confidence
that COVID-19 had been eliminated from Aotearoa. On 11 August 2020,
after 102 days without community transmission, four people tested
positive for COVID-19 in Auckland. Ministers moved Auckland into an
Alert Level 3 lockdown, and the rest of the country into Alert Level
2.

A temporary "Level 2.5" step was later introduced to Auckland, was
place for three weeks and was not used again in the pandemic. By 8
October 2020, the whole country was back at Alert Level 1. For a few
weeks in February and March 2021, Auckland again moved in and out of
Alert Level 3 lockdown several times. The total time spent in Level 3
lockdown was relatively brief, but came at a disruptive time at the
beginning of the school year.

In June 2021, Wellington spent nearly a week at Alert Level 2 after a
visitor from Australia tested positive following a short but busy
weekend in the city.

Aside from these short-lived regional disruptions, 2021 saw Aotearoa
New Zealand remaining largely out of lockdown until the arrival of the
Delta variant in August.

Ooh, another graph!

{COVID-19 cases and periods under lockdown/restricted settings
(February–March, June and August–December) in 2021. 16 different alert
level and protection policy changes are indicated in the graph, about
half of them around March 2021, and the remainder sprinkled around
from August 2022.}

On 17 August 2021, a community case was detected in Auckland.

Officials assumed (correctly) that the case was the highly
transmissible and more virulent Delta variant. The Prime Minister
announced an imminent Level 4 lockdown for the whole country. For the
second half of August 2021, New Zealanders returned to settings and
experiences that were largely familiar from the first Level 4
lockdown. Many people and organisations were better placed to respond
with existing systems to manage in lockdown. After two weeks, all
regions except Northland and Auckland moved down to Alert Level 3,
with Northland following a few days later. On 7 September 2021, all
regions except Auckland finally moved out of lockdown to a
strengthened Alert Level 2.

By 4 October 2021, Auckland had spent 49 consecutive days in either
Alert Levels 3 or 4. The public health risk remained, but cabinet
decided the country would transition away from the Alert Level System
to the new COVID-19 Protection Framework.

[I'm going to skip over this bit, because it doesn't gel with my own
opinion on what happened. My version of
[p.114](https://www.covid19lessons.royalcommission.nz/reports-lessons-learned/main-report/)
is that Cabinet ignored public opinion and rational health advice, and
steamrolled the traffic lights despite the increased threat of Delta.]

In seeking to balance health concerns against economic concerns,
officials developed proposals to modestly relax Alert Level 3 settings
in Auckland in a staged manner: Level 3.1 from 5 October 2021, then
‘Level 3.2’ on 9 November 2021. Level 3.3 was never activated. On 2
December 2021, Cabinet adopted the ‘minimisation and protection’
approach and Aotearoa moved to the ‘traffic light’ system to manage
the COVID-19 response.

Auckland and several other regions were put at the ‘Red’ level.

Auckland was technically out of ‘lockdown’, although some significant
restrictions remained in place. In total, Auckland spent more than six
months at Alert Level 3 or 4, compared to 74 days for most of the rest
of the country.

There was widespread criticism in our public submissions of the
duration of the Auckland lockdown.

The Government was also criticised for having transitioned to the new
‘traffic light’ system before the 90 percent vaccination goal had been
reached.

In the Haumaru report, released in late 2021, the Waitangi Tribunal
found the Crown had breached te Tiriti o Waitangi principles in its
decision to transition to the COVID-19 Protection Framework without
meeting the original vaccination threshold.

Regional boundaries, while valuable, were hard to implement –
particularly at short notice and with no prior preparation across the
system. These timing and preparedness issues caused many challenges
for communities, businesses, workers and enforcement.

There were also unique pressures for Northland from the regional
boundaries which saw them cut off from the rest of the country, apart
from limited channels through Auckland. Some in Northland felt
forgotten or overlooked and lumped into Auckland’s mess.

Distress at not being able to visit a dying loved one in a rest home,
hospice or hospital care was one of the most recurring themes in our
public submissions and engagements we held with bereaved families.

Aotearoa’s use of lockdowns during the COVID-19 pandemic, while
stricter than many countries, was comparatively sparing in terms of
time spent in lockdown conditions. Few countries avoided using
lockdown-type measures as part of their COVID-19 response. Taiwan
managed to eliminate COVID-19 transmission in 2020 without a lockdown
by mounting a rapid and highly effective public health
response. Several Pacific Island nations – including Samoa, Tonga, the
Solomon Islands, Tuvalu and Tokelau – protected their populations by
closing their borders before any cases of COVID-19 had reached them
(i.e. an exclusion strategy).

If Aotearoa had benefited from similar investment in key public health
tools, capacity and capability as Taiwan, it might have been possible
to eliminate COVID-19 transmission early in the pandemic with less
reliance on lockdowns.

Deciding when to end lockdowns was extremely
challenging. Decision-makers had to balance the aim of protecting
people from COVID-19 against the growing social and economic impacts
of requiring large parts of the population to remain under tight
restrictions.

Regarding the Delta outbreak and late-2021 Auckland lockdowns,
international evidence was emerging that showed vaccine-related
protection from COVID-19 transmission started to wane some weeks
following vaccination. Officials were aware of waning transmission
from Delta; waning immunity was included in models from January 2022
to help inform decisions about management of the Omicron
outbreak. This immunity was not included in modelling prior to January
2022.

While some senior ministers we spoke to thought that, in hindsight,
the last round of Auckland lockdowns perhaps went on too long, others
felt that the need to protect equity in health outcomes meant they
could not have made any other decision. 

There was confusion and frustration around the ‘essential services’
designation, which some felt was discriminatory and unfairly
harsh. the scheme did not allow ‘safe’ work where there was little
risk of viral transmission (e.g. working alone outdoors). Some
businesses misused the ‘essential service’ designation to require
staff to be onsite when this was not necessary or appropriate under
Alert Level 4 conditions. Unions told us this was a common problem.

Workers in essential services continued to go to work during lockdown,
putting themselves at risk of exposure to the COVID-19 virus, and
sometimes taking extraordinary measures to protect their
families. Some essential workers were celebrated and praised for their
efforts and sacrifices during the pandemic. Others – or even the same
workers at different times – faced abuse, anger, fear, discrimination
or distress from the public.

The impact of lockdowns was felt by small businesses, with many
sectors impacted, including tourism, retail, hospitality, personal
services and trades. Despite government support measures, many small
businesses faced challenges as to their future viability. Women and
children at risk of violence due to the heightened stress had reduced
opportunities to seek support. Disabled people and older people
relying on in-home care faced significant challenges in maintaining
adequate levels of care. During the Alert Level 4 lockdown in 2020,
women were more likely to report a significant increase in caring
demands. These effects were felt by a range of women who picked up
additional care responsibilities in their household during the
pandemic.

There were also benefits from the increased flexibility of working
from home, the availability of new digital tools for work, connection
and collaboration, and the normalisation of hybrid work.

People in informal and precarious work were hit hard. For some,
informal work supplements their main income, while for others, it *is*
their income.

Little consideration for income protection was given to these issues.

The elimination strategy had served Aotearoa students well in terms of
minimising the interruption to their education. Students missed fewer
days of school instruction in 2020, with the third lowest number of
days closed in the OECD. Despite *relatively* good impact, disruption
to education for students in Aotearoa still had a significant and
negative impact – particularly for Māori and Pacific students, those
from lower socio-economic backgrounds, and likely for students in
Auckland.

Auckland – especially South Auckland – did it tough. The cumulative
impacts of repeated lockdowns on Aotearoa New Zealand’s largest city
were multifaceted, encompassing economic, mental health and wellbeing,
educational outcomes and social cohesion.

[Might change this bit to copy my skeets]

## Chapter 4 - Border Restrictions and Quarantine

Technically, Aotearoa New Zealand’s borders did not in fact ‘close’,
neither then nor later. No legal mechanism was ever in place
preventing people or goods from arriving: planes continued to land and
ships to dock (apart from cruise ships) throughout the pandemic. A
changing combination of immigration settings and public health
regulations – particularly the requirement to quarantine in a
designated facility – meant that, for all practical purposes, most
non-New Zealanders could not enter the country for two years.

New Zealand citizens and residents, whose legal right to enter was
never extinguished, had varying responses to these restrictions. While
some were supportive, others felt as if the border had closed to them
too.

While we consider these measures were effective in stopping the virus
from entering the country, and limiting its spread when it did, we
also recognise the social, economic and personal costs were very
high. How those costs might be mitigated in a future pandemic is
something we return to in our lessons for the future and
recommendations.

From early 2020, Aotearoa New Zealand began amending the Immigration
Instructions to refuse entry to people from certain countries, due to
soaring COVID-19 case numbers in certain overseas locations. These
instructions were specifically to manage the COVID-19
outbreak. However, temporary bans on foreign travellers from high-risk
destinations did not succeed in stopping COVID-19 from reaching
Aotearoa New Zealand. The COVID-19 virus began spreading in the
community between early and mid-March 2020. At that point, events
began moving very quickly.

Over a 12-day period, New Zealand’s first COVID-19 case was reported,
COVID-19 became a quarantinable disease under the Health Act 1956, and
the World Health Organization declared a global pandemic. On 19 March
2020, Cabinet agreed that the country’s borders would be closed to
everyone except New Zealand citizens and residents (with case-by-case
border exceptions granted in other specific cases). By midnight, these
tight border restrictions had come into effect

For the next two years, the border remained effectively closed to
everyone except those qualifying for ‘border exemptions’:

* Aotearoa citizens and permanent residents, along with their
  partners, guardians and children
* Australian citizens and permanent residents ordinarily resident in
  Aotearoa
* air and cargo ship crew
* diplomatic and consular staff.

The rupture in the country’s connections with the world had major
impacts on people’s lives and on many sectors of the economy –
including international education, tourism and hospitality – and
created labour shortages in industries relying on temporary and
migrant seasonal labour.

Over the course of the pandemic, policies and measures for controlling
the border – and MIQ (managed isolation and quarantine) – were
constantly reviewed, adjusted and added to as circumstances changed,
globally and domestically.

Work on options for reopening the border began early in the pandemic,
and there were short-lived periods of quarantine-free aviation travel
with parts of Australia and the Pacific in the first half of 2021. In
mid-2021, Cabinet signalled that it was time to plan to progressively
change New Zealand’s border settings.

Omicron significantly impacted this planned approach to reopening the
border. The first step towards reconnection – whereby vaccinated New
Zealanders could enter the country from Australia without going into
MIQ – took effect on 28 February 2022.

On 31 July 2022, Aotearoa New Zealand’s borders fully reopened to all
travellers and visa-holders from anywhere in the world. All quarantine
and isolation requirements were removed.

Throughout the pandemic, people wanting to enter Aotearoa New Zealand
who were not citizens or residents could be granted a border exception
in specific circumstances. By 28 May 2020, 11,842 people had expressed
interest in obtaining an exception to border restrictions across the
five categories. The bar was high: of those applying, only around 20
percent (2,354 people) were deemed to meet the criteria.

Over the course of the pandemic, those granted border exceptions
ranged from dairy workers, Recognised Seasonal Employer scheme
workers, agricultural machinery operators, silviculturists and
shearers to veterinarians, teachers, technology sector specialists and
auditors.

Processing of visa applications of all kinds came to a near halt from
19 March 2020, although people could still lodge some applications
online. Offshore visa processing offices closed. Operating at
significantly reduced capacity, Immigration New Zealand stopped
accepting or processing applications for all temporary visas from
offshore, such as those normally available to students and visitors.

As the border progressively reopened in 2022, normal visa pathways and
processing gradually resumed. With the end of the border exemption
regime, the Critical Purpose Visitor Visas were gradually phased
out. Applications for all work visas reopened on 4 July 2022, and for
student and visitor visas at the end of that month.

The first international arrivals to enter a managed facility due to
the risk of COVID-19 were repatriated New Zealanders from Wuhan: they
spent 14 days quarantining in campervans at a military training centre
in Whangaparāoa in February 2020.

As the global pandemic accelerated, Aotearoa New Zealand’s quarantine
arrangements became increasingly stringent:

1. 16 March 2020 - Arrivals from all countries expected to quarantine
at home for 14 days.

2. 19 March 2020 - Arrivals required to quarantine at home.

3. 9 April 2020 - Everyone arriving by air (except aircrew and
diplomats) to quarantine at a designated facility.

The government-run MIQ system, within three months, saw all incoming
travellers being accommodated in 32 hotels across five regions. Until
late 2021, most people in the community who tested positive for
COVID-19 also completed their required 14 days of isolation in MIQ
facilities.

Permission to leave MIQ was rarely granted. Pilots, flight crews and
marine crews flying in to join their ships were exempt from quarantine
to help keep supply chains and transit routes open.

There was no way of knowing how many of New Zealanders temporarily
overseas would return home and need to stay in MIQ facilities, making
it very difficult for officials to assess the capacity required or
likely demand. This difficulty was compounded by the absence of
regular patterns to arriving flights and the delay or cancellation of
many scheduled flights.

By July 2020 conditions at MIQ facilities had changed since the start
of the pandemic, particularly after security breaches at some
hotels. They now had an increased police presence and extra security
staff.

Arrival numbers fell from a peak of 6,000 people in July 2020 to
around 5,000- 5,500 people per fortnight from August 2020, leaving
some MIQ capacity unused. Also, the Government could now require
people to register to enter an MIQ facility before they arrived in the
country. This paved the way for the online Managed Isolation
Allocation System (MIAS), introduced on 5 October 2020. From 3
November 2020, travellers were legally required to have an MIQ voucher
before flying to Aotearoa New Zealand.

Decisions about emergency allocations were some of the most fraught
aspects of MIQ, and the criteria were amended through the pandemic in
response to changing needs and experience.

By 19 January 2021, 100,000 returning New Zealanders and other
eligible people had entered MIQ facilities (including critical workers
and others qualifying for a border exception). For the next six
months, demand for MIQ declined.

From May 2021 onwards, repeated COVID-19 outbreaks in Australia saw
quarantine- free travel with specific states paused. On 23 July 2021,
amid concerns about growing outbreaks of the Delta variant in
Australia, the trans-Tasman bubble was suspended completely. MIQ
capacity had to be made available for New Zealanders returning from
Australia, reducing the number of rooms available for other
travellers.

In July and August 2021, officials and industry experts again examined
alternatives to ‘first come first served.’ This time, they recommended
a ‘virtual lobby’ system on the basis this would meet requirements in
terms of cost, speed of implementation (a solution was needed
urgently), and transparency for travellers.

The Ombudsman would later question the approval of a virtual lobby in
his 2022 review of the MIQ allocation system, since the virtual lobby
was unable to prioritise travellers on the basis of need.

Quarantine and isolation settings changed with the transition from the
Alert Level System to the COVID-19 Protection Framework (the ‘traffic
lights’) in December 2021, and again when the Omicron variant arrived
in Aotearoa New Zealand.

Once Omicron was circulating in the community, international arrivals
no longer posed a greater risk of COVID-19 transmission than anyone
else; thus, the border restrictions and MIQ were no longer justified
from a public health perspective.

From 28 February 2022, vaccinated New Zealanders and other eligible
travellers from the rest of the world were permitted to travel to
Aotearoa New Zealand without entering MIQ. From 5 March 2022, they
were no longer required to self-quarantine at home. On 3 May 2022, the
Government removed the requirement of the COVID-19 Public Health
(Maritime Border) Order (No 2) 2020 for people arriving by sea to
enter MIQ.

Border controls and quarantine/isolation requirements were two of the
four key pillars supporting the Government’s elimination strategy. As
we have noted elsewhere, the elimination strategy was highly effective
in containing COVID-19 transmission until most of the population was
vaccinated.

While the managed quarantine system was effective in keeping COVID-19
out of Aotearoa New Zealand, there were occasional breaches. In the
year to June 2021, researchers identified 10 instances where COVID-19
was transmitted from someone in a quarantine or isolation facility to
a border worker or (occasionally) the wider community, and an outbreak
occurred.

The quarantine system successfully prevented the vast majority of
COVID-19 infected travellers from seeding infection into Aotearoa New
Zealand.

Allowing New Zealanders to return while protecting those already here
was a difficult trade-off for the Government to manage. As noted by
the authors of the quarantine escape study, ‘The most direct way to
substantially reduce the risk of SARS-CoV-2 escaping quarantine [was]
to reduce the number of arriving travellers from areas with high
infection levels’. But limiting citizens’ return travel raised complex
ethical, human rights and legal issues, and created significant
distress for those affected.

The operation of quarantine facilities was costly and required the
support of a large workforce – covering transport, hospitality,
security, cleaning, catering, health care, operations and
logistics. Using hotel facilities (which would otherwise have been
largely empty) was more cost-efficient initially than building bespoke
quarantine facilities, while the location of hotel facilities near
Aotearoa New Zealand’s international airports had practical
advantages.

While border controls and quarantine and isolation requirements were
an essential part of the elimination strategy, we saw evidence that
the social, economic and personal costs of these measures were very
high.

Closing the international border and setting up a nation-wide managed
system for quarantine and isolation were extraordinary undertakings –
unprecedented, and indeed almost unimaginable before March 2020. The
fact that these measures were implemented so swiftly, and provided
such a robust line of defence during the elimination phase, is
commendable.

There was a constant need for speed and agility. Border arrangements
continued to be monitored and rapidly adjusted as the pandemic changed
course, locally and overseas: between January 2021 and October 2022,
for example, around 58 changes were made to air border settings alone.

Even if health officials are not experts when it comes to managing the
border, infection control and health system expertise will remain
essential when decisions are made about using border restrictions in
another pandemic. That expertise is vital if those restrictions are to
successfully prevent the virus from spreading within quarantine
facilities and escaping into the community.

Whether and when most people could enter Aotearoa New Zealand was
ultimately determined by MIQ capacity, not by border settings. It is
therefore unsurprising that MIQ capacity, and the mechanisms the
Government used to manage and prioritise demand, became highly
contentious issues. Demand was higher than the system’s capacity, and
options to increase the availability of MIQ places were limited.

In a judicial review application to the High Court, Grounded Kiwis
challenged aspects of the MIQ system, which they said operated as
unjustified limits on citizens’ right to enter New Zealand. The High
Court held that while the system did not in and of itself amount to an
infringement of New Zealanders’ right to enter their country, the
evidence indicated that at least some New Zealanders had experienced
unreasonable delays in exercising their right to enter.

[starting updated "no historical information" summary policy]

MIQ facilities were not purpose-built for quarantine, creating a range
of problems for staff and users. The hotels were ‘not optimally
configured to manage separation of returnee flows on entry, exit and
inside the building’, and security and ventilation systems needed
remediation.

Despite all the challenges, it is clear that hotels were made to work
as quarantine facilities. The evidence shows that the MIQ system
learned from its mistakes and the frequent reviews of its operations,
and made improvements in response. For example, following some
well-publicised instances of people spreading COVID-19 into the
community after leaving MIQ, procedures were strengthened to ensure
thorough cleaning and ventilation, and steps were introduced to
minimise the risk of guests becoming infected after their final test
(required on day 12 of their 14-day stay).

Accommodating community cases in MIQ began to create significant
operational and governance challenges for the MIQ system. It was not
in fact a new development – providing accommodation for positive cases
who could not safely isolate at home had always been one of the
functions of MIQ.

MIQ had worked well as a border intervention: ‘We knew our swim lane,’
said one interviewee. Now, ‘the most vulnerable and unwell people are
being triaged into MIQ by medical officers of health’, placing
pressure on a system not designed for people who were presenting with
‘vulnerabilities, health concerns, addictions or violent
behaviour’. There was no over-arching all-of-government plan to help
the system adapt to its community care role, and gaps in governance
were evident, interviewees reported.

Responsibilities for the MIQ system were split between the Ministry of
Health and the Ministry of Business, Innovation and Employment. The
lack of a clear point of responsibility for the overall COVID-19
response expose the challenge of coordinating the response and
planning at a system level.

Restrictions on who could enter Aotearoa, and compulsory quarantine at
the border, were key to the success of Aotearoa's elimination
strategy. Both measures undoubtedly saved lives and reduced the burden
on the health system.

While setting up new border processes and MIQ quickly was a
significant achievement, both systems had significant
shortcomings. Border restrictions and MIQ took a significant toll on
Aotearoa New Zealand, particularly because demand for MIQ spaces
outstripped capacity and because of the length of time restrictions
were in place for.

## Chapter 5 - The Health System Response

Aotearoa’s health and disability system was already facing multiple
pressures going into the pandemic:

* money
* capacity (staff, facilities, beds)
* communication
* racism

Authors of a report delivered on 16 March 2020 acknowledged that the
absence of any social measures was unrealistic – people would change
their behaviour on a voluntary basis, even if no mandatory measures
were introduced – but the modelling was intended to give a sense of
the potential health impacts for different levels of infectiousness.

[In hindsight, I disagree with this]

Pooling of PCR tests was used effectively through 2020 and 2021, but
started to become problematic in early 2022 when the arrival of the
Omicron variant led to widespread community infection. Community case
numbers soared, severely straining laboratory capacity.

It’s now well established that mask wearing can reduce the spread of
respiratory infections like COVID-19. Wearing masks not only protects
people during one-to-one encounters, but also lowers the overall
spread of respiratory viruses in the community.

We were unable to ascertain what number of ventilated patients the
health system could have surged capacity to care for, had COVID-19
case numbers and hospitalisations dramatically increased. Certainty
about surge capacity will be important for future pandemics

There was no national infection prevention and control capability at
the start of the pandemic. The initial absence of such guidance, and
later its frequently changing nature, created additional pressures
during an already very challenging period.

Considerable efforts were made to preserve wider health system
capacity and ensure the health workforce was available to deliver
necessary and ongoing care. Steps were taken throughout the pandemic
to minimise system capacity disruptions and ensure that health workers
could keep coming to work.

Iwi and Māori health providers quickly adapted, developing new models,
and taking a holistic and flexible approach to ensure their
communities had ongoing access to essential services, including
healthcare. Māori got the “why” of the protection measures and
mobilised rapidly – sometimes ahead, sometimes more rigorously than
the national response.

While some technical barriers affected transmission between general
practitioners (GPs) and pharmacies, the move to e-prescribing was
overall a ‘superb’ example of how the health system can make
significant changes when it identifies priorities and steps into
action. GPs had been talking about the barriers to e-prescriptions for
15 years, then it happened in 48 hours.

Whakarongorau Aotearoa, the National Telehealth Service, was a
pandemic success story. Factors in the service’s success were
described as its ‘scalability’, its strong pre-existing relationship
with the Ministry of Health and the Auckland Regional Public Health
Service, a high-trust contracting model, its use of remote technology
that allowed people to work from home, and to rapidly recruit, train
and surge their workforce.

Continuity of cancer services was maintained throughout the pandemic
period. In fact, the Health Quality and Safety Commission reported
that new cancer registrations actually increased by five percent in
2021 (compared with 2018/19). There were also positive equity trends
in the provision of some services, and increased rates of diagnostic
procedures for Māori.

[Aside: My Mum was represented in the early cancer detection
statistics during 2020/2021. Unfortunately, she had an aggressive
cancer (DLBCL), which combined with a bad first chemotherapy
experience, and she didn't want more when the cancer came back.]

Aotearoa New Zealand’s health system was not overwhelmed, and most
people – especially vulnerable groups – were well protected from
COVID-19. New Zealand’s COVID-19 response was highly effective at
protecting public health, preventing the health system from being
overwhelmed, and minimising unequal health impacts for disadvantaged
or vulnerable populations, including Māori.

 While the lockdowns of early 2020 and late 2021 were highly
disruptive, they also ensured that case numbers were very
low. Following the initial success of the first national lockdown in
2020, community transmission was successfully re-eliminated in August
of that year.  Not until the arrival of the Delta variant in August
2021 did it became re-established – and even then, case numbers,
hospitalisations, and deaths in this period were very low – barely
visible compared with what came later in 2022.

When COVID-19 transmission did eventually become widespread in
Aotearoa New Zealand, the population had high levels of immunity from
vaccination. Not only did this protect many people from developing
severe illness when infected with COVID-19, it also meant that New
Zealand’s health system was never overwhelmed.

New Zealand’s COVID-19 hospitalisations peaked in March 2022 at just
under three admissions per 100,000 population per day. There were
challenging moments for New Zealand’s hospitals, but the system was
largely able to absorb these peaks.

{COVID-19 cases, hospitalisations and deaths per 100,000 population
(7-day rolling average) in Aotearoa New Zealand, 2020–2022. These
statistics are essentially flat lines along the bottom of the graphs
until March 2022, where there's a sharp rise (to 400 cases, 3
admissions, 0.3 deaths), then a peaky decrease down to a base level
near 100/50 cases, 1 admission, and 0.2/0.1 deaths).}

Aotearoa’s hospitalisations and deaths from COVID-19 have been much
lower than those seen in countries where the first waves of infection
occurred before vaccination.

The elimination strategy offered the best protection for the
population as a whole, and greater protection for Māori, Pacific
people, older people and medically vulnerable people than would have
been possible with either a suppression or mitigation strategy.

The story is complicated, however, because these groups *did*
experience more severe impacts from COVID-19 than the general
population. Severe illness from COVID-19 was more common in less
privileged ethnic and socioeconomic groups, who were more likely to be
hospitalised and to die from their illness.

Although the 2021 Delta outbreak had a disproportionate impact on
Māori and Pacific communities, most Kiwis (including Māori and Pacific
people) were not exposed until the less virulent Omicron variant was
circulating. By this time, most had been vaccinated.

[Aside: I don't think it's correct to call the Omicron variant "less
virulent", except with the caveat that the world was overall more
vaccinated. That categorisation was a hypothesis that spread round the
world before it could put its boots on.]

Aside from early clusters, aged care facilities were highly effective
in protecting their residents from COVID-19 and Aotearoa saw
significantly fewer aged care deaths during the pandemic, compared to
other countries.

The overall absence of severe outbreaks in Aotearoa’s aged residential
care facilities was a major success story of the pandemic. This was
not without harm for residents who lived through long periods of
limited contact with their loved ones.

Senior DHB leaders told us in direct engagements that strict visitor
policies were one of the hardest public health protection measures for
them to manage. Some told us that these restrictions affected the
provision of care to patients, relationships with family and whānau,
and expressed a view that they were too restrictive, especially when
people were dying and unable to have whānau present. We heard the view
that while well-intended, the personal consequences – and, in some
cases, trauma – caused by such restrictions will be enduring for many
families. This view was also evident in the public submissions we
received.

Aotearoa’s health system response is simultaneously a story of
remarkable success at protecting public health (via the elimination
strategy) and a cautionary tale of potentially disastrous pandemic
impacts that were narrowly averted.

Some of the pressure points that COVID-19 revealed in New Zealand’s
health system – including workforce issues, ageing infrastructure,
pandemic readiness, regional inconsistencies and underlying health
inequities – are assessed below. They presented some significant
risks; while not all of them were realised during the COVID-19
pandemic, Aotearoa New Zealand may not be so fortunate next time.

The lack of diagnostic testing options outside of PCR tests
(e.g. Rapid Antigen Tests) caused frustrations for many, including
business representatives who told us that testing options which
returned rapid results should have been available sooner.

[Aside: In 2020 I was very slowly working on an alternative fast
sequencing-based test for COVID. The small amounts of funding /
support I had for that dried up after the country shifted back to
Level 2, and I once again got loaded with other non-COVID work]

More effective and efficient COVID-19 testing would have been achieved
if there had been more pragmatic use of alternatives to PCR tests
(alongside PCR testing, when higher accuracy was needed) and earlier
planning for the rollout of RAT tests. In planning for a scenario with
a highly vaccinated population and less reliance on stringent public
health measures, the benefits of RAT testing should have been seen in
advance as outweighing their lower accuracy, and planned for by
ordering and stockpiling tests in advance of when they needed to be
deployed.

The COVID Tracer App was a ubiquitous part of many New Zealanders’
experience of the pandemic. We heard through public submissions that
many people found the app easy to use and a useful reminder to be
conscious of COVID-19 precautions.  However, the app may not have been
as useful for contact tracing as was envisaged.

The Ministry of Health was able to rapidly establish the National
Close Contact Service and evolve this service as the pandemic
progressed.  But with better preparation, Aotearoa New Zealand could
be more confident that such a system can be quickly scaled-up, and be
effective, in another pandemic.

Aotearoa had limited intensive care capacity going into the
pandemic. Evidence available to our Inquiry suggests this was not
substantially increased during the first two years of the COVID-19
response.

Aotearoa never experienced the dramatic peaks in illness that
overwhelmed hospitals in many other countries. We were therefore
fortunate that the absence of meaningful expansion in intensive care
capacity in 2020 and 2021 did not limit our pandemic response.

The health workforce is in a worse position now than before COVID-19,
as a direct result of pandemic pressures. While this situation is not
unique to Aotearoa, it is nevertheless serious, for many reasons – not
least for future pandemics. We heard from several stakeholders that
many DHBs held themselves at ‘red’ for long periods. We heard it
called a ‘misuse’ of the framework, while others expressed the view
that too many services were cancelled, for too long.

We have not been able to find evidence that the Ministry of Health
actively monitored the impacts of the COVID-19 response on provision
of non- pandemic care. The Ministry did not publish any follow-up
reports on healthcare disruption after an initial one following the
first COVID-19 outbreak.128 We were also unable to find evidence that
the Ministry sought to change guidance to DHBs or to increase
prioritisation of non-pandemic care in the COVID-19 response.

Aotearoa’s health system was not well prepared for a pandemic of the
scale and duration of COVID. The elimination strategy was highly
effective in preventing the health system from being overwhelmed and
protecting vulnerable groups, but with notable costs.

The pandemic exposed some key vulnerabilities and pressure points in
our health system:

* low baselines
* dated infrastructure
* sustained capacity increases
* a substantial toll on healthcare workers

Provision of non-COVID-19 care was substantially disrupted during the
pandemic, to a greater extent than was necessary. This happened even
between alert level increases, when there were no known positive cases
in the country.

## Chapter 6 - Economic & Social Impacts and Responses

The strict public health measures, especially the border closure and
national lockdowns, were essential to protect the economy and society
from the immediate and devastating effects of the pandemic.

The pandemic also highlighted or exacerbated many existing social
challenges – including unaffordable housing, high rates of mental ill
health, long-standing inequities for Māori and other groups, and the
persistent disadvantage experienced by a significant proportion of the
population.

Aotearoa’s economic response to the pandemic, and the trajectory of
economic developments, were in line with what happened elsewhere. Some
differences can be attributed to the relative generosity and extended
duration, of Aotearoa’s economic response.

The pandemic continued for much longer than initially expected, and
changes in alert levels due to community outbreaks made it necessary
to return to, or extend, earlier support measures.

The overall COVID-19 Response and Recovery Fund package represented the
second highest additional spending and/or revenue foregone in relative terms
by any OECD government in response to COVID-19 [NZ 17.7%, US 22.2%].

Many of the steps public organisations took to protect the Wage
Subsidy Scheme’s integrity were consistent with good practice guidance
for emergency situations, but robust post-payment verification
measures were absent (and needed).

Between The Reserve Bank of New Zealand and Treasury, there was no
active *coordination* of monetary and fiscal policy in the economic
response to the pandemic, in the sense of having a broad common
understanding of how they might interact with each other. Such an
understanding can matter enormously in a crisis, where matters are
evolving fast. If (for example) both Reserve Bank monetary policy and
Treasury fiscal policy are strongly stimulating the economy, they may
create too much stimulus. Or, if monetary and fiscal policy diverge,
they may unintentionally work against each other. However, we saw no
evidence of the Reserve Bank and the Treasury jointly advising the
Government in a coordinated manner on the broad pattern of how the
quantum and mix of fiscal and monetary stimulus should be provided,
and how these should be adjusted as the pandemic evolved.

There was a general lack of understanding about the inputs required by
manufacturers of essential goods. e.g forestry operations and wood
processing declared to not be essential industries, despite being the
only supplier of chlorine for drinking water.

At a high level, the initial economic measures package delivered
against its immediate aims to support the public health response and
ensure that all essential services were accessible. In doing so,
Aotearoa had generated better economic and social outcomes than most
other OECD countries.

Initially, the Reserve Bank and the Treasury were also concerned that
the health crisis could develop into a financial crisis. This too was
successfully avoided. Moreover, it had achieved these outcomes with
restrictions that were of comparatively short duration and, over the
course of the pandemic, less stringent on average than in many other
countries.

Aotearoa maintained a relatively stable economic position, prevented
large scale unemployment, supported people’s incomes, and the economy
rebounded very fast in 2020 and 2021 – both in absolute terms and
relative to other comparable OECD economies.

The success of the initial economic response – including the Response
and Recovery Fund, and the monetary and fiscal policies that were
adopted – had a flipside. From mid-2021 onwards, both the pandemic
itself and the policy responses to it started having economic and
social impacts across society, sectors and regions that were strong,
unevenly distributed and negative. At the same time, the Government
had to act to address the inflationary pressures and the sharp rise in
public debt created by the initial response, within tight time
constraints. Overall, these factors led to a slow and protracted
medium- to long-term economic recovery, the effects of which are still
with us in 2024.

Inflationary pressures and other impacts on the economy cannot be
attributed exclusively to domestic policy responses; global
developments (including the war in Ukraine) also played a significant
role.

We believe the "least regrets" stance taken at the beginning of the
pandemic was fundamentally sound. Little was known about the nature
and likely impact of the virus at that stage, apart from what might be
inferred from the then-severe events unfolding overseas, in Italy and
elsewhere. As the pandemic evolved, the tools available were not used
to properly balance between avoiding deflation and economic depression
in the short term, and limiting the extent of inflation, debt and lost
productivity in the medium to long term.

While wage subsidy and other supports were available to help those in
traditional employment, people in other situations struggled. For
example, casual workers whose employers did not apply for the wage
subsidy did not receive any wage subsidy payments.

Another group which experienced significant economic hardship during
the pandemic was temporary visa-holders. The Department of Internal
Affairs took five months to partner with the Red Cross to provide
humanitarian relief to temporary visa-holders. This took the form of
food supplies, housing assistance or support to return to their home
country.

Loss of incomes increased the material hardship many households
experienced in the pandemic. The burden of these negative impacts was
not shared equally across all Kiwis.

The Ministry of Health had little understanding of the practicalities
of implementing many of the health orders in their
workplaces. Businesses expressed frustration with the Government on
various matters. Some sectors were well positioned to shift to digital
or remote work. But other types of businesses that relied on physical,
in-situ, or in-person work (e.g. construction or personal services)
simply could not do so during the initial Alert Level 4 lockdown.

Many businesses and sector groups told us that officials making
decisions about public health settings were initially unwilling to
consider options which would have allowed non-essential industries to
keep working safely – for example, road and bridge construction could
have continued because workers generally keep a considerable distance
apart anyway. The supply chain was disrupted by international and
domestic developments during the pandemic, but the impacts have not
yet been fully analysed; doing so is essential for improving the
supply chain’s resilience in another such crisis.

[Insert rant about economy issues]

Social services provide a much-needed safety net. During a pandemic,
they also play a critical role in minimising the spread of
infection. The availability of social services means people can comply
with health measures, including staying home safely, while still
having their basic needs met.

One factor that made local service providers so effective in the
pandemic response was their well-established and trusted relationships
with local communities and families – which, in turn, gave them a deep
understanding of the issues facing individual households. Many
providers we met with told us how the shift to more flexible contracts
with Government gave them the room they needed to identify and best
support the needs presenting in their communities, using their local
knowledge of their clients and communities to provide tailored
support.

Whānau-centred service delivery and support through Whānau Ora formed
a bedrock for Māori communities during the pandemic. The flexible
arrangements government agencies put in place allowed Māori to deliver
the support whānau needed, based on manaakitanga, trust and
connections. There was a high degree of integrity in social service
response; the assistance reached the people who needed it. There were
many cases where providers used their own reserves to support their
community, beyond funding received from Government.

[This social services section has a lot of "during the pandemic"
mentions, which irritates me. We're still living within the global
COVID-19 pandemic; it's not going to end by treating our most
effective years as a fleeting memory]

As of 2024, the Regional Public Service Commissioner model is still
maturing. As expected with a new initiative like this, we heard of
some variability in the way it has been applied across regions. But we
believe the model is a promising one that may, in future, support
better coordination between local preparedness planning and welfare
responses managed by central government agencies.

Based on the evidence we reviewed, we think Care in the Community is
another initiative that offers a model that should be used in future
pandemics, may have utility in other crises, and has lessons for
service provision in non-emergency times.

While some of the pandemic’s impacts on individuals and groups –
particularly those already identified as vulnerable – were
predictable, the funding Government provided to mitigate them was
inadequate. While agencies worked hard to disperse the funding that
was available, they said vulnerable groups and communities were
nonetheless disproportionately affected by the pandemic.

When people belonged to multiple ‘at risk’ groups, the impacts
amassed, and those least able to absorb the shocks faced the most
impacts.

From the start of the pandemic response, government agencies and
Cabinet were aware of the risks to many vulnerable groups. Thus,
alongside ‘across the board’ measures aimed at helping everyone
withstand the impacts of the pandemic, Government did seek to mitigate
the pandemic’s harmful effects on vulnerable groups through various
targeted interventions.

Some vulnerable groups came through the pandemic better than expected,
as a result of targeted mitigations:

* older people
* homeless, or insecure housing
* Māori

For some vulnerable groups, pandemic mitigations were not
well-targeted; these groups experienced variable impacts:

* children and young people
* rainbow / LGBTQIA+ communities
* ethnic minority communities

Despite policies and programmes to mitigate the pandemic’s unequal
impacts, some vulnerable groups were still disproportionately
affected:

* Pacific people
* women
* disabled people

More women than men lost their jobs, left the workforce, or lost hours
and pay and thus experienced greater employment and economic impacts,
largely because they were more likely to work in impacted sectors such
as tourism and hospitality. Despite this, the wage subsidy was more
likely to be used to support jobs held by men, which points to a
mismatch between what was occurring and the response.

Even though some women entered the pandemic with existing inequalities
and were a group identified as likely to face increased
vulnerabilities, the Inquiry has found limited consideration of gender
in targeted mitigation or recovery efforts.

Some vulnerable groups were overlooked in the response:

* foreign workers
* international students on temporary visas
* Recognised Seasonal Employer scheme workers
* precariously employed or gig economy workers
* prisoners

The ‘convenience’ of keeping prisoners locked down during the
pandemic had created a culture among prison staff which persisted,
even though there were now better ways to protect prisoners from
COVID-19.

COVID-19 was one of the biggest challenges to our collective mental
wellbeing seen in many generations. For some, the stress developed
into something more serious, often worsening an existing mental health
condition. International literature on disasters often describes
periods of unity and collective determination as honeymoon phases that
give way to disillusionment when people start to realise how long
recovery is going to take, and what it might take to get there.

Some feel overwhelmed by the situation, by the unrelenting stress and
fatigue, and by feelings of anger, depression, isolation, loneliness,
frustration and grief. Hostility may increase, and financial pressures
and relationship problems set in. The fourth stage is reconstruction,
or recovery, a gradual return to life. International literature
suggests psychosocial recovery can take up to ten years.

[I'm not going to *start* recovering until the damage caused by
COVID-19 is generally acknowledged]

There was an increase in mental health issues, especially for young
people, consistent with some causal effect of the pandemic and
pandemic response, in addition to trends before COVID-19.

Children and young people experience the world, and the passage of
time, differently from adults – meaning the pandemic probably seemed
endless to many, compounded by ongoing uncertainty about when it might
end and life would return to normal. Many missed key milestones or
significant childhood events during what was a confusing, distressing
and unusual time.

The mental health effects of the ongoing pandemic are likely to have a
long tail. This view was supported by evidence showing significant and
maybe even intergenerational consequences for the cohort of young
people experiencing high rates of mental distress.

Marae, schools, churches, NGOs and other community networks and hubs
are crucial points for community connection, leadership, practical
support and resilience building. Their value to society as a whole
often goes unnoticed, but the COVID-19 pandemic put the spotlight on
their good work, if only for a short time.

Local groups and networks powerful assets in the response to
COVID-19. In our view, they must be cultivated and strengthened as
part of Aotearoa New Zealand’s preparations for another pandemic.

A locally led, regionally enabled, and nationally supported approach
is emerging as a valuable framework for supporting community wellbeing
and recovery. Whatever that community is, you really need to resource
and empower it and give it its head.

Leaving an unsafe home environment to stay somewhere else was deemed
essential travel, but not everyone who needed to hear this message
did. Moreover, lockdown conditions made it difficult for people at
risk to access help without alerting the perpetrator. Reporting rates
aside, there are indicators that the nature and severity of family
violence and sexual violence worsened. In some cases, Alert Level
restrictions resulted in new or opportunistic forms of physical and
psychological violence.

For the most part, government agencies created a supportive,
high-trust environment for community organisations and specialist
services to respond effectively to the emerging risks of family
violence and sexual violence. This was appreciated.

COVID-19 highlighted community resilience and the power of communities
to respond and support their members. However, COVID-19 also exposed
emergent weaknesses and vulnerabilities that had been forming in our
social and economic fabric for decades.

For many individuals, families, households and communities, the
pandemic is not over. They continue to struggle with its consequences
– long COVID, loss of learning, mental health issues and more.

Much of the COVID-19-related support allocated to the social sector
(both to government and non-government providers) was one-off or
time-limited. It has been steadily scaled back and sometimes removed
altogether.

The initial package of economic measures the Government provided was
comprehensive and generous, and met its immediate aims.

The pandemic’s economic impacts put households and businesses under
great pressure, especially during lockdowns, despite introduced
mitigation measures. Businesses had different abilities to absorb the
shock of the pandemic.

The social sector – including government agencies and non-governmental
and community organisations – did a remarkable job of ensuring people
had their needs met.

A network of non-governmental organisations, iwi and Māori groups, and
community organisations provided the frontline services and support
that kept families safe and well.

The economic and social response to COVID-19 helped prevent deaths and
protected many people. But the pandemic’s economic, social and
wellbeing impacts on individuals and families have been unevenly
distributed.

For many individuals and families, COVID-19 is not over, showing that
wide-ranging pandemic support measures are needed even after the
immediate crisis has passed.

## Chapter 7 - Vaccination

[I previously worked at a cancer and immunology research institute,
but I'm not myself an immunologist. If this gets close to talking
about genetics, I'll probably have a few words to say.]

By the time community transmission became well-established, most of
the population had received at least one dose of the
vaccine. Optimising population immunity through vaccination was a
crucial pillar of the long-term approach to managing the virus.

The approval process was undertaken on an accelerated time frame but
followed the normal process, including review of company-provided
data, requests for further evidence in response to specific questions,
expert advice and review by the Medicines Assessment Advisory
Committee. Ongoing evidence reviews have continued to evaluate the
Pfizer vaccine as effective in substantially reducing the risk of
severe illness and death from COVID-19 and safe in terms of carrying a
very low risk of serious adverse side-effects.

Myocarditis (inflammation of the heart) is a ‘rare adverse event’ that
can occur following administration of mRNA COVID-19 vaccines,
including the Pfizer vaccine. Myocarditis following vaccination is
generally mild and responds to treatment. The World Health
Organization advises that people receiving the COVID-19 vaccine
‘should be instructed to seek immediate medical attention if they
develop symptoms indicative of myocarditis or pericarditis, such as
new onset and persisting chest pain, shortness of breath, or
palpitations following vaccination’.

In 2024, a study was published modelling the health impacts
attributable to COVID-19 vaccination in Aotearoa New Zealand between
Jan 2022 and Jun 2023. It estimated that during this period vaccines
saved 6,650 lives and prevented 45,100 hospitalisations. The benefits
of vaccination were not enjoyed equitably, with Māori having lower
vaccination rates and correspondingly higher rates of preventable
hospitalisations and deaths.

A group of public health experts writing in the New Zealand Medical
Journal also highlighted the interdependence of the elimination
strategy (which successfully delayed widespread COVID-19 transmission
for nearly two years) and the vaccination strategy (which delivered
high population immunity before the virus became established). Even
though New Zealand later experienced high rates of infection and
reinfection, especially during Omicron waves, levels of excess
mortality were exceptionally low, particularly compared with other
countries.

The rollout achieved the Government’s’ central objective – ensuring
high population immunity before exposure to COVID-19 became
widespread. This outcome is testament to the enormous effort of
officials, health providers (including primary care providers,
pharmacies and Māori and Pacific organisations), communities, local
leaders and individuals. Many members of the public who made
submissions to our Inquiry acknowledged these efforts. They were
grateful that vaccines were free of charge and easily accessible to
many, and they commended the rollout’s effectiveness and
accessibility. We heard again and again that Māori and Pacific health
providers were particularly effective in the vaccine rollout,
especially with their own populations (although these providers were
often frustrated by what they saw as missed opportunities to mobilise
earlier and maximise their effectiveness.

A highly centralised approach to the vaccine rollout meant
opportunities were missed to ensure vaccine access and uptake were
optimised for high-risk groups, including Māori and Pacific peoples,
at the same time as for the rest of the population. Delivering on the
immunisation programme’s stated commitments to equity would have
required earlier involvement of Māori and Pacific providers and a
greater willingness to relax central control in favour of more
community-led provision. We heard from senior figures both inside
and outside of government that more could have been done to ensure
earlier involvement and better resourcing of local health providers
(particularly Māori and Pacific organisations), which might have
improved early vaccine uptake in some high-risk groups.

Pasifika leaders were advocating for Pacific-led vaccination centres
and bespoke training of Pacific vaccinators, but the system just could
not respond. DHBs and the Ministry of Health were told to use trucks
for mobile vaccinations, but approval took too long. The Government’s
highly centralised approach had serious and damaging consequences for
already vulnerable groups and may have also delayed Aotearoa’s
recovery overall.

Government messaging had presented vaccination as the pathway out of,
and justification for, the elimination strategy and the restrictions
it involved. A stronger and earlier focus on achieving equity in the
vaccine rollout – including through targeted measures to increase
Māori and Pacific vaccination rates – would have seen the country
reach its immunisation target earlier, allowing lockdowns and other
stringent restrictions to be relaxed sooner.

The vaccine rollout was challenged by people delaying or declining
vaccination because they lacked confidence, motivation and/or ease of
access. This became a major global challenge, complicated by the rapid
spread of misinformation and disinformation. Vaccine hesitancy was a
particular issue for Māori and Pacific communities given their younger
age structure and historically lower trust of mainstream health
providers. We missed an opportunity to vaccinate communities early,
and saw resistance come in. The issue of vaccine hesitancy is linked
with – and complicated by – the fact that vaccines (like most
medicines) are not entirely without risk. COVID-19 vaccines were very
and it was not possible to fully understand the risk of very rare
adverse effects.

The 2024 modelling study published in Vaccine estimated that if Māori
vaccination rates had been the same as non-Māori, between 11 and 26
percent of the 292 Māori COVID-19 deaths recorded between January 2022
and June 2023 could have been prevented. That up to a quarter of Māori
deaths could have been avoided if vaccination rates had been equal is
a stark demonstration of the meaning of health equity and what happens
in its absence. Concerns about equity were raised with decision-makers
even before vaccination had started. The initial approach to the
rollout did not facilitate sufficient involvement of Māori, Pacific
and other community providers.

Ministry of Health officials had sought to place equity at the centre
of the COVID-19 immunisation programme. In March 2021, the COVID-19
Vaccine Technical Advisory Group advocated prioritising Māori and
Pacific peoples (and some other vulnerable groups) for vaccination at
a younger age than the rest of the population since they were at
greater risk of serious illness. Cabinet did not follow officials’
advice to use a younger age threshold for Māori and Pacific peoples in
the vaccine rollout. Instead, they sought to ensure equitable vaccine
access via other mechanisms – including promoting a ‘whānau-centred’
approach to the vaccine rollout.

Cabinet’s decision not to follow officials’ was criticised in the
Waitangi Tribunal’s priority report Haumaru, released at the end of
2021. These actions and others occurred despite advice that Māori
health leaders and iwi leaders were giving the Government. One equity
review (commissioned by the Ministry of Health and based on interviews
with mostly Māori and Pacific whānau, stakeholders and service
providers) concluded that equity had been ‘actively discarded as an
objective’ in the vaccination strategy and that the Ministry needed to
do better in any future pandemic response. The general thrust of the
Waitangi Tribunal’s findings are consistent with evidence we reviewed
from many sources, showing the significant benefits that were achieved
when the Government did undertake genuine Te Tiriti-based engagement
with iwi and Māori. In our view, when responding to a future pandemic,
the Government must not only document its responsibility to ensure
equitable outcomes for Māori in policy statements, but also give
effect to this responsibility in implementation.

The vaccination rollout also fell short of delivering equitable
outcomes for Pacific peoples. We do not have quantitative evidence of
the likely impact on Pacific COVID-19 mortality, but as Pacific
peoples suffered the highest mortality risk of all ethnic groups, it
is only logical that inequities in vaccine access and uptake
contributed to this outcome.

According to Hauora Hokianga, some Ministry of Health directions for
the rollout simply didn’t work for their communities, which were
rural, widely dispersed and had a younger age profile than the general
population. Ngāti Whātua Ōrākei, Whai Māia – which provides cultural
and social support for the people of Ngāti Whātua Ōrākei, including
through healthcare services – also described developing a bespoke
‘outreach’ approach. ‘[That’s] when it took off for Māori vaccination
rates and it [was] all about engagement. The centre does not attract
Māori – you have to go out to the community. According to the National
Hauora Coalition, the system didn’t give permission or provide for
different access options for vaccinations – after hours, drive through
or whole whānau. They had to battle the political arguments about mass
vaccination sites. Sir Brian Roche, a key independent advisor to
ministers and officials throughout the pandemic response, described a
failure to unleash ‘the power of the community to respond and to lead’
– backed by resourcing – as a weakness of the pandemic response
overall. In relation to vaccination specifically, he said: ‘What a
wasted opportunity. When they finally began to use the community to
vaccinate, the rates went up exponentially.’

Medsafe followed its usual process to properly assess the efficacy and
safety of COVID-19 vaccines, albeit on an expedited
timeline. Arguably, its review of the Pfizer vaccine was even more
rigorous than those of regulators in other countries.

## Chapter 8 - Mandatory Measures

The rules and mandates declared & announced during COVID-19
(particularly vaccination requirements) were among the most
controversial aspects of the pandemic experience, and prompted a
strong response from many people. Aotearoa’s relatively strong levels
of social cohesion and trust prior to the COVID-19 pandemic have been
cited as a key factor in the success of the elimination
strategy. However, measures taken in response to pandemics can erode
social cohesion and trust. Fostering trust and cohesion will be an
important part of future pandemic preparedness, as will thinking ahead
about how to balance the use of ‘compulsion’ to protect public health
against the need to uphold individual rights and avoid marginalising
people.

The combination of testing and contact tracing ensured that positive
cases and their close contacts could be identified, then isolated or
quarantined until they were no longer contagious or at risk. Mask use
reduced the likelihood of a community outbreak from cases that had not
been detected by these methods. Given the effectiveness of testing,
contact tracing, and masking depends on them being widely adopted, we
are confident that making them compulsory contributed usefully to the
success of the elimination strategy during 2020 and 2021. While we are
satisfied that compulsory protective measures were reasonable, and
that benefits outweighed costs, we identified some practical issues
with their implementation that provide useful learning opportunities
for future pandemics.

Mandatory testing for groups at higher risk of exposure appears to
have been effective. The vast majority of positive COVID-19 cases
detected at the border did not result in, or coincide with, any
community transmission.

Contact tracing reduces the risk of COVID-19 transmission. Contact
tracing in Taiwan was so effective in identifying cases that it
enabled Taiwan (with other measures) to successfully eliminate
COVID-19 transmission in 2020 without the need for lockdowns. Older
people, lower socio-economic groups, and some disabled people
encountered barriers in using or accessing smartphones. The COVID
Tracer app could have been improved by following smartphone
accessibility guidelines. Many issues prevented disabled people from
scanning in with the COVID Tracer app (e.g. QR posters located too
high for people in wheelchairs). Some people had privacy concerns
about the Tracer app, despite the Privacy Commissioner’s supportive
assessments. Māori participants expressed distrust of the Government’s
motivation for gathering data about people’s contacts and movements,
reflecting Aotearoa’s history of colonisation, and the
disproportionate number of tamariki Māori being taken into state
care. A key lesson from the pandemic was to think carefully about
privacy concerns and keep data from contact tracing separate from
other parts of the health system.

Use of the NZ COVID Tracer app rose considerably in August and
September 2020 following the re-emergence of community transmission
and the Government’s decision to make the display of official QR code
posters mandatory.71 After this decision, the number of users grew
from about 600,000 to 2.2 million, while the number of posters
displayed rose from 87,000 to 381,000 by late September 2020.  A later
review of the NZ COVID Tracer app’s effectiveness suggested around 45
percent of the population used it to scan their locations (considered
a high rate of uptake for a tool of this nature). The QR function of
the COVID Tracer app was effective at identifying casual contacts, but
not for close contacts of cases. The app ‘likely made a negligible
impact on the COVID-19 response in relation to isolating or testing
potential contacts of cases’. It was challenging to ensure that
members of the public participated in record-keeping activities (such
as QR scanning) without placing impossible or unworkable requirements
on business owners or enforcement agencies.

For masks to have a significant impact on community transmission, they
need to be worn correctly and be used by most people. Making masks
compulsory in a wide range of public and high-risk settings was an
effective tool to encourage their large-scale use.

[Aside: I believe that even poorly-worn masks have a social benefit in
reminding people about health care and airborne transmission. Also, if
the masks are worn in such a way to restrict airflow, some protection
is far better than nothing.]

Many of our public submitters supported the use of masks as a
protective measure. We heard that they made people feel safe, by
providing a perceived added layer of protection for themselves, their
family, or for others who were immunocompromised. Some told us that
they have continued to use masks, and expressed a view that they
should be used more as a tool for general health management. Some
submitters who were immunocompromised or had other medical
vulnerabilities described feeling ‘relieved’ that mask measures were
enforced to help them feel safe. Some submitters expressed the view
that mask mandates should have been introduced earlier.

Mask requirements were criticised by a substantial number of
submitters. They cited changing evidence about mask use over the
course of the pandemic as proof that masks ‘did not work’ against
COVID-19 and found the evolving guidance about mask use confusing.

[I have substantial frustration about the messaging around
masking. The discussions about effectiveness rarely consider social
aspects. I think Aotearoa got it right when they asked for "face
coverings", and people used whatever they felt comfortable with.]

Compulsory use of masks created difficulty for people who rely on lip
reading to communicate. Some health workers refused to remove their
masks, even at a distance, and refused alternative ways of
communicating to convey important information (e.g. writing). Mask
requirements were difficult to carry out in practice in some settings,
particularly in schools. Some people expressed distress about children
having to be masked at school, and some said that obtaining sufficient
masks to uphold these requirements was also difficult. An education
union told us that it took ‘far too long for masks to arrive, and when
they did, they were no longer needed. Mask exemptions caused ongoing
issues for some members of the disability community. Issuing mask
exemptions was poorly managed; some organisations contracted to issue
mask exemption certificates had minimal notice and were overwhelmed
with requests. Many businesses did not trust the integrity of mask
exemption certificates; the purpose and criteria for these were not
well-communicated to the general public. This led to some mask-less
disabled people feeling subjected to discrimination and abuse.

Retail workers and members of the public found it difficult to
distinguish between people who were legitimately exempt from mask
requirements, and people who refused to wear a mask for other reasons,
including as a point of protest. In attempting to verify whether
people were genuinely exempt, some workers, especially in retail
settings such as supermarkets, experienced escalating and unsafe
behaviour from some customers.

Cabinet decisions to issue vaccination mandates, allow employers to
set workplace-specific vaccine policies, and require vaccine passes
for certain locations were attempts to strike an acceptable balance
between time pressure and lockdown stress. The basis on which
ministers were satisfied that they were justified in limiting people’s
right to refuse vaccination was that these requirements would
substantially reduce community transmission.

[The biology behind vaccines is tricky. The Pfizer vaccine was
designed quickly, with a goal of *reducing* death and severe
illness. It wasn't designed to reduce transmission, and establishing
if that's actually happening is quite difficult.]

The original wording of the COVID-19 Public Health Response Act 2020
allowed ministers to make orders premised on vaccines reducing the
risk of between-person transmission, and not premised on vaccines
protecting against serious illness.

In October 2021, Cabinet agreed to amend the COVID-19 Public Health
Response Act 2020 to introduce vaccine mandates on public interest
grounds - a subtle but important shift. For example, for preventing
workers in essential services from becoming sick.

It was reasonable for the government to introduce a simplified health
and safety risk assessment tool in late 2021 that employers could use
as the country sought to find a way of living with established
COVID-19 transmission. It was sensible to introduce a vaccine pass
system in December 2021 with the intention of reducing the risk of
Delta ‘superspreader’ events and protecting vulnerable groups, while
reducing reliance on more stringent public health and social
measures. The case for requiring vaccination became less clear in 2022
with Omicron. By late 2021, it was clear that protection against
transmission waned in the weeks and months following vaccination.

A precautionary approach to removing mandates is understandable in the
context of growing rates of infection from Omicron after previously
stringent public health and social measures were
removed. Nevertheless, the case for retaining vaccine mandates became
less clear once the peak of Omicron infection had passed (in March
2022), when it was apparent that measures under the COVID-19
Protection Framework were sufficient to manage infection peaks and
prevent the health system from being overwhelmed.

[I don't have a particularly strong view about vaccine mandates. I
think the "health system not being overwhelmed" argument is reasonable
and rational at a national decision level, even though I don't like it
at a personal level; it's a bit short-sighted]

It is the view of this Inquiry that the retention of occupational
vaccine mandates until well into 2022 was too long. Once the peak of
Omicron had passed, the Government could have confidence that the new
COVID-19 Protection Framework was effective. The Inquiry is also of
the view that the extension of vaccination requirements into a broad
range of workplaces went too far – although we also acknowledge that
these requirements were introduced by employers and businesses rather
than the Government. The move from encouraging to compelling
vaccination was a significant one that affected how people felt about
the pandemic response overall. The long-term impacts of these
decisions had negative social and economic impacts which have been
deep and lasting.

A 2024 evaluation concluded that Aotearoa’s occupational vaccination
mandates are likely to have had limited impact on population
protection from COVID-19. Vaccination levels in relevant workforces
were already high at the point the mandates were introduced. Since the
vaccination mandates had little discernible impact on vaccine coverage
– increases during that period appeared as a continuation of an
existing upward trend – they would not have meaningfully increased
population protection from COVID-19.

Vaccine requirements supported the elimination strategy and protected
vulnerable people from COVID-19. In 2021 it made sense to require
vaccination for workers at higher risk of being exposed to or passing
on COVID-19.

* Medical exemptions to vaccination requirements were difficult to obtain.
* Exemptions to prevent significant service disruption were also
  possible, but controversial.
* It fell to employers in affected sectors to uphold Government-issued
  vaccine mandates.

Some people lost income or employment as a result of vaccination
mandates. Others did not have their employment fully terminated but
still faced mandate-related consequences (e.g. assigned different
work, or losing relationships with colleagues). Occupational mandates
exacerbated existing staffing shortages in several key areas –
including healthcare. Some submitters felt the mandates had undermined
their long-term employment prospects.

One of the key benefits of vaccination certificates was the
reassurance they provided to members of the public that it was
relatively safe to return to indoor venues, hold large gatherings, and
make use of close-proximity businesses. However, it was clear from
early in the rollout that the available vaccines could not eliminate
the risk of contracting or passing on COVID-19, nor guarantee that a
vaccinated person would not become seriously unwell if they contracted
the virus. They could – and did – however, reduce the risk on both
scores.

Some unvaccinated people felt ostracised, lost relationships and/or
were unable to access certain locations and services, including some
types of healthcare. They were treated as if they were selfish,
responsible for spreading COVID-19, and to be avoided. It was not
permitted for essential services (including primary healthcare) to
require vaccination certificates for entry. However, the strict
protocols adopted by many services, such as seeing unvaccinated
patients in their cars or delaying routine visits, made some people
feel as though they could not access basic services. Many people who
submitted to the Inquiry expressed grief and anger over divisions they
said the mandates had caused, describing families and friends who were
‘torn apart’ or ‘split’ over the issue, and strained relationships
that had never been repaired. Health providers felt the mandates had
caused many people to disengage from the system and had even decreased
the likelihood that some groups would take up vaccination. Several
spoke of Māori experiencing this as a loss of their agency,
exacerbating mistrust of the health system. Official data indicates a
drop-off in childhood vaccination levels since 2020, with pronounced
declines among Māori and Pacific children: declined from over 90
percent in the pre-pandemic period to 80 percent (for Pacific) and 68
percent (for Māori).

The occupation of Parliament grounds was perhaps the most visible
expression of the pandemic’s impact on social cohesion and
trust. However, the challenges to trust, social licence and social
cohesion were recognised by some senior decision-makers from at least
the second half of 2021. Misinformation and disinformation present an
increasing global challenge, and that those who are already
marginalised and with low trust in government (including Māori) are
most susceptible. Repairing, fostering and maintaining trust and
social cohesion will be key to both countering the impacts of
COVID-19-related misinformation and disinformation, and ensuring
Aotearoa is in a good position to respond effectively to a future
pandemic.

### Chapter 8 / Looking Back

The use of compulsion was one of the most controversial aspects of the
COVID-19 response.

Testing, contact-tracing and masking requirements were reasonable, but
their implementation could be improved in a future pandemic.

It was reasonable to introduce some targeted vaccine requirements
based on information available at the time.

Some vaccine requirements were applied more broadly than originally
envisaged.

The case for vaccine requirements became weaker in 2022.

[The report claims this is due to the Omicron variant, but I'm not
convinced. By 2022 there was extensive community transmission, making
vaccination less effective for population protection.]

While some people found vaccine requirements reassuring, they had
wider social and economic consequences.

The use of mandatory measures – and other aspects of the COVID-19
pandemic – affected trust and social cohesion in ways that may make
future pandemic responses more difficult.

## Chapter 9 - Taking Stock

If we consider the elements of the pandemic response collectively,
what are they telling us? If we were to explain Aotearoa’s pandemic
response to a future generation who did not live through it, what
story would we tell?

Encouraged by the example of countries like Singapore, Aotearoa
instead chose a more aggressive path: a complete national lockdown
accompanied by stringent public health measures, with the possibility
of eliminating the virus until a vaccine was available. Early signs of
success prompted decision-makers to move away from suppression as the
backstop strategy and instead fully embrace the elimination
strategy. With the initial shock passing, Aotearoa resolved to keep
going hard.

The elimination strategy provided a coherent, easily understood
national goal that found widespread acceptance. The ground-breaking
Alert Level System supporting it set out four levels of increasingly
restrictive public health measures.

By the start of June 2020, community transmission had been
eliminated. This was a remarkable, collective achievement. Despite the
difficulties of lockdown, people had a common purpose, and the
knowledge that Aotearoa could return to something like normality.

Community solidarity was one factor in the effectiveness of the
elimination strategy throughout 2020 and much of 2021. Strong
leadership was another. Ministers and public health officials, most
notably Prime Minister Jacinda Ardern and Director-General of Health
Ashley Bloomfield, were exceptional in their public communications,
something that was acknowledged domestically and internationally.

The pandemic response’s benefits went beyond public health. Holding
fast to the elimination strategy allowed Aotearoa’s society and
economy to function comparatively well at a time when many other
countries were facing extreme disruption. Generous economic supports
meant that, once the initial shock had passed, economic activity and
growth bounced back quickly. There was no large-scale unemployment and
workers largely stayed connected to their jobs and workplaces, despite
the lockdowns. The devastating effects of the pandemic were mitigated
by government-funded supports and services, and by the efforts of the
networks of non governmental and local organisations, iwi and Māori
groups, volunteers and many others. There is much to be proud of.

However, the longer-term human, social and economic cost of pursuing
elimination was high. With the benefit of hindsight, it is possible to
see that some harm might have been avoided or at least reduced if
things had unfolded differently.

Despite a lack of experience or tools to build the pandemic response
plane, in flight and under pressure, officials and agencies were
remarkably quick in standing up the systems, services and supports
that would allow Aotearoa to pursue elimination. The effectiveness of
the response also depended on the private sector, iwi and Māori,
Pacific and other ethnic communities, non-government social service
and health providers, volunteers and many more. They could often do
what central government could not. Community groups were known and
trusted; they understood local or sector-specific needs; they could
reach people who might otherwise be overlooked. Yet these groups were
not always adequately consulted or relied on by government, especially
early on. The strength, leadership and capacities found within
non-government groups cannot be over-stated as prime enablers of the
‘government’ response.

Those who went into the pandemic already experiencing health, economic
or other inequities were often disproportionately affected, such as
Māori and Pacific peoples. Some people were impacted in unique ways or
suffered specific disruptions to their life plans, such as essential
workers, New Zealanders overseas or people needing treatment for
non-COVID-19 medical conditions.

The elimination strategy was the best way to protect all Kiwis and
look after those at highest risk from a pandemic. By delaying
widespread transmission until most people had been vaccinated,
thousands of premature deaths were prevented.

[I break ranks with the report here, because I don't think the move
away from elimination was justified. Vaccination works best in
conjunction with low population prevalence, and we didn't have that
when the country was opened up to the virus. The report offers no
"What if" scenarios modelling how things might have been different
with a later transition, or with more prioritisation on vaccinating
South Auckland, or with national Level 3 protections while Auckland
was still suffering.]

Once Aotearoa reached the point when elimination was no longer
required or viable – because the population was largely vaccinated1️⃣,
and the arrival of more easily spread variants made elimination
infeasible2️⃣ – a new strategy was needed.

[1️⃣ != 2️⃣]

For much of 2020/21, planning for recovery, preparing exit strategies
and considering possible future scenarios received less attention than
they should have. The health system had trouble judging when there was
scope to resume more non-COVID-19 services.

The decision to keep Auckland locked down until all population groups
had adequate vaccination coverage was laudable in intent, but the
costs (individual, social, economic, educational) were high and they
were borne by all Aucklanders and some in neighbouring regions.

[My memory is that we had vaccines at the time of the Auckland
outbreak, but they were not proritised to the regions of outbreaks -
especially South Auckland. I had a substantial breakdown of hope,
expressed through tweets, when I saw that happening.]

On 22 October, the Government announced that Aotearoa would shift to a
suppression strategy in December, described as ‘minimisation and
protection’. Alert levels would be replaced by a traffic light system.

A contentious announcement, for many reasons.

People felt unprepared to move in the new suppression direction, and
the goals of the new strategy were unclear. Many who had felt
protected by the elimination strategy were now anxious about the
health risks if COVID-19 was allowed to become established. When
announcing the shift away from elimination, vaccination coverage among
Māori and Pacific peoples was still below the 90 percent level which
the Government’s health advisers had recommended should be reached
before adopting the ‘traffic light’ system. The pandemic response
never regained its initial clarity of purpose or the public support it
had earlier enjoyed. There was also increasing resistance to
compulsory public health measures, especially vaccine mandates and
employer-issued vaccine rules.

In light of evidence suggesting vaccination was less effective for
newer COVID-19 viral strains, the Government might have considered
removing vaccine requirements in January and February 2022. However,
this move would not have been risk-free. Even if vaccines were not as
good at stopping the transmission ofOmicron as other variants, they
would have helped to flatten the first wave to some degree. Vaccine
requirements would also have helped dampen down any further outbreaks
of the Delta variant.

While some people were anxious about ‘living with’ the virus, for
others the persistence of measures such as vaccine requirements had a
corrosive effect. These sentiments partly fuelled the Parliamentary
occupation that ended violently in March 2022. By the time of the
parliamentary occupation, core pandemic response measures were already
being dismantled. Border restrictions and MIQ were gradually reduced
starting in February 2022. Vaccine policies were progressively rolled
back from April 2022.

Collectively, the global pandemic and additional shocks like the war
in Ukraine left a legacy of economic, health and social after-effects,
many of which remain with us – cost increases, global supply chain
problems, the high cost of living, loss of learning, long COVID, poor
mental health, loss of income, business failures, broken relationships
and widening inequalities among them.

From an international perspective, Aotearoa’s pandemic response was
comparatively a positive one. Aotearoa had one of the lowest health
losses from COVID-19 and fared comparatively well economically and
socially, at least in the short term.

## Chapter 10 - Lessons For The Future

The COVID-19 pandemic is a transformative and disruptive worldwide
event. It has expanded what we know about pathogens, their origins and
spread, and how science & data can help us prepare for and combat
future pandemics.

Prevention is the best form of pandemic preparedness, and Aotearoa
actively supports many international pandemic prevention efforts:

* reducing risks at wet markets
* discouraging human settlement in high-risk areas
* improving global surveillance systems

Definitive evidence emerged during the pandemic that SARS-CoV-2 and
other respiratory viruses mostly spread through aerosol
transmission. Aotearoa and Australia played an important role in
demonstrating this via detailed MIQ facility monitoring. Knowledge of
airborne transmission underscores the critical role of ventilation and
air flow in a pandemic, and has implications for the design of many
indoor environments – including schools, hospitals and quarantine
facilities.

COVID-19 reinforced the growing understanding that, if worn properly,
masks are effective in limiting the spread of respiratory
viruses. Even if the next pandemic pathogen has a shorter incubation
period (making contact tracing less effective), decision-makers now
know that masking, ventilation and air filtration will go a long way
towards dampening transmission. Much can be done ahead of time to
ensure these measures are in place and ready to use.

The pandemic was a powerful reminder of the ability of pathogens to
change their stripes. What we saw from 2020 onwards was a virus being
constantly selected for a fitter variant that could infect people more
easily, spread faster and evade immunity.

[The report talks about the SARS-CoV-2 having an amazing mutability,
but to me that's just expected with a pathogen that spreads to
billions of people. It also repeats this weird bit about Omicron being
less virulent, but also more immune-escaping.]

Stopping the spread of an infectious disease may require protective
measures that affect the whole population – whether or not they are
sick or symptomatic yet. Blanket restrictions may be required before
fuller information about viral spread is available. Successful
pandemic responses rely on high initial levels of social trust and
cohesion. Yet if a pandemic response requires measures that are
all-encompassing and drastic, these can erode trust and cohesion over
time.

Throughout a pandemic response, governments must constantly balance
the short and long-term effects of their actions and
policies. Maintaining incomes for health and home is obviously an
immediate goal in a people-centred pandemic response. Governments will
be judged not only on how well they achieve their initial strategic
objectives, but also on the country’s long-term economic and social
health – including whatever scars the response may have caused (or
exacerbated). Decision-makers should also be prepared to adjust their
thinking about risk as the pandemic response evolves, both with
regards to the pathogen, and also with regards to the flow-on effects
on economic and social measures.

Entering a pandemic with high trust and social cohesion may be as
important as stockpiles of PPE and a strong balance sheet. Erosion of
trust could be held in check with transparent decision-making that is
respectful and accommodating of the minority. Trust levels vary hugely
between people, as do attitudes to compliance. Some people like to be
told precisely what to do, while others want to know the desired
outcome and find their own way to best achieve it.

No society has yet solved the problem of misinformation and
disinformation. It is important for citizens to speak up on social
issues that concern them. Similarly, it is vital for experts to help
inform the public about their area of expertise.

Like global warming, pandemics are an example of what is sometimes
called a ‘wicked’ policy problem: unclear, complex, cutting across
different systems, underpinned by unclear causal relationships, and
liable to result in unanticipated consequences. Responding better to
the ‘wicked’ problems which pandemics raise will require a shift in
thinking towards what the OECD terms anticipatory innovation
governance: embedding foresight, innovation and continuous learning
into policy and investment decisions. One excellent technique is to
manage chaos and innovation in parallel: The minute you encounter a
crisis, appoint a team to resolve the issue. At the same time, pick
out a separate team and focus on the opportunities for doing things
differently.

We have become accustomed to living in a world in which capital,
goods, knowledge, people, cultures and trends cross borders at
dizzying speed – even for countries like Aotearoa, which are
geographically isolated. The pandemic exposed Aotearoa’s heavy
reliance on international supply chains that were long, thin and
complex. Aotearoa has a small, open economy that depends on trade and
the easy movement of people, goods, and services in and out of the
country. Aotearoa’s supply chain vulnerability was heightened by its
use of ‘just in time’ delivery which, though efficient in normal
times, meant goods that were essential in the pandemic were in
dangerously short supply or had to be used past their expiry date.

Greater global connectedness meant the latest scientific knowledge and
research about COVID-19 became available almost immediately. The
research informed countries’ pandemic responses and planning, and
helped to counter misinformation and disinformation. We have seen
multilateral efforts to better prepare the world for future pandemics
– such as the revised International Health Regulations, ongoing work
to forge a global pandemic accord and various initiatives to improve
poorer countries’ access to vaccines.

Providing they act early and fast enough, countries can opt to keep
the pathogen out or stamp it out. They can keep doing so until they
are ready to let the virus in and exit from an elimination strategy on
their own terms and at the time of their choosing. The COVID-19
experience empowered governments to consider the option of imposing
tight border restrictions. That prospect was unimaginable before
COVID-19, and contrasted sharply with WHO’s initial view about
barriers to international trade and mobility.

If Aotearoa had planned for the possibility of closing borders faster,
the COVID-19 virus might not have entered Aotearoa at all in February
and March – and we could have avoided the first national lockdown
entirely.

[Ooh, finally, a "What if?". Let's see if the report applies that same
thinking to the situation we had in 2021 when we had no known
infections, experience of prior lockdowns, and would become a
well-vaccinated population within a few months.]

The global experience showed that when tools and tactics are
well-prepared, and the population is willing to collectively implement
them, it is more likely that the chosen strategy will deliver great
benefits with minimal harm.

We need to manage pandemics to look after people. This means
recognising the broad range of impacts that a future pandemic may have
on all aspects of people’s lives, and minimising both immediate and
long-term harms.

A good response is summarised as follows:

* make good decisions
* build health system resilience
* build economic and social resilience
* work together
* build the foundations for future responses

As the pandemic wore on – especially in the second half of 2021 – the
goal of (re)eliminating community transmission began to move out of
reach.

[But What If? Y'know? What if it didn't?]

Our role is not to stipulate exactly what decisions should be made in a pandemic (either in the now-past COVID-19 pandemic...

[Whoah, hold up. Lemme check WHO:

"... This does not mean the pandemic itself is over..."]

https://www.who.int/europe/emergencies/situations/covid-19

An effective pandemic response requires dedicated, future-focused
planning to be carried out separately from (but in parallel with) the
immediate operational response.

There is value in making more explicit use of ethical principles that
can consistently and transparently guide decision-makers. These
principles could be applied at all levels of the response.

[Or maybe just Toitū Te Tiriti]

Both the Oxford Handbook and the Aotearoa frameworks set out core
values and principles that can guide decision-makers towards a
people-centred pandemic response.

Processes normally followed within Government – including
comprehensive consultation and advice – are designed to support good
decisions. After an emergency, they should be resumed as early and
fully as possible to ensure the best advice to inform decisions. While
the breadth of input will be determined by the time available,
Governments should still seek out advice and perspectives on what is
happening, what might happen and how they might adjust their approach
to meet changing pandemic circumstances. While it is important to keep
the possibility of changing scenarios in mind all the time, in a
future pandemic, decision-makers should be alert to opportunities
presented by periods of relative stability and ensure they are used
well. It is difficult for decision-makers to remain adaptable and
innovative when they are exhausted. This was one reason why leaders
struggled to develop and communicate a forward-looking plan, despite
breathing space in mid-2020 when Aotearoa was COVID-19-free.

Modelled projections of COVID-19’s health impacts under different
approaches were a key catalyst for the initial decision to place the
country in lockdown, while later decisions about moving up and down
alert levels were also informed by modelling. It is important that
modelling is treated as a guide and considered alongside other inputs,
including the views of key partners, stakeholders, experts and the
wider public. It is not vaccine coverage that should be the target for
when to loosen public health measures, but the estimated immunity in
the population. As new evidence emerges, it should be factored into
any modelling alongside other variables as soon as possible.

Taking time to engage the public, Māori, communities, businesses and
key partners ensures decision-makers are aware of concerns and
receptive to suggestions about how they might be addressed.

It is important that, at regular intervals, leaders describe long-term
plans and anticipated steps as the country moves towards a new
‘normal’. Leaders should be clear that the exact timing will depend on
many factors and will therefore require flexibility.

Our analysis of the response to COVID-19 has shown that the need to
use more stringent measures such as lockdowns may be reduced by
building the capacity and resilience of core public health services
and tools.

Quarantine and isolation options other than hotels – ranging from a
blend of facilities and home-based quarantine, to bespoke facilities
and more hospital level care facilities – should be investigated ahead
of the next pandemic.

A resilient economy and social support systems are important to reduce
disruptions to ‘normal’ life as much as possible, during and after a
pandemic.  These sectors provide essential scaffolding of daily life
that becomes even more critical – and comes under greater pressure –
in times of crisis.

We suggest building and strengthening financial relationships, as well
as those with other agencies as appropriate, such as the Ministry of
Business, Innovation and Employment, Inland Revenue, the Ministry of
Transport and the Financial Markets Authority. The fiscal reserves
that provided important support during COVID-19 now need to be
restored. This should be achievable at a sensible pace that does not
drive the economy into negative territory. Think in advance about how
to ensure economic and social support measures will reach everyone
they need to, and that their effects will be fair and
proportionate. Setting up programmes under urgency can lead to less
efficient spending of limited resources. Financial support for
workers, whānau, households and businesses is critical for the overall
success of public health measures. International evidence suggests
that the economic and financial costs of not deploying such measures
could be even greater. As well as access to essential goods and
services, such as food, housing and lifeline utilities, a good
pandemic response needs to maintain people’s access to government and
community services as much as possible.

The ‘essential’ classifications change over time. The passage of time
significantly impacts what is considered necessary and/or
essential. Without changes to the essential category to enable repair
or replacement, some businesses may not be able to operate. We think
there is scope in the future to designate some activities as ‘safe to
continue’ rather than ‘essential’. These may well be able to operate
safely in a future pandemic, reducing social and economic costs of
public health restrictions.

Given the expansive kāwanatanga powers exercised in this emergency and
the need for agile decision-making by the Executive, the Crown’s
obligation to actively protect tino rangatiratanga and partner with
Māori is, in fact, intensified. A Te Tiriti partnership in a pandemic
context ensures iwi, hapū, whānau, and Māori communities are active
partners in preventing, managing, and recovering from the impacts of
the pandemic. Working closely with iwi and Māori is the best way to do
this. One important way of ensuring any future pandemic response is
consistent with Te Tiriti is by ensuring that principles and tools
used in the response are developed together with Māori, so that such
tools and principles help the Crown uphold its
obligations. Community-led approaches using local knowledge and
leadership often delivered the most effective local solutions. A
locally-led, regionally-enabled, and nationally supported approach is
a valuable framework for supporting wellbeing and recovery. Many
remarkable success stories of Aotearoa’s COVID-19 response involved
iwi and Māori organisations exercising tino rangatiratanga as well as
Te Ao Māori values like whakapapa, manaakitanga and kaitiakitanga to
support the community at large.

Government agencies working closely with the business sector allowed
important aspects of life to continue – in particular, the flow of
essential goods and services (including lifeline utilities), ongoing
employment and economic activity.

The strong global emphasis on influenza as the likely cause of the
next pandemic meant that, prior to COVID-19, the preparation that was
in place did not consider all options that might be relevant for
responding to a different type of infection.

[I expect we'd be blindsided by an Influenza pandemic as well,
possibly even worse than COVID-19. It has similar airborne
transmission profiles and a higher fatality rate, and too many people
will be going around saying, "It's just the Flu"]

To prepare better for future pandemics and other types of emergencies,
current and future governments should invest in a strong national risk
management system, with central oversight, coordination and evaluation
across all relevant government agencies. Ongoing investment in
modelling capacity across multiple disciplines – coupled with the
development of pandemic scenarios – is essential to building the
foundations for a future pandemic response.

[This has tinkled a bell in my head. We already have an "Environmental
Risk Management Authority". Perhaps that could be expanded to a more
general "Risk Management Angency", without the need for starting
completely from scratch.]

While the legislation in place in March 2020 was sufficient to support
the initial response, there is value in developing an improved legal
framework ahead of time to cater for a national public health
emergency. Legislation should be updated to provide legal grounds to
enable a speedy and effective *immediate* response to a pandemic,
providing adequate time for any bespoke pandemic legislation or
legislative amendments to be developed and considered by Parliament.
It would also be useful for ‘model’ pandemic legislation to be
developed and consulted on, but not enacted. This ensures there is
ready-to-go ‘model’ legislation available, which can be modified to
ensure it meets the bespoke needs of a future pandemic. Aotearoa
should build on the Indo-Pacific Economic Framework for Prosperity
initiative to improve international and domestic supply chain
resilience. We should also global efforts to strengthen pandemic
preparedness and responsiveness.

## Chapter 11 - Recommendations | Ngā tūtohutanga

Establish a central agency function to coordinate all-of-government
preparation and response planning for pandemics and other national
risks.

Strengthen oversight and accountability for pandemic preparedness.

Develop and practise an all-of-government response plan for a
pandemic, covering the national-level response and integrating
sector-specific plans.

Ensure an all-of-government response structure is ready to be
activated if needed in a pandemic, supported by adequate staffing and
the provision of comprehensive advice under urgency.

Refine the health system pandemic plan and link it with the
all-of-government pandemic plan.

Plans in place for scaling-up and implementing significant public
health measures in a pandemic.

Ensure each sector has a pandemic plan and is able to support activity
within their sector to keep going safely in a pandemic.

Build resilience to ensure continuity of non-pandemic health care.

Improve ventilation in hospitals and other public spaces.

Ensure plans are in place to address the way that the economy
functions during a pandemic – including economic and fiscal policy,
the labour market, management of supply chains, the operation of
lifeline utilities, and the provision of financial support.

Strengthen coordination at local, regional and national levels.

Ensure access to welfare support, food and housing.

Maintain access to services and ensure the rights and wellbeing of
prisoners are protected.

Plan to keep educational facilities open as much as possible.

Maintain access to education through remote learning.

Improve the way public sector agencies work with iwi and Māori during
a pandemic, to support the Crown in its relationship with Māori under
te Tiriti.

Review legislation to ensure it is fit for purpose for a future
pandemic.

Ensure core infrastructure is fit for purpose to support each sector’s
pandemic response.

Assign a minister to lead the recommendation response, ensure
six-monthly progress reports, and report to Parliament within 12
months of this report being completed.

We urge the Government to consider and implement these Phase One
recommendations as soon as practicable. The minister charged with
leading this work should receive regular progress reports on how the
recommendations are being implemented at the all-of-government level
and by individual agencies, and keep Parliament informed.

[I expect this recommendations chapter will be the most read and
discussed by others. I'll try to bear that in mind, and think about
things I consider particularly important (more than the document
average), or things that might be missed.]

Specialist advice, including external expertise, should be routinely
used to support decision-making on issues including safeguarding human
rights and democratic principles in a pandemic response, and the
Crown’s Te Tiriti obligations.

There should be processes in place to quickly secure adequate staffing
and rotate staff to prevent burnout. Processes for developing advice
under urgency, while still taking account of critical considerations
such as human rights issues, are also needed.

Lockdowns should stay as part of the response toolkit, as a scenario
involving a virus that is even more infectious or deadly than the
COVID-19 virus is possible; even with good preparation and a good
policy response we may still need to reach for lockdowns.

We recommend that health and other agencies investigate ways to
improve ventilation and airflow in buildings, which we now know play a
significant role in limiting the transmission of respiratory viruses.

Government agencies and appropriate iwi and Māori organisations should
review successful examples of Crown-Māori partnerships in the COVID-19
response and make changes that will embed Te Tiriti relationships,
frameworks and partnerships in a future response.

[Aside: The "iwi and Māori organisations" phrasing is used extensively
throughout the report, which I find curious. It's clearly a deliberate
directive, and makes me wonder why these are explicitly split out as
separate entities / groups.]

The risk of a future pandemic remains high. While this Inquiry will
not be complete until the Phase Two report is presented in early 2026,
we strongly suggest immediate application and implementation of our
recommendations.

Developing plans requires collaboration and appropriate consultation
and engagement (including with iwi and Māori, community groups,
businesses, local and regional government, and
internationally). Consider also how the plans give effect to Te
Tiriti.

[The remainder of the chapter is a checklist / table of specific
recommendations. I assume this will form the basis of progress reports
delivered to the government as the recommendations are implemented].

---

Ngā mihi,

Haumi ē! Hui ē! Tāiki ē!