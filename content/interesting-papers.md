---
title: "Interesting Papers"
author: "David Eccles"
---

# Bioinformatics

## GW: ultra-fast chromosome-scale visualisation of genomics data

[https://doi.org/10.1101/2024.07.26.605272](https://doi.org/10.1101/2024.07.26.605272)

Fast genome browser that supports both tiled and X views, as well as
composable filtering and counting commands. It has two possible views:
alignment view and grid view; it might be possible to create REPAVER
tiles to work with this, as I did with GBrowse.

> GW supports extensive data interactions using 35 built-in commands,
  enabling users to load, save, navigate and search files, filter and
  count reads, change appearances and organise data (Fig. 1e-g). Data
  is displayed using either the 'alignment-view' or 'image-view'. With
  the alignment-view (Fig. 1b), sequencing reads are arranged as
  stacks with colours representing information such as mismatches or
  discordant reads. Coverage profiles appear above and users can
  arrange multiple files and regions in rows and columns. Data tracks
  are placed below, and can be expanded, collapsed, and dynamically
  resized. Interactive elements display information to the terminal
  (Fig. 1c). GW supports themes including ‘slate’ (shown) and ‘dark’
  to reduce eye strain, and ‘igv’ (Supplementary Fig. 1) (1).

> The ‘image-view’ (Fig. 1d) organises images into a grid, simplifying
  exploration and curation of large datasets. Images can be loaded
  from disk or generated in real-time from files, while right-clicking
  an image will switch between alignment and image-views, permitting
  static-image exploration and dynamic data interactions.


## Tiberius: End-to-End Deep Learning with an HMM for Gene Prediction

[https://doi.org/10.1101/2024.07.21.604459](https://doi.org/10.1101/2024.07.21.604459)

Could be useful for annotating non-model organisms, although it would
probably need to be re-trained for that purpose. No idea if it'd be
useful, but at least it's mentioning CLST and HMM rather than the
over-hyped LLMs.

> We present Tiberius, a novel deep learning-based ab initio gene
predictor that end-to-end integrates convolutional and long short-term
memory layers with a differentiable HMM layer. Tiberius uses a custom
gene prediction loss and was trained for prediction in mammalian
genomes and evaluated on human and two other genomes. It significantly
outperforms existing ab initio methods, achieving F1-scores of 62% at
gene level for the human genome, compared to 21% for the next best ab
initio method. In de novo mode, Tiberius predicts the exon-intron
structure of two out of three human genes without error. Remarkably,
even Tiberius’s ab initio accuracy matches that of BRAKER3, which uses
RNA-seq data and a protein database. Tiberius’s highly parallelized
model is the fastest state-of-the-art gene prediction method,
processing the human genome in under 2 hours.

## QuST-LLM: Integrating Large Language Models for Comprehensive Spatial Transcriptomics Analysis

[https://doi.org/10.48550/arXiv.2406.14307](https://doi.org/10.48550/arXiv.2406.14307)

I attended a 10x seminar on using LLMs for spatial transcriptomic data
querying; this is likely the associated paper.

> The integration of large language models (LLMs) into spatial
  transcriptomics (ST) analysis, embodied in the QuST-LLM tool,
  signifies a considerable leap in the genomics field. QuST-LLM’s
  ability to transform intricate, high-dimensional ST data into
  comprehensible biological narratives significantly enhances the
  interpretability and accessibility of ST data. Our study has
  demonstrated that QuST-LLM is not only capable of interpreting
  biological spatial patterns, but it can also identify specific
  single-cell clusters or regions based on user-provided natural
  language descriptions. This represents the transformative potential
  of LLMs in computational biology research and their ability to
  assist researchers in deciphering the spatial and functional
  complexities of tissues.

It somewhat amuses me that they turn the results tables into short
sentences (e.g. "The first top ranked gene is MUC1. The second top
ranked gene is GATA3. The third top ranked gene is STC2..."), and then
process that data through a LLM to create feature matrices that are
then used to derive meaning from the data. It seems like an "all you
have is a hammer" situation; but given how much attention and
development there is on LLMs at the moment, it likely ends up being a
substantial time / cost saving.

## Probabilistic embedding, clustering, and alignment for integrating spatial transcriptomics data with PRECAST

[https://doi.org/10.1038/s41467-023-35947-w](https://doi.org/10.1038/s41467-023-35947-w), Added Jul 5, 2024

I’m using a PRECAST helper function to embed spatial information into
Seurat objects. Looks like the other tools it contains might be
helpful for integration and normalisation:

> PRECAST takes, as input, matrices of normalized expression levels
  and the physical location of each spot across multiple tissue
  slides. The output of PRECAST comprises all aligned embeddings for
  cellular biological effect, slide-specific embeddings that capture
  spatial dependence in neighboring cells/spots, and estimated cluster
  labels. In contrast to other existing methods of data integration,
  PRECAST is a unified and principled probabilistic model that
  simultaneously estimates embeddings for cellular biological effects,
  performs spatial clustering, and more importantly, aligns the
  estimated embeddings across multiple tissue sections. Thus, we
  recommend applying PRECAST first before a comprehensive data
  analysis pipeline is deployed.

## Transformers are SSMs: Generalized Models and Efficient Algorithms Through Structured State Space Duality

[https://arxiv.org/abs/2405.21060](https://arxiv.org/abs/2405.21060), Added Jun 23, 2024

Introduces structured state-space models (SSMs) as an equivalent, but
more efficient, computational structure that matches or outperforms
Transformers.

> Compared to general attention, SSD’s advantage is having a
  controllable state expansion factor N that compresses the history,
  compared to quadratic attention’s cache of the entire history
  scaling with sequence length T ≫ N

## Batch correction methods used in single cell RNA-sequencing analyses are often poorly calibrated

[https://doi.org/10.1101/2024.03.19.585562](https://doi.org/10.1101/2024.03.19.585562), Added Jun 23, 2024

> Overall we see that the methods tend to fall in 3 categories of
  performance. Combat and Harmony perform the best or close to it in
  all the evaluations we perform. Those two methods introduce the
  fewest artifacts into the data during the process of batch
  correction. Seurat introduces artifacts that are more easily
  identifiable, but seems to retain the overall structure of data when
  comparing clusters. Finally, MNN, SCVI, LIGER and BBKNN, all
  introduce a significant change that alters local neighborhood
  structure, clusters and differential expression results.

## Data Thinning for Convolution-Closed Distributions; see also Generalized Data Thinning Using Sufficient Statistics

[https://www.jmlr.org/papers/v25/23-0446.html](https://www.jmlr.org/papers/v25/23-0446.html);
[https://arxiv.org/abs/2303.12931](https://arxiv.org/abs/2303.12931)

Added Apr 23, 2024 via [Twitter](https://twitter.com/daniela_witten/status/1782479268620140914)

Process for decomposing a dataset into two (or more) subsets without
double-dipping. Data thinning works when expected sampling
distributions have a specific compositional property that is hard for
me to understand… but their examples show that it works for single
cell data, and gaussian distributions, which covers a substantial
portion of the bioinformatics work I do. They have example code for
the single cell thinning, which will be interesting for me to look at.

## Improving the RNA velocity approach with single-cell RNA lifecycle (nascent, mature and degrading RNAs) sequencing technologies

[https://doi.org/10.1093/nar/gkad969](https://doi.org/10.1093/nar/gkad969)

Added May 17, 2024 via email (Warren Bach)

Comparison of different sequencing methods based on Rhapsody Single
Cell Sample preparation (BD Express).

> In this research, our goal was to generate 100 million cDNA reads at
  a low cost using nanopore high-throughput sequencing. To achieve
  this goal, we developed the FLOUR-seq platform, which combines the
  BD Rhapsody microwell system with nanopore sequencing (Figure
  1A). Single cells were captured in microwells and labeled with beads
  containing 52 bp mega barcodes. The large Levenshtein distance among
  these barcodes compensated for the accuracy deficiency (70–90%) of
  nanopore sequencing (Figure 1D).

# Fair Funding

## Do grant proposal texts matter for funding decisions?

[https://doi.org/10.1007/s11192-024-04968-7](https://doi.org/10.1007/s11192-024-04968-7)

Added Jun 4, 2024 via [Twitter](https://x.com/cMadan/status/1797741669716242513)

> We conclude that panelist assessment of an application changes
  little when the proposal text is omitted from it. Writing and
  evaluating proposals comprises the lion’s share of the costs of
  grant peer review (Graves et al., 2011). Our findings suggest that
  funding agencies using single-blind panel review, at least in a
  pre-selection stage prior to external review, can expect to achieve
  similar candidate selections by screening on the basis of CV and
  proposal abstract only.

This is consistent with my own understanding / experience. I have
personally found that well-written one-paragraph summaries (or
abstracts) often transfer over to well-written proposals.

However, this does not mean that well-written abstracts should be
preferentially funded. In my opinion, the biggest cost waste in grant
funding is not the review process, it’s the default expectation of
rejection of hundreds of applicants.

My preference is still for much smaller amounts of money to be given
to all applicants that pass a low, fixed (and therefore predictable)
threshold of entry.
