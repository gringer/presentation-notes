---
title: "Bioinformatician Job Description"
author: "David Eccles"
---

# Summary

This document describes a Bioinformatics Analyst role within
<organsiation>, as fulfilled by <employee>.

The primary purpose of this role is to provide bioinformatics support
to research staff at <organisation>, reducing the time taken to
process research data into a form that is easily interpretable by
other researchers. The data to be processed will be primarily, but not
exclusively, stored on a computer system.

A secondary / supplementary purpose of this role is to provide support
and training for high-throughput sequencing to research staff at
<organisation>.

# Detail

## Primary Purpose

The primary purpose will be achieved by <employee> through the
following activities, where required:

* Consultation about experimental design for grant writing, research
  proposals, and other experiment planning
* Designing applications and software pipelines for automated
  processing of research data
* Pilot-testing of new data processing / analysis software
* Communicating with developers of software to fix bugs and implement
  needed features
* Training research staff in the use of data processing / analysis
  software
* Processing machine data into a form that is interpretable by data
  processing / analysis software (e.g. format shifting, transcriptome
  / genome mapping, sample / cell / molecule barcode demultiplexing)
* Assembly of amplicons, plasmids, genomes, transcriptomes, and
  metagenomes from raw reads
* Large-scale bulk and single-cell group assignment (e.g. clustering)
* Large-scale bulk and single-cell group comparisons
  (e.g. differential expression)
* Large-scale variant analysis (e.g. STR, SNP, VNTR, haplotype
  quantification)
* 3D modeling for bespoke research instruments
* 3D printing of research instruments
* Training of staff at <organisation> to perform the above activities
* Education of staff at <organisation> about the above activities

## Secondary Purpose

The secondary purpose will be achieved by <employee> through the
following activities, where required:

* Consultation about high-throughput sequencing (e.g. feasibility,
  cost, experimental design)
* Training and supervision of research staff to perform sample
  preparation
* Training and supervision of research staff to perform sequencing
* Training of research staff to perform analysis of high-throughput
  sequencing data
* Education of staff at MIMR about high-throughput sequencing
* Attendance at conferences and workshops involving high-throughput
  sequencing

## Other Activities

Other activities will be carried out by <employee>, as generally
expected of research scientists at <organisation>, in Aotearoa, and
worldwide. These include, but are not limited to:

* Following exemplary ethical behaviour and world class research and
  scholarly practices as explained in the Royal Society Code of
  Professional Standards and Ethics
* Identifying and advocating for ways in which research practices can
  be improved at <organisation>
* Attendance at group meetings (i.e. individual, team, and group
  meetings)
* Regular reporting to the team leader
* Attendance at regular <organisation> meetings and seminars
* Attendance at conferences, workshops and meetings to improve
  research knowledge and skills
* Complying with safety regulations and maintaining a safe and clean
  working environment
* Ensuring accurate research records are kept and maintained
* Contributing to the interests and initiatives of <organisation> as
  approved by the general manager

The above statements are intended to describe the general nature and
level of work being performed. Other activities in addition to the
above named activities may be carried out by <employee> where
necessary to help support researchers and staff at <organisation>.

## Intellectual Property

Due to the highly-collaborative nature of research, and especially
bioinformatics, it is important that data processing scripts are made
publicly available to others as early as possible. Software and
pipelines specific to <organisation> work will be stored in a suitable
shared and Institute-accessible repository. <employee> will advocate
for the early publication and release of <organisation> work
(including processing scripts and research outputs) under a permissive
attribution license (e.g. CC-BY for research articles, OSF-supported
licence for software), understanding that collaboration helps make
research better.

Where software for more general use is developed and/or modified by
<employee>, <organisation> will be given an irrevocable, worldwide,
perpetual, royalty-free, non-exclusive license to use, distribute,
and/or modify that software.

 
