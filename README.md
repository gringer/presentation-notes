---
title: "About"
--- 
Presentations seen and transcribed by David Eccles

This is meant to be read as a Gitlab Pages site; see [here](https://gringer.gitlab.io/presentation-notes/contents/)
